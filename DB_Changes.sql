/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  sai
 * Created: Oct 1, 2020
 */

ALTER TABLE `tbl_coaching_page` CHANGE `coaching_page_short_name` `coaching_page_short_name` VARCHAR(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `tbl_country_page` CHANGE `country_page_short_name` `country_page_short_name` VARCHAR(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `tbl_visa_page` CHANGE `visa_page_short_name` `visa_page_short_name` VARCHAR(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;


/**
 * Author:  sai
 * Created: 02,10,2020
 */

ALTER TABLE `tbl_coaching_page`  ADD `is_show_home_page` INT NOT NULL DEFAULT '0'  AFTER `is_active`;
ALTER TABLE `about_us`  ADD `background_image` TEXT NOT NULL  AFTER `section_3_desc`;
ALTER TABLE `tbl_coaching_page`  ADD `background_image` TEXT NOT NULL  AFTER `icon`;
ALTER TABLE `tbl_country_page`  ADD `background_image` TEXT NOT NULL  AFTER `country_flag_image`;
ALTER TABLE `tbl_visa_page`  ADD `background_image` TEXT NOT NULL  AFTER `icon`;
ALTER TABLE `tbl_service_setting`  ADD `background_image` TEXT NOT NULL  AFTER `sub_setting_desc_4`;
ALTER TABLE `contact`  ADD `background_image` TEXT NOT NULL  AFTER `contact_number_title`;
ALTER TABLE `privacy_policy`  ADD `background_image` TEXT NOT NULL  AFTER `page_id`;
ALTER TABLE `tbl_country_page`  ADD `continent_id` INT NULL DEFAULT NULL  AFTER `country_page_name`;

DROP TABLE IF EXISTS `tbl_continent`;
CREATE TABLE IF NOT EXISTS `tbl_continent` (
  `continent_id` int(11) NOT NULL AUTO_INCREMENT,
  `continent_code` varchar(250) NOT NULL,
  `continent_name` varchar(250) NOT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`continent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_continent`
--

INSERT INTO `tbl_continent` (`continent_id`, `continent_code`, `continent_name`, `del_status`) VALUES
(1, 'AF', 'Africa', 'Live'),
(2, 'AS', 'Asia', 'Live'),
(3, 'EU', 'Europe', 'Live'),
(4, 'NA', 'North America', 'Live'),
(5, 'SA', 'South America', 'Live'),
(6, 'OC', 'Oceania', 'Live'),
(7, 'AN', 'Antarctica', 'Live');

/**
 * Author:  Jay Pandya
 * Created: 02,10,2020
 */


ALTER TABLE `tbl_visa_page_document` CHANGE `document` `document` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `tbl_visa_page_document` CHANGE `document_name` `document_name` VARCHAR(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;

ALTER TABLE `tbl_about`  ADD `page_image` TEXT NOT NULL  AFTER `about_route`;
ALTER TABLE `tbl_about`  ADD `logo_image` TEXT NOT NULL  AFTER `page_image`;
/**
 * Author:  Mohammed
 * Created: 26-05-2021
 */

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `del_status`) VALUES (NULL, 'Blog', 'BLOG', 'admin/Blog', NULL, 'fal fa-blog', '12', 'Live');

ALTER TABLE `footer_setting`  ADD `register_popup_image` TEXT NOT NULL  AFTER `footer_linkedin`;

DROP TABLE IF EXISTS `tbl_blog`;
CREATE TABLE IF NOT EXISTS `tbl_blog` (
  `blog_id` int NOT NULL AUTO_INCREMENT,
  `blog_title` varchar(250) NOT NULL,
  `blog_short_desc` text NOT NULL,
  `blog_date` date NOT NULL,
  `blog_desc` text NOT NULL,
  `blog_poster_image` text NOT NULL,
  `blog_image` text,
  `blog_video` text,
  `background_image` text NOT NULL,
  `blog_poster_type` enum('image','video') NOT NULL,
  `facebook` text,
  `instagram` text,
  `linkedin` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `is_active` int NOT NULL DEFAULT '1',
  `InsUser` int DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`blog_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_mail_template` (`mail_template_id`, `type`, `subject`, `message`, `fromname`, `fromemail`, `active`, `del_status`) VALUES
(NULL, 'register_customer', 'Regsiter For Free Counseling', '<h2><br />\r\nHiiii<br />\r\n<strong>{person_name} </strong>Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.</h2>\r\n\r\n<h2><strong>Thank you.</strong></h2>\r\n', 'Canopus Global Education', 'info@canopusedu.com', 1, 'Live'),
(NULL, 'register_admin', 'appointment_time', '<h1 id=\"exampleModalLabel\"><strong>Registration&nbsp;For Free Counseling</strong></h1>\r\n\r\n<h2><br />\r\nName : {name}<br />\r\nPhone : {phone}<br />\r\nWhatsapp Number: {whatsapp_no}<br />\r\nEmail : {email}<br />\r\nDOB: {dob}<br />\r\nCurrent Education : {current_education}<br />\r\nExperience: {experience}<br />\r\nService: {service}<br />\r\nRegistration: {registration}<br />\r\nIELTS Appeared: {ielts_appeared}<br />\r\nAppointment Date: {appointment_date}<br />\r\nAppointment Time: {appointment_time}</h2>\r\n<br />\r\n&nbsp;', 'Canopus Global Education', 'info@canopusedu.com', 1, 'Live');


ALTER TABLE `tbl_blog`  ADD `twitter` TEXT NULL  AFTER `linkedin`;

INSERT INTO `slider` (`slider_id`, `slider_title`, `slider_description`, `slider_image`, `slider_order_no`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `is_active`, `del_status`) VALUES (NULL, 'Welcome To The Fastest Way', 'Get A <strong class=\"cmt-textcolor-skincolor\">Visa </strong> & <strong class=\"cmt-textcolor-skincolor\">Immigration</strong>', 'assets/images/slides/services-details_02.jpg', '1', '1', '::1', '2020-07-29 00:00:00', '1', '::1', '2020-10-02 10:14:50', '1', 'Live'), (NULL, 'Welcome To The Fastest Way', 'Get A <strong class=\"cmt-textcolor-skincolor\">Visa </strong> & <strong class=\"cmt-textcolor-skincolor\">Immigration</strong>', 'assets/images/slides/slider-mainbg-002.jpg', '2', '1', '::1', '2020-07-29 00:00:00', '1', '::1', '2020-09-28 04:10:28', '1', 'Live');

INSERT INTO `slider` (`slider_id`, `slider_title`, `slider_description`, `slider_image`, `slider_order_no`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `is_active`, `del_status`) VALUES (NULL, 'Welcome To The Fastest Way', 'Get A <strong class=\"cmt-textcolor-skincolor\">Visa </strong> & <strong class=\"cmt-textcolor-skincolor\">Immigration</strong>', 'assets/images/slides/services-details_02.jpg', '1', '1', '::1', '2020-07-29 00:00:00', '1', '::1', '2020-10-02 10:14:50', '1', 'Live');

/**
 * Author:  Kushal
 * Created: 21-08-2021
 */
ALTER TABLE `tbl_blog` ADD `meta_title` VARCHAR(250) NULL DEFAULT NULL AFTER `del_status`, ADD `meta_desc` VARCHAR(250) NULL DEFAULT NULL AFTER `meta_title`, ADD `meta_key` VARCHAR(250) NULL DEFAULT NULL AFTER `meta_desc`;
ALTER TABLE `tbl_visa_page`  ADD `meta_title` VARCHAR(250) NULL DEFAULT NULL  AFTER `del_status`,  ADD `meta_desc` VARCHAR(250) NULL DEFAULT NULL  AFTER `meta_title`,  ADD `meta_key` VARCHAR(250) NULL DEFAULT NULL  AFTER `meta_desc`;
ALTER TABLE `tbl_country_page`  ADD `meta_title` VARCHAR(250) NULL DEFAULT NULL  AFTER `del_status`,  ADD `meta_desc` VARCHAR(250) NULL DEFAULT NULL  AFTER `meta_title`,  ADD `meta_key` VARCHAR(250) NULL DEFAULT NULL  AFTER `meta_desc`;
ALTER TABLE `tbl_coaching_page`  ADD `meta_title` VARCHAR(250) NULL DEFAULT NULL  AFTER `del_status`,  ADD `meta_desc` VARCHAR(250) NULL DEFAULT NULL  AFTER `meta_title`,  ADD `meta_key` VARCHAR(250) NULL DEFAULT NULL  AFTER `meta_desc`;
ALTER TABLE `about_us`  ADD `meta_title` VARCHAR(250) NULL DEFAULT NULL  AFTER `del_status`,  ADD `meta_desc` VARCHAR(250) NULL DEFAULT NULL  AFTER `meta_title`,  ADD `meta_key` VARCHAR(250) NULL DEFAULT NULL  AFTER `meta_desc`;
ALTER TABLE `contact`  ADD `meta_title` VARCHAR(250) NULL DEFAULT NULL  AFTER `del_status`,  ADD `meta_desc` VARCHAR(250) NULL DEFAULT NULL  AFTER `meta_title`,  ADD `meta_key` VARCHAR(250) NULL DEFAULT NULL  AFTER `meta_desc`;
ALTER TABLE `privacy_policy`  ADD `meta_title` VARCHAR(250) NULL DEFAULT NULL  AFTER `del_status`,  ADD `meta_desc` VARCHAR(250) NULL DEFAULT NULL  AFTER `meta_title`,  ADD `meta_key` VARCHAR(250) NULL DEFAULT NULL  AFTER `meta_desc`;
ALTER TABLE `footer_setting`  ADD `meta_title` VARCHAR(250) NULL DEFAULT NULL  AFTER `del_status`,  ADD `meta_desc` VARCHAR(250) NULL DEFAULT NULL  AFTER `meta_title`,  ADD `meta_key` VARCHAR(250) NULL DEFAULT NULL  AFTER `meta_desc`;

UPDATE `footer_setting` SET `meta_title` = 'Abroad Education Consultants Surat - Canopus Global Education', `meta_desc` = 'Canopus Global Education is the leading abroad education consultation in Surat. We offer overseas visa and immigration services for Canada, USA, UK, Europe and many more.', `meta_key` = 'abroad education consultants' WHERE `footer_setting`.`footer_id` = 1;

UPDATE `tbl_country_page` SET `meta_title` = 'Best Canada Visa Consultants in Surat - Canopus Global Education', `meta_desc` = 'Canopus Global Education provides online consultation services for Canada visitors, immigrations or student Visa. To know more visit our center in Surat!', `meta_key` = 'canada visa consultants near me, Canada visa consultants in surat' WHERE `tbl_country_page`.`country_page_id` = 1;
UPDATE `tbl_country_page` SET `meta_title` = 'USA Visa & Immigration Consutants Surat - Canopus Global Education', `meta_desc` = 'We are the leading USA visa and immigration consulting in Surat offering services such as student visas, visitor visas, immigration visas, dependent visas & many more.', `meta_key` = 'USA Visa consutants surat' WHERE `tbl_country_page`.`country_page_id` = 2;
UPDATE `tbl_country_page` SET `meta_title` = 'UK Visa & Immigration Consutants Surat - Canopus Global Education', `meta_desc` = 'Looking for UK visa and immigration consultants in surat? Canopus Global Education provides overseas consultation for those who want to live or study in abroad. Contact us today!', `meta_key` = 'UK Visa consutants surat' WHERE `tbl_country_page`.`country_page_id` = 3;
UPDATE `tbl_country_page` SET `meta_title` = 'Best Europe Visa & Immigration Consutants Surat - Canopus Global Education', `meta_desc` = 'Canopus Global Education is the leading European visa & immigration consulting in Surat. We cover different Europe locations such as Poland, Germany, Latvia, etc.', `meta_key` = 'Europe visa consultants surat' WHERE `tbl_country_page`.`country_page_id` = 7;

UPDATE `tbl_visa_page` SET `meta_title` = 'Student Abroad Visa Consultants Surat - Canopus Global Education', `meta_desc` = 'Canopus Global Education is the leading student abroad visa consultant in Surat for those who want to study overseas. Visit our site today for Canada, USA, UK, Europe visa consultation.', `meta_key` = 'student visa consultant in surat, study abroad consultants in surat' WHERE `tbl_visa_page`.`visa_page_id` = 2;

UPDATE `tbl_coaching_page` SET `meta_title` = 'Online IELTS Coaching Classes Surat - Canopus Global Education', `meta_desc` = 'Canopus Global Education provides the best online IELTS coaching classes in Surat from experts having years of experience. We prepare students for both general and academic IELTS exams.', `meta_key` = 'ielts coaching in surat, ielts coaching classes online' WHERE `tbl_coaching_page`.`coaching_page_id` = 1;

UPDATE `about_us` SET `meta_title` = 'About - Canopus Global Education' WHERE `about_us`.`about_us_id` = 1;


/**
 * Author:  Kushal
 * Created: 25-08-2021
 */
ALTER TABLE `tbl_service_setting`  ADD `meta_title` VARCHAR(250) NULL  AFTER `del_status`,  ADD `meta_desc` VARCHAR(250) NULL  AFTER `meta_title`,  ADD `meta_key` VARCHAR(250) NULL  AFTER `meta_desc`;

UPDATE `tbl_side_menu` SET `menu_constant` = 'BLOG_PAGE' WHERE `tbl_side_menu`.`menu_id` = 41;
UPDATE `tbl_side_menu` SET `menu_url` = '#' WHERE `tbl_side_menu`.`menu_id` = 41;
INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `del_status`) VALUES
(42, 'Blog', 'BLOG', 'admin/Blog', 41, NULL, 1, 'Live'),
(43, 'Blog Setting', 'BLOG_SETTING', 'admin/Blog/addEditBlogSetting', 41, '', 2, 'Live');

CREATE TABLE `canopus`.`tbl_blog_setting` ( `blog_setting_id` INT NOT NULL AUTO_INCREMENT , `blog_setting_title` VARCHAR(250) NOT NULL , `blog_setting_desc` VARCHAR(250) NOT NULL , `background_image` TEXT NOT NULL , `InsUser` INT NULL , `InsTerminal` VARCHAR(250) NULL , `InsDateTime` DATETIME NULL , `UpdUser` INT NULL , `UpdTerminal` VARCHAR(250) NULL , `UpdDateTime` DATETIME NULL , `del_status` VARCHAR(50) NOT NULL DEFAULT 'Live' , `meta_title` VARCHAR(250) NOT NULL , `meta_desc` VARCHAR(250) NOT NULL , `meta_key` VARCHAR(250) NOT NULL , PRIMARY KEY (`blog_setting_id`)) ENGINE = InnoDB;

DROP TABLE IF EXISTS `tbl_blog_setting`;
CREATE TABLE IF NOT EXISTS `tbl_blog_setting` (
  `blog_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_setting_title` varchar(250) NOT NULL,
  `blog_setting_desc` varchar(250) NOT NULL,
  `background_image` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  `meta_title` varchar(250) NOT NULL,
  `meta_desc` varchar(250) NOT NULL,
  `meta_key` varchar(250) NOT NULL,
  PRIMARY KEY (`blog_setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_blog_setting`
--

INSERT INTO `tbl_blog_setting` (`blog_setting_id`, `blog_setting_title`, `blog_setting_desc`, `background_image`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`, `meta_title`, `meta_desc`, `meta_key`) VALUES
(1, 'About Our Visa Consultancy', 'Founded In 2010 Surat, India', 'assets/images/pagetitle-bg.jpg', 1, '::1', '2021-08-25 08:05:47', 1, '::1', '2021-08-25 08:50:05', 'Live', 'df', 'fd', 'df');

DROP TABLE IF EXISTS `tbl_slug`;
CREATE TABLE IF NOT EXISTS `tbl_slug` (
  `slug_id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(250) NOT NULL,
  `ref_primary_id` int(11) NOT NULL,
  `ref_table_name` varchar(250) NOT NULL,
  PRIMARY KEY (`slug_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_slug`
--

INSERT INTO `tbl_slug` (`slug_id`, `slug`, `ref_primary_id`, `ref_table_name`) VALUES
(1, 'about', 1, 'about_us'),
(2, 'services', 1, 'tbl_service_setting'),
(3, 'blog', 1, 'tbl_blog_setting'),
(4, 'contactt', 1, 'contact'),
(5, 'privacy-policy', 1, 'privacy_policy'),
(6, 'home', 1, 'footer_setting');

ALTER TABLE `footer_setting`  ADD `register_popup_active` INT NOT NULL  AFTER `register_popup_image`;


ALTER TABLE `contact`  ADD `contact_title` VARCHAR(250) NOT NULL  AFTER `background_image`,  ADD `contact_description` VARCHAR(300) NOT NULL  AFTER `contact_title`;


/**
 * Author:  Mohmed
 * Created: 13-10-2021
 */

UPDATE `tbl_side_menu` SET `menu_order_no` = '6' WHERE `tbl_side_menu`.`menu_id` = 29;
INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `del_status`) VALUES (NULL, 'Client', 'CLIENT', 'admin/Home/client', '2', NULL, '7', 'Live');
INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `del_status`) VALUES
(NULL, 'Faq', 'FAQ', 'admin/Faq', NULL, 'fal fa-user-md-chat', 13, 'Live');

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `client_id` int NOT NULL AUTO_INCREMENT,
  `client_name` varchar(250) NOT NULL,
  `client_image` text NOT NULL,
  `is_active` int NOT NULL DEFAULT '1',
  `InsUser` int DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(20) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `tbl_faq_item`;
CREATE TABLE IF NOT EXISTS `tbl_faq_item` (
  `faq_item_id` int NOT NULL AUTO_INCREMENT,
  `ref_country_page_id` int NOT NULL,
  `faq_item_title` varchar(250) NOT NULL,
  `del_status` varchar(10) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`faq_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `tbl_faq_item_det`;
CREATE TABLE IF NOT EXISTS `tbl_faq_item_det` (
  `faq_item_det_id` int NOT NULL AUTO_INCREMENT,
  `ref_faq_item_id` int NOT NULL,
  `faq_item_det_title` varchar(250) NOT NULL,
  `faq_item_det_desc` text NOT NULL,
  `del_status` varchar(10) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`faq_item_det_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `tbl_education_loan`;
CREATE TABLE IF NOT EXISTS `tbl_education_loan` (
  `education_loan_id` int NOT NULL AUTO_INCREMENT,
  `section_1_title` varchar(250) NOT NULL,
  `section_1_short_desc` varchar(250) NOT NULL,
  `section_1_desc` text NOT NULL,
  `section_1_image_1` text NOT NULL,
  `section_1_image_2` text NOT NULL,
  `section_1_image_3` text NOT NULL,
  `section_2_title` varchar(250) NOT NULL,
  `section_3_title` varchar(250) NOT NULL,
  `section_4_title` varchar(250) NOT NULL,
  `section_5_title` varchar(250) NOT NULL,
  `section_5_desc` varchar(250) NOT NULL,
  `section_5_image` text NOT NULL,
  `InsUser` int DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(10) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`education_loan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_education_loan`
--

INSERT INTO `tbl_education_loan` (`education_loan_id`, `section_1_title`, `section_1_short_desc`, `section_1_desc`, `section_1_image_1`, `section_1_image_2`, `section_1_image_3`, `section_2_title`, `section_3_title`, `section_4_title`, `section_5_title`, `section_5_desc`, `section_5_image`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'Education Loan', 'The Most Eminent Visas and Immigration Consultant service provider. Branches in India and overseas.', '<p>Studying abroad is an important decision and the ultimate deciding factor for sure will be the financial aspect as the cost to study abroad and other related expenses vary from country to country, university to university, and also depend upon the course you want to pursue.</p>\r\n\r\n<p>Study abroad scholarships and part time jobs do help you to support your higher education and living overseas, however, they might not be sufficient to fund your entire expenses. In such a case if you are able to get an education loan, the immediate financial burden can be reduced to a great extent. KC Overseas Education has an exclusive Overseas Education Loan Division to resolve all your queries and provide end to end assistance for overseas education loan.</p>\r\n\r\n<p>Our mission is to provide</p>\r\n\r\n<ul>\r\n	<li>Affordable funding options</li>\r\n	<li>Thorough assistance for Overseas Education Loan process</li>\r\n	<li>Education loan sanction prior to admission</li>\r\n	<li>Assisting you in showing &#39;proof of funds&#39; to the Universities</li>\r\n</ul>\r\n\r\n<p>To achieve this, proper guidance is required and our Education Loan Advisor can assist you by streamlining the entire process.We thus provide the platform for you to choose the best financing options to fund your overseas education with minimum hassles.</p>\r\n<br />\r\n&nbsp;', 'assets/images/education_loan/single-img-04.jpg', 'assets/images/education_loan/single-img-06.jpg', 'assets/images/education_loan/single-img-07.jpg', 'Role of our Education Loan Advisor', 'An Insight', 'Our Banking Partners', 'TESTIMONIALS', 'What Our <strong>Client Says?</strong>', 'assets/images/bg-image/col-bgimage-3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'Live');

DROP TABLE IF EXISTS `tbl_section_2_left_education_loan`;
CREATE TABLE IF NOT EXISTS `tbl_section_2_left_education_loan` (
  `section_2_left_item_id` int NOT NULL AUTO_INCREMENT,
  `ref_education_loan_id` int NOT NULL,
  `section_2_left_item_data` varchar(250) CHARACTER SET utf8mb4 NOT NULL,
  `section_2_left_item_is_icon` int NOT NULL,
  `del_status` varchar(10) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`section_2_left_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=341 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_section_2_left_education_loan`
--

INSERT INTO `tbl_section_2_left_education_loan` (`section_2_left_item_id`, `ref_education_loan_id`, `section_2_left_item_data`, `section_2_left_item_is_icon`, `del_status`) VALUES
(335, 1, 'Disseminating information on Education Loan for Studies Overseas', 1, 'Live'),
(336, 1, 'Checking the eligibility criteria', 1, 'Live'),
(337, 1, 'Suggesting the right financial institution to be approached', 1, 'Live'),
(338, 1, 'Suggesting the right financial institution to be approached', 1, 'Live'),
(339, 1, 'Answering all the related queries', 1, 'Live'),
(340, 1, 'Assisting in documentation and submission of the file to the respective financial institutions', 1, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_section_2_right_education_loan`
--

DROP TABLE IF EXISTS `tbl_section_2_right_education_loan`;
CREATE TABLE IF NOT EXISTS `tbl_section_2_right_education_loan` (
  `section_2_right_item_id` int NOT NULL AUTO_INCREMENT,
  `ref_education_loan_id` int NOT NULL,
  `section_2_right_item_data` varchar(250) CHARACTER SET utf8mb4 NOT NULL,
  `section_2_right_item_is_icon` int NOT NULL,
  `del_status` varchar(10) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`section_2_right_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_section_2_right_education_loan`
--

INSERT INTO `tbl_section_2_right_education_loan` (`section_2_right_item_id`, `ref_education_loan_id`, `section_2_right_item_data`, `section_2_right_item_is_icon`, `del_status`) VALUES
(243, 1, 'Apart from the above mentioned tasks, our Loan Advisor also assists you with ancillary services like', 2, 'Live'),
(244, 1, 'Foreign Exchange', 1, 'Live'),
(245, 1, 'Global Forex Card', 1, 'Live'),
(246, 1, 'International Sim Card', 1, 'Live'),
(247, 1, 'Overseas Travel Insurance', 1, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_section_3_education_loan`
--

DROP TABLE IF EXISTS `tbl_section_3_education_loan`;
CREATE TABLE IF NOT EXISTS `tbl_section_3_education_loan` (
  `section_3_item_id` int NOT NULL AUTO_INCREMENT,
  `ref_education_loan_id` int NOT NULL,
  `section_3_item_title` varchar(250) NOT NULL,
  `section_3_item_desc` text NOT NULL,
  `del_status` varchar(10) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`section_3_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_section_3_education_loan`
--

INSERT INTO `tbl_section_3_education_loan` (`section_3_item_id`, `ref_education_loan_id`, `section_3_item_title`, `section_3_item_desc`, `del_status`) VALUES
(103, 1, 'When is the right time for submitting an application to the Universities?', 'Every foreign University/College has their intakes during a year. Some have two intakes while others may have three or only one or a rolling intake during the academic year. Majority Institutions in a particular country follow the same intake. Hence, you should initiate steps for admission process at least one year in advance for the respective intake. In some cases, you could begin these steps 3-4 months in advance as well.', 'Live'),
(104, 1, 'What is an application package?', 'An application package consists of the material required by the University. It consists of Application forms Application fees Recommendations Transcripts and mark sheets Essays Financial aid form', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_section_4_education_loan`
--

DROP TABLE IF EXISTS `tbl_section_4_education_loan`;
CREATE TABLE IF NOT EXISTS `tbl_section_4_education_loan` (
  `section_4_item_id` int NOT NULL AUTO_INCREMENT,
  `ref_education_loan_id` int NOT NULL,
  `section_4_image` text NOT NULL,
  `del_status` varchar(10) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`section_4_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_section_4_education_loan`
--

INSERT INTO `tbl_section_4_education_loan` (`section_4_item_id`, `ref_education_loan_id`, `section_4_image`, `del_status`) VALUES
(45, 1, 'assets/images/bank/sbi.jpg', 'Live'),
(46, 1, 'assets/images/bank/bob.jpg', 'Live');

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `del_status`) VALUES (NULL, 'Education Loan', 'EDUCATION_LOAN', 'admin/Education_loan', NULL, 'fal fa-user-md-chat', '14', 'Live');

ALTER TABLE `tbl_faq_item` CHANGE `faq_item_title` `faq_item_title` VARCHAR(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL;