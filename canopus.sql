-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 03, 2020 at 02:04 PM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `canopus`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

DROP TABLE IF EXISTS `about_us`;
CREATE TABLE IF NOT EXISTS `about_us` (
  `about_us_id` int(11) NOT NULL AUTO_INCREMENT,
  `about_us_title` text NOT NULL,
  `about_us_desc` text NOT NULL,
  `about_us_alt_title` varchar(250) NOT NULL,
  `about_us_alt_desc` text NOT NULL,
  `about_us_alt_sub_icon_1` varchar(250) DEFAULT NULL,
  `about_us_alt_sub_title_1` varchar(250) DEFAULT NULL,
  `about_us_alt_sub_desc_1` text,
  `about_us_alt_sub_icon_2` varchar(250) DEFAULT NULL,
  `about_us_alt_sub_title_2` varchar(250) DEFAULT NULL,
  `about_us_alt_sub_desc_2` text,
  `about_us_alt_image` text NOT NULL,
  `section_3_title` varchar(250) NOT NULL,
  `section_3_desc` text NOT NULL,
  `background_image` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`about_us_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`about_us_id`, `about_us_title`, `about_us_desc`, `about_us_alt_title`, `about_us_alt_desc`, `about_us_alt_sub_icon_1`, `about_us_alt_sub_title_1`, `about_us_alt_sub_desc_1`, `about_us_alt_sub_icon_2`, `about_us_alt_sub_title_2`, `about_us_alt_sub_desc_2`, `about_us_alt_image`, `section_3_title`, `section_3_desc`, `background_image`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'About', 'Canopus Global<strong class=\"cmt-textcolor-skincolor\"> Education</strong>', 'Why Choose <strong>Canopus?</strong>', 'Foundation was established with a small idea that was incepted in the minds of its promoters in year 2010! We skillfully guide the applicants for their immigration process to any country they aspire to settle. ', 'flaticon-policy', 'Accurate Guidance', 'Skilled professionals are always ready to provide reliable services to our clients!', 'flaticon-contract', 'Our Presence', 'Branches are situated in major metro cities and overseas, always open for you!', 'assets/images/single-img-08.jpg', 'WE MAKE A DIFFERENCE', 'We Have Been Counselling <strong>Students For Educational Opportunities In Foreign Countries</strong>', '', NULL, NULL, NULL, 1, '::1', '2020-10-03 13:49:23', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `about_us_item`
--

DROP TABLE IF EXISTS `about_us_item`;
CREATE TABLE IF NOT EXISTS `about_us_item` (
  `about_us_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_about_us_id` int(11) NOT NULL,
  `about_us_item_title` varchar(250) NOT NULL,
  `about_us_item_desc` text NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`about_us_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us_item`
--

INSERT INTO `about_us_item` (`about_us_item_id`, `ref_about_us_id`, `about_us_item_title`, `about_us_item_desc`, `is_active`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(161, 1, 'Who we are?', 'Established in Surat, Gujarat, India in 2010, Canopus Global Education has gone on to achieve many great feats and help thousands of people fulfill their foreign education and ambitions. What started as a small company, is now a renowned name. Canopus Global Education was founded by MrBhargav Gauswani a well-trained and experienced expert with extensive knowledge and strategic alliances and a vision to be recognized as a leading overseas education and immigration service provider catering to the needs of a myriad of people globally.', 1, 1, '::1', '2020-10-03 13:49:23', NULL, NULL, NULL, 'Live'),
(162, 1, 'Our Vision', 'Our vision is to provide smooth, transparent, and hassle-free services of overall consultation and immigration to people planning for long term foreign education', 1, 1, '::1', '2020-10-03 13:49:23', NULL, NULL, NULL, 'Live'),
(163, 1, 'Our Mission', 'Our Endeavour is to be recognized as world’s leading Immigration service provider catering to a wide array of clients globally and expand globally.', 1, 1, '::1', '2020-10-03 13:49:23', NULL, NULL, NULL, 'Live'),
(164, 1, 'Our Values', 'Our Values make us stand apart. We ensure that we work towards our clients’ goals with integrity, accuracy, and transparency.', 1, 1, '::1', '2020-10-03 13:49:23', NULL, NULL, NULL, 'Live'),
(165, 1, 'What Will We Do for You?', 'We  will be your direct window to British further education. Selecting a university for postgraduate studies in an unfamiliar country can be a daunting task. Gateway Abroad will help you to find the right university, based on your individual requirements. Once a pre-selection is made, we can contact the institutions and make all enquiries and admissions arrangements on your behalf.', 1, 1, '::1', '2020-10-03 13:49:23', NULL, NULL, NULL, 'Live'),
(166, 1, 'How Much Will This Cost?', 'We will not charge students for its services and advice. Upon request, we are able to provide extra services such as obtaining visas, arranging flight and UK travel tickets, accommodation in the UK, at an extra charge. Please enquire for specific extra services.', 1, 1, '::1', '2020-10-03 13:49:23', NULL, NULL, NULL, 'Live'),
(167, 1, 'Our Guiding Statements', '1. Providing counselling & learning as well as removing barriers to information for its clients through personal service and innovative practices\r\n2. Always striving towards excellence\r\n3. Developing a high brand equity in all areas of operations\r\n4. Achieving high profitability through ‘customer-delight’\r\nEvolving an organisation that achieves and retains a ‘Numero - Uno’ position through learning, hard work, service and honesty.', 0, 1, '::1', '2020-10-03 13:49:23', NULL, NULL, NULL, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `all_cities`
--

DROP TABLE IF EXISTS `all_cities`;
CREATE TABLE IF NOT EXISTS `all_cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` text,
  `city_code` text,
  `state_code` text,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=756 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `all_cities`
--

INSERT INTO `all_cities` (`id`, `city_name`, `city_code`, `state_code`, `del_status`) VALUES
(1, 'Alipur', '101', '1', 'Live'),
(2, 'Andaman Island', '102', '1', 'Live'),
(3, 'Anderson Island', '103', '1', 'Live'),
(4, 'Arainj-Laka-Punga', '104', '1', 'Live'),
(5, 'Austinabad', '105', '1', 'Live'),
(6, 'Bamboo Flat', '106', '1', 'Live'),
(7, 'Barren Island', '107', '1', 'Live'),
(8, 'Beadonabad', '108', '1', 'Live'),
(9, 'Betapur', '109', '1', 'Live'),
(10, 'Bindraban', '110', '1', 'Live'),
(11, 'Bonington', '111', '1', 'Live'),
(12, 'Brookesabad', '112', '1', 'Live'),
(13, 'Cadell Point', '113', '1', 'Live'),
(14, 'Calicut', '114', '1', 'Live'),
(15, 'Chetamale', '115', '1', 'Live'),
(16, 'Cinque Islands', '116', '1', 'Live'),
(17, 'Defence Island', '117', '1', 'Live'),
(18, 'Digilpur', '118', '1', 'Live'),
(19, 'Dolyganj', '119', '1', 'Live'),
(20, 'Flat Island', '120', '1', 'Live'),
(21, 'Geinyale', '121', '1', 'Live'),
(22, 'Great Coco Island', '122', '1', 'Live'),
(23, 'Haddo', '123', '1', 'Live'),
(24, 'Havelock Island', '124', '1', 'Live'),
(25, 'Henry Lawrence Island', '125', '1', 'Live'),
(26, 'Herbertabad', '126', '1', 'Live'),
(27, 'Hobdaypur', '127', '1', 'Live'),
(28, 'Ilichar', '128', '1', 'Live'),
(29, 'Ingoie', '128', '1', 'Live'),
(30, 'Inteview Island', '130', '1', 'Live'),
(31, 'Jangli Ghat', '131', '1', 'Live'),
(32, 'Jhon Lawrence Island', '132', '1', 'Live'),
(33, 'Karen', '133', '1', 'Live'),
(34, 'Kartara', '134', '1', 'Live'),
(35, 'KYD Islannd', '135', '1', 'Live'),
(36, 'Landfall Island', '136', '1', 'Live'),
(37, 'Little Andmand', '137', '1', 'Live'),
(38, 'Little Coco Island', '138', '1', 'Live'),
(39, 'Long Island', '138', '1', 'Live'),
(40, 'Maimyo', '140', '1', 'Live'),
(41, 'Malappuram', '141', '1', 'Live'),
(42, 'Manglutan', '142', '1', 'Live'),
(43, 'Manpur', '143', '1', 'Live'),
(44, 'Mitha Khari', '144', '1', 'Live'),
(45, 'Neill Island', '145', '1', 'Live'),
(46, 'Nicobar Island', '146', '1', 'Live'),
(47, 'North Brother Island', '147', '1', 'Live'),
(48, 'North Passage Island', '148', '1', 'Live'),
(49, 'North Sentinel Island', '149', '1', 'Live'),
(50, 'Nothen Reef Island', '150', '1', 'Live'),
(51, 'Outram Island', '151', '1', 'Live'),
(52, 'Pahlagaon', '152', '1', 'Live'),
(53, 'Palalankwe', '153', '1', 'Live'),
(54, 'Passage Island', '154', '1', 'Live'),
(55, 'Phaiapong', '155', '1', 'Live'),
(56, 'Phoenix Island', '156', '1', 'Live'),
(57, 'Port Blair', '157', '1', 'Live'),
(58, 'Preparis Island', '158', '1', 'Live'),
(59, 'Protheroepur', '159', '1', 'Live'),
(60, 'Rangachang', '160', '1', 'Live'),
(61, 'Rongat', '161', '1', 'Live'),
(62, 'Rutland Island', '162', '1', 'Live'),
(63, 'Sabari', '163', '1', 'Live'),
(64, 'Saddle Peak', '164', '1', 'Live'),
(65, 'Shadipur', '165', '1', 'Live'),
(66, 'Smith Island', '166', '1', 'Live'),
(67, 'Sound Island', '167', '1', 'Live'),
(68, 'South Sentinel Island', '168', '1', 'Live'),
(69, 'Spike Island', '169', '1', 'Live'),
(70, 'Tarmugli Island', '170', '1', 'Live'),
(71, 'Taylerabad', '171', '1', 'Live'),
(72, 'Titaije', '172', '1', 'Live'),
(73, 'Toibalawe', '173', '1', 'Live'),
(74, 'Tusonabad', '174', '1', 'Live'),
(75, 'West Island', '175', '1', 'Live'),
(76, 'Wimberleyganj', '176', '1', 'Live'),
(77, 'Yadita', '177', '1', 'Live'),
(78, 'Adilabad', '201', '2', 'Live'),
(79, 'Anantapur', '201', '2', 'Live'),
(80, 'Chittoor', '203', '2', 'Live'),
(81, 'Cuddapah', '204', '2', 'Live'),
(82, 'East Godavari', '205', '2', 'Live'),
(83, 'Guntur', '206', '2', 'Live'),
(84, 'Hyderabad', '207', '2', 'Live'),
(85, 'Karimnagar', '208', '2', 'Live'),
(86, 'Khammam', '209', '2', 'Live'),
(87, 'Krishna', '210', '2', 'Live'),
(88, 'Kurnool', '211', '2', 'Live'),
(89, 'Mahabubnagar', '212', '2', 'Live'),
(90, 'Medak', '213', '2', 'Live'),
(91, 'Nalgonda', '214', '2', 'Live'),
(92, 'Nellore', '215', '2', 'Live'),
(93, 'Nizamabad', '216', '2', 'Live'),
(94, 'Prakasam', '217', '2', 'Live'),
(95, 'Rangareddy', '218', '2', 'Live'),
(96, 'Srikakulam', '219', '2', 'Live'),
(97, 'Visakhapatnam', '220', '2', 'Live'),
(98, 'Vizianagaram', '221', '2', 'Live'),
(99, 'Warangal', '222', '2', 'Live'),
(100, 'West Godavari', '223', '2', 'Live'),
(101, 'Anjaw', '301', '3', 'Live'),
(102, 'Changlang', '302', '3', 'Live'),
(103, 'Dibang Valley', '303', '3', 'Live'),
(104, 'East Kameng', '304', '3', 'Live'),
(105, 'East Siang', '305', '3', 'Live'),
(106, 'Itanagar', '306', '3', 'Live'),
(107, 'Kurung Kumey', '307', '3', 'Live'),
(108, 'Lohit', '308', '3', 'Live'),
(109, 'Lower Dibang Valley', '309', '3', 'Live'),
(110, 'Lower Subansiri', '310', '3', 'Live'),
(111, 'Papum Pare', '311', '3', 'Live'),
(112, 'Tawang', '312', '3', 'Live'),
(113, 'Tirap', '313', '3', 'Live'),
(114, 'Upper Siang', '314', '3', 'Live'),
(115, 'Upper Subansiri', '315', '3', 'Live'),
(116, 'West Kameng', '316', '3', 'Live'),
(117, 'West Siang', '317', '3', 'Live'),
(118, 'Barpeta', '401', '4', 'Live'),
(119, 'Bongaigaon', '402', '4', 'Live'),
(120, 'Cachar', '403', '4', 'Live'),
(121, 'Darrang', '404', '4', 'Live'),
(122, 'Dhemaji', '405', '4', 'Live'),
(123, 'Dhubri', '406', '4', 'Live'),
(124, 'Dibrugarh', '407', '4', 'Live'),
(125, 'Goalpara', '408', '4', 'Live'),
(126, 'Golaghat', '409', '4', 'Live'),
(127, 'Guwahati', '410', '4', 'Live'),
(128, 'Hailakandi', '411', '4', 'Live'),
(129, 'Jorhat', '412', '4', 'Live'),
(130, 'Kamrup', '413', '4', 'Live'),
(131, 'Karbi Anglong', '414', '4', 'Live'),
(132, 'Karimganj', '415', '4', 'Live'),
(133, 'Kokrajhar', '416', '4', 'Live'),
(134, 'Lakhimpur', '417', '4', 'Live'),
(135, 'Marigaon', '418', '4', 'Live'),
(136, 'Nagaon', '419', '4', 'Live'),
(137, 'Nalbari', '420', '4', 'Live'),
(138, 'North Cachar Hills', '421', '4', 'Live'),
(139, 'Silchar', '422', '4', 'Live'),
(140, 'Sivasagar', '423', '4', 'Live'),
(141, 'Sonitpur', '424', '4', 'Live'),
(142, 'Tinsukia', '425', '4', 'Live'),
(143, 'Udalguri', '426', '4', 'Live'),
(144, 'Araria', '501', '5', 'Live'),
(145, 'Aurangabad', '502', '5', 'Live'),
(146, 'Banka', '503', '5', 'Live'),
(147, 'Begusarai', '504', '5', 'Live'),
(148, 'Bhagalpur', '505', '5', 'Live'),
(149, 'Bhojpur', '506', '5', 'Live'),
(150, 'Buxar', '507', '5', 'Live'),
(151, 'Darbhanga', '508', '5', 'Live'),
(152, 'East Champaran', '509', '5', 'Live'),
(153, 'Gaya', '510', '5', 'Live'),
(154, 'Gopalganj', '511', '5', 'Live'),
(155, 'Jamshedpur', '512', '5', 'Live'),
(156, 'Jamui', '513', '5', 'Live'),
(157, 'Jehanabad', '514', '5', 'Live'),
(158, 'Kaimur (Bhabua)', '515', '5', 'Live'),
(159, 'Katihar', '516', '5', 'Live'),
(160, 'Khagaria', '517', '5', 'Live'),
(161, 'Kishanganj', '518', '5', 'Live'),
(162, 'Lakhisarai', '519', '5', 'Live'),
(163, 'Madhepura', '520', '5', 'Live'),
(164, 'Madhubani', '521', '5', 'Live'),
(165, 'Munger', '522', '5', 'Live'),
(166, 'Muzaffarpur', '523', '5', 'Live'),
(167, 'Nalanda', '524', '5', 'Live'),
(168, 'Nawada', '525', '5', 'Live'),
(169, 'Patna', '526', '5', 'Live'),
(170, 'Purnia', '527', '5', 'Live'),
(171, 'Rohtas', '528', '5', 'Live'),
(172, 'Saharsa', '529', '5', 'Live'),
(173, 'Samastipur', '530', '5', 'Live'),
(174, 'Saran', '531', '5', 'Live'),
(175, 'Sheikhpura', '532', '5', 'Live'),
(176, 'Sheohar', '533', '5', 'Live'),
(177, 'Sitamarhi', '534', '5', 'Live'),
(178, 'Siwan', '535', '5', 'Live'),
(179, 'Supaul', '536', '5', 'Live'),
(180, 'Vaishali', '537', '5', 'Live'),
(181, 'West Champaran', '538', '5', 'Live'),
(182, 'Chandigarh', '601', '6', 'Live'),
(183, 'Mani Marja', '602', '6', 'Live'),
(184, 'Bastar', '701', '7', 'Live'),
(185, 'Bhilai', '702', '7', 'Live'),
(186, 'Bijapur', '703', '7', 'Live'),
(187, 'Bilaspur', '704', '7', 'Live'),
(188, 'Dhamtari', '705', '7', 'Live'),
(189, 'Durg', '706', '7', 'Live'),
(190, 'Janjgir-Champa', '707', '7', 'Live'),
(191, 'Jashpur', '708', '7', 'Live'),
(192, 'Kabirdham-Kawardha', '709', '7', 'Live'),
(193, 'Korba', '710', '7', 'Live'),
(194, 'Korea', '711', '7', 'Live'),
(195, 'Mahasamund', '712', '7', 'Live'),
(196, 'Narayanpur', '713', '7', 'Live'),
(197, 'Norh Bastar-Kanker', '714', '7', 'Live'),
(198, 'Raigarh', '715', '7', 'Live'),
(199, 'Raipur', '716', '7', 'Live'),
(200, 'Rajnandgaon', '717', '7', 'Live'),
(201, 'South Bastar-Dantewada', '718', '7', 'Live'),
(202, 'Surguja', '719', '7', 'Live'),
(203, 'Amal', '801', '8', 'Live'),
(204, 'Amli', '802', '8', 'Live'),
(205, 'Bedpa', '803', '8', 'Live'),
(206, 'Chikhli', '804', '8', 'Live'),
(207, 'Dadra & Nagar Haveli', '805', '8', 'Live'),
(208, 'Dahikhed', '806', '8', 'Live'),
(209, 'Dolara', '807', '8', 'Live'),
(210, 'Galonda', '808', '8', 'Live'),
(211, 'Kanadi', '809', '8', 'Live'),
(212, 'Karchond', '810', '8', 'Live'),
(213, 'Khadoli', '811', '8', 'Live'),
(214, 'Kharadpada', '812', '8', 'Live'),
(215, 'Kherabari', '813', '8', 'Live'),
(216, 'Kherdi', '814', '8', 'Live'),
(217, 'Kothar', '815', '8', 'Live'),
(218, 'Luari', '816', '8', 'Live'),
(219, 'Mashat', '817', '8', 'Live'),
(220, 'Rakholi', '818', '8', 'Live'),
(221, 'Rudana', '819', '8', 'Live'),
(222, 'Saili', '820', '8', 'Live'),
(223, 'Sili', '821', '8', 'Live'),
(224, 'Silvassa', '822', '8', 'Live'),
(225, 'Sindavni', '823', '8', 'Live'),
(226, 'Udva', '824', '8', 'Live'),
(227, 'Umbarkoi', '825', '8', 'Live'),
(228, 'Vansda', '826', '8', 'Live'),
(229, 'Vasona', '827', '8', 'Live'),
(230, 'Velugam', '828', '8', 'Live'),
(231, 'Brancavare', '901', '9', 'Live'),
(232, 'Dagasi', '902', '9', 'Live'),
(233, 'Daman', '903', '9', 'Live'),
(234, 'Diu', '904', '9', 'Live'),
(235, 'Magarvara', '905', '9', 'Live'),
(236, 'Nagwa', '906', '9', 'Live'),
(237, 'Pariali', '907', '9', 'Live'),
(238, 'Passo Covo', '908', '9', 'Live'),
(239, 'Central Delhi', '1001', '10', 'Live'),
(240, 'East Delhi', '1002', '10', 'Live'),
(241, 'New Delhi', '1003', '10', 'Live'),
(242, 'North Delhi', '1004', '10', 'Live'),
(243, 'North East Delhi', '1005', '10', 'Live'),
(244, 'North West Delhi', '1006', '10', 'Live'),
(245, 'Old Delhi', '1007', '10', 'Live'),
(246, 'South Delhi', '1008', '10', 'Live'),
(247, 'South West Delhi', '1009', '10', 'Live'),
(248, 'West Delhi', '1010', '10', 'Live'),
(249, 'Canacona', '1101', '11', 'Live'),
(250, 'Candolim', '1102', '11', 'Live'),
(251, 'Chinchinim', '1103', '11', 'Live'),
(252, 'Cortalim', '1104', '11', 'Live'),
(253, 'Goa', '1105', '11', 'Live'),
(254, 'Jua', '1106', '11', 'Live'),
(255, 'Madgaon', '1107', '11', 'Live'),
(256, 'Mahem', '1108', '11', 'Live'),
(257, 'Mapuca', '1109', '11', 'Live'),
(258, 'Marmagao', '1110', '11', 'Live'),
(259, 'Panji', '1111', '11', 'Live'),
(260, 'Ponda', '1112', '11', 'Live'),
(261, 'Sanvordem', '1113', '11', 'Live'),
(262, 'Terekhol', '1114', '11', 'Live'),
(263, 'Ahmedabad', '1201', '12', 'Live'),
(264, 'Amreli', '1202', '12', 'Live'),
(265, 'Anand', '1203', '12', 'Live'),
(266, 'Banaskantha', '1204', '12', 'Live'),
(267, 'Baroda', '1205', '12', 'Live'),
(268, 'Bharuch', '1206', '12', 'Live'),
(269, 'Bhavnagar', '1207', '12', 'Live'),
(270, 'Dahod', '1208', '12', 'Live'),
(271, 'Dang', '1209', '12', 'Live'),
(272, 'Dwarka', '1210', '12', 'Live'),
(273, 'Gandhinagar', '1211', '12', 'Live'),
(274, 'Jamnagar', '1212', '12', 'Live'),
(275, 'Junagadh', '1213', '12', 'Live'),
(276, 'Kheda', '1214', '12', 'Live'),
(277, 'Kutch', '1215', '12', 'Live'),
(278, 'Mehsana', '1216', '12', 'Live'),
(279, 'Nadiad', '1217', '12', 'Live'),
(280, 'Narmada', '1218', '12', 'Live'),
(281, 'Navsari', '1219', '12', 'Live'),
(282, 'Panchmahals', '1220', '12', 'Live'),
(283, 'Patan', '1221', '12', 'Live'),
(284, 'Porbandar', '1222', '12', 'Live'),
(285, 'Rajkot', '1223', '12', 'Live'),
(286, 'Sabarkantha', '1224', '12', 'Live'),
(287, 'Surat', '1225', '12', 'Live'),
(288, 'Surendranagar', '1226', '12', 'Live'),
(289, 'Vadodara', '1227', '12', 'Live'),
(290, 'Valsad', '1228', '12', 'Live'),
(291, 'Vapi', '1229', '12', 'Live'),
(292, 'Ambala', '1301', '13', 'Live'),
(293, 'Bhiwani', '1302', '13', 'Live'),
(294, 'Faridabad', '1303', '13', 'Live'),
(295, 'Fatehabad', '1304', '13', 'Live'),
(296, 'Gurgaon', '1305', '13', 'Live'),
(297, 'Hisar', '1306', '13', 'Live'),
(298, 'Jhajjar', '1307', '13', 'Live'),
(299, 'Jind', '1308', '13', 'Live'),
(300, 'Kaithal', '1309', '13', 'Live'),
(301, 'Karnal', '1310', '13', 'Live'),
(302, 'Kurukshetra', '1311', '13', 'Live'),
(303, 'Mahendragarh', '1312', '13', 'Live'),
(304, 'Mewat', '1313', '13', 'Live'),
(305, 'Panchkula', '1314', '13', 'Live'),
(306, 'Panipat', '1315', '13', 'Live'),
(307, 'Rewari', '1316', '13', 'Live'),
(308, 'Rohtak', '1317', '13', 'Live'),
(309, 'Sirsa', '1318', '13', 'Live'),
(310, 'Sonipat', '1319', '13', 'Live'),
(311, 'Yamunanagar', '1320', '13', 'Live'),
(312, 'Bilaspur', '1401', '14', 'Live'),
(313, 'Chamba', '1402', '14', 'Live'),
(314, 'Dalhousie', '1403', '14', 'Live'),
(315, 'Hamirpur', '1404', '14', 'Live'),
(316, 'Kangra', '1405', '14', 'Live'),
(317, 'Kinnaur', '1406', '14', 'Live'),
(318, 'Kullu', '1407', '14', 'Live'),
(319, 'Lahaul & Spiti', '1408', '14', 'Live'),
(320, 'Mandi', '1409', '14', 'Live'),
(321, 'Shimla', '1410', '14', 'Live'),
(322, 'Sirmaur', '1411', '14', 'Live'),
(323, 'Solan', '1412', '14', 'Live'),
(324, 'Una', '1413', '14', 'Live'),
(325, 'Anantnag', '1501', '15', 'Live'),
(326, 'Baramulla', '1502', '15', 'Live'),
(327, 'Budgam', '1503', '15', 'Live'),
(328, 'Doda', '1504', '15', 'Live'),
(329, 'Jammu', '1505', '15', 'Live'),
(330, 'Kargil', '1506', '15', 'Live'),
(331, 'Kathua', '1507', '15', 'Live'),
(332, 'Kupwara', '1508', '15', 'Live'),
(333, 'Leh', '1509', '15', 'Live'),
(334, 'Poonch', '1510', '15', 'Live'),
(335, 'Pulwama', '1511', '15', 'Live'),
(336, 'Rajauri', '1512', '15', 'Live'),
(337, 'Srinagar', '1513', '15', 'Live'),
(338, 'Udhampur', '1514', '15', 'Live'),
(339, 'Bokaro', '1601', '16', 'Live'),
(340, 'Chatra', '1602', '16', 'Live'),
(341, 'Deoghar', '1603', '16', 'Live'),
(342, 'Dhanbad', '1604', '16', 'Live'),
(343, 'Dumka', '1605', '16', 'Live'),
(344, 'East Singhbhum', '1606', '16', 'Live'),
(345, 'Garhwa', '1607', '16', 'Live'),
(346, 'Giridih', '1608', '16', 'Live'),
(347, 'Godda', '1609', '16', 'Live'),
(348, 'Gumla', '1610', '16', 'Live'),
(349, 'Hazaribag', '1611', '16', 'Live'),
(350, 'Jamtara', '1612', '16', 'Live'),
(351, 'Koderma', '1613', '16', 'Live'),
(352, 'Latehar', '1614', '16', 'Live'),
(353, 'Lohardaga', '1615', '16', 'Live'),
(354, 'Pakur', '1616', '16', 'Live'),
(355, 'Palamu', '1617', '16', 'Live'),
(356, 'Ranchi', '1618', '16', 'Live'),
(357, 'Sahibganj', '1619', '16', 'Live'),
(358, 'Seraikela', '1620', '16', 'Live'),
(359, 'Simdega', '1621', '16', 'Live'),
(360, 'West Singhbhum', '1622', '16', 'Live'),
(361, 'Bagalkot', '1701', '17', 'Live'),
(362, 'Bangalore', '1702', '17', 'Live'),
(363, 'Bangalore Rural', '1703', '17', 'Live'),
(364, 'Belgaum', '1704', '17', 'Live'),
(365, 'Bellary', '1705', '17', 'Live'),
(366, 'Bhatkal', '1706', '17', 'Live'),
(367, 'Bidar', '1707', '17', 'Live'),
(368, 'Bijapur', '1708', '17', 'Live'),
(369, 'Chamrajnagar', '1709', '17', 'Live'),
(370, 'Chickmagalur', '1710', '17', 'Live'),
(371, 'Chikballapur', '1711', '17', 'Live'),
(372, 'Chitradurga', '1712', '17', 'Live'),
(373, 'Dakshina Kannada', '1713', '17', 'Live'),
(374, 'Davanagere', '1714', '17', 'Live'),
(375, 'Dharwad', '1715', '17', 'Live'),
(376, 'Gadag', '1716', '17', 'Live'),
(377, 'Gulbarga', '1717', '17', 'Live'),
(378, 'Hampi', '1718', '17', 'Live'),
(379, 'Hassan', '1719', '17', 'Live'),
(380, 'Haveri', '1720', '17', 'Live'),
(381, 'Hospet', '1721', '17', 'Live'),
(382, 'Karwar', '1722', '17', 'Live'),
(383, 'Kodagu', '1723', '17', 'Live'),
(384, 'Kolar', '1724', '17', 'Live'),
(385, 'Koppal', '1725', '17', 'Live'),
(386, 'Madikeri', '1726', '17', 'Live'),
(387, 'Mandya', '1727', '17', 'Live'),
(388, 'Mangalore', '1728', '17', 'Live'),
(389, 'Manipal', '1729', '17', 'Live'),
(390, 'Mysore', '1730', '17', 'Live'),
(391, 'Raichur', '1731', '17', 'Live'),
(392, 'Shimoga', '1732', '17', 'Live'),
(393, 'Sirsi', '1733', '17', 'Live'),
(394, 'Sringeri', '1734', '17', 'Live'),
(395, 'Srirangapatna', '1735', '17', 'Live'),
(396, 'Tumkur', '1736', '17', 'Live'),
(397, 'Udupi', '1737', '17', 'Live'),
(398, 'Uttara Kannada', '1738', '17', 'Live'),
(399, 'Alappuzha', '1801', '18', 'Live'),
(400, 'Alleppey', '1802', '18', 'Live'),
(401, 'Alwaye', '1803', '18', 'Live'),
(402, 'Ernakulam', '1804', '18', 'Live'),
(403, 'Idukki', '1805', '18', 'Live'),
(404, 'Kannur', '1806', '18', 'Live'),
(405, 'Kasargod', '1807', '18', 'Live'),
(406, 'Kochi', '1808', '18', 'Live'),
(407, 'Kollam', '1809', '18', 'Live'),
(408, 'Kottayam', '1810', '18', 'Live'),
(409, 'Kovalam', '1811', '18', 'Live'),
(410, 'Kozhikode', '1812', '18', 'Live'),
(411, 'Malappuram', '1813', '18', 'Live'),
(412, 'Palakkad', '1814', '18', 'Live'),
(413, 'Pathanamthitta', '1815', '18', 'Live'),
(414, 'Perumbavoor', '1816', '18', 'Live'),
(415, 'Thiruvananthapuram', '1817', '18', 'Live'),
(416, 'Thrissur', '1818', '18', 'Live'),
(417, 'Trichur', '1819', '18', 'Live'),
(418, 'Trivandrum', '1820', '18', 'Live'),
(419, 'Wayanad', '1821', '18', 'Live'),
(420, 'Agatti Island', '1901', '19', 'Live'),
(421, 'Bingaram Island', '1902', '19', 'Live'),
(422, 'Bitra Island', '1903', '19', 'Live'),
(423, 'Chetlat Island', '1904', '19', 'Live'),
(424, 'Kadmat Island', '1905', '19', 'Live'),
(425, 'Kalpeni Island', '1906', '19', 'Live'),
(426, 'Kavaratti Island', '1907', '19', 'Live'),
(427, 'Kiltan Island', '1908', '19', 'Live'),
(428, 'Lakshadweep Sea', '1909', '19', 'Live'),
(429, 'Minicoy Island', '1910', '19', 'Live'),
(430, 'North Island', '1911', '19', 'Live'),
(431, 'South Island', '1912', '19', 'Live'),
(432, 'Anuppur', '2001', '20', 'Live'),
(433, 'Ashoknagar', '2002', '20', 'Live'),
(434, 'Balaghat', '2003', '20', 'Live'),
(435, 'Barwani', '2004', '20', 'Live'),
(436, 'Betul', '2005', '20', 'Live'),
(437, 'Bhind', '2006', '20', 'Live'),
(438, 'Bhopal', '2007', '20', 'Live'),
(439, 'Burhanpur', '2008', '20', 'Live'),
(440, 'Chhatarpur', '2009', '20', 'Live'),
(441, 'Chhindwara', '2010', '20', 'Live'),
(442, 'Damoh', '2011', '20', 'Live'),
(443, 'Datia', '2012', '20', 'Live'),
(444, 'Dewas', '2013', '20', 'Live'),
(445, 'Dhar', '2014', '20', 'Live'),
(446, 'Dindori', '2015', '20', 'Live'),
(447, 'Guna', '2016', '20', 'Live'),
(448, 'Gwalior', '2017', '20', 'Live'),
(449, 'Harda', '2018', '20', 'Live'),
(450, 'Hoshangabad', '2019', '20', 'Live'),
(451, 'Indore', '2020', '20', 'Live'),
(452, 'Jabalpur', '2021', '20', 'Live'),
(453, 'Jagdalpur', '2022', '20', 'Live'),
(454, 'Jhabua', '2023', '20', 'Live'),
(455, 'Katni', '2024', '20', 'Live'),
(456, 'Khandwa', '2025', '20', 'Live'),
(457, 'Khargone', '2026', '20', 'Live'),
(458, 'Mandla', '2027', '20', 'Live'),
(459, 'Mandsaur', '2028', '20', 'Live'),
(460, 'Morena', '2029', '20', 'Live'),
(461, 'Narsinghpur', '2030', '20', 'Live'),
(462, 'Neemuch', '2031', '20', 'Live'),
(463, 'Panna', '2032', '20', 'Live'),
(464, 'Raisen', '2033', '20', 'Live'),
(465, 'Rajgarh', '2034', '20', 'Live'),
(466, 'Ratlam', '2035', '20', 'Live'),
(467, 'Rewa', '2036', '20', 'Live'),
(468, 'Sagar', '2037', '20', 'Live'),
(469, 'Satna', '2038', '20', 'Live'),
(470, 'Sehore', '2039', '20', 'Live'),
(471, 'Seoni', '2040', '20', 'Live'),
(472, 'Shahdol', '2041', '20', 'Live'),
(473, 'Shajapur', '2042', '20', 'Live'),
(474, 'Sheopur', '2043', '20', 'Live'),
(475, 'Shivpuri', '2044', '20', 'Live'),
(476, 'Sidhi', '2045', '20', 'Live'),
(477, 'Tikamgarh', '2046', '20', 'Live'),
(478, 'Ujjain', '2047', '20', 'Live'),
(479, 'Umaria', '2048', '20', 'Live'),
(480, 'Vidisha', '2049', '20', 'Live'),
(481, 'Ahmednagar', '2101', '21', 'Live'),
(482, 'Akola', '2102', '21', 'Live'),
(483, 'Amravati', '2103', '21', 'Live'),
(484, 'Aurangabad', '2104', '21', 'Live'),
(485, 'Beed', '2105', '21', 'Live'),
(486, 'Bhandara', '2106', '21', 'Live'),
(487, 'Buldhana', '2107', '21', 'Live'),
(488, 'Chandrapur', '2108', '21', 'Live'),
(489, 'Dhule', '2109', '21', 'Live'),
(490, 'Gadchiroli', '2110', '21', 'Live'),
(491, 'Gondia', '2111', '21', 'Live'),
(492, 'Hingoli', '2112', '21', 'Live'),
(493, 'Jalgaon', '2113', '21', 'Live'),
(494, 'Jalna', '2114', '21', 'Live'),
(495, 'Kolhapur', '2115', '21', 'Live'),
(496, 'Latur', '2116', '21', 'Live'),
(497, 'Mahabaleshwar', '2117', '21', 'Live'),
(498, 'Mumbai', '2118', '21', 'Live'),
(499, 'Mumbai City', '2119', '21', 'Live'),
(500, 'Mumbai Suburban', '2120', '21', 'Live'),
(501, 'Nagpur', '2121', '21', 'Live'),
(502, 'Nanded', '2122', '21', 'Live'),
(503, 'Nandurbar', '2123', '21', 'Live'),
(504, 'Nashik', '2124', '21', 'Live'),
(505, 'Osmanabad', '2125', '21', 'Live'),
(506, 'Parbhani', '2126', '21', 'Live'),
(507, 'Pune', '2127', '21', 'Live'),
(508, 'Raigad', '2128', '21', 'Live'),
(509, 'Ratnagiri', '2129', '21', 'Live'),
(510, 'Sangli', '2130', '21', 'Live'),
(511, 'Satara', '2131', '21', 'Live'),
(512, 'Sholapur', '2132', '21', 'Live'),
(513, 'Sindhudurg', '2133', '21', 'Live'),
(514, 'Thane', '2134', '21', 'Live'),
(515, 'Wardha', '2135', '21', 'Live'),
(516, 'Washim', '2136', '21', 'Live'),
(517, 'Yavatmal', '2137', '21', 'Live'),
(518, 'Bishnupur', '2201', '22', 'Live'),
(519, 'Chandel', '2202', '22', 'Live'),
(520, 'Churachandpur', '2203', '22', 'Live'),
(521, 'Imphal East', '2204', '22', 'Live'),
(522, 'Imphal West', '2205', '22', 'Live'),
(523, 'Senapati', '2206', '22', 'Live'),
(524, 'Tamenglong', '2207', '22', 'Live'),
(525, 'Thoubal', '2208', '22', 'Live'),
(526, 'Ukhrul', '2209', '22', 'Live'),
(527, 'East Garo Hills', '2301', '23', 'Live'),
(528, 'East Khasi Hills', '2302', '23', 'Live'),
(529, 'Jaintia Hills', '2303', '23', 'Live'),
(530, 'Ri Bhoi', '2304', '23', 'Live'),
(531, 'Shillong', '2305', '23', 'Live'),
(532, 'South Garo Hills', '2306', '23', 'Live'),
(533, 'West Garo Hills', '2307', '23', 'Live'),
(534, 'West Khasi Hills', '2308', '23', 'Live'),
(535, 'Aizawl', '2401', '24', 'Live'),
(536, 'Champhai', '2402', '24', 'Live'),
(537, 'Kolasib', '2403', '24', 'Live'),
(538, 'Lawngtlai', '2404', '24', 'Live'),
(539, 'Lunglei', '2405', '24', 'Live'),
(540, 'Mamit', '2406', '24', 'Live'),
(541, 'Saiha', '2407', '24', 'Live'),
(542, 'Serchhip', '2408', '24', 'Live'),
(543, 'Dimapur', '2501', '25', 'Live'),
(544, 'Kohima', '2502', '25', 'Live'),
(545, 'Mokokchung', '2503', '25', 'Live'),
(546, 'Mon', '2504', '25', 'Live'),
(547, 'Phek', '2505', '25', 'Live'),
(548, 'Tuensang', '2506', '25', 'Live'),
(549, 'Wokha', '2507', '25', 'Live'),
(550, 'Zunheboto', '2508', '25', 'Live'),
(551, 'Angul', '2601', '26', 'Live'),
(552, 'Balangir', '2602', '26', 'Live'),
(553, 'Balasore', '2603', '26', 'Live'),
(554, 'Baleswar', '2604', '26', 'Live'),
(555, 'Bargarh', '2605', '26', 'Live'),
(556, 'Berhampur', '2606', '26', 'Live'),
(557, 'Bhadrak', '2607', '26', 'Live'),
(558, 'Bhubaneswar', '2608', '26', 'Live'),
(559, 'Boudh', '2609', '26', 'Live'),
(560, 'Cuttack', '2610', '26', 'Live'),
(561, 'Deogarh', '2611', '26', 'Live'),
(562, 'Dhenkanal', '2612', '26', 'Live'),
(563, 'Gajapati', '2613', '26', 'Live'),
(564, 'Ganjam', '2614', '26', 'Live'),
(565, 'Jagatsinghapur', '2615', '26', 'Live'),
(566, 'Jajpur', '2616', '26', 'Live'),
(567, 'Jharsuguda', '2617', '26', 'Live'),
(568, 'Kalahandi', '2618', '26', 'Live'),
(569, 'Kandhamal', '2619', '26', 'Live'),
(570, 'Kendrapara', '2620', '26', 'Live'),
(571, 'Kendujhar', '2621', '26', 'Live'),
(572, 'Khordha', '2622', '26', 'Live'),
(573, 'Koraput', '2623', '26', 'Live'),
(574, 'Malkangiri', '2624', '26', 'Live'),
(575, 'Mayurbhanj', '2625', '26', 'Live'),
(576, 'Nabarangapur', '2626', '26', 'Live'),
(577, 'Nayagarh', '2627', '26', 'Live'),
(578, 'Nuapada', '2628', '26', 'Live'),
(579, 'Puri', '2629', '26', 'Live'),
(580, 'Rayagada', '2630', '26', 'Live'),
(581, 'Rourkela', '2631', '26', 'Live'),
(582, 'Sambalpur', '2632', '26', 'Live'),
(583, 'Subarnapur', '2633', '26', 'Live'),
(584, 'Sundergarh', '2634', '26', 'Live'),
(585, 'Bahur', '2701', '27', 'Live'),
(586, 'Karaikal', '2701', '27', 'Live'),
(587, 'Mahe', '2701', '27', 'Live'),
(588, 'Pondicherry', '2701', '27', 'Live'),
(589, 'Purnankuppam', '2701', '27', 'Live'),
(590, 'Valudavur', '2701', '27', 'Live'),
(591, 'Villianur', '2701', '27', 'Live'),
(592, 'Yanam', '2701', '27', 'Live'),
(593, 'Amritsar', '2801', '28', 'Live'),
(594, 'Barnala', '2801', '28', 'Live'),
(595, 'Bathinda', '2801', '28', 'Live'),
(596, 'Faridkot', '2801', '28', 'Live'),
(597, 'Fatehgarh Sahib', '2801', '28', 'Live'),
(598, 'Ferozepur', '2801', '28', 'Live'),
(599, 'Gurdaspur', '2801', '28', 'Live'),
(600, 'Hoshiarpur', '2801', '28', 'Live'),
(601, 'Jalandhar', '2801', '28', 'Live'),
(602, 'Kapurthala', '2801', '28', 'Live'),
(603, 'Ludhiana', '2801', '28', 'Live'),
(604, 'Mansa', '2801', '28', 'Live'),
(605, 'Moga', '2801', '28', 'Live'),
(606, 'Muktsar', '2801', '28', 'Live'),
(607, 'Nawanshahr', '2801', '28', 'Live'),
(608, 'Pathankot', '2801', '28', 'Live'),
(609, 'Patiala', '2801', '28', 'Live'),
(610, 'Rupnagar', '2801', '28', 'Live'),
(611, 'Sangrur', '2801', '28', 'Live'),
(612, 'SAS Nagar', '2801', '28', 'Live'),
(613, 'Tarn Taran', '2801', '28', 'Live'),
(614, 'Ajmer', '2901', '29', 'Live'),
(615, 'Alwar', '2902', '29', 'Live'),
(616, 'Banswara', '2903', '29', 'Live'),
(617, 'Baran', '2904', '29', 'Live'),
(618, 'Barmer', '2905', '29', 'Live'),
(619, 'Bharatpur', '2906', '29', 'Live'),
(620, 'Bhilwara', '2907', '29', 'Live'),
(621, 'Bikaner', '2908', '29', 'Live'),
(622, 'Bundi', '2909', '29', 'Live'),
(623, 'Chittorgarh', '2910', '29', 'Live'),
(624, 'Churu', '2911', '29', 'Live'),
(625, 'Dausa', '2912', '29', 'Live'),
(626, 'Dholpur', '2913', '29', 'Live'),
(627, 'Dungarpur', '2914', '29', 'Live'),
(628, 'Hanumangarh', '2915', '29', 'Live'),
(629, 'Jaipur', '2916', '29', 'Live'),
(630, 'Jaisalmer', '2917', '29', 'Live'),
(631, 'Jalore', '2918', '29', 'Live'),
(632, 'Jhalawar', '2919', '29', 'Live'),
(633, 'Jhunjhunu', '2920', '29', 'Live'),
(634, 'Jodhpur', '2921', '29', 'Live'),
(635, 'Karauli', '2922', '29', 'Live'),
(636, 'Kota', '2923', '29', 'Live'),
(637, 'Nagaur', '2924', '29', 'Live'),
(638, 'Pali', '2925', '29', 'Live'),
(639, 'Pilani', '2926', '29', 'Live'),
(640, 'Rajsamand', '2927', '29', 'Live'),
(641, 'Sawai Madhopur', '2928', '29', 'Live'),
(642, 'Sikar', '2929', '29', 'Live'),
(643, 'Sirohi', '2930', '29', 'Live'),
(644, 'Sri Ganganagar', '2931', '29', 'Live'),
(645, 'Tonk', '2932', '29', 'Live'),
(646, 'Udaipur', '2933', '29', 'Live'),
(647, 'Barmiak', '3001', '30', 'Live'),
(648, 'Be', '3002', '30', 'Live'),
(649, 'Bhurtuk', '3003', '30', 'Live'),
(650, 'Chhubakha', '3004', '30', 'Live'),
(651, 'Chidam', '3005', '30', 'Live'),
(652, 'Chubha', '3006', '30', 'Live'),
(653, 'Chumikteng', '3007', '30', 'Live'),
(654, 'Dentam', '3008', '30', 'Live'),
(655, 'Dikchu', '3009', '30', 'Live'),
(656, 'Dzongri', '3010', '30', 'Live'),
(657, 'Gangtok', '3011', '30', 'Live'),
(658, 'Gauzing', '3012', '30', 'Live'),
(659, 'Gyalshing', '3013', '30', 'Live'),
(660, 'Hema', '3014', '30', 'Live'),
(661, 'Kerung', '3015', '30', 'Live'),
(662, 'Lachen', '3016', '30', 'Live'),
(663, 'Lachung', '3017', '30', 'Live'),
(664, 'Lema', '3018', '30', 'Live'),
(665, 'Lingtam', '3019', '30', 'Live'),
(666, 'Lungthu', '3020', '30', 'Live'),
(667, 'Mangan', '3021', '30', 'Live'),
(668, 'Namchi', '3022', '30', 'Live'),
(669, 'Namthang', '3023', '30', 'Live'),
(670, 'Nanga', '3024', '30', 'Live'),
(671, 'Nantang', '3025', '30', 'Live'),
(672, 'Naya Bazar', '3026', '30', 'Live'),
(673, 'Padamachen', '3027', '30', 'Live'),
(674, 'Pakhyong', '3028', '30', 'Live'),
(675, 'Pemayangtse', '3029', '30', 'Live'),
(676, 'Phensang', '3030', '30', 'Live'),
(677, 'Rangli', '3031', '30', 'Live'),
(678, 'Rinchingpong', '3032', '30', 'Live'),
(679, 'Sakyong', '3033', '30', 'Live'),
(680, 'Samdong', '3034', '30', 'Live'),
(681, 'Singtam', '3035', '30', 'Live'),
(682, 'Siniolchu', '3035', '30', 'Live'),
(683, 'Sombari', '3036', '30', 'Live'),
(684, 'Soreng', '3037', '30', 'Live'),
(685, 'Sosing', '3038', '30', 'Live'),
(686, 'Tekhug', '3039', '30', 'Live'),
(687, 'Temi', '3040', '30', 'Live'),
(688, 'Tsetang', '3041', '30', 'Live'),
(689, 'Tsomgo', '3042', '30', 'Live'),
(690, 'Tumlong', '3043', '30', 'Live'),
(691, 'Yangang', '3044', '30', 'Live'),
(692, 'Yumtang', '3045', '30', 'Live'),
(693, 'Chennai', '3101', '31', 'Live'),
(694, 'Chidambaram', '3102', '31', 'Live'),
(695, 'Chingleput', '3103', '31', 'Live'),
(696, 'Coimbatore', '3104', '31', 'Live'),
(697, 'Courtallam', '3105', '31', 'Live'),
(698, 'Cuddalore', '3106', '31', 'Live'),
(699, 'Dharmapuri', '3107', '31', 'Live'),
(700, 'Dindigul', '3108', '31', 'Live'),
(701, 'Erode', '3109', '31', 'Live'),
(702, 'Hosur', '3110', '31', 'Live'),
(703, 'Kanchipuram', '3111', '31', 'Live'),
(704, 'Kanyakumari', '3112', '31', 'Live'),
(705, 'Karaikudi', '3113', '31', 'Live'),
(706, 'Karur', '3114', '31', 'Live'),
(707, 'Kodaikanal', '3115', '31', 'Live'),
(708, 'Kovilpatti', '3116', '31', 'Live'),
(709, 'Krishnagiri', '3117', '31', 'Live'),
(710, 'Kumbakonam', '3118', '31', 'Live'),
(711, 'Madurai', '3119', '31', 'Live'),
(712, 'Mayiladuthurai', '3120', '31', 'Live'),
(713, 'Nagapattinam', '3121', '31', 'Live'),
(714, 'Nagarcoil', '3122', '31', 'Live'),
(715, 'Namakkal', '3123', '31', 'Live'),
(716, 'Neyveli', '3124', '31', 'Live'),
(717, 'Nilgiris', '3125', '31', 'Live'),
(718, 'Ooty', '3126', '31', 'Live'),
(719, 'Palani', '3127', '31', 'Live'),
(720, 'Perambalur', '3128', '31', 'Live'),
(721, 'Pollachi', '3129', '31', 'Live'),
(722, 'Pudukkottai', '3130', '31', 'Live'),
(723, 'Rajapalayam', '3131', '31', 'Live'),
(724, 'Ramanathapuram', '3132', '31', 'Live'),
(725, 'Salem', '3133', '31', 'Live'),
(726, 'Sivaganga', '3134', '31', 'Live'),
(727, 'Sivakasi', '3135', '31', 'Live'),
(728, 'Thanjavur', '3136', '31', 'Live'),
(729, 'Theni', '3137', '31', 'Live'),
(730, 'Thoothukudi', '3138', '31', 'Live'),
(731, 'Tiruchirappalli', '3139', '31', 'Live'),
(732, 'Tirunelveli', '3140', '31', 'Live'),
(733, 'Tirupur', '3141', '31', 'Live'),
(734, 'Tiruvallur', '3142', '31', 'Live'),
(735, 'Tiruvannamalai', '3143', '31', 'Live'),
(736, 'Tiruvarur', '3144', '31', 'Live'),
(737, 'Trichy', '3145', '31', 'Live'),
(738, 'Tuticorin', '3146', '31', 'Live'),
(739, 'Vellore', '3147', '31', 'Live'),
(740, 'Villupuram', '3148', '31', 'Live'),
(741, 'Virudhunagar', '3149', '31', 'Live'),
(742, 'Yercaud', '3150', '31', 'Live'),
(743, 'Agartala', '3201', '32', 'Live'),
(744, 'Ambasa', '3202', '32', 'Live'),
(745, 'Bampurbari', '3203', '32', 'Live'),
(746, 'Belonia', '3204', '32', 'Live'),
(747, 'Dhalai', '3205', '32', 'Live'),
(748, 'Dharam Nagar', '3206', '32', 'Live'),
(749, 'Kailashahar', '3207', '32', 'Live'),
(750, 'Kamal Krishnabari', '3208', '32', 'Live'),
(751, 'Khopaiyapara', '3209', '32', 'Live'),
(752, 'Khowai', '3210', '32', 'Live'),
(753, 'Phuldungsei', '3211', '32', 'Live'),
(754, 'Radha Kishore Pur', '3212', '32', 'Live'),
(755, 'Tripura', '3213', '32', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `all_states`
--

DROP TABLE IF EXISTS `all_states`;
CREATE TABLE IF NOT EXISTS `all_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state_code` text,
  `state_name` text,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `all_states`
--

INSERT INTO `all_states` (`id`, `state_code`, `state_name`, `del_status`) VALUES
(1, '1', 'Andaman & Nicobar [AN]', 'Live'),
(2, '2', 'Andhra Pradesh [AP]', 'Live'),
(3, '3', 'Arunachal Pradesh [AR]', 'Live'),
(4, '4', 'Assam [AS]', 'Live'),
(5, '5', 'Bihar [BH]', 'Live'),
(6, '6', 'Chandigarh [CH]', 'Live'),
(7, '7', 'Chhattisgarh [CG]', 'Live'),
(8, '8', 'Dadra & Nagar Haveli [DN]', 'Live'),
(9, '9', 'Daman & Diu [DD]', 'Live'),
(10, '10', 'Delhi [DL]', 'Live'),
(11, '11', 'Goa [GO]', 'Live'),
(12, '12', 'Gujarat [GU]', 'Live'),
(13, '13', 'Haryana [HR]', 'Live'),
(14, '14', 'Himachal Pradesh [HP]', 'Live'),
(15, '15', 'Jammu & Kashmir [JK]', 'Live'),
(16, '16', 'Jharkhand [JH]', 'Live'),
(17, '17', 'Karnataka [KR]', 'Live'),
(18, '18', 'Kerala [KL]', 'Live'),
(19, '19', 'Lakshadweep [LD]', 'Live'),
(20, '20', 'Madhya Pradesh [MP]', 'Live'),
(21, '21', 'Maharashtra [MH]', 'Live'),
(22, '22', 'Manipur [MN]', 'Live'),
(23, '23', 'Meghalaya [ML]', 'Live'),
(24, '24', 'Mizoram [MM]', 'Live'),
(25, '25', 'Nagaland [NL]', 'Live'),
(26, '26', 'Orissa [OR]', 'Live'),
(27, '27', 'Pondicherry [PC]', 'Live'),
(28, '28', 'Punjab [PJ]', 'Live'),
(29, '29', 'Rajasthan [RJ]', 'Live'),
(30, '30', 'Sikkim [SK]', 'Live'),
(31, '31', 'Tamil Nadu [TN]', 'Live'),
(32, '32', 'Tripura [TR]', 'Live'),
(33, '33', 'Uttar Pradesh [UP]', 'Live'),
(34, '34', 'Uttaranchal [UT]', 'Live'),
(35, '35', 'West Bengal [WB]', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `background_img`
--

DROP TABLE IF EXISTS `background_img`;
CREATE TABLE IF NOT EXISTS `background_img` (
  `bg_id` int(11) NOT NULL AUTO_INCREMENT,
  `bg_image` text NOT NULL,
  `bg_title` varchar(250) DEFAULT NULL,
  `bg_desc` varchar(250) NOT NULL,
  `page_id` varchar(100) NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`bg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE IF NOT EXISTS `contact` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_map` text NOT NULL,
  `contact_address` text NOT NULL,
  `contact_email` text NOT NULL,
  `contact_email_alt` text,
  `contact_phone` varchar(50) NOT NULL,
  `contact_number_title` varchar(300) NOT NULL,
  `background_image` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`contact_id`, `contact_map`, `contact_address`, `contact_email`, `contact_email_alt`, `contact_phone`, `contact_number_title`, `background_image`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3720.4076207142466!2d72.80569181493519!3d21.175960285918723!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be04ddff027cd05%3A0x2559fd2b2e6e96cb!2sCANOPUS%20GLOBAL%20EDUCATION!5e0!3m2!1sen!2sin!4v1599731082996!5m2!1sen!2sin', 'C-1 Purnima Society, Near Kotak House, Ghod Dod Road, Surat- 395001', 'info@canopusedu.com', 'support@canopusedu.com', '+91 90234 31612', 'We will get back to you within 24 hours, or call us everyday, 10:00 AM - 06:00 PM', '', 1, '::1', '2020-07-30 00:00:00', 1, '::1', '2020-09-29 04:50:04', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

DROP TABLE IF EXISTS `contact_us`;
CREATE TABLE IF NOT EXISTS `contact_us` (
  `contact_us_id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_name` varchar(250) NOT NULL,
  `contact_email` text NOT NULL,
  `contact_phone` varchar(50) NOT NULL,
  `contact_message` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`contact_us_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `footer_setting`
--

DROP TABLE IF EXISTS `footer_setting`;
CREATE TABLE IF NOT EXISTS `footer_setting` (
  `footer_id` int(11) NOT NULL AUTO_INCREMENT,
  `logo_image` text NOT NULL,
  `description_title` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `sun_time` varchar(250) NOT NULL,
  `mon_time` varchar(250) NOT NULL,
  `tue_time` varchar(250) NOT NULL,
  `wed_time` varchar(250) NOT NULL,
  `thu_time` varchar(250) NOT NULL,
  `fri_time` varchar(250) NOT NULL,
  `sat_time` varchar(250) NOT NULL,
  `footer_address` text NOT NULL,
  `header_address` varchar(250) NOT NULL,
  `footer_contact` varchar(100) NOT NULL,
  `footer_email` text NOT NULL,
  `footer_facebook` text,
  `footer_insta` text,
  `footer_twitter` text,
  `footer_google` text,
  `footer_linkedin` text,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`footer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footer_setting`
--

INSERT INTO `footer_setting` (`footer_id`, `logo_image`, `description_title`, `description`, `sun_time`, `mon_time`, `tue_time`, `wed_time`, `thu_time`, `fri_time`, `sat_time`, `footer_address`, `header_address`, `footer_contact`, `footer_email`, `footer_facebook`, `footer_insta`, `footer_twitter`, `footer_google`, `footer_linkedin`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'assets/images/logo-img.png', 'About Us', 'The Most Eminent Visas and Immigration Consultant service provider in major metros and overseas with reliability since 2010. We are committed to provide reliable client support.', '<em style=\"background-color: #0067ED;color: #FFF;padding: 4px 7px;\">Closed</em>', '10:00 AM - 6:00 PM', '10:00 AM - 6:00 PM', '10:00 AM - 6:00 PM', '10:00 AM - 6:00 PM', '10:00 AM - 6:00 PM', '10:00 AM - 6:00 PM', '<a href=\"https://goo.gl/maps/qeqfnUqMA5UE6diV7\" target=\"_blank\">C-1 Purnima Society, Near Kotak House, Ghod Dod Road, Surat- 395001</a>', '<a href=\"https://goo.gl/maps/qeqfnUqMA5UE6diV7\" target=\"_blank\">C-1 Purnima Society, Ghod Dod Road, Surat - 395001 (IND)</a>', '+91 90234 31612', 'Info@canopusedu.com', 'https://www.facebook.com/canopusglobaleducation/', 'https://www.instagram.com/canopus_global/', 'https://twitter.com/canopus_global/status/1248471383132139526', 'javascript:void(0);', 'https://in.linkedin.com/in/bhargav-gauswami-6916aa165', 1, '::1', '2020-07-31 00:00:00', 1, '::1', '2020-10-02 06:24:47', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
CREATE TABLE IF NOT EXISTS `newsletter` (
  `newsletter_id` int(11) NOT NULL AUTO_INCREMENT,
  `newsletter_email` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`newsletter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `newsletter_section`
--

DROP TABLE IF EXISTS `newsletter_section`;
CREATE TABLE IF NOT EXISTS `newsletter_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `newsletter_title` varchar(250) NOT NULL,
  `newsletter_desc` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `privacy_policy`
--

DROP TABLE IF EXISTS `privacy_policy`;
CREATE TABLE IF NOT EXISTS `privacy_policy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_title` text NOT NULL,
  `page_desc` text NOT NULL,
  `page_id` varchar(250) NOT NULL,
  `background_image` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `privacy_policy`
--

INSERT INTO `privacy_policy` (`id`, `page_title`, `page_desc`, `page_id`, `background_image`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'Privacy Policy', '<p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"font-family: Roboto;\">The Website and its Content is owned by (“Company”, “we”, or “us”). The term “you” refers to the user or viewer of </span><a href=\"http://canopusedu.com/\"><span style=\"font-family: Roboto;\">Canopus Global Education</span></a><span style=\"font-family: Roboto;\"> (“Website”).</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal;\"><span style=\"font-family: Roboto;\">Please read this Privacy Policy carefully. We reserve the right to change this Privacy Policy on the Website at any time without notice. Use of any information or contribution that you provide to us, or which is collected by us on or through our Website or its Content is governed by this Privacy Policy. By using our Website or its Content, you consent to this Privacy Policy, whether or not you have read it. If you do not agree with this Privacy Policy, please do not use our Website or its Content.</span></p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">SUBMISSION, STORAGE AND SHARING OF PERSONAL DATA</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"font-family: Roboto;\">To use our Website or its Content, we may seek personal data including your name, e-mail address, street address, city, state, billing information, or other personally identifying information (“Confidential Information”), or you may offer or provide a comment, photo, image, video or any other submission to us when visiting or interacting with our Website and its Content (“Other Information”). By providing such Confidential Information or Other Information to us, you grant us permission to use and store such information.</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"font-family: Roboto;\">Your Confidential Information is stored through by us internally or through a data management system. Your Confidential Information will only be accessed by those who help to obtain, manage or store that Information, or who have a legitimate need to know such Confidential Information.</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"font-family: Roboto;\">There may be an occasion where we may ask for demographic information such as gender or age, but if you choose not to provide such data and information, you may still use the Website and its Content, but you may not be able to use those services where demographic information may be required.</span></p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">CONFIDENTIALITY</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"font-family: Roboto;\">We aim to keep the Confidential Information that you share with us confidential. Please note that we may disclose such Confidential Information if required to do so by law or in the good-faith belief that: (1) such action is necessary to protect and defend our rights or property or those of our users or licensees, (2) to act as immediately necessary in order to protect the personal safety or rights of our users or the public, or (3) to investigate or respond to any real or perceived violation of this Privacy Policy or of our Disclaimer, Terms and Conditions, or any other terms of use or agreement with us.</span></p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">VIEWING BY OTHERS</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">Note that whenever you voluntarily make your Confidential Information or Other Information available for viewing by others online through this Website or its Content, it may be seen, collected and used by others, and therefore, we cannot be responsible for any unauthorized or improper use of the Confidential Information or Other Information that you voluntarily share.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">PASSWORDS</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">To use certain features of the Website or its Content, you may need a username and password. You are responsible for maintaining the confidentiality of the username and password, and you are responsible for all activities, whether by you or by others, that occur under your username or password and within your account. You agree to notify us immediately of any unauthorized or improper use of your username or password or any other breach of security. To help protect against unauthorized or improper use, make sure that you log out at the end of each session requiring your username and password.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">It is your responsibility to protect your own username and password from disclosure to others. We cannot and will not be liable for any loss or damage arising from your failure to protect your username, password or account information. If you share your username or password with others, they may be able to obtain access to your personal information at your own risk.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">By using our Website and its Content you agree to enter true and accurate information on the Website and its Content. If you enter a bogus email address we have the right to immediately inactivate your account.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We will use our best efforts to keep your username and password(s) private and will not otherwise share your password(s) without your consent, except as necessary when the law requires it or in the good faith belief that such action is necessary, particularly when disclosure is necessary to identify, contact or bring legal action against someone who may be causing injury to others or interfering with our rights or property.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">UNSUBSCRIBE</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">You may unsubscribe to our e-newsletters or updates at any time through the unsubscribe link at the footer of all e-mail communications.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We manage e-mail lists through a list management system. Unsubscribing from one list managed by us will not necessarily remove you from all publication email lists. If you have questions or are experiencing problems unsubscribing, please contact us at <font color=\"#ffffff\" face=\"Nunito\"><span style=\"background: rgb(255, 255, 255); transition-duration: 0.4s; transition-timing-function: ease-in-out; transition-property: all; font-size: 14px; letter-spacing: 0.5px; text-align: start;\">info@sweetnrush.com</span></font>.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">ANTI-SPAM POLICY</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We have a no spam policy and provide you with the ability to opt-out of our communications by selecting the unsubscribe link at the footer of all e-mails. We have taken the necessary steps to ensure that we are compliant with the CAN-SPAM Act of 2003 by never sending out misleading information. We will not sell, rent or share your email address.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">CHILDREN’S ONLINE PRIVACY PROTECTION ACT COMPLIANCE</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We do not collect any information from anyone under 13 years of age in compliance with COPPA (Children’s Online Privacy Protection Act), and our Website and its Content is directed to individuals who are at least 13 years old or older.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">ANONYMOUS DATA COLLECTION AND USE</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">To maintain our Website’s high quality, we may use your IP address to help diagnose problems with our server and to administer the Website by identifying which areas of the Website are most heavily used, and to display content according to your preferences. Your IP address is the number assigned to computers connected to the Internet. This is essentially “traffic data” which cannot personally identify you, but is helpful to us for marketing purposes and for improving our services. Traffic data collection does not follow a user’s activities on any other Websites in any way. Anonymous traffic data may also be shared with business partners and advertisers on an aggregate basis.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">USE OF “COOKIES”</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We may use the standard “cookies” feature of major web browsers. We do not set any personally identifiable information in cookies, nor do we employ any data-capture mechanisms on our Website other than cookies. You may choose to disable cookies through your own web browser’s settings. However, disabling this function may diminish your experience on the Website and some features may not work as intended. We have no access to or control over any information collected by other individuals, companies or entities whose website or materials may be linked to our Website or its Content.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">PRIVACY POLICIES OF OTHER WEBSITES</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We have no responsibility or liability for the content and activities of any other individual, company or entity whose website or materials may be linked to our Website or its Content, and thus we cannot be held liable for the privacy of the information on their website or that you voluntarily share with their website. Please review their privacy policies for guidelines as to how they respectively store, use and protect the privacy of your Confidential Information and Other Information.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">ASSIGNMENT OF RIGHTS</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">In the event of an assignment, sale, joint venture, or other transfer of some or all of our assets, you agree we can assign, sell, license or transfer any information that you have provided to us. Please note, however, that any purchasing party is prohibited from using the Confidential Information or Other Information submitted to us under this Privacy Policy in a manner that is materially inconsistent with this Privacy Policy without your prior consent.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">NOTIFICATION OF CHANGES</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We may use your contact information to inform you of changes to the Website or its Content, or, if requested, to send you additional information about us. We reserve the right, at our sole discretion, to change, modify or otherwise alter our Website, its Content and this Privacy Policy at any time. Such changes and/or modifications shall become effective immediately upon posting our updated Privacy Policy. Please review this Privacy Policy periodically. Continued use of any of information obtained through or on the Website or its Content following the posting of changes and/or modifications constitutes acceptance of the revised Privacy Policy.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">If you have any questions about this Privacy Policy, please contact us at <font color=\"#ffffff\" face=\"Nunito\"><span style=\"background: rgb(255, 255, 255); transition-duration: 0.4s; transition-timing-function: ease-in-out; transition-property: all; font-size: 14px; letter-spacing: 0.5px; text-align: start;\">info@sweetnrush.com</span></font>.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"font-family: Roboto;\">The Website and its Content is owned by (“Company”, “we”, or “us”). The term “you” refers to the user or viewer of </span><span style=\"font-family: Roboto;\"><a href=\"http://sweetnrush.weborative.com/\">C</a>anopus Global Education</span><span style=\"font-family: Roboto;\"> (“Website”).</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal;\"><span style=\"font-family: Roboto;\">Please read this Privacy Policy carefully. We reserve the right to change this Privacy Policy on the Website at any time without notice. Use of any information or contribution that you provide to us, or which is collected by us on or through our Website or its Content is governed by this Privacy Policy. By using our Website or its Content, you consent to this Privacy Policy, whether or not you have read it. If you do not agree with this Privacy Policy, please do not use our Website or its Content.</span></p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">SUBMISSION, STORAGE AND SHARING OF PERSONAL DATA</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"font-family: Roboto;\">To use our Website or its Content, we may seek personal data including your name, e-mail address, street address, city, state, billing information, or other personally-identifying information (“Confidential Information”), or you may offer or provide a comment, photo, image, video or any other submission to us when visiting or interacting with our Website and its Content (“Other Information”). By providing such Confidential Information or Other Information to us, you grant us permission to use and store such information.</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"font-family: Roboto;\">Your Confidential Information is stored through by us internally or through a data management system. Your Confidential Information will only be accessed by those who help to obtain, manage, or store that Information, or who have a legitimate need to know such Confidential Information.</span></p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"font-family: Roboto;\">There may be an occasion where we may ask for demographic information such as gender or age, but if you choose not to provide such data and information, you may still use the Website and its Content, but you may not be able to use those services where demographic information may be required.</span></p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">CONFIDENTIALITY</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\"><span style=\"font-family: Roboto;\">We aim to keep the Confidential Information that you share with us confidential. Please note that we may disclose such Confidential Information if required to do so by law or in the good-faith belief that: (1) such action is necessary to protect and defend our rights or property or those of our users or licensees, (2) to act as immediately necessary in order to protect the personal safety or rights of our users or the public, or (3) to investigate or respond to any real or perceived violation of this Privacy Policy or of our Disclaimer, Terms, and Conditions, or any other terms of use or agreement with us.</span></p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">VIEWING BY OTHERS</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">Note that whenever you voluntarily make your Confidential Information or Other Information available for viewing by others online through this Website or its Content, it may be seen, collected and used by others, and therefore, we cannot be responsible for any unauthorized or improper use of the Confidential Information or Other Information that you voluntarily share.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">PASSWORDS</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">To use certain features of the Website or its Content, you may need a username and password. You are responsible for maintaining the confidentiality of the username and password, and you are responsible for all activities, whether by you or by others, that occur under your username or password and within your account. You agree to notify us immediately of any unauthorized or improper use of your username or password or any other breach of security. To help protect against unauthorized or improper use, make sure that you log out at the end of each session requiring your username and password.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">It is your responsibility to protect your own username and password from disclosure to others. We cannot and will not be liable for any loss or damage arising from your failure to protect your username, password or account information. If you share your username or password with others, they may be able to obtain access to your personal information at your own risk.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">By using our Website and its Content you agree to enter true and accurate information on the Website and its Content. If you enter a bogus email address we have the right to immediately inactivate your account.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We will use our best efforts to keep your username and password(s) private and will not otherwise share your password(s) without your consent, except as necessary when the law requires it or in the good faith belief that such action is necessary, particularly when disclosure is necessary to identify, contact or bring legal action against someone who may be causing injury to others or interfering with our rights or property.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">UNSUBSCRIBE</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">You may unsubscribe to our e-newsletters or updates at any time through the unsubscribe link at the footer of all e-mail communications.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We manage e-mail lists through a list management system. Unsubscribing from one list managed by us will not necessarily remove you from all publication email lists. If you have questions or are experiencing problems unsubscribing, please contact us at <font color=\"#ffffff\" face=\"Nunito\"><span style=\"background: rgb(255, 255, 255); transition-duration: 0.4s; transition-timing-function: ease-in-out; transition-property: all; font-size: 14px; letter-spacing: 0.5px; text-align: start;\">info@sweetnrush.com</span></font>.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">ANTI-SPAM POLICY</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We have a no spam policy and provide you with the ability to opt-out of our communications by selecting the unsubscribe link at the footer of all e-mails. We have taken the necessary steps to ensure that we are compliant with the CAN-SPAM Act of 2003 by never sending out misleading information. We will not sell, rent, or share your email address.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">CHILDREN’S ONLINE PRIVACY PROTECTION ACT COMPLIANCE</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We do not collect any information from anyone under 13 years of age in compliance with COPPA (Children’s Online Privacy Protection Act), and our Website and its Content is directed to individuals who are at least 13 years old or older.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">ANONYMOUS DATA COLLECTION AND USE</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">To maintain our Website’s high quality, we may use your IP address to help diagnose problems with our server and to administer the Website by identifying which areas of the Website are most heavily used, and to display content according to your preferences. Your IP address is the number assigned to computers connected to the Internet. This is essentially “traffic data” which cannot personally identify you, but is helpful to us for marketing purposes and for improving our services. Traffic data collection does not follow a user’s activities on any other Websites in any way. Anonymous traffic data may also be shared with business partners and advertisers on an aggregate basis.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">USE OF “COOKIES”</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We may use the standard “cookies” feature of major web browsers. We do not set any personally identifiable information in cookies, nor do we employ any data-capture mechanisms on our Website other than cookies. You may choose to disable cookies through your own web browser’s settings. However, disabling this function may diminish your experience on the Website and some features may not work as intended. We have no access to or control over any information collected by other individuals, companies, or entities whose website or materials may be linked to our Website or its Content.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">PRIVACY POLICIES OF OTHER WEBSITES</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We have no responsibility or liability for the content and activities of any other individual, company or entity whose website or materials may be linked to our Website or its Content, and thus we cannot be held liable for the privacy of the information on their website or that you voluntarily share with their website. Please review their privacy policies for guidelines as to how they respectively store, use and protect the privacy of your Confidential Information and Other Information.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">ASSIGNMENT OF RIGHTS</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">In the event of an assignment, sale, joint venture, or other transfer of some or all of our assets, you agree we can assign, sell, license or transfer any information that you have provided to us. Please note, however, that any purchasing party is prohibited from using the Confidential Information or Other Information submitted to us under this Privacy Policy in a manner that is materially inconsistent with this Privacy Policy without your prior consent.</p><h3 style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 10px; border: 0px; outline: 0px; font-size: 22px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: 1em; text-transform: uppercase; font-family: Abel, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">NOTIFICATION OF CHANGES</h3><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px 0px 1em; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">We may use your contact information to inform you of changes to the Website or its Content, or, if requested, to send you additional information about us. We reserve the right, at our sole discretion, to change, modify or otherwise alter our Website, it\'s Content, and this Privacy Policy at any time. Such changes and/or modifications shall become effective immediately upon posting our updated Privacy Policy. Please review this Privacy Policy periodically. Continued use of any of the information obtained through or on the Website or its Content following the posting of changes and/or modifications constitutes acceptance of the revised Privacy Policy.</p><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 17px; text-size-adjust: 100%; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: Lato, Helvetica, Arial, Lucida, sans-serif; letter-spacing: normal; text-align: justify;\">If you have any questions about this Privacy Policy, please contact us at <font color=\"#ffffff\" face=\"Nunito\"><span style=\"background: rgb(255, 255, 255); transition-duration: 0.4s; transition-timing-function: ease-in-out; transition-property: all; font-size: 14px; letter-spacing: 0.5px; text-align: start;\">info@sweetnrush.com</span></font>.</p>', 'privacy', '', 1, '::1', '2020-08-06 00:00:00', 1, '::1', '2020-10-02 08:44:30', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `service_item`
--

DROP TABLE IF EXISTS `service_item`;
CREATE TABLE IF NOT EXISTS `service_item` (
  `service_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_service_id` int(11) NOT NULL,
  `service_item_title` varchar(250) NOT NULL,
  `service_item_desc` text NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`service_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_item`
--

INSERT INTO `service_item` (`service_item_id`, `ref_service_id`, `service_item_title`, `service_item_desc`, `is_active`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(16, 1, 'Career Counselling', 'It is always difficult for the students to select which career path they should follow. With the increasing number of career options today it has become far more complex a decision. It requires an individual’s interest, skill set, qualifications and personality to correspond with each other. Moreover, there are various emotional hazards such as doubt, confusion and apprehension. <br/><br/>Connect to our experts for best Career Counselling which enables you to make the right Career choices. Our Career Counselling Experts can help you make the perfect career plan depending on your area of interest. Our unbiased Career Advice service helps you bring clarity to your thought process and simplify the decision to take the right career decisions.', 1, 1, '::1', '2020-10-03 14:03:02', NULL, NULL, NULL, 'Live'),
(17, 1, 'Course Selection', ' <p class=\"tab-text\">\r\nWe provide special attention to student course selection procedure with highlighting their past academic records to avoid error free application. We assist students in choosing the right course as it can change their whole life! With this intention, we always suggest the best courses to all our students to study abroad. As our trained counselors have the latest information on job prospects and other required details, we help students to select the right career path for studying abroad.</p><br/>\r\n\r\n		<p class=\"tab-text\">We fulfill their requirement by providing them number of options available. We always believe to suggest students all the options so they can make a satisfactory choice of the subject they want to study. </p>', 1, 1, '::1', '2020-10-03 14:03:02', NULL, NULL, NULL, 'Live'),
(18, 1, 'Admission Guidence', '<p class=\"tab-text\">Once you have decided your career pathway and have selected the course you wish to study it is equally important to get admission to right institution. There are hundreds of institutions which offer similar course you wish to study…. How will you decide which institution is best for you.</p>\r\n\r\n<br/><p class=\"tab-text\">Our expert provides you admission guidance based on your profile, area of interest, academic scores, test scores, financial budgets and preferred location etc.</p><br/>\r\n\r\n\r\n<p class=\"tab-text\">They give you full information about the ranking and opportunities so that you make an informed decision while selecting the right university for you.</p>\r\n\r\n<p class=\"tab-text\">They are fully trained and equipped to help you with the admission requirements which include application process, making resume, statement of purpose, portfolio etc. We do regular follow-up with university so that you get the timely decision and have sufficient time to apply for your visa and make travel plan.</p>', 1, 1, '::1', '2020-10-03 14:03:02', NULL, NULL, NULL, 'Live'),
(19, 1, 'Exam & Coaching', ' <p class=\"tab-text\">\r\n                          Canopus Global Education has been widely acclaimed for its quality Test Coaching and teaching method. Be it be IELTS, TOEFL, SAT or GRE, Our Preparation courses are carefully structured and specifically designed to help you prepare for this internationally recognised examination. </p>\r\n                          \r\n <br/><p class=\"tab-text\">Or faculties are trained by British Council and ETS hence Preparing for exams significantly increases your chance of getting the score you need in your exam.</p>', 1, 1, '::1', '2020-10-03 14:03:02', NULL, NULL, NULL, 'Live'),
(20, 1, 'Scholarship Guidance', '\r\n                   <p>We understand the value of money spent and try our best to get students some form of financial aid in the form of tuition fee bursaries, scholarships, application fee waivers, etc.</p>\r\n                   \r\n <br/><p>You will be surprised to know that Scholarship opportunities are numerous.  Many schools or organisations that offer Scholarships &amp; Financial aid fee waivers are awarded to international students on the basis of academic merit and the need for it. Though these awards will be at the discretion of the Scholarship Committee of that institution. Candidate with strong academics, good performance on standardized exams &amp; extra-curricular achievements would be eligible for scholarship awards &amp; financial assistance.</p>\r\n \r\n <br/><p>But this information needs to be presented in manner that it gets highlighted and noticed.</p>\r\n \r\n <br/><p>Gateway Abroad have set up unique resources to give a step-by-step guidance for the entire process. Our systematized approach and a network with Universities help students to be successful in their scholarship applications.</p>\r\n \r\n<br/> <p>We also highlight those programs that have internship options, industrial placements, etc to help students get a foothold into the work place much before they set out to look for jobs during their study and post study periods.</p>\r\n                ', 1, 1, '::1', '2020-10-03 14:03:02', NULL, NULL, NULL, 'Live'),
(21, 1, 'Visa Assistance', '\r\n<p>As an International student it is very likely that you will require a valid visa before commencing study in most of the countries across the world.</p>\r\n\r\n<br/><p>With Gateway Abroad, applying for a student visa is never complicated. Our dedicated team is familiar with student visa applications and will offer assistance and advice for all of your enquiries.</p>\r\n\r\n<br/><p>Gateway Abroad has excellent relationships with Embassies and High Commissions worldwide and is able to offer you accurate study abroad student visa information and a seamless experience in applying for visas to study overseas.</p>\r\n\r\n<br/><p>This includes completing all the necessary documentation needed for the student visa, lodging the student visa application and coordinating with the respective embassy. We also prepare students for their visa interview through mock interview sessions. Our counsellors will also carefully review the documents and check if everything is in order before submitting them.</p>\r\n\r\n<br/><p>No doubt that’s why we have a success rate of closer to 100%</p>\r\n               ', 1, 1, '::1', '2020-10-03 14:03:02', NULL, NULL, NULL, 'Live'),
(22, 1, 'Education Loan', '\r\n                   <p>Most public sector banks and private banks provide education loans to students for pursuing higher professional or technical programs abroad. Sanctioned loan can be used to cover university fees, purchase of books, equipment, travel expenses and purchase of computers.</p>\r\n    <br/>               \r\n<p>We help you connect to the bank/financial institution who can help you get the education loan for your studies. Our experts can also guide you with the documentations and requirements for getting education loan.</p>\r\n              ', 1, 1, '::1', '2020-10-03 14:03:02', NULL, NULL, NULL, 'Live'),
(23, 1, 'Finance Assistance', 'We have tie-up with various companies and banks which are authorised by RBI to assist students to pay their fees and maintenance cost. We also have tieup with the travel agents to arrange your tickets and get you student deals their by helping you get all your needs done under a same roof.', 1, 1, '::1', '2020-10-03 14:03:02', NULL, NULL, NULL, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
CREATE TABLE IF NOT EXISTS `slider` (
  `slider_id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_title` varchar(250) NOT NULL,
  `slider_description` text,
  `slider_image` text NOT NULL,
  `slider_order_no` int(11) NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`slider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`slider_id`, `slider_title`, `slider_description`, `slider_image`, `slider_order_no`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `is_active`, `del_status`) VALUES
(1, 'Welcome To The Fastest Way', 'Get A <strong class=\"cmt-textcolor-skincolor\">Visa </strong> & <strong class=\"cmt-textcolor-skincolor\">Immigration</strong>', 'assets/images/slides/services-details_02.jpg', 1, 1, '::1', '2020-07-29 00:00:00', 1, '::1', '2020-10-02 10:14:50', 1, 'Live'),
(2, 'Welcome To The Fastest Way', 'Get A <strong class=\"cmt-textcolor-skincolor\">Visa </strong> & <strong class=\"cmt-textcolor-skincolor\">Immigration</strong>', 'assets/images/slides/slider-mainbg-002.jpg', 2, 1, '::1', '2020-07-29 00:00:00', 1, '::1', '2020-09-28 04:10:28', 1, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `smtp_setting`
--

DROP TABLE IF EXISTS `smtp_setting`;
CREATE TABLE IF NOT EXISTS `smtp_setting` (
  `smtp_id` int(11) NOT NULL AUTO_INCREMENT,
  `smtp_protocol` varchar(250) NOT NULL,
  `smtp_host` text NOT NULL,
  `smtp_port` text NOT NULL,
  `smtp_user` text NOT NULL,
  `smtp_pass` text NOT NULL,
  `smtp_crypto` text NOT NULL,
  `mailtype` text NOT NULL,
  `smtp_timeout` text NOT NULL,
  `charset` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`smtp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smtp_setting`
--

INSERT INTO `smtp_setting` (`smtp_id`, `smtp_protocol`, `smtp_host`, `smtp_port`, `smtp_user`, `smtp_pass`, `smtp_crypto`, `mailtype`, `smtp_timeout`, `charset`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'smtp', 'md-in-66.webhostbox.net', '465', 'inquiry@foodmohalla.in', 'inquiry@123#', 'ssl', 'html', '100', 'UTF-8', 1, '::1', '2020-07-31 00:00:00', 1, '::1', '2020-09-29 09:26:53', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_coaching_item_page`
--

DROP TABLE IF EXISTS `tbl_coaching_item_page`;
CREATE TABLE IF NOT EXISTS `tbl_coaching_item_page` (
  `coaching_item_page_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_coaching_page_id` int(11) NOT NULL,
  `coaching_item_page_title` varchar(300) NOT NULL,
  `coaching_item_page_desc` text NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`coaching_item_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_coaching_item_page`
--

INSERT INTO `tbl_coaching_item_page` (`coaching_item_page_id`, `ref_coaching_page_id`, `coaching_item_page_title`, `coaching_item_page_desc`, `is_active`, `del_status`) VALUES
(1, 1, 'Overview', '<p>IELTS is the International English Language Testing System which tests English proficiency across the globe. IELTS is jointly owned by British Council, IDP Australia and Cambridge English Language Assessment through more than 1000 locations across 140 countries.</p>\r\n\r\n<p>IELTS is the world&rsquo;s most popular test of English for higher education and global migration. This continues the important role which IELTS has long played in language testing for study and immigration in the UK and other countries, including Australia, Canada and New Zealand.</p>\r\n\r\n<p>IELTS tests the four language skills &ndash; listening, reading, writing and speaking. IELTS is a secure, valid and reliable test of real-life ability to communicate in English for education, immigration and professional accreditation.</p>\r\n\r\n<p>Candidates can sit an IELTS test in over 1,000 centres and locations around the world. This global test has the highest levels of quality control. Conducting 1.4 million tests globally.</p>\r\n\r\n<p>Candidate can choose from two types of IELTS test: Academic, General Training, or UKVI Approved depending on whether you want to study, work or migrate. Both modules are made up of four parts &ndash; Listening, Reading, Writing and Speaking. IELTS results are graded on the unique IELTS 9-band scale.<br />\r\n&nbsp;</p>\r\n\r\n<h3>IELTS has two formats</h3>\r\n\r\n<ul>\r\n	<li><strong>Academic IELTS &ndash;</strong>&nbsp;This test is for student who aspire for higher education in foreign universities and for professionals like medical professionals who either want to study or practice in an English speaking country</li>\r\n	<li><strong>General Training &ndash;</strong>&nbsp;This test is designed for those who aspire to undertake non- academic training / gain work experience or for immigration to English speaking countryThere are 4 parts of this examination which lasts for approximately 3 hours &ndash; Listening, Reading, Writing and Speaking. The scores are normally valid for 2 years and the results are accepted all over the world including France, Singapore, Australia, UK, Ireland, Canada, New Zealand, USA, Dubai and many institutions in Europe.</li>\r\n</ul>\r\n', 1, 'Live'),
(2, 1, 'Test Format', '<table class=\"table table-striped\">\r\n	<thead>\r\n		<tr>\r\n			<th>Section</th>\r\n			<th>Duration</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<th>Listening (There are 4 sections and 40 questions)</th>\r\n			<th>30 minutes</th>\r\n		</tr>\r\n		<tr>\r\n			<td>Reading (There are 3 sections and 40 questions)</td>\r\n			<td>60 minutes</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Writing (There are 2 tasks only)</td>\r\n			<td>60 minutes</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Speaking ( Direct interviews with task to talk on some topic)</td>\r\n			<td>11 to 14 minutes</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\nMost universities accept band scores from 6 to more. For professional courses like medical profession minimum band score of 7 is essential.', 1, 'Live'),
(3, 1, 'Fees', '<table class=\"table table-striped\">\r\n	<thead>\r\n		<tr>\r\n			<th>EXAM</th>\r\n			<th>FEES</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<th>IELTS EXAM</th>\r\n			<th>Rs 11,800</th>\r\n		</tr>\r\n		<tr>\r\n			<td>Re-marking Charges of Test</td>\r\n			<td>Rs. 8,850 ( For IDP)<br />\r\n			Rs. 6,500 (For BC)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Request for Transfer of Test Dates</td>\r\n			<td>Rs. 2,950</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Additional TRF postage charges per TRF</td>\r\n			<td>Rs. 250/- (including Service Tax) to send it via airmail. Rs. 1250/-(including Service Tax) to international destination via courier.</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 1, 'Live'),
(4, 1, 'FAQ', '<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">How can I register for the IELTS test? </strong><br/>\r\n            IELTS score is valid 2 years.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">How can I register for the IELTS test? </strong><br/>\r\n           A candidate may resit the IELTS test at any time.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">How can I register for the IELTS test? </strong><br/>\r\n           No. Payment of the test fee by cheque or cash is NOT acceptable.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">How can I register for the IELTS test? </strong><br/>\r\n            Only if the candidate withdraws the application before 5 weeks of the examination date. A particular amount is deducted and rest is transferred. If withdrawn later than 5 weeks nothing will be refunded. \r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">How can I register for the IELTS test? </strong><br/>\r\n           In the event a registered candidate fails to appear for their IELTS test, the candidate is deemed an absentee and a refund will not be applicable.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">How can I register for the IELTS test? </strong><br/>\r\n           Under normal circumstances, you will be treated as an absentee. However, there are some special circumstances under which your application may be considered with some conditions.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">How can I register for the IELTS test? </strong><br/>\r\n          This form MUST be completed by the Candidate and the FEE MUST be enclosed to enable processing. Centre may send up to five additional TRF’s to reorganizations. The centre may charge a postal fee for results sent internationally or by courier.\r\n        </span>\r\n    </li>\r\n    \r\n</ul>\r\n\r\n', 1, 'Live'),
(5, 2, 'Overview', '<p>The&nbsp;<strong>SAT</strong>&nbsp;is a standardized test extensively used for college admissions in the United States of America. It was introduced in 1926, since then its name and scoring pattern has changed quite a lot of times, being initially called the Scholastic Aptitude Test, then the name was changed to the Scholastic Assessment Test, and again it was renamed as the SAT Reasoning Test, and now just the SAT.The SAT is owned and governed by the College Board, a nonprofitable private organization in the United States of America. It is developed and administered by the Educational Testing Service on behalf of the College Board. The test is intended to determine a student&rsquo;s keenness for college.SAT is further categorized into two types. One is the general SAT which analyze the subjects&rsquo; knowledge that student has gained during the schooling. Another one is SAT Subject Test to figure out a student&rsquo;s expertise in an exceptional stream. This test is attempted by those students who want to pursue under-graduate course in Humanities, Biology, Physics, English Literature and Mathematics.The New pattern of SAT, which is due to be revised in 2016, takes 3 hours and 50 minutes to complete. The scores of the SAT range from 400 to 1600, amalgamating test results from two sections each of 800-point : Critical Reading, and Mathematics.</p>\r\n', 1, 'Live'),
(6, 2, 'Test Format', '<table class=\"table table-striped\">\r\n	<thead>\r\n		<tr>\r\n			<th colspan=\"2\">Exam Pattern</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Evidence-Based Reading and Writing</td>\r\n			<td>Total Marks - 800<br />\r\n			Reading Test - 52 standard multiple-choice questions<br />\r\n			Time Allotted - 65 minutes<br />\r\n			Writing &amp; Language Test - 44 standard<br />\r\n			multiple - choice Time Allotted - 35 minutes</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Mathematics</td>\r\n			<td>Total Marks - 800<br />\r\n			No Calculator - 25 questions<br />\r\n			Time Allotted - 25 minutes<br />\r\n			Calculator - 38 questions<br />\r\n			Time Allotted - 55 minutes</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Essay (Optional and Separate)</td>\r\n			<td>Score - 2 to 8 scale<br />\r\n			1 evidence based essay<br />\r\n			Time Allotted - 50 minutes</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Total Time</td>\r\n			<td>SAT without Essay - 3 hours<br />\r\n			SAT with Essay - 3 hours and 50 minutes.</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Nature of Exam</td>\r\n			<td>Paper Based Test (PBT)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Total Scores</td>\r\n			<td>SAT score is out of 1600</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 1, 'Live'),
(7, 2, 'Fees', '<table class=\"table table-striped\">\r\n	<thead>\r\n		<tr>\r\n			<th>EXAM</th>\r\n			<th>FEES</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<th>SAT</th>\r\n			<th>$ 106</th>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 1, 'Live'),
(8, 2, 'How We Help?', '<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n         Latest and sufficient study material with additional classwork assignment sheets.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           Experienced Faculty\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n        Test series on each topic of English and Math.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n        5 Paper based mock tests\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n          Added advantage for slightly weak students - They get to re-attend the next SAT batch for selected topics, free of charge.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           Applications and visa assistance for admissions to Undergraduate Schools in the US.\r\n        </span>\r\n    </li>\r\n</ul>', 1, 'Live'),
(9, 3, 'Overview', '<p>The Duolingo English Test (DET) is a modernized language assessment test which has come to the fore due to the current scenario. As with other language assessment tests like IELTS, TOEFL and PTE, the DET tests all the four core language skills of the candidates: Listening, Reading, Writing, and Speaking. However, the DET has its own revolutionary ways of testing. It is a computer-adaptive test: if you answer a question correctly, the next one gets harder. If you answer it wrong, the next one gets easier.</p>\r\n\r\n<p>There are several clear benefits that stand in favour of the DET if compared with other tests.</p>\r\n\r\n<ol>\r\n	<li>It can be taken from home, anytime as per the comfort of the candidate.</li>\r\n	<li>It costs merely $49; whereas, other tests in this category cost more than $200.</li>\r\n	<li>It can be completed within a short duration of 1 hour as compared to other tests which consume 3 hours on average.</li>\r\n	<li>Results can be obtained within a very short span of 48 hours after completing the test.</li>\r\n</ol>\r\n\r\n<p>Candidates can appear for the test as many times as they want, but not more than twice in a 30-day window.</p>\r\n', 1, 'Live'),
(10, 3, 'Test Structure', '<h3>Exam Pattern</h3>\r\n\r\n<p>As the DET is computer-adaptive, it does not follow a clear structure. There are 3 main sections involved.</p>\r\n&nbsp;\r\n\r\n<h5>Section-1: Quick Setup (5 minutes)</h5>\r\n\r\n<p>Here, the candidates are required to check their computer and other equipment (like microphone, speakers, and webcam). After this, they need to ensure that they are in a well-lit and quiet room. They must take a photograph of themselves and of their government-issued ID card in order to start the test.</p>\r\n\r\n<h5>Section-2: The Test (30-45 minutes)</h5>\r\n\r\n<p>This is the time duration when the candidate is assessed on his/her language proficiency. As the test is computer-adaptive, if the candidates show a higher level of English, their test can end in about 30 minutes; otherwise, this section will take about 45 minutes. The questions are not arranged in any prescribed format/order and simply depend upon what the candidate has chosen as answer to the previous question. There are 12 question types involved in this rigorous testing environment which assess a candidate on his/her Listening, Reading, Writing, and Speaking skills.</p>\r\n\r\n<h5>Section-3: Video Interview (10 minutes)</h5>\r\n\r\n<p>This is an unscored section of the test. The replies in this section are sent to the institutes along with the candidate&rsquo;s proficiency score. This section is divided in two parts.</p>\r\n\r\n<h5>Part-1: Spoken Response (1-3 minutes)</h5>\r\n\r\n<p>The candidates are given a choice to speak about 1 of the 2 topics. They must make their choice in 20 seconds and then speak about the topic for a minimum of 1 minute and a maximum of 3 minutes.</p>\r\n\r\n<h5>Part-2: Written Response (3-5 minutes)</h5>\r\n\r\n<p>The candidates are given a choice to write about 1 of the 2 topics. They must make their choice in 20 seconds and then write about the topic for a minimum duration of 3 minutes and a maximum duration of 5 minutes.</p>\r\n', 1, 'Live'),
(11, 3, 'How we Help', '&lt;ul class=&quot;cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5&quot;&gt;<br />\r\n&nbsp; &nbsp; &lt;li&gt;&lt;i class=&quot;cmt-textcolor-skincolor fa fa-check-circle&quot;&gt;&lt;/i&gt;<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &lt;span class=&quot;cmt-list-li-content&quot;&gt;<br />\r\nMore than 15 hours of comprehensive program to master every question-type involved in the DET.<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &lt;/span&gt;<br />\r\n&nbsp; &nbsp; &lt;/li&gt;<br />\r\n&nbsp; &nbsp; &lt;li&gt;&lt;i class=&quot;cmt-textcolor-skincolor fa fa-check-circle&quot;&gt;&lt;/i&gt;<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &lt;span class=&quot;cmt-list-li-content&quot;&gt;<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Experienced Faculty<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &lt;/span&gt;<br />\r\n&nbsp; &nbsp; &lt;/li&gt;<br />\r\n&nbsp; &nbsp; &lt;li&gt;&lt;i class=&quot;cmt-textcolor-skincolor fa fa-check-circle&quot;&gt;&lt;/i&gt;<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &lt;span class=&quot;cmt-list-li-content&quot;&gt;<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; Up to date study material to excel in the test.<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &lt;/span&gt;<br />\r\n&nbsp; &nbsp; &lt;/li&gt;<br />\r\n&nbsp; &nbsp; &lt;li&gt;&lt;i class=&quot;cmt-textcolor-skincolor fa fa-check-circle&quot;&gt;&lt;/i&gt;<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &lt;span class=&quot;cmt-list-li-content&quot;&gt;<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; Test taking tips and strategies to get your dream score on the DET.<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &lt;/span&gt;<br />\r\n&nbsp; &nbsp; &lt;/li&gt;<br />\r\n&nbsp; &nbsp; &lt;li&gt;&lt;i class=&quot;cmt-textcolor-skincolor fa fa-check-circle&quot;&gt;&lt;/i&gt;<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &lt;span class=&quot;cmt-list-li-content&quot;&gt;<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Targeted practice according to the weak areas of students.<br />\r\n&nbsp; &nbsp; &nbsp; &nbsp; &lt;/span&gt;<br />\r\n&nbsp; &nbsp; &lt;/li&gt;<br />\r\n&lt;/ul&gt;', 1, 'Live'),
(12, 3, 'FAQ', '<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n <li>\r\n <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n <span class=\"cmt-list-li-content\">\r\n <strong class=\"mr-2 cmt-textcolor-skincolor\">How can I register for the IELTS test? </strong><br/>\r\n IELTS score is valid 2 years.\r\n </span>\r\n </li>\r\n <li>\r\n <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n <span class=\"cmt-list-li-content\">\r\n <strong class=\"mr-2 cmt-textcolor-skincolor\">How can I register for the IELTS test? </strong><br/>\r\n A candidate may resit the IELTS test at any time.\r\n </span>\r\n </li>\r\n <li>\r\n <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n <span class=\"cmt-list-li-content\">\r\n <strong class=\"mr-2 cmt-textcolor-skincolor\">How can I register for the IELTS test? </strong><br/>\r\n No. Payment of the test fee by cheque or cash is NOT acceptable.\r\n </span>\r\n </li>\r\n <li>\r\n <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n <span class=\"cmt-list-li-content\">\r\n <strong class=\"mr-2 cmt-textcolor-skincolor\">How can I register for the IELTS test? </strong><br/>\r\n Only if the candidate withdraws the application before 5 weeks of the examination date. A particular amount is deducted and rest is transferred. If withdrawn later than 5 weeks nothing will be refunded. \r\n </span>\r\n </li>\r\n <li>\r\n <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n <span class=\"cmt-list-li-content\">\r\n <strong class=\"mr-2 cmt-textcolor-skincolor\">How can I register for the IELTS test? </strong><br/>\r\n In the event a registered candidate fails to appear for their IELTS test, the candidate is deemed an absentee and a refund will not be applicable.\r\n </span>\r\n </li>\r\n <li>\r\n <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n <span class=\"cmt-list-li-content\">\r\n <strong class=\"mr-2 cmt-textcolor-skincolor\">How can I register for the IELTS test? </strong><br/>\r\n Under normal circumstances, you will be treated as an absentee. However, there are some special circumstances under which your application may be considered with some conditions.\r\n </span>\r\n </li>\r\n <li>\r\n <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n <span class=\"cmt-list-li-content\">\r\n <strong class=\"mr-2 cmt-textcolor-skincolor\">How can I register for the IELTS test? </strong><br/>\r\n This form MUST be completed by the Candidate and the FEE MUST be enclosed to enable processing. Centre may send up to five additional TRF’s to reorganizations. The centre may charge a postal fee for results sent internationally or by courier.\r\n </span>\r\n </li>\r\n \r\n</ul>\r\n\r\n', 1, 'Deleted');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_coaching_page`
--

DROP TABLE IF EXISTS `tbl_coaching_page`;
CREATE TABLE IF NOT EXISTS `tbl_coaching_page` (
  `coaching_page_id` int(11) NOT NULL AUTO_INCREMENT,
  `coaching_page_name` varchar(250) NOT NULL,
  `coaching_page_short_name` varchar(250) DEFAULT NULL,
  `coaching_page_desc` varchar(300) NOT NULL,
  `coaching_page_image` text NOT NULL,
  `icon` varchar(250) NOT NULL,
  `background_image` text NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_show_home_page` int(11) NOT NULL DEFAULT '0',
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`coaching_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_coaching_page`
--

INSERT INTO `tbl_coaching_page` (`coaching_page_id`, `coaching_page_name`, `coaching_page_short_name`, `coaching_page_desc`, `coaching_page_image`, `icon`, `background_image`, `is_active`, `is_show_home_page`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'IELTS', NULL, 'Scoring method the bond of 0-9.', 'assets/images/services/services-details_0411.jpg', 'flaticon-book-1', 'assets/images/pagetitle-bg.jpg', 1, 1, 1, '::1', '2020-10-03 12:40:26', 1, '::1', '2020-10-03 12:48:19', 'Live'),
(2, 'SAT', NULL, 'The test score is an important parameter', 'assets/images/services/services-details_06111.jpg', 'flaticon-contract', 'assets/images/pagetitle-bg.jpg', 1, 1, 1, '::1', '2020-10-03 12:42:50', 1, '::1', '2020-10-03 12:48:39', 'Live'),
(3, 'Duolingo ', NULL, '30+ languages online with bite-size lessons based on science.', 'assets/images/services/services-details_0611.jpg', 'flaticon-policy', 'assets/images/pagetitle-bg.jpg', 1, 1, 1, '::1', '2020-10-03 12:45:13', NULL, NULL, NULL, 'Live'),
(4, 'French', NULL, 'With our online French lessons, you will be able to learn to speak French', 'assets/images/services/services-details_021.jpg', 'flaticon-certificate', 'assets/images/pagetitle-bg.jpg', 2, 0, 1, '::1', '2020-10-03 12:46:43', 1, '::1', '2020-10-03 13:34:59', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_coaching_setting`
--

DROP TABLE IF EXISTS `tbl_coaching_setting`;
CREATE TABLE IF NOT EXISTS `tbl_coaching_setting` (
  `coaching_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `coaching_setting_title` varchar(250) NOT NULL,
  `coaching_setting_desc` varchar(300) NOT NULL,
  `coaching_setting_image` text NOT NULL,
  `setting_image_title` varchar(250) NOT NULL,
  `setting_image_desc` varchar(300) NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`coaching_setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_coaching_setting`
--

INSERT INTO `tbl_coaching_setting` (`coaching_setting_id`, `coaching_setting_title`, `coaching_setting_desc`, `coaching_setting_image`, `setting_image_title`, `setting_image_desc`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'value for visa coaching', 'benifits of visa online <strong>coaching & preparation</strong>', 'assets/images/single-img-04.jpg', 'Coaching Test <span class=\"cmt-textcolor-skincolor\"> Preparation</span> Series', 'Free Guid To A Top Line Band Score!', NULL, NULL, NULL, 1, '::1', '2020-10-02 08:36:21', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_continent`
--

DROP TABLE IF EXISTS `tbl_continent`;
CREATE TABLE IF NOT EXISTS `tbl_continent` (
  `continent_id` int(11) NOT NULL AUTO_INCREMENT,
  `continent_code` varchar(250) NOT NULL,
  `continent_name` varchar(250) NOT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`continent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_continent`
--

INSERT INTO `tbl_continent` (`continent_id`, `continent_code`, `continent_name`, `del_status`) VALUES
(1, 'AF', 'AFRICA', 'Live'),
(2, 'AS', 'ASIA', 'Live'),
(3, 'EU', 'EUROPE', 'Live'),
(4, 'NA', 'North America', 'Live'),
(5, 'SA', 'South America', 'Live'),
(6, 'OC', 'Oceania', 'Live'),
(7, 'AN', 'Antarctica', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_country_item_page`
--

DROP TABLE IF EXISTS `tbl_country_item_page`;
CREATE TABLE IF NOT EXISTS `tbl_country_item_page` (
  `country_item_page_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_country_page_id` int(11) NOT NULL,
  `country_item_page_title` varchar(300) NOT NULL,
  `country_item_page_desc` text NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`country_item_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_country_item_page`
--

INSERT INTO `tbl_country_item_page` (`country_item_page_id`, `ref_country_page_id`, `country_item_page_title`, `country_item_page_desc`, `is_active`, `del_status`) VALUES
(1, 1, 'About Canada', 'Canada is the second-largest country in the world, and shares the world&#39;s largest border with its neighbor, the United States. Canada&#39;s population is just over 35 million people - a small population compared to the size of the country. It is known for diversity, inclusive values and high standard of living, is internationally recognized as one of the best countries to live and study in. Studying in Canada is an exciting and rewarding experience for many students from around the world and annually near about 180,000 students join Canadian education system and it is increasingly every year. Canadian College or University should be a top priority for those who may want to pursue a Quality education in their respective areas of interest and professions as it has been incubators for innovation such as the BlackBerry phone, flat-screen technology, SMART boards, and IMAX film.', 1, 'Live'),
(2, 1, 'Reason for Study in Canada', '<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n                                        <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i><span class=\"cmt-list-li-content\"><strong class=\"mr-2 cmt-textcolor-skincolor\">World Recognized:</strong>Canadian education system has a good quality control with high academic standards that give you opportunity to take your career to another height. A Canadian degree, diploma or certificate is globally recognized as being equivalent to those obtained from the United States or Commonwealth countries.</span></li>\r\n                                        <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n                                            <span class=\"cmt-list-li-content\">\r\n                                                <strong class=\"mr-2 cmt-textcolor-skincolor\">Affordable Education:</strong>\r\n                                                The quality of education and living standards in Canada are among the highest in the world, but the cost of living and tuition fees for international students are generally lower than other countries’, that is the reason Canada is most preferred choice for students.\r\n                                            </span>\r\n                                        </li>\r\n                                        <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n                                            <span class=\"cmt-list-li-content\">\r\n                                                <strong class=\"mr-2 cmt-textcolor-skincolor\">Healthy and Safe:</strong>\r\n                                                Canada is the healthiest country to stay &amp; live for international student. It is one of the safest places to study with very low crime rates and cases of racial discrimination. Canadian people are very helpful and friendly in nature and they help international student to live in a healthy environment.\r\n                                            </span>\r\n                                        </li>\r\n                                        <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n                                            <span class=\"cmt-list-li-content\">\r\n                                                <strong class=\"mr-2 cmt-textcolor-skincolor\">Co-op Program:</strong>\r\n                                                Ample of universities in Canada provides co-op program in various field, which allow students to paid work experience with your academic studies and earn your degree from a global leader in co-op. These programs help students apply their classroom education to real world experiences and full-time market driven programs.\r\n                                            </span>\r\n                                        </li>\r\n                                        <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n                                            <span class=\"cmt-list-li-content\">\r\n                                                <strong class=\"mr-2 cmt-textcolor-skincolor\">Work Opportunities:</strong>\r\n                                                Canada has a thriving and vibrant economy with plenty of room for growth and many opportunities. International students in Canada can work up to 40 hours per fortnight during the stay, allowing them to earn money, gain useful work experience in their field of interest and opportunity to meet with variety of people.\r\n                                            </span>\r\n                                        </li>\r\n                                        <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n                                            <span class=\"cmt-list-li-content\">\r\n                                                <strong class=\"mr-2 cmt-textcolor-skincolor\">Possibility of Immigration:</strong>\r\n                                                International graduates of Canadian institutions may obtain a Post-Graduate Work Permit (PGWP) after their studies, allowing them to work anywhere in Canada, for any employer, for up to three years. Work experience gained on a PGWP can contribute towards an international graduate’s eligibility for permanent immigration programs, and can provide a way to remain in Canada and work while an application for permanent residence is in process.\r\n                                            </span>\r\n                                        </li>\r\n                                        <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n                                            <span class=\"cmt-list-li-content\">\r\n                                                <strong class=\"mr-2 cmt-textcolor-skincolor\">Multicultural Society:</strong>\r\n                                                Canada has traditionally been a country of immigrants and has a policy of encouraging multicultural diversity with people of all religions, races and cultures. A multitude of languages are used by Canadians, with English and French. This vibrant environment gives students different perspectives and an opportunity to interact with people of all backgrounds.\r\n                                            </span>\r\n                                        </li>\r\n                                    </ul>', 1, 'Live'),
(3, 1, 'Education System', 'Canada is not only hunting for a greater number of international students but also for extraordinary and talented young people, potential for future experts. Canada does not have a national education system but it does vary in all provinces and territories. Public education is under the jurisdiction of all provinces and territories. This means there are some differences and similarities in the systems across the country. There are more than 10,000 undergraduate and graduate degree programs available in Canadian universities, as well as professional degree programs and certificates. Universities and university colleges focus on degree programs but may also offer some diplomas and certificates, often in professional designations. University degrees are offered at all three levels, bachelor, master and doctoral with highly innovative research in the fields of health, nanotechnology, biotechnology, high-performance computing, environmental technologies, nutraceuticals, and renewable fuels.', 1, 'Live'),
(4, 1, 'Study Option In Canada', 'Canadian universities, colleges, and institutes offers all type of formal teaching program such as vocational, technical, or the professional education. Canada has a range of education institutions i.e. Universities, Colleges, Community colleges, Career colleges or technical institutes. Canadian college programs offer diverse options in the technical and professional fields including: business, agriculture and agri-food, health, social services, broadcasting and journalism, hospitality management, design, technology, sciences, information technology, engineering, environment, languages, and arts.\r\n<table class=\"table table-striped\">\r\n	<thead>\r\n		<tr>\r\n			<th>Higer Education</th>\r\n			<th>Language Study</th>\r\n			<th>Others</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Certificate level (1 year)</td>\r\n			<td>English Language</td>\r\n			<td>Vocational Study</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Diploma level (1-2 years)</td>\r\n			<td>French Language</td>\r\n			<td>Co-op Programs</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Advanced Diploma (2-3 years)</td>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Bachelor degrees (4 years)</td>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Post-graduate Diplomas/Certificates (1-2 years)</td>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Master&rsquo;s degrees (1-2 years)</td>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Doctorate or PhD (4-7 years)</td>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 1, 'Live'),
(5, 1, 'Living In Canada', 'Living in Canada, varies depending on the region and the type of stay there is wide variety of safe and modern accommodations offered to students, you can choose according to your needs and budget.\r\n<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n                                        <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n                                            <span class=\"cmt-list-li-content\">\r\n                                                <strong class=\"mr-2 cmt-textcolor-skincolor\">On Campus: </strong>\r\n                                                living in university campus will be a great option as you can make friends and be a part of student community and it will be once-in-a-lifetime experience!, make you comfortable in new environment.\r\n                                            </span>\r\n                                        </li>\r\n                                        <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n                                            <span class=\"cmt-list-li-content\">\r\n                                                <strong class=\"mr-2 cmt-textcolor-skincolor\">Off Campus: </strong>\r\n                                                Many Students choose off campus option (Private, ranted accommodation) as they want to explore their life beyond the campus. If you are renting a property, then you have to follow some legal formalities.\r\n                                            </span>\r\n                                        </li>\r\n                                        <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n                                            <span class=\"cmt-list-li-content\">\r\n                                                <strong class=\"mr-2 cmt-textcolor-skincolor\">Home stay: </strong>\r\n                                                Canadian Families offering home-stay accommodation to international students are ensure they can provide a suitable living environment for students.\r\n                                            </span>\r\n                                        </li>\r\n                                    </ul>\r\n <h5 class=\"mt-10 mb-2 cmt-textcolor-skincolor\">Estimated living costs</h5>\r\n                                    <p>\r\n                                        The cost of living can vary greatly depending on your lifestyle, budget and spending habits. You can consider a part-time job while studying in the Canada to help pay off expenses or to cover your basic needs.\r\n                                    </p>\r\n                                    <table class=\"table table-striped\">\r\n                                        <thead>\r\n                                            <tr>\r\n                                                <th colspan=\"2\" class=\"text-center\">Accommodation (Monthly, in Canadian Dollar)</th>\r\n                                            </tr>\r\n                                        </thead>\r\n                                        <tbody>\r\n                                            <tr>\r\n                                                <td>Accommodation (Single, off-campus, not shared)</td>\r\n                                                <td>$800 - $1,050</td>\r\n                                            </tr>\r\n\r\n                                            <tr>\r\n                                                <th colspan=\"2\" class=\"text-center\">Other living expenses (Monthly, in Canadian Dollar)</th>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td>Groceries and eating out</td>\r\n                                                <td>$240 to $290</td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td>Clothing</td>\r\n                                                <td>$40 to $90</td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td>Public transit</td>\r\n                                                <td>$80 to $120</td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td>Miscellaneous*</td>\r\n                                                <td>$140 to $190</td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <th colspan=\"2\" class=\"cmt-textcolor-skincolor\">Estimated Annual Cost Of living, in Canadian Dollar - $15,000 to $18,000</th>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>', 1, 'Live'),
(6, 1, 'Visa In Canada', 'Canada is highly preferable choice in study abroad destination. Students are expected to start planning for their studies in Canada one year in advance. Canada have generally two intakes, first major intake is in September and the other intake is in January, but it depends on the students program and the institution. Very few institutes might have the May intake as well. An international application for study in Canada can be a slow process, and can take up to 6-8 weeks processing time. You need to submit application as soon as possible. If you want to pursue your study in Canada then you need to apply for Canadian student visa, or study permit and you will require offer of acceptance from your chosen Canadian university. In order to kick the process you have to meet with some requirements such as application fee, language proficiency proof (IELTS), medical certificates, photographs, valid passport, proof of fund in the form of bank statements, receipts or certificates that you have enough money to pay for your tuition and Living expenses.', 1, 'Live'),
(7, 1, 'Before You Leave', 'Before departing for studies in Canada there are many things that need to be done.\r\n<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Ensure that you have accepted the offer of a place and have responded.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Make sure that you have a valid passport.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Fill in immigration or study documents that will be required.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Set up a Canadian bank account.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Buy travel and health insurance.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Prescriptions to support your use of any required medications.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Check that every piece of paperwork has been completed.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Make sure you have Photocopy of all important documents.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Make sure that your appliances will work there – if not get an adapter.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Contact your Canadian educational institution with any questions you may have.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Check baggage allowance with your airline.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Your luggage should be clearly labeled with your name, contact address and the address of your institution.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Climate will be different from your home country. So pack you baggage according to that.\r\n        </span>\r\n    </li>\r\n</ul>\r\n', 1, 'Live'),
(8, 2, 'About USA', 'The USA is a vast country, and every state is unique in its culture, climate, history, economy and more. It is one of the best destinations in the whole world, for higher education in the fields of science, technology and management. No other country extends its arms as widely as the US, to welcome international students. It has been third largest country in size and population apart from that it is one of the strongest economies of the world. It continues to host an increasing number of students and scholars from around the world. U.S. offers various options to choose from according to your academic and cultural preferences, it is important to introspect and make an informed decision before taking the plunge. Studying abroad doesn&rsquo;t just give you tangible degrees and certificates; it is an experience that helps you evolve once you set out of your comfort zone. Study in USA not only broadens student&rsquo;s educational experience, but Cultural opportunities as well.', 1, 'Live'),
(9, 2, 'Reason for Study in USA', '<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Quality Assurance:&nbsp;</strong>In the United States the review of educational programmes to ensure that acceptable standards of education, scholarship and infrastructure are being maintained is independent of government and performed by private membership associations. The US Department of Education and the Council for Higher Education Accreditation (CHEA) (a non-governmental organization) both recognize reputable accrediting bodies for institutions of higher education.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Academic Excellence:</strong> TUS education is well recognized and continues to maintain a strong presence among top-ranked universities in the world., it has more institutions of higher education than any other country, but despite the numbers, it is the quality of education provided by universities, colleges, and institution that is acknowledged worldwide for their excellence. The quality of the wide range of courses and program ensures that the institutions have high standards..\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Multifarious courses:</strong> An ideal environment for students In American Institutions which is characterized by amenable methods of education and continuous development process for students in the various fields of studies according to their needs and interests ranging from an undergraduate, graduate, masters program.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Research and Training:</strong> Institution ensures advancement of facilities, new methods and skills in research and training for international students at the graduate level. Large research institutions in the US, offer opportunities for students to work with their professors which helps in the broadening of their knowledge and skill set. Student may be fortunate enough to meet and study, with the leading scholars.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Career Opportunities:</strong> An international degree attained by students from USA becomes a key for wide range of career prospects as you have the chance to look for work in fields that are always seeking ambitious and hard-working students. International students in the US can work in a job on campus up to 20 hours a week along with numerous extra-curricular activities that provide real-world job experience.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Cost of Living:</strong> Being the strongest economies of the world and host for international student, undoubtedly the standard of living and its cost is little high. Higher education in the U.S. is expensive. But there are ample of financial assistance, and the vast majority of universities which supports international students through teaching and research assistantships.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Exciting Campus Life:</strong> Home to millions of international students in place to make you feel as welcome as possible. Students enjoy campus life with so many cultural and non cultural activities, which give them exciting environment and countless experience.\r\n        </span>\r\n    </li>\r\n     <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Multicultural Society & Diversity:</strong> U.S. has evolved into a vibrant blend of culture and adobe to people of every country. The well mixed society will allow you to integrate into American society, is one way to nurture your tolerance and openness to other cultures; studying in the U.S. adds another dimension, exposing you not only to culture, but to the languages and beliefs of people from nationalities all over the world.\r\n        </span>\r\n    </li>\r\n</ul>\r\n\r\n', 1, 'Live'),
(10, 2, 'Education System', 'Compared to most other higher education systems around the world, the U.S. system is largely independent from federal government regulation and is highly decentralized. U.S. Education system consists of primary school, middle school, secondary school, and then postsecondary education. Types of higher institutions are Public Universities, Private Universities, Technical Institutes, The Ivy League, Liberal Arts Colleges and Community Colleges. Higher education includes non-degree programs that lead to certificates and diplomas plus six degree levels: associate, bachelor, first professional, master, advanced intermediate, and research doctorate. It is renowned for world\'s most flexible education system; students can easily study in specialization in particular chosen subject. U.S.A. is not leading with centralized education system; all institutes can determine its program and admission standards. The U.S. system does not offer a second or higher doctorate, but does offer postdoctoral research programs.', 1, 'Live'),
(11, 2, 'Study Option In USA', '    Education system of USA provides all levels of degrees with different fields of study in more than 3500 accredited universities, offering a broad range of programs including biotechnology, advertising, designing, engineering, business, Job-oriented management programe, research-centric science and technology courses which provides students outstanding teaching, practical knowledge & flexibility. Students have multiple options to pursue higher studies from public or private universities, Technical Institutes, The Ivy League, Liberal Arts Colleges and Community Colleges.\r\n<table class=\"table table-striped\">\r\n	<thead>\r\n		<tr>\r\n			<th>Higer Education</th>\r\n			<th>Language Study</th>\r\n			<th>Vocational Studay</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Associate degree (2 Years)</td>\r\n			<td>General English</td>\r\n			<td>Colleges of Further Education</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Bachelor Degree (4 Years)</td>\r\n			<td>English for Academic Purposes</td>\r\n			<td></td>\r\n		</tr>\r\n		<tr>\r\n			<td>Graduate Certificate (6 Months)</td>\r\n			<td>English for Specific Purposes</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Graduate Diploma (1 Year)</td>\r\n			<td>English for Teaching</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Doctoral Degree (3-5 years)</td>\r\n			<td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		\r\n	</tbody>\r\n</table>\r\n', 1, 'Live'),
(12, 2, 'Living In USA', 'There is wide variety of safe and modern accommodations offered to students in USA , you can choose accommodation that suits your needs and budget.\r\n<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">On Campus: </strong>\r\n            living in on campus in Hall of Residence will be a great option. It will minimize your Travel & expenses as on-campus students as the university buildings are within reach. Living in campus give you option to mingle with student from different background and origin\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Off Campus: </strong>\r\n            Off-campus accommodation is one of the best options for international students as it gives them a chance to explore the American lifestyle closely.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Home stay: </strong>\r\n            It is a great option for undergraduate students as they get to live with an American family where they can have their own room as well as meals which make things easier in adapting new lifestyle.\r\n        </span>\r\n    </li>\r\n</ul>\r\n<h5 class=\"mt-10 mb-2 cmt-textcolor-skincolor\">Estimated living costs</h5>\r\n<p>\r\n    Cost of living expenses for international students in the USA may vary according to city and region and also the lifestyle, budget and spending habit of a student. You can consider a part-time job while studying in to help pay off any expenses or to cover your basic needs.\r\n</p>\r\n<table class=\"table table-striped\">\r\n    <thead>\r\n        <tr>\r\n            <th colspan=\"2\" class=\"text-center\">\r\nAccommodation (You will choose one of them) ( in US$)</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n        <tr>\r\n            <td>Off Campus (Rental) ,On campus or Home-stay</td>\r\n            <td>$450 to $1000 per month</td>\r\n        </tr>\r\n\r\n        <tr>\r\n            <th colspan=\"2\" class=\"text-center\">Other living expenses (in US$)</th>\r\n        </tr>\r\n        <tr>\r\n            <td>Groceries and eating out</td>\r\n            <td>$200 to $500 per month</td>\r\n        </tr>\r\n        <tr>\r\n            <td>Gas, electricity</td>\r\n            <td>$150 to $200 per month</td>\r\n        </tr>\r\n        <tr>\r\n            <td>Phone and Internet</td>\r\n            <td>$50 to $80 per month</td>\r\n        </tr>\r\n        <tr>\r\n            <td>Public transport</td>\r\n            <td>$45 to $100 per month</td>\r\n        </tr>\r\n        <tr>\r\n            <td>Clothing & Entertainment</td>\r\n            <td>$80 to $250 per month</td>\r\n        </tr>\r\n        <tr>\r\n            <th colspan=\"2\" class=\"cmt-textcolor-skincolor\">Estimated Annual Cost Of living - $11000 to $19000</th>\r\n        </tr>\r\n    </tbody>\r\n</table>', 1, 'Live'),
(13, 2, 'Visa In Canada', 'Canada is highly preferable choice in study abroad destination. Students are expected to start planning for their studies in Canada one year in advance. Canada have generally two intakes, first major intake is in September and the other intake is in January, but it depends on the students program and the institution. Very few institutes might have the May intake as well. An international application for study in Canada can be a slow process, and can take up to 6-8 weeks processing time. You need to submit application as soon as possible. If you want to pursue your study in Canada then you need to apply for Canadian student visa, or study permit and you will require offer of acceptance from your chosen Canadian university. In order to kick the process you have to meet with some requirements such as application fee, language proficiency proof (IELTS), medical certificates, photographs, valid passport, proof of fund in the form of bank statements, receipts or certificates that you have enough money to pay for your tuition and Living expenses.', 1, 'Live'),
(14, 2, 'Before You Leave', 'It is important to make sure that everything is sorted out before you leave for USA , Make sure that you have completed the following:\r\n<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           Important documents (originals in your carry-on luggage and photocopies in your check-in luggage)\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           Your valid passport with relevant visa .\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Your letter of offer from the American institution, your Confirmation of Enrolment (CoE), and your accommodation details.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n          Receipts of any related payments you have made including tuition fees, health cover etc.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            All mark sheets ,certificates and work experience letters.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Your birth certificate, credit card, ten passport size photographs, an international driver’s license.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Important telephone numbers and addresses in India and USA.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Prescriptions to support your use of any required medications.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Health insurance and medical cover.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Work out the exchange rate and make sure you take enough cash for the first couple of days.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Ensure you have full knowledge about of and are complying with the airlines baggage restrictions.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Your luggage should be clearly labeled with your name, contact address and the address of your institution.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Read up on the area and make sure you understand and religious or cultural differences you may face.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Climate will be different from your home country. So pack you baggage according to that.\r\n        </span>\r\n    </li>\r\n    \r\n</ul>\r\n', 1, 'Live'),
(15, 3, 'About United Kingdom', '<strong class=\"mr-2 cmt-textcolor-skincolor\">United Kingdom</strong>&nbsp;is the most popular study destination in the world because of it&rsquo;s modern &amp; conducive learning environment and quality of education that provide a vibrant, creative and challenging environment to develop versatility in your attitude . UK qualifications are recognized and respected throughout the world. It&lsquo;s flexible education system suits all streams of students and gives countless carrier opportunities worldwide. Excluding quality of education you will explore diversity in culture, warm welcome and much more.', 1, 'Live'),
(16, 3, 'Reason for Study in United Kingdom', '<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">International acceptance:&nbsp;</strong>\r\n            In this competitive world, it is necessary to have a degree which is acceptable worldwide. A degree that you gain from an UK university will be recognized internationally by universities, employers and government bodies.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Good quality of Education:</strong> \r\n            The Quality Assurance Agency (QAA) is responsible for teaching quality and general facilities of all universities and colleges.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Renowned for research:</strong>\r\n            The UK is one of the largest manufacturing economies in the world, and at least 40 per cent of the UK service sector is based on science, technology, engineering and mathematics.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Academic Courses:</strong> \r\n            The UK education system offers innumerable courses, out of which you can choose course of your interest and stream accordingly.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Work Placements:</strong> \r\n            Many UK courses offer work placements as part of these programme and part time job opportunities, which helps you to improve work proficiency.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Affordable:</strong> \r\n            Undergraduate and postgraduate courses offered in UK are generally much shorter than other, which results in lesser tuition fees and living costs.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Business language:</strong> \r\n            As a non-native speaker, you have an opportunity to improve your language skills while working and studying in UK.\r\n        </span>\r\n    </li>\r\n     <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Multicultural Society & Diversity:</strong> \r\n            Studying in UK is an opportunity to experience a multicultural environment, meet new people and fellow international students from across the world and discover new places that will develop a range of skills in you which is necessary to be part of today\'s global workforce.\r\n        </span>\r\n    </li>\r\n</ul>\r\n\r\n', 1, 'Live'),
(17, 3, 'Education System', '<p class=\"mb-10\">UK Education system has a global reputation for quality assured education with broad range of subjects. The UK government held strict education standards on all universities &amp; colleges. Surrounding of UK definitely, motivate and encourage you to perform well in chosen subject.</p>\r\n\r\n<p class=\"mb-10\">The quality of a university&#39;s or college&rsquo;s teaching and its general facilities are assessed by the Quality Assurance Agency (QAA). Research standards are examined by the Research Assessment Exercise (RAE), which publishes its findings every five years. The education system differs slightly in the all four countries of UK i.e, England, Scotland, Wales and Northern Ireland.</p>\r\n\r\n<p>UK universities and colleges gives you combined knowledge of practical learning with lectures and seminars, they encourage you to asks questions, debate and generate new ideas.</p>\r\n', 1, 'Live'),
(18, 3, 'Study Option In United Kingdom', '<p class=\"mb-10\">The UK traditionally offers 3-year undergraduate degrees and 1-year master’s programme and for non-native speaker who needs to improve their English or meet conditional offer agreements, foundation courses and English language schools will help to achieve your target. There are so many courses offers by UK education system as follows:</p>\r\n<p><strong class=\"mr-2 cmt-textcolor-skincolor\">Degree, Certificate, Diploma in any stream -</strong> Engineering, Science, Math & Technology, Business & Management, Health & Medicine, Architecture, Law & Legal Studies, Language & Literature, Arts & Social Sciences, Education, Nursing, Fine Arts & Fashion Studies, Journalism & Communication, Sports & Physical Sciences, Divinity/Community, Languages program and many more.</p>\r\n<table class=\"table table-striped\">\r\n	<thead>\r\n		<tr>\r\n			<th>Higer Education</th>\r\n			<th>Pre-university Study</th>\r\n                        <th>English Language Study</th>\r\n			<th>Vocational Studay</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Undergraduate</td>\r\n			<td>Foundation</td>\r\n			<td>English Language Schools</td>\r\n                        <td>Colleges of Further Education</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Pre-Masters</td>\r\n			<td>A-Levels</td>\r\n			<td></td>\r\n                        <td></td>\r\n		</tr>\r\n		<tr>\r\n			<td>Postgraduate</td>\r\n			<td>Boarding School</td>\r\n			<td></td>\r\n                        <td></td>\r\n                        <td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Two Year Degree</td>\r\n			<td>English for Teaching</td>\r\n			<td>&nbsp;</td>\r\n                        <td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Research Degree</td>\r\n			<td>&nbsp;</td>\r\n                        <td>&nbsp;</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		\r\n	</tbody>\r\n</table>\r\n', 1, 'Live'),
(19, 3, 'Living In United Kingdom', '<p><strong class=\"mr-2 cmt-textcolor-skincolor\">There are wide varieties of safe and modern accommodation offered to students in UK, you need to choose from them.</strong></p>\r\n<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">On Campus: </strong>\r\n            Most UK universities offer places for new students in their halls of residence. Halls can vary from single rooms with shared kitchen and living areas, to self-contained studios.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Off Campus: </strong>\r\n            Many Students choose off campus option (Private, ranted accommodation) as they want to explore their life beyond the campus.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Home stay: </strong>\r\n            home stay is an option where you live with a UK family in their home. It will be a great opportunity to experience UK culture.\r\n        </span>\r\n    </li>\r\n</ul>\r\n<h5 class=\"mt-10 mb-2 cmt-textcolor-skincolor\">Estimated living costs</h5>\r\n<p>\r\n    The cost of living can vary greatly depending on your lifestyle, budget and spending habits. You can consider a part-time job while studying in the UK to help pay off any loans or to cover your basic needs.\r\n</p>\r\n<table class=\"table table-striped\">\r\n    <thead>\r\n        <tr>\r\n            <th>Level</th>\r\n            <th>Estimated living costs (£)</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n        <tr>\r\n            <td>Undergraduate 40 weeks</td>\r\n            <td>7000-9000</td>\r\n        </tr>\r\n        <tr>\r\n            <td>Postgraduate 52 weeks</td>\r\n            <td>9000-11000</td>\r\n        </tr>\r\n        \r\n    </tbody>\r\n</table>', 1, 'Live'),
(20, 3, 'Visa In United Kingdom', 'Once you choose the study programme, you need to check, eligibility criteria, entry date, application deadlines and English language requirements. The main intake is in September each year, apart from this there are fewer January intakes available in some universities. If you wish to begin your course in September, keep in mind that the application deadline is in June, so make sure you have submitted all your application documents.<br />\r\n<br />\r\nAnother thing to remember is that most courses have their own language requirements, so you may have to show evidence of your language skills. The preferred English language test is IELTS, and you can find course related detail information from your chosen university website.<br />\r\n<br />\r\nFor more info visits:&nbsp;<a href=\"http://www.ukba.homeoffice.gov.uk/\"><strong>UK Government Border Agency</strong></a>', 1, 'Live'),
(21, 3, 'Before You Leave', '<p>When you are going to pack your luggage for UK studies, there are so many things which you need to remember. It will make the things easier where the place you will be staying.</p>\r\n<p><strong class=\"mr-2 cmt-textcolor-skincolor\">Make sure that :</strong></p>\r\n<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            you have accepted their offer of a place.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Passport with valid visa stamped\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           Financial documents.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           Prescriptions for necessary medicines by doctor.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Medical And insurance Letter.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Find out your Baggage regulations before you start packing\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Check all required Documents.\r\n        </span>\r\n    </li>\r\n</ul>', 1, 'Live'),
(22, 4, 'About Australia ', '<strong class=\"mr-2 cmt-textcolor-skincolor\">Australia </strong>is truly unique &mdash; it is the only country in the world that covers an entire continent and it is also the largest island in the world. You will wonder to know, it has eight of the top 100 universities in the world; it has a highly modern quality education system that produced so many scientists&rsquo; designers, Entrepreneurs &amp; artists who have change the world. You will proud to be a part of it as there are so many awards winning personality from Oscars to Nobel prizes. Many international students are choosing to study in Australia because of its Calm &amp; friendly, excellent education system, and high standard of living.', 1, 'Live');
INSERT INTO `tbl_country_item_page` (`country_item_page_id`, `ref_country_page_id`, `country_item_page_title`, `country_item_page_desc`, `is_active`, `del_status`) VALUES
(23, 4, 'Reason for Study in Australia ', '<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">World Recognized:&nbsp;</strong>\r\n            Universities in Australia have a strong reputation worldwide, having a degree from Australian University gives you a big platform to work & develop your skills anywhere, all over the world.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Upgraded Technology:</strong> \r\n            Student who will be studying in Australia, take benefits of its advance technology & research resources because they have so many quality scientific research programs for international students.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Quality Assurance:</strong>\r\n            The TEQSA and AQF are Australian government bodies, who maintain higher education standard of Australia.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Innumerable courses:</strong> \r\n            Australia is most significant global education powerhouses that offer numerous courses and degrees in all universities and colleges, so anybody can choose right course and stream for the future.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Cost of Living:</strong> \r\n          The lifestyle and living standard of Australia is quite high in the world. It is the third most popular destination for international students because it’s living cost and educational fee is considerably lower than UK & USA.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Work Opportunities:</strong> \r\n            International students in Australia can work up to 40 hours per fortnight during the stay, allowing them to earn money, gain useful work experience in their field of interest and opportunity to meet with variety of people.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Friendly & Safe:</strong> \r\n          Australia has a very friendly and safe environment, which helps you to focus on study and be more innovative with your ideas.\r\n        </span>\r\n    </li>\r\n     <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Multicultural Society & Diversity:</strong> \r\n           People live in Australia for job and study purpose; people are here from almost every aspect of world. Multicultural environment and cultural activities are very important in our life, Studying & Traveling in Australia will be a wonderful experience and diversity of education religion and place will be enhance your skills.\r\n        </span>\r\n    </li>\r\n</ul>\r\n\r\n', 1, 'Live'),
(24, 4, 'Education System', '<p>Every student want to do their study in a good English speaking country, and Australia is the third most popular destination behind the United States and the UK. Eight out of hundred top universities in the world are Australian. Institutions in Australia offer so many courses and degrees, so that students can easily find their choice of course in universities, vocational education, and English language training.</p>\r\n\r\n<p>The Australian education system is most exclusive from many other countries by the Australian Qualifications Framework (AQF). If you are studying an AQF qualification, you can be sure that your institution is Government-authorized and nationally accredited, and that your degree or other AQF qualification will be genuine.</p>\r\n', 1, 'Live'),
(25, 4, 'Study Option In Australia ', '<p class=\"mb-10\">Australian universities feature in the top 50 ranked universities in the world in arts & Humanities, Clinical or Pre-clinical & Health, Engineering & Technology, Life Sciences, Physical Sciences and Social Sciences.</p>\r\n<p><strong class=\"mr-2 cmt-textcolor-skincolor\">Degree, Certificate, Diploma -</strong>  in Australia subject stream known as “field of study”. It is a classification system used by Australian institutions to describe courses, specializations and units of study. Student can opt any choice of course in follows</p>\r\n<table class=\"table table-striped\">\r\n	<thead>\r\n		<tr>\r\n			<th>Higer Education</th>\r\n			<!--<th>Pre-university Study</th>-->\r\n                        <th>English Language Study</th>\r\n			<th>Vocational Studay</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Associate degree (2 Years)</td>\r\n			<td>General English</td>\r\n			<td>Colleges of Further Education</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Bachelor Degree (3 Years)</td>\r\n			<td>English for Academic Purposes</td>\r\n			<td></td>\r\n		</tr>\r\n		<tr>\r\n			<td>Bachelor Degree (Honors) (4 Years)</td>\r\n			<td>English for Specific Purposes (ESP)</td>\r\n			<td></td>\r\n		</tr>\r\n		<tr>\r\n			<td>Graduate Certificate (6 Months)</td>\r\n			<td>Examination Preparation</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Graduate Diploma (1 Year)</td>\r\n			<td>Study Tours</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n                <tr>\r\n			<td>Master Degree (1-2 Years)</td>\r\n			<td></td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n                <tr>\r\n			<td>Doctoral Degree (Typically 3 years)</td>\r\n			<td></td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		\r\n	</tbody>\r\n</table>\r\n', 1, 'Live'),
(26, 4, 'Living In Australia ', '<p><strong class=\"mr-2 cmt-textcolor-skincolor\">There is wide variety of safe and modern accommodations offered to students in Australia, you can choose accommodation that suits your needs and budget.</strong></p>\r\n<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">On Campus: </strong>\r\n            living in university campus will be a great option. It will minimize your Travel & expenses. Living in campus give you option to live with classmates and friends.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Off Campus: </strong>\r\n            Many Students choose off campus option (Private, ranted accommodation) as they want to explore their life beyond the campus. If you are renting a property, you need to pay a security deposit or \'bond\' as well as rent in advance.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Home stay: </strong>\r\n            Australian Families offering home-stay accommodation to international students are ensure they can provide a suitable living environment for students. It is a good option for younger student.\r\n        </span>\r\n    </li>\r\n</ul>\r\n<h5 class=\"mt-10 mb-2 cmt-textcolor-skincolor\">Estimated living costs</h5>\r\n<p>\r\n    The cost of living can vary greatly depending on your lifestyle, budget and spending habits. You can consider a part-time job while studying in the Australia to help pay off any Expenses or to cover your basic needs.\r\n</p>\r\n<table class=\"table table-striped\">\r\n    <thead>\r\n        <tr>\r\n            <th colspan=\"2\" class=\"text-center\">\r\n                Accommodation (You will choose one of them)</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n        <tr>\r\n            <td>Off Campus (Rental)</td>\r\n            <td>$85 to $440 per week</td>\r\n        </tr>\r\n        <tr>\r\n            <td>ON Campus</td>\r\n            <td>$90 to $280 per week</td>\r\n        </tr>\r\n        <tr>\r\n            <td>Home-stay</td>\r\n            <td>$235 to $325 per week</td>\r\n        </tr>\r\n        <tr>\r\n            <th colspan=\"2\" class=\"text-center\">Other living expenses</th>\r\n        </tr>\r\n        <tr>\r\n            <td>Groceries and eating out</td>\r\n            <td>$80 to $280 per week</td>\r\n        </tr>\r\n        <tr>\r\n            <td>Gas, electricity</td>\r\n            <td>$35 to $140 per week</td>\r\n        </tr>\r\n        <tr>\r\n            <td>Phone and Internet</td>\r\n            <td>$20 to $55 per week</td>\r\n        </tr>\r\n        <tr>\r\n            <td>Public transport</td>\r\n            <td>$15 to $55 per week</td>\r\n        </tr>\r\n        <tr>\r\n            <td>Entertainment</td>\r\n            <td>$80 to $150 per week</td>\r\n        </tr>\r\n        <tr>\r\n            <th colspan=\"2\" class=\"cmt-textcolor-skincolor\">Estimated Annual Cost Of living - $18,500 to $ 20,000</th>\r\n        </tr>\r\n    </tbody>\r\n</table>', 1, 'Live'),
(27, 4, 'Visa In Australia ', 'There is a range of entry requirements that you need to meet both for your visa application. That is academic requirements, English language requirements, evidence of funds to support your study and overseas student health cover. Australia have generally two intakes, first is in February &amp; early march or second is in July. Few universities offering some of courses in multiple intake i.e. September &amp; November. Make sure you will start making applications around 7-8 months prior to the intake. University generally takes around 4-6 weeks to process the applications. For more visits: study in Australia', 1, 'Live'),
(28, 4, 'Before You Leave', '<p>Things you need to be remember when you are packing your bag. Make sure that:</p>\r\n\r\n<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           Important documents (originals in your carry-on luggage and photocopies in your check-in luggage)\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            your valid passport with a valid Australian student visa\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           your letter of offer from the Australian institution, your Confirmation of Enrolment (CoE), and your accommodation details.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           Receipts of any related payments you have made including tuition fees, health cover etc.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           Attested mark sheets and certificates and work experience letters\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Your birth certificate, credit card, ten passport size photographs, an international driver’s license\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           Important telephone numbers and addresses in India and Australia\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           Prescriptions to support your use of any required medications.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           Health insurance details\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Check baggage allowance with your airline.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Your luggage should be clearly labeled with your name, contact address and the address of your institution.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Climate will be different from your home country. So pack you baggage according to that.\r\n        </span>\r\n    </li>\r\n</ul>\r\n', 1, 'Live'),
(29, 5, 'About New Zealand', 'New Zealand is a breathtaking country because it&rsquo;s one of the most picturesque and photogenic places on the earth. It is a small island nation in the southwestern Pacific Ocean consisting of just 4 million people who are also referred as KIWIs. This country offers an internationally renowned education system leading to an attractive destination for international students but that is not the only thing, It also provides a perfect mix of a modern cosmopolitan society that exists in perfect harmony with the country&rsquo;s unbeatable lifestyle. There are various universities in New Zealand, which offer high-quality education it also encompasses colleges, private institutions, and polytechnics. New Zealand is a cost-effective study destination with various streams such as Business and science, Sports, Arts&amp; Recreation, 3D Animation &amp; Graphic design, Engineering, Culinary Arts, Communication studies, &amp; many more though it is particularly known for Information Technology, Hospitality &amp;Tourism and Accounting related courses.', 1, 'Live'),
(30, 5, 'Reason for Study in New Zealand', '<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Quality Assurance:&nbsp;</strong>\r\n            The institutions of new Zealand are quality oriented as there are two Functional independent bodies NZ’s Committee on University Academic Programmes (CUAP) and the Academic Quality Agency for New Zealand Universities (AQA ) which ensures that academic processes are of an internationally respected standard and maintains the quality assurance.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Employment prospect:</strong> \r\n            Internships are the first option through which students can earn pocket money but there are other options as well that helps them to earn while pursuing their educational courses in which they are allowed to work up to 20 hours a week through the semester; during vacations up to 40 hours, owing to the skill shortages across the various sectors Students are given a visa to search for a job after completion of their course and also have a good opportunity to live and work permanently. Also, married students can avail for a full time work permit while their spouses study in New Zealand.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Cost of living:</strong>\r\n            The best thing about studying in NZ is that the expenses involved are comparatively less than the other famous study destination, it provides one with value for money as the economy is incredibly stable, living expenses and tuition costs and it can be easily afforded by any international student.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Multifarious study programme:</strong> \r\n           New Zealand institutions ranks high in world education rankings and have been rated among the best in the world. Wide range of quality study program has been provided by the eight renowned universities, colleges and polytechnic institutes ranging from certificate, diploma, degree, postgraduate and masters courses. You have innumerable options to choose, depending upon your choice of interest varying from, arts, science, medicine, business, law etc.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Friendly People and culture:</strong> \r\n         Kiwis are the most amazing and welcoming people that you will ever meet. Apart from studies, multiple options are in your hand; you can easily encounter the beauty of country and enjoy life to fullest. Multicultural mix of Europeans, Asians, and Pacific Islanders, making a nice melting pot of cultures. The best part of their culture is that English is the day to day language and that’s why international students find it easy to study and work.\r\n        </span>\r\n    </li>\r\n</ul>', 1, 'Live'),
(31, 5, 'Education System', 'A student seeking for perfect study environment and an outdoorsy lifestyle remains a big question but undoubtedly New Zealand is a correct place for them as this country provides an expert teaching staff with world class facilities and an abundance of natural resources. New Zealand has ample of study options for students pursuing their higher education in such education system that allows for resilience, choice and extensive support. New Zealand Qualifications&#39; Framework (NZQF) is the core of the entire education system. This lists all qualifications in a series of levels, from Level 1 to 10, and includes certificates, diplomas and degrees which means its quality assured. Additionally, the education system in New Zealand is centrally managed by the New Zealand Qualifications Authority (NZQA) which requires the registration of institutes that enroll international student.', 1, 'Live'),
(32, 5, 'Study Option In New Zealand', '<p class=\"mb-10\">New Zealand delivers a wide range of providers including Universities, Institutes of Technology, Polytechnics, Private Training Establishments (PTEs) and Industry Training Organizations Which offers qualifications at every level - certificates, diplomas, graduate and post-graduate degrees. Broad range of subjects for undergraduate, Master’s and Doctoral (PhD) degrees in commerce, science and the humanities. Some universities offer degrees in specialist fields - such as medicine, agriculture, engineering, etc.\r\n\r\n<table class=\"table table-striped\">\r\n	<thead>\r\n		<tr>\r\n			<th>Higer Education</th>\r\n			<!--<th>Pre-university Study</th>-->\r\n                        <th>English Language Study</th>\r\n			<th>Vocational Studay</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Graduate Certificate (6 Months)</td>\r\n			<th>Pathway academic programmes for</th>\r\n			<td>Colleges of Further education and training.</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Graduate Diploma (1 Year)</td>\r\n			<td>Undergraduates studies</td>\r\n			<td></td>\r\n		</tr>\r\n		<tr>\r\n			<td>Bachelor Degree (3 Years)</td>\r\n			<td>Postgraduates studies</td>\r\n			<td></td>\r\n		</tr>\r\n		<tr>\r\n			<td>Bachelor Degree (Honors) (4 Years)</td>\r\n			<td></td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Post-Graduate Certificate (6 Months)</td>\r\n			<td></td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n                <tr>\r\n			<td>Post-Graduate Diploma (1 Year)</td>\r\n			<td></td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n                <tr>\r\n			<td>Master Degree (1-2 Years)</td>\r\n			<td></td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n                <tr>\r\n			<td>Doctoral Degree (Typically 3 years)</td>\r\n			<td></td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		\r\n	</tbody>\r\n</table>\r\n', 1, 'Live'),
(33, 5, 'Living In New Zealand', '<p>living in New-Zealand give you international exposure as it have diversity, now the major task is choosing accommodation type that suits your requirement.</p>\r\n<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">On Campus: </strong>\r\n          living in university campus or hall of residence will be a great option. It will minimize your Travel & expenses. Living in campus give you option to live with classmates and friends.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Off Campus: </strong>\r\n            Many Students choose off campus option (Private, rented accommodation) as they want to explore their life beyond the campus. If you are renting a property, you need to pay a security deposit or \'bond\' as well as rent in advance.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Home stay: </strong>\r\n           Kiwis offering home-stay accommodation to international students are ensure they can provide a suitable home like living environment for students.\r\n        </span>\r\n    </li>\r\n</ul>\r\n<h5 class=\"mt-10 mb-2 cmt-textcolor-skincolor\">Estimated living costs</h5>\r\n<p>\r\n    Costs to live in New Zealand may be quite different from your home country. How it compares depends on where you are coming from and what part of New Zealand you settle in.\r\n</p>\r\n<table class=\"table table-striped\">\r\n    <thead>\r\n        <tr>\r\n            <th colspan=\"2\" class=\"text-center\">\r\n                Accommodation (You will choose one of them)</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n        <tr>\r\n            <td>Off Campus (Rental)</td>\r\n            <td>$105 to $145 (weekly)</td>\r\n        </tr>\r\n        <tr>\r\n            <td>ON Campus (hall of residence :full catered)</td>\r\n            <td>$230-$500 (weekly)</td>\r\n        </tr>\r\n        <tr>\r\n            <td>Home-stay</td>\r\n            <td>$200-$350 (weekly)</td>\r\n        </tr>\r\n        <tr>\r\n            <th colspan=\"2\" class=\"text-center\">Other living expenses</th>\r\n        </tr>\r\n        <tr>\r\n            <td>Food</td>\r\n            <td>$136.50 (weekly)</td>\r\n        </tr>\r\n        <tr>\r\n            <td>Transport</td>\r\n            <td>$79.85 (weekly)</td>\r\n        </tr>\r\n        <tr>\r\n            <td>Communication</td>\r\n            <td>$23.80 (weekly)</td>\r\n        </tr>\r\n        <tr>\r\n            <td>Miscellaneous goods and services</td>\r\n            <td>$71.70 (weekly)</td>\r\n        </tr>\r\n      \r\n        <tr>\r\n            <th colspan=\"2\" class=\"cmt-textcolor-skincolor\">Total estimated cost of living : NZ$14000 to NZ$ 20000 annually</th>\r\n        </tr>\r\n    </tbody>\r\n</table>', 1, 'Live'),
(34, 5, 'Visa In New Zealand', 'Being a non-native English speaker, if you want to study in New Zealand for more than 3 months will need a valid student visa. A part from this you have to check, university or colleges, courses, eligibility criteria, entry dates, application deadlines and language requirement as well as. There are certain things you need to remember for visa application i.e. offer letter from institution, proof of funds, PCC(a police certificate of good character), Proof of accommodation, medical certificate, Health Insurance &amp; Many More. In order to get your student visa, you need to officially apply for it and pay a student visa application fee. New Zealand has two main intakes, February, and July, and sometimes in September also. Student can apply for visa and submit documents within 90 days period prior to the date of start of the program.', 1, 'Live'),
(35, 5, 'Before You Leave', '<p>There are some important things which you need to carry for studying in New Zealand. It will help you to stay & Study there without any hassle.</p>\r\n<!--<p><strong class=\"mr-2 cmt-textcolor-skincolor\">Make sure that :</strong></p>-->\r\n<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n          Passport and study visa\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           Travel/medical insurance\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n          Any medication you take regularly\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n          Your birth certificate\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n          All important academic qualifications Documents\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            An international driver’s license or permit\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           Avoid oversize and overweight baggage & Check baggage allowance with your airline.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           Ahead of time, mark luggage tags clearly with your name, home address and phone number and keep this information inside your bags too.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           Climate will be different from your home country. So pack you baggage according to that.\r\n        </span>\r\n    </li>\r\n</ul>', 1, 'Live'),
(36, 6, 'About Singapore', '<p>Singapore, the &ldquo;Lion City&rdquo; is known for its economy and financial success. The nation is among four &ldquo;Asian Tiger&rdquo; economies (along with Hong Kong, South Korea and Taiwan) apart from this, it&rsquo;s also one of the leading education hub in Asia , Education is priority in Singapore and the nation is recognized as a &lsquo;global schoolhouse&rsquo;. This island nation is famous for its universities and student-friendly climate. It has been seen in global surveys on research and innovation, Singapore is the most prominent country in the list. It may be a small country, but scope for higher education in Singapore is significant. English is the officially designated language for the education system. Every year, an increasing number of international students at all levels of education makes the decision to study for their degrees in Singapore.</p>\r\n\r\n<h4>&nbsp;</h4>\r\n', 1, 'Live'),
(37, 6, 'Reason for Study in Singapore', '<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Quality Education:&nbsp;</strong>\r\n            Universities & institutions of Singapore are being taken care by Ministry of education (MOE) which aims high quality and maintain strong education system which encourage student to follow their passions, and promote a diversity of talents among them – in academic fields, and in sports and the arts.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Research & innovation:</strong> \r\n            Singapore is also a hot-bed of technological and IT invention and innovation. It has been named the most innovative country throughout the entire Asia Pacific region. In 2016, Singapore ranked 6th in the INSEAD Global Innovation Index Rankings.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Global Business Hub:</strong>\r\n           More than 7000 Multinational Corporations (MNC) alongside 1,00,000 small and medium enterprises (SME) have set up their base in Singapore which offers limitless career opportunities and networking possibilities to all international students.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Stable Economy:</strong> \r\n           Singapore is an amazing place with dynamic economy. It has been top rated country in term of doing business across the world. It is mainly pushed by the fact that it is a well- known financial centre and all major companies of the world have created their base in Singapore.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Multi-Cultural Nation:</strong> \r\n        Singapore\'s high standards of living are something that student can be assured of. The nation\'s rich multicultural heritage is highlighted through the various ethnic groups (Chinese, Malays, Indians, Eurasians) living together harmoniously. The variety of religions is a direct reflection of the diversity of races living here.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Work opportunities:</strong> \r\n          Singapore is one of the most attractive countries for foreigners seeking highly-skilled work and being a hub for business and high-tech industries in South-East Asia, Singapore offers a wide-range of opportunities, notably in key sectors such as banking and finance, biomedical sciences, chemicals, communications and media, electronics, healthcare and information technologies.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Comfortable living:</strong> \r\n          Singapore is regarded as the easiest city to adjust in Asia, specifically for international students. Singapore is a small island but known for all over attraction and activities. Dining and Shopping are two of the top rated activities of Singaporeans. It is also strategically located at the heart of Asia and can be a hub to explore the Southeast Asian region.\r\n        </span>\r\n    </li>\r\n</ul>', 1, 'Live'),
(38, 6, 'Education System', 'Education has always been the key to growth for development of Singapore, particularly since 1965, when it became independent republic. Singapore’s universities education aspires to prepare students not only for today’s world but also for a world where there will be jobs that have yet to be invented and challenges not yet foreseen. Education System in Singapore comprised with pre-schooling, primary education, secondary education, pre-university education and higher education. The Ministry of Education is the main official Government body of Singapore that directs the formulation and implementation of policies related to Singapore. The education system in Singapore with its flexibility and diversity has joined ranks with the existent leading education systems of the world and has emerged as a premier destination to pursue higher education in world-class universities with state-of-the-art infrastructure.', 1, 'Live'),
(39, 6, 'Study Option In Singapore', '<p class=\"mb-10\">With its highly affordable education and top-notch courses in computer science, law and animation, Singapore is an excellent study destination for international students. Many of Singapore&rsquo;s institutions of higher education are internationally ranked, with so many good course opportunities in various fields. Additionally, institutions from the USA, UK and Australia have opened satellite campuses in Singapore to offer their courses at a subsidized cost. International student&rsquo;s desires to pursue education in Singapore will enjoy access to world-class technology in conducive learning environment.</p>\r\n\r\n<table class=\"table table-striped\">\r\n	<thead>\r\n		<tr>\r\n			<th class=\"text-center\" colspan=\"3\">Higher Education Structure in Outline</th>\r\n			\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<th>Pre-University Education</th>\r\n			<th>Post-Secondary Education</th>\r\n		</tr>\r\n		<tr>\r\n			<td>Junior Colleges</td>\r\n			<td>Polytechnics</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Centralized Institute</td>\r\n			<td>Institute of Technical Education</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Integrated Programmed Schools</td>\r\n			<td>Specialized Arts Schools</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n			<td>Universities</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 1, 'Live'),
(40, 6, 'Living In Singapore', '<p>College students have a wide choice when it comes to accommodation. Being an international student you’ll need a place to lay your head at night and relax too! And you can choose any suitable accommodation in any location.</p>\r\n<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">On Campus: </strong>\r\n          living in university campus will be a great option. Generally first year student choose it. It will minimize student’s travel expenses and on other hand, give you option to live in campus with classmates and friends also.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Off Campus: </strong>\r\n            Many Students choose off campus option (Private, rented accommodation) as they want to explore their life beyond the campus. You can live in private hostels which are run by educational institutions or independent agencies.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Home stay: </strong>\r\n           Some local families in Singapore open up their homes and offer full boarding to international students. A home-stay programme lets you sample the local flavors of life in Singapore, as well as enjoy a homely environment.\r\n        </span>\r\n    </li>\r\n</ul>\r\n<h5 class=\"mt-10 mb-2 cmt-textcolor-skincolor\">Estimated living costs</h5>\r\n<p>\r\n    The living cost you incur shall depend on your lifestyle\r\n</p>\r\n<table class=\"table table-striped\">\r\n    <thead>\r\n        <tr>\r\n            <th colspan=\"2\" class=\"text-center\">\r\n                Accommodation (You will choose one of them) (Approximate)</th>\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n        <tr>\r\n            <td>Off Campus (Rental)</td>\r\n            <td>S$500 to S$1200 per month</td>\r\n        </tr>\r\n        <tr>\r\n            <td>ON Campus</td>\r\n            <td>S$500 to S$800 per month</td>\r\n        </tr>\r\n        <tr>\r\n            <td>Home-stay</td>\r\n            <td>S$600 to S$1200 per month</td>\r\n        </tr>\r\n        <tr>\r\n            <th colspan=\"2\" class=\"text-center\">Other living expenses  (Approximate)</th>\r\n        </tr>\r\n        <tr>\r\n            <td>Groceries and eating out</td>\r\n            <td>S$300 to S$400 per month</td>\r\n        </tr>\r\n        <tr>\r\n            <td>Public transport</td>\r\n            <td>S$100 to S$150 per month</td>\r\n        </tr>\r\n        <tr>\r\n            <td>Personal expense</td>\r\n            <td>S$200 to S$400 per month</td>\r\n        </tr>\r\n        <tr>\r\n            <td>Miscellaneous Expenses</td>\r\n            <td>S$100 to S$500 per month</td>\r\n        </tr>\r\n      \r\n        <tr>\r\n            <th colspan=\"2\" class=\"cmt-textcolor-skincolor\">Estimated Annual Cost Of living - S$11000 to S$20000</th>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<p>*These amounts will vary accordingly depending on your personal needs and lifestyle pattern.</p>', 1, 'Live'),
(41, 6, 'Visa In Singapore', 'The main academic intake takes place in September each year with a smaller intake is January and the deadline for September intake is June. There is a range of entry requirements that you need to meet both for your visa application and University criteria i.e. academic requirements, English language requirements etc. International students who wish to study in Singapore will need a valid visa. In order to apply for a study visa you have to go for application process which is linked to university application and student accept study visa with their letter of approval. This letter works as a visa and it can be used at entry checkpoints. Apart from this, for Singapore visa you will require one more important document i.e. a student pass. You need to apply two month earlier before your course commence. To obtain your Student Pass, you have to sign up with the special Student&rsquo;s Pass Online Application &amp; Registration System (SOLAR). A Student Pass allows you to work during your studies in 16 hours per week in term time &amp; full time in holiday. But you need to check course regulations first because all students are not allowed to work during term-time.', 1, 'Live'),
(42, 6, 'Before You Leave', '\r\n\r\n<p>Pre-departure checklist will help you to keep everything organized and save you from last minute hurdle.</p>\r\n<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n          Please ensure that you have received the IPA (Student\'s Pass in-principal approval) letter before you leave for Singapore.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           Make sure that you undergo medical examination in home country, as specified by the visa authorities. In Singapore, you will have to submit your medical report at the authorized hospital.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n          Please ensure that you have a valid passport for international travel with a validity of at least six months.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n          Applicants should make sure that they have all the original copies and complete set of Xerox of their academic result and working experience (if you have) for verification in Singapore.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n          Make sure that you have all important documents such as birth certificate, photographs, credit card.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           In case your documents are not in English, please get their official translation done in English.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           The confirmation e-mail or letter stating that you do have a place to stay at Singapore might be needed when you apply for study visa.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n          Student must carry at least S$1000.00 for incurring the first week of stay there. Apart from this, you need to maintain Singapore bank account to make your transaction hassle free.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n          Check baggage allowance with your airline.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Your luggage should be clearly labeled with your name, contact address and the address of your institution.\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">After Arriving:</strong>\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            Complete your Student\'s Pass formalities at the Immigration Checkpoints Authority (ICA).\r\n        </span>\r\n    </li>\r\n     <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            At almost all good educational institutions at Singapore, there is a department providing specialized services to international students. Contact the department and ask for help, whenever you need it.\r\n        </span>\r\n    </li>\r\n     <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            If you do not have Singapore currency already, you will find currency exchange counters at the airport in Singapore.\r\n        </span>\r\n    </li>\r\n</ul>\r\n', 1, 'Live'),
(43, 7, 'About Poland', '<p>It was 1998, almost a decade after the fall of communism, and the country was flying in the aftermath of Sachs&#39; market reforms. Deregulation and privatization made Poland&#39;s economy one of Europe&#39;s fastest growing. Poland now has the fourth-highest number of higher education students in Europe, behind the U.K., Germany and France. By 2012, the time the OECD conducted its survey, Poland was one of the best teaching countries on earth.</p>\r\n\r\n<p>Besides Poland is a fascinating country that serves as the geographical and cultural crossroads of Eastern and Western Europe. Located at the center of the Northern European plain, Poland has been a nation of survivors since the foundation of the first Polish state more than 1000 years ago. Through its turbulent history its people have managed to maintain their identity, and today, the country enjoys a crucial position as the largest of the former Eastern European states and one of the most populous members of the European Union.<br />\r\n&nbsp;</p>\r\n\r\n<p>For students from outside the European Union who come to study in Poland, this may well be the beginning of a fascinating adventure of discovering Europe. Being invited to study in Poland means that you are invited to the European Union, of which Poland is an active member state. We encourage you to discover the European Union, which offers not only varied and interesting cultures and the opportunities associated with strong, innovative economies, but it also provides the very best conditions for successful higher education study in a challenging and friendly atmosphere. With top-quality, internationally recognized degrees, almost no other region in the world can set your career off to such a promising start.</p>\r\n', 1, 'Live');
INSERT INTO `tbl_country_item_page` (`country_item_page_id`, `ref_country_page_id`, `country_item_page_title`, `country_item_page_desc`, `is_active`, `del_status`) VALUES
(44, 7, 'Reason for Study in Poland', '<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Rich History and Tradition:&nbsp;</strong>\r\n            Polish university education system has a history of 650 years of educating high profile professionals. It resulted with a profit not only for Poland, but also for many countries all over the world, where the Poles brought their professionalism and the spirit of innovation. Want some examples? Ignacy Domeyko established the geology research in 19th century Chile. Between 1872 and 1876 Ernest Malinowski built the world\'s highest located railroad in Peru. Bronis?aw Malinowski was a creator of modern anthropology. Ten Nobel Prizes were awarded to Polish artists, scientists and other public figures. Maria Sk?odowska-Curie is one of only four laureates to receive a prize twice.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Modernity:</strong> \r\n            Today, the Polish higher education system is developing rapidly. Poland holds fourth place in Europe (after the United Kingdom, Germany and France) in terms of the number of people enrolled in higher education. The total student population at over 400 university level schools is almost 1,5 million. Each year almost half a million young people begin their education at universities and colleges. Most schools offer courses in foreign languages.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Bologna Process:</strong>\r\n            Poland plays an active part in the Bologna Process. Owing to the introduction of three-stage education modelled on Bachelor/Master/Doctoral studies as well as the European Credit Transfer System, both Polish students and foreigners studying in Poland stay fully mobile and can continue their education elsewhere in the European Union. Within just the Erasmus Program that has been going on for over 20 years now, over 43,000 foreign students have come to study in Poland while almost 100,000 students from Poland have taken part of their education in another country within the European Union. Foreign students coming to Poland can expect the most attractive and diversified education opportunities meeting high European standards. They can study medicine, biotechnology or engineering, but also art and business. The diploma awarded to them upon graduation is recognized not only Europe-wide but also in key countries of the world.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">High Quality of Education:</strong> \r\n            The Polish higher education system is well developed. The quality of the education provided is monitored and regularly evaluated. The main Polish institutions in charge of quality assurance in higher education are: the Polish Accreditation Committee, the General Council for Science and Higher Education and the Conference of Rectors of the Academic Schools in Poland. There are over 5000 courses available in Poland and each of them has had to gain the Polish Accreditation Committee’s approval. Among them there are a number of fields of study that have received the grade: excellent. The list of excellent fields of study is available at the Polish Accreditation Committee website: http://www.pka.edu.pl/?q=en/oceny.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Competitive Costs of Living and Studying:</strong> \r\n            Compared to other EU countries, the tuition fees in Poland are highly competitive and the costs of living are a fraction of what a foreign student would have to spend in other European cities.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Academic Courses:</strong> \r\n            The Poland education system offers innumerable courses, out of which you can choose course of your interest and stream accordingly.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Work Placements:</strong> \r\n            Many Poland courses offer work placements as part of these programme and part time job opportunities, which helps you to improve work proficiency\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Affordable:</strong> \r\n            Undergraduate and postgraduate courses offered in Poland are generally much shorter than other, which results in lesser tuition fees and living costs.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Affordable:</strong> \r\n            Undergraduate and postgraduate courses offered in Poland are generally much shorter than other, which results in lesser tuition fees and living costs.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Business language:</strong> \r\n            As a non-native speaker, you have an opportunity to improve your language skills while working and studying in Poland.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">ENIC and NARIC Recognized:</strong> \r\n            <p><strong class=\"mr-2 cmt-textcolor-skincolor\">ENIC –</strong> European Network of National Information Centres for Academic Recognition and Mobility established by the Council of Europe and UNESCO for the European Region.</p>\r\n            <p><strong class=\"mr-2 cmt-textcolor-skincolor\">NARIC – </strong>National Academic Recognition and Recognition Centres is a network set up by the European Commission in order to establish effective and close cooperation between EU member states in terms of academic recognition and they support the system for professional recognition.</p>\r\n        </span>\r\n    </li>\r\n</ul>\r\n\r\n', 1, 'Live'),
(45, 7, 'Education System', '<p>Since 2007/2008 academic year Polish higher education system has been divided to three stages, which are: Bachelor (Licencjat, In?ynier), Master (Magister), and Doctor (Doktor). This system applies to all fields of education except Law, Pharmacy, Psychology, Veterinary Medicine, Medicine and Dentistry, which are still based on two-stage system (Master and Doctor).</p>\r\n\r\n<p>The first university in Poland, Krak&oacute;w&#39;s Jagiellonian University, was established in 1364 by Casimir III the Great in Krak&oacute;w. It is the oldest university in Poland. It is the second oldest university in Central Europe (after Prague University) and one of the oldest universities in the world. Casimir III realized that the nation needed a class of educated people, especially lawyers, who could codify the country&#39;s laws and administer the courts and offices. His efforts to found an institution of higher learning in Poland were finally rewarded when Pope Urban V granted him permission to open the University of Krak&oacute;w.</p>\r\n\r\n<p>A large part of the Polish higher education market is made up of private colleges and universities. There are about 310 privately owned universities and colleges and 138 state schools of higher learning. This has resulted in a high level of competition that has given Poland lower prices for studying than in many other European countries. The higher education system is one of high quality and all leading universities offer programmes thought in English, within study areas such as medicine, engineering, humanities, business and finance. More than 100 higher education institution in Poland currently offer study programmes in English. Poland has taken active part in the Bologna Process. The ECTS (European Credit Transfer System) allows students to be geographically mobile and continue their education in other countries.</p>\r\n\r\n<p>There is no central administration for admission. Instead, each institute is responsible for their own admission process. However, each applicant must generally hold a &ldquo;maturity certificate&rdquo; to qualify for admission in Poland.</p>\r\n\r\n<p>The grading is done every semester (twice a year), not just once in a school year. Depending on the subject, the final grade may be based on the result of a single exam, or on the student&#39;s performance during the whole semester. In the latter case, usually a point system, not the 2&ndash;5 scale is used. The points accumulated during the semester are added and converted to a final grade according to some scale.</p>\r\n\r\n<p>As a failing grade means merely having to repeat the failed subject, and can usually be corrected on a retake exam (and in some cases also on a special &quot;committee exam&quot;), it is used much more liberally, and it is quite common for a significant number of students to fail a class on the first attempt.</p>\r\n', 1, 'Live'),
(46, 7, 'Study Option In Poland', '<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">1<sup>st</sup> CYCLE :</strong>\r\n            First-cycle studies (3 to 4 years) leading to the professional title of a licencjat or in?ynier (Engineer, in the field of engineering, agriculture or economics). This is the Polish equivalent of the Bachelor’s degree. It is focused on preparing students for future employment or for continued education within a Master’s de\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">2<sup>nd</sup> CYCLE:</strong> \r\n            Second-cycle studies – Master’s degree programme (1.5 to 2 years) following the first cycle studies and leading to the professional title of Master (magister, or an equivalent degree depending on the course profile). It is focused on theoretical knowledge as well as the application and development of creative skills. In arts disciplines, the focus is on the development of creativity and talents. Master’s degree holders may enter a doctoral programme (third-cycle studies). To obtain the degree, students must earn 90-120 ECTS credits.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">LONG-CYCYLE STUDIES:</strong>\r\n            In addition to the general structure, 11 fields of study including acting, art conservation and restoration, canon law, dentistry, law, medical analysis, medicine, production and photography, pharmacy, psychology and veterinary medicine, offer long-cycle programmes only. Long-cycle studies – Master’s degree programme (4.5 to 6 years) leading to the professional title of Master (magister), or an equivalent degree depending on the course profile). To obtain this degree, students must earn 270-360 ECTS credits. Such single long-cycle studies are based on an integrated study programme containing both basic studies and in-depth specialisation. Completion of this degree will provide a qualification corresponding to a second-cycle Master’s degree.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">3<sup>rd</sup> CYCLE:</strong> \r\n            Third-cycle studies – Doctoral degree programmes (normally 3 to 4 years) accessible for graduates of a Master’s degree programme, leading to a PhD degree, offered by universities as well as some research institutions (departments of the Polish Academy of Sciences as well as research and development institutions). A PhD degree is awarded to candidates who submit and successfully defend a doctoral dissertation before a thesis committee and pass a doctoral examination.\r\n        </span>\r\n    </li>\r\n</ul>', 1, 'Live'),
(47, 7, 'Living In Poland', '<h4 class=\"cmt-textcolor-skincolor\">Rules to live in Poland</h4>\r\n<p>To obtain a temporary residence permit, you should provide documented reasons for your living in Poland. Taking up a job or education in Poland.</p>\r\n<p>A temporary residence permit can be obtained for any period, but not longer than 3 years. The application for temporary residence can be submitted at any time in a Polish consulate at your place of residence or in a voivodeship office in Poland.\r\n<p>Warszawa (Warsaw) the capital of Poland with over 1.7 million inhabitants. It is a business city, to which many Poles migrate searching for education and job opportunities. Thanks to its 50 plus higher education institutions, it has a vibrant spirit and constitutes an important scientific and cultural centre. The city was almost completely destroyed during World War II. Its present architectural landscape has largely been shaped by the years of communism (symbolized by the Palace of Science and Culture) and its entrepreneurial character (skyscrapers).</p>    \r\n    \r\n<h4 class=\"cmt-textcolor-skincolor\">Accommodation</h4>\r\n<p>There are many different options for arranging student accommodation in Poland. They vary depending on the city and higher education institution you choose. Many Polish HEIs have their own dormitories, which are usually the cheapest option available. However, most Polish students prefer to rent a room in a private apartment.</p>\r\n<p><strong class=\"mr-2 cmt-textcolor-skincolor\">Student houses and dormitories</strong>Student houses and dormitories The pricing of the student houses depends on the particular HEI. Usually the cost of accommodation in a dormitory ranges is around EUR 60-80 monthly for a shared room and between EUR 100-150 for a single room. However, the standard of the dormitories may differ greatly even between various student houses of the same HEI, so it’s good to do some research before the final decision. What doesn’t differ is the friendly and helpful atmosphere in the student houses.</p>\r\n<p><strong class=\"mr-2 cmt-textcolor-skincolor\">Private housing</strong>It is quite common in Poland to rent a room in a bigger apartment. Most of the out-of-town students share flats in this way. The cost varies between cities and it depends greatly on the location of the apartment as well as the size and quality of the room. The monthly rent is usually between EUR 150 and 200. Some landlords may require a deposit of a similar amount. Don’t worry if you don’t have friends to live with. Sharing a flat with locals is an amazing opportunity: you can pay for the accommodation and get great friends for free! And if you really don’t like to share, you can find an independent apartment. The rent for the smallest, one-room apartment starts from about EUR 300 (in Warsaw).\r\n\r\n\r\n<table class=\"table table-striped\">\r\n	<thead>\r\n		<tr>\r\n                    <th colspan=\"2\" class=\"text-center\">Monthly Expenses (Average value)</th>\r\n			\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Rent in a shared flat (or dormitory)</td>\r\n			<td>80-150 EUR</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Food</td>\r\n			<td>100-150 EUR</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Transportation (in big cities)</td>\r\n			<td>15-20 EUR</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Telephone/mobile, internet, TV</td>\r\n			<td>20-30 EUR</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Study materials</td>\r\n			<td>70-100 EUR</td>\r\n		</tr>\r\n                <tr>\r\n                    <th colspan=\"2\" class=\"text-center\">Examples of other selected prices</th>\r\n		</tr>\r\n                <tr>\r\n			<td>Bread (1 loaf )</td>\r\n			<td>0.80 EUR</td>\r\n		</tr>\r\n                  <tr>\r\n			<td>Milk (1 litre)</td>\r\n			<td>0.70 EUR</td>\r\n		</tr>\r\n                 <tr>\r\n			<td>Lunch at a canteen</td>\r\n			<td>3.00 EUR</td>\r\n		</tr>\r\n                  <tr>\r\n			<td>Coffee in a café</td>\r\n			<td>2.50 EUR</td>\r\n		</tr>\r\n                <tr>\r\n			<td>Cinema ticket</td>\r\n			<td>5.00 EUR</td>\r\n		</tr>\r\n             \r\n	</tbody>\r\n</table>\r\n', 1, 'Live'),
(48, 7, 'Visa In Poland', '<p>We will be happy to help you in getting your student visa. Thanks to our experience and good contacts with Polish embassies and consulates around the world, we can assist you with the process of acquiring a visa.</p>\r\n<p>It takes from 2 weeks to a month to obtain a Polish visa. In other words, you need to start collecting all the necessary documents for the visa application at least two months prior to your planned departure.</p>\r\n<p>In order to apply for a student visa to Poland you need the following:</p>\r\n<!--<p><strong class=\"mr-2 cmt-textcolor-skincolor\">Make sure that :</strong></p>-->\r\n<!--<p>Pre-departure checklist will help you to keep everything organized and save you from last minute hurdle.</p>-->\r\n<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n          A completed visa application form\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           2 passport photos on a black and white background (3.5 x 4.5 cm)\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n          A valid passport (for at least 3 months after your visa expires)\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n          The admission letter from university\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n          Confirmation of the paid tuition fee\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n           Medical insurance for your stay in Poland\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n         A confirmation of funds for living in Poland\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n          A short CV\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n          Your school certificates and diplomas\r\n        </span>\r\n    </li>\r\n    <li><i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n          Evidence of proficiency in the language in which you wish to study\r\n        </span>\r\n    </li>\r\n</ul>\r\n<p>Once all the formalities have been finalized, we will invite you to our pre-travel training, during which you will receive all the necessary information on studying and living in Poland.</p>', 1, 'Live'),
(49, 7, 'Before You Leave', '<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Accommodation:</strong>\r\n            There are many different options for arranging student accommodation in Poland. They vary depending on the city and higher education institution you choose. Many Polish HEIs have their own dormitories, which are usually the cheapest option available. However, most Polish students prefer to rent a room in a private apartment.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Job Prospects and work permit:</strong> \r\n            he booming IT-industry in Poland creates a great incentive for those that are in a need for change. Therefore, it’s recommend you equip yourself by the best advice so have a look at the following job sectors to boost your career and unleash your success.<br/>\r\n            What is more, if you study or are planning to start studying in Poland, you can experience certain conveniences concerning taking up a job. As a full-time student possessing also a residence card, you are allowed to start work in Poland without applying for additional permits.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Health insurance:</strong>\r\n            If possible it is recommended that students from non-EU/EEA countries purchase their own international medical insurance prior to their arrival in Poland. Otherwise they are required to sign a voluntary health insurance agreement with the National Health Fund (Narodowy Fundusz Zdrowia – NFZ) and pay their own insurance fees, which amount to about EUR 15 a month. Under this insurance scheme, students are entitled to free medical care and can use university health care clinics and health centres. Also, all foreign students have the option of purchasing additional accident insurance. For detailed information visit the website of the National Health Fund.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Cost of living in Poland:</strong> \r\n            In comparison to other European countries Poland is a relatively cheap place to live and study. Prices depend greatly on the city, but a student can get by with about EUR 300 at their monthly disposal. Average costs of student living range from EUR 350 up to EUR 550. Please remember, that to be able to study in Poland non-EU/EEA students have to possess sufficient means to cover the living costs.\r\n            <br>Below are some examples to give an idea of the amounts students spend per month.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Transportation Facilities:</strong> \r\n            Simply put, transportation is Poland is fantastic. There are no words for how every part is so well connected with each other. If you are planning to live and work in one of the cities, there will be no need for a car. Public transport is relatively cheap compared to western public transportation and the app Jakdojade makes it nearly impossible to get lost.\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Monthly Expenses (Average value):</strong> \r\n            <table class=\"table table-striped\">\r\n                <tr>\r\n                    <td>Rent in a shared flat (or dormitory)</td>\r\n                    <td>80-150 EUR</td>\r\n                </tr>\r\n                <tr>\r\n                    <td>Food</td>\r\n                    <td>100-150 EUR</td>\r\n                </tr>\r\n                <tr>\r\n                    <td>Transportation (in big cities)</td>\r\n                    <td>15-20 EUR</td>\r\n                </tr>\r\n                <tr>\r\n                    <td>Telephone/mobile, internet, TV</td>\r\n                    <td>20-30 EUR</td>\r\n                </tr>\r\n                <tr>\r\n                    <td>Study materials</td>\r\n                    <td>30-50 EUR</td>\r\n                </tr>\r\n                <tr>\r\n                    <td>Other expenses (leisure/entertainment)</td>\r\n                    <td>70-100 EUR</td>\r\n                </tr>\r\n            </table>`\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Climate:</strong> \r\n            Poland has a moderate climate with both maritime and continental elements. You can count on many sunny days and many rainy days and don’t be surprised when the summer turns out to be quite hot or quite rainy. Winters are usually cold, with temperatures well below freezing, and more or less snowy. If you come from a warmer climate, make sure you have proper clothing.\r\n        </span>\r\n    </li>\r\n</ul>', 1, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_country_page`
--

DROP TABLE IF EXISTS `tbl_country_page`;
CREATE TABLE IF NOT EXISTS `tbl_country_page` (
  `country_page_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_page_name` varchar(250) NOT NULL,
  `continent_id` int(11) DEFAULT NULL,
  `country_page_short_name` varchar(250) DEFAULT NULL,
  `country_page_desc` varchar(300) NOT NULL,
  `home_page_desc` varchar(300) NOT NULL,
  `country_page_image` text NOT NULL,
  `home_page_image` text NOT NULL,
  `country_flag_image` text NOT NULL,
  `background_image` text NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_show_home_page` int(11) NOT NULL DEFAULT '0',
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`country_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_country_page`
--

INSERT INTO `tbl_country_page` (`country_page_id`, `country_page_name`, `continent_id`, `country_page_short_name`, `country_page_desc`, `home_page_desc`, `country_page_image`, `home_page_image`, `country_flag_image`, `background_image`, `is_active`, `is_show_home_page`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'Canada', NULL, NULL, 'Apply to travel, study, work or immigrate citizenship or PR.', 'Apply to travel, study, work or immigrate citizenship or PR.', 'assets/images/services/142_(2)4.jpg', 'assets/images/country/142_(1)4.jpg', 'assets/images/country/country-03_icon_img1.jpg', '', 1, 1, 1, '::1', '2020-10-02 10:32:20', NULL, NULL, NULL, 'Live'),
(2, 'USA', NULL, NULL, 'We will help you in every step of the Visa application process.', 'We will help you in every step of the Visa application process.', 'assets/images/services/country-details_01.jpg', 'assets/images/country/country-011.jpg', 'assets/images/country/country-01_icon_img1.jpg', '', 1, 1, 1, '::1', '2020-10-02 11:09:25', NULL, NULL, NULL, 'Live'),
(3, 'United Kingdom', NULL, NULL, 'Millions of the decisions about who has the right to visit or stay.', 'Millions of the decisions about who has the right to visit or stay.', 'assets/images/services/country-details_04.jpg', 'assets/images/country/country-041.jpg', 'assets/images/country/country-04_icon_img1.jpg', '', 1, 1, 1, '::1', '2020-10-02 11:12:29', NULL, NULL, NULL, 'Live'),
(4, 'Australia', NULL, NULL, 'Applicants are of Australia, returning to live citizenship', 'Applicants are of Australia, returning to live citizenship', 'assets/images/services/country-details_02.jpg', 'assets/images/country/country-021.jpg', 'assets/images/country/country-02_icon_img1.jpg', '', 1, 1, 1, '::1', '2020-10-02 11:14:52', NULL, NULL, NULL, 'Live'),
(5, 'New Zealand', NULL, NULL, 'Apply to travel, study, work or immigrate citizenship or PR.', 'Apply to travel, study, work or immigrate citizenship or PR.', 'assets/images/services/1296.jpg', 'assets/images/country/1296_(1).jpg', 'assets/images/country/27119.jpg', '', 1, 0, 1, '::1', '2020-10-02 11:32:59', NULL, NULL, NULL, 'Live'),
(6, 'Singapore', NULL, NULL, 'Apply to travel, study, work or immigrate citizenship or PR.', 'Apply to travel, study, work or immigrate citizenship or PR.', 'assets/images/services/890.jpg', 'assets/images/country/890_(1).jpg', 'assets/images/country/250px-Flag_of_.jpg', '', 1, 0, 1, '::1', '2020-10-02 11:41:51', NULL, NULL, NULL, 'Live'),
(7, 'Poland', 3, NULL, 'Apply to travel, study, work or immigrate citizenship or PR.', 'Apply to travel, study, work or immigrate citizenship or PR.', 'assets/images/services/why-poland-new.jpg', 'assets/images/country/why-poland-new_(1).jpg', 'assets/images/country/polish02_(1).jpg', 'assets/images/pagetitle-bg.jpg', 1, 0, 1, '::1', '2020-10-02 12:56:27', 1, '::1', '2020-10-03 10:07:24', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_country_setting`
--

DROP TABLE IF EXISTS `tbl_country_setting`;
CREATE TABLE IF NOT EXISTS `tbl_country_setting` (
  `country_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_setting_title` varchar(250) NOT NULL,
  `country_setting_description` text NOT NULL,
  `country_setting_desc_2` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`country_setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_country_setting`
--

INSERT INTO `tbl_country_setting` (`country_setting_id`, `country_setting_title`, `country_setting_description`, `country_setting_desc_2`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(2, 'who we are', 'Immigration and citizenship<strong> choose your country!</strong>', 'We are an expert visa consultant focusing on providing quick services to all your travelling needs. Be it a visa, travel insurance, flight ticketing, we cover it all.', NULL, NULL, NULL, 1, '::1', '2020-09-30 11:49:24', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mail_template`
--

DROP TABLE IF EXISTS `tbl_mail_template`;
CREATE TABLE IF NOT EXISTS `tbl_mail_template` (
  `mail_template_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` mediumtext NOT NULL,
  `subject` mediumtext NOT NULL,
  `message` text NOT NULL,
  `fromname` mediumtext NOT NULL,
  `fromemail` varchar(100) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`mail_template_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_mail_template`
--

INSERT INTO `tbl_mail_template` (`mail_template_id`, `type`, `subject`, `message`, `fromname`, `fromemail`, `active`, `del_status`) VALUES
(1, 'contact_us_customer', 'Contac Us', '<h2><br />\r\nHiiii<br />\r\n<strong>{person_name} </strong>Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.</h2>\r\n\r\n<h2><strong>Thank you.</strong></h2>\r\n', 'Canopus Global Education', 'info@designfactoryinteriors.in', 1, 'Live'),
(7, 'contact_us_admin', 'Contac Us', '<h2><br />\r\nName : {contact_name}\r\n<br>Email : {contact_email}\r\n<br>Contact Number : {contact_phone}\r\n<br> Messsage : {contact_message}\r\n', 'Canopus Global Education', 'info@designfactoryinteriors.in', 1, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_service_setting`
--

DROP TABLE IF EXISTS `tbl_service_setting`;
CREATE TABLE IF NOT EXISTS `tbl_service_setting` (
  `service_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `visa_section_desc` varchar(300) NOT NULL,
  `setting_title` varchar(250) NOT NULL,
  `setting_desc` text NOT NULL,
  `visa_section_title` varchar(250) NOT NULL,
  `sub_setting_icon_1` varchar(250) NOT NULL,
  `sub_setting_title_1` varchar(250) NOT NULL,
  `sub_setting_desc_1` text NOT NULL,
  `sub_setting_icon_2` varchar(250) NOT NULL,
  `sub_setting_title_2` varchar(250) NOT NULL,
  `sub_setting_desc_2` text NOT NULL,
  `sub_setting_icon_3` varchar(250) NOT NULL,
  `sub_setting_title_3` varchar(250) NOT NULL,
  `sub_setting_desc_3` text NOT NULL,
  `sub_setting_icon_4` varchar(250) NOT NULL,
  `sub_setting_title_4` varchar(205) NOT NULL,
  `sub_setting_desc_4` text NOT NULL,
  `background_image` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`service_setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_service_setting`
--

INSERT INTO `tbl_service_setting` (`service_setting_id`, `visa_section_desc`, `setting_title`, `setting_desc`, `visa_section_title`, `sub_setting_icon_1`, `sub_setting_title_1`, `sub_setting_desc_1`, `sub_setting_icon_2`, `sub_setting_title_2`, `sub_setting_desc_2`, `sub_setting_icon_3`, `sub_setting_title_3`, `sub_setting_desc_3`, `sub_setting_icon_4`, `sub_setting_title_4`, `sub_setting_desc_4`, `background_image`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'Visa Immigration <strong>  Consultation Services</strong>', 'ABOUT AGENCY', 'An Expert Advisory For Great <br><strong>Value for Visa</strong>', 'WHAT WE DO', 'ti ti-package', 'Registration Online', 'You can register yourself online for our services. Fill up form details and we get back to you.', 'ti ti-package', 'Documentation', 'Our experts suggest documentation submission as per country’s policy and applicant base.', 'ti ti-package', 'We Will Call', 'After reviewing your documents we will get in touch with you for the next personal meeting for guidance.', 'ti ti-package', 'Enjoy Your Freedom', 'And you are all ready to apply. Professionals suggestions are always proven 100% guaranteed.', 'assets/images/pagetitle-bg.jpg', NULL, NULL, NULL, 1, '::1', '2020-10-03 14:03:02', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_side_menu`
--

DROP TABLE IF EXISTS `tbl_side_menu`;
CREATE TABLE IF NOT EXISTS `tbl_side_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(250) NOT NULL,
  `menu_constant` varchar(250) NOT NULL,
  `menu_url` text NOT NULL,
  `ref_menu_id` int(11) DEFAULT NULL,
  `menu_icon` varchar(250) DEFAULT NULL,
  `menu_order_no` int(11) DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_side_menu`
--

INSERT INTO `tbl_side_menu` (`menu_id`, `menu_name`, `menu_constant`, `menu_url`, `ref_menu_id`, `menu_icon`, `menu_order_no`, `del_status`) VALUES
(1, 'Dashboard', 'DASHBOARD', 'admin/Dashboard', NULL, 'fal fa-info-circle', 1, 'Live'),
(2, 'Home', 'HOME', '#', NULL, 'fal fa-home-alt', 2, 'Live'),
(3, 'About', 'ABOUT', 'admin/About', NULL, 'fal fa-user', 3, 'Live'),
(5, 'Contact', 'CONTACT', 'admin/Contact', NULL, 'fal fa-address-book', 8, 'Live'),
(6, 'Mail', 'MAIL', '#', NULL, 'fal fa-envelope', 9, 'Live'),
(7, 'Setting', 'FOOTER', '#', NULL, 'fal fa-sliders-h', 10, 'Live'),
(8, 'User', 'USER', 'admin/Dashboard/user', NULL, 'fal fa-user', 11, 'Live'),
(9, 'Slider', 'SLIDER', 'admin/Slider', 2, NULL, 1, 'Live'),
(12, 'Testimonial', 'TESTIMONIAL', 'admin/Home/testimonial', 2, NULL, 4, 'Live'),
(18, 'SMTP Setting', 'SMTP_SETTING', 'admin/Mail', 6, NULL, 1, 'Live'),
(19, 'Newsletter', 'NEWSLETTER', 'admin/Mail/newsletter', 6, NULL, 2, 'Deleted'),
(20, 'Contact Us', 'CONTACT_US', 'admin/Mail/contact_us', 6, NULL, 3, 'Live'),
(21, 'Mail Template', 'MAIL_TEMPLATE', 'admin/Mail/mailTemplate', 6, NULL, 4, 'Live'),
(22, 'Footer Setting', 'FOOTER_SETTING', 'admin/Footer', 7, NULL, 1, 'Live'),
(23, 'Privacy Policy <br>Terms & Condition', 'PRIVACY_POLICY', 'admin/Footer/privacyPolicy', 7, NULL, 2, 'Live'),
(26, 'Visa category Setting', 'VISA_CATEGORY_SETTING', 'admin/Home/visaCategorySetting', 2, NULL, 3, 'Live'),
(27, 'Testimonial Setting', 'TESTIMONIAL_SETTING', 'admin/Home/testimonialSetting', 2, NULL, 5, 'Live'),
(29, 'Coaching Preparation Setting', 'VISA_PREPARATION_SETTING', 'admin/Home/coachingPreparationSetting\r\n', 2, NULL, 7, 'Live'),
(30, 'Coaching', 'COACHING_PAGE', '#', NULL, 'fal fa-file-code', 4, 'Live'),
(31, 'Manage Coaching Page', 'COACHING_MAIN_PAGE', 'admin/Coaching_page', 30, NULL, 1, 'Live'),
(32, 'Manage Coaching Page Data', 'COACHING_PAGE_DATA', 'admin/Coaching_page/coachingPageData', 30, NULL, 2, 'Live'),
(33, 'Service', 'SERVICE', '#', NULL, 'fal fa-cogs', 7, 'Live'),
(34, 'Service Setting', 'SERVICE_SETTING', 'admin/Service/addEditServiceSetting', 33, NULL, 3, 'Live'),
(35, 'Countries', 'COUNTRY_PAGE', '#', NULL, 'fal fa-map-marked-alt\r\n', 5, 'Live'),
(36, 'Manage Country Page', 'COUNTRY_MAIN_PAGE', 'admin/Country_page', 35, NULL, 1, 'Live'),
(37, 'Country Page Data', 'COUNTRY_PAGE_DATA', 'admin/Country_page/countryPageData', 35, NULL, 2, 'Live'),
(38, 'Visa Page', 'VISA_PAGE', 'admin/Visa_page', NULL, 'fal fa-passport', 6, 'Live'),
(39, 'Statistic', 'STATISTIC', 'admin/Footer/statistic', 7, NULL, 3, 'Live'),
(40, 'Country Setting', 'COUNTRY_SETTING', 'admin/Home/CountrySetting', 2, NULL, 2, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_side_menu_rights`
--

DROP TABLE IF EXISTS `tbl_side_menu_rights`;
CREATE TABLE IF NOT EXISTS `tbl_side_menu_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_user_id` int(11) NOT NULL,
  `ref_menu_id` int(11) NOT NULL,
  `full_access` tinyint(4) NOT NULL,
  `view_right` tinyint(4) NOT NULL,
  `add_right` tinyint(4) NOT NULL,
  `edit_right` tinyint(4) NOT NULL,
  `delete_right` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_statistic`
--

DROP TABLE IF EXISTS `tbl_statistic`;
CREATE TABLE IF NOT EXISTS `tbl_statistic` (
  `statistic_id` int(11) NOT NULL AUTO_INCREMENT,
  `exp_icon` varchar(250) NOT NULL,
  `exp_title` varchar(250) NOT NULL,
  `exp_desc` text NOT NULL,
  `cus_icon` varchar(250) NOT NULL,
  `cus_title` varchar(250) NOT NULL,
  `cus_desc` text NOT NULL,
  `new_cus_icon` varchar(250) NOT NULL,
  `new_cus_title` varchar(250) NOT NULL,
  `new_cus_desc` text NOT NULL,
  `description` text NOT NULL,
  `main_title` varchar(250) NOT NULL,
  `main_desc` longtext NOT NULL,
  `main_image` text NOT NULL,
  `sub_desc` text NOT NULL,
  `sub_title` varchar(250) NOT NULL,
  `sub_desc_2` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`statistic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_statistic`
--

INSERT INTO `tbl_statistic` (`statistic_id`, `exp_icon`, `exp_title`, `exp_desc`, `cus_icon`, `cus_title`, `cus_desc`, `new_cus_icon`, `new_cus_title`, `new_cus_desc`, `description`, `main_title`, `main_desc`, `main_image`, `sub_desc`, `sub_title`, `sub_desc_2`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'fa fa-calendar', '9', 'Years Of Experiencec', 'fa fa-smile-o', '1490', 'Happy Members', 'fa fa-user-plus', '850', 'New Members', 'Canopus Global Education is one of the largest and most trusted names in Overseas Education & Immigration industry today..', 'Who We Are?', 'Established in Surat, Gujarat, India in 2009, Canopus Global Education has gone on to achieve many great feats and help thousands of people fulfill their foreign education and ambitions. What started as a small company, is now a renowned name. Canopus Global Education was founded by Mr.Bhargav Gauswami, a well-trained and experienced expert with extensive knowledge and strategic alliances and a vision to be recognized as a leading overseas education and immigration service provider catering to the needs of a myriad of people globally.', 'assets/images/single-img-12.jpg', ' ', 'Why Choose Us?', 'We strive to establish a global presence so that we can help you achieve your goals through transparent, ethical, and extensive knowledge of the overseas education & immigration laws and regulations that guide each country of interest. Consisting of a team of certified and licensed consultants and legal advisors, we make sure your overseas education application & immigration process is completely hassle-free and successful. We excel in providing a wide range of services including:', NULL, NULL, NULL, 1, '::1', '2020-10-03 11:50:42', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_statistic_item`
--

DROP TABLE IF EXISTS `tbl_statistic_item`;
CREATE TABLE IF NOT EXISTS `tbl_statistic_item` (
  `statistic_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_statistic_id` int(11) NOT NULL,
  `statistic_item_title` varchar(250) NOT NULL,
  `statistic_item_desc` text NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`statistic_item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_statistic_item`
--

INSERT INTO `tbl_statistic_item` (`statistic_item_id`, `ref_statistic_id`, `statistic_item_title`, `statistic_item_desc`, `is_active`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(10, 1, 'Expert VISA Consultation', 'Canopus Global Education  providers of expert VISA Consultancy services and advice for Education, Employment, Immigration and Travel VISA to USA, UK, Canada, Australia, New Zealand and many other countries.', 1, 1, '::1', '2020-10-03 11:50:42', NULL, NULL, NULL, 'Live'),
(11, 1, 'Continuous Support', 'We take care that our clients do not leave unsatisfied. That is all because of the staggering support we provide in all the International “services” desired. It’s not just our words, view people saying so!', 1, 1, '::1', '2020-10-03 11:50:42', NULL, NULL, NULL, 'Live'),
(12, 1, 'We Providing', 'Canopus Global Education efficient service providing quality makes it highly reliable.', 1, 1, '::1', '2020-10-03 11:50:42', NULL, NULL, NULL, 'Live'),
(13, 1, 'Perfect Documentation', 'We pay keen attention to Perfect Documentation!', 1, 1, '::1', '2020-10-03 11:50:42', NULL, NULL, NULL, 'Live'),
(14, 1, '9+ years of experience', 'We are the pioneers of successful VISA consultation continually benefiting the customers for more than 8 years now.', 1, 1, '::1', '2020-10-03 11:50:42', NULL, NULL, NULL, 'Live'),
(15, 1, 'Client Transparency', 'Unlike others, we believe in keeping Client Transparency.', 1, 1, '::1', '2020-10-03 11:50:42', NULL, NULL, NULL, 'Live'),
(16, 1, 'Better understand of requirement', 'A very good understanding of the queries and assistance on every move, makes the clients contended of the services catered.', 1, 1, '::1', '2020-10-03 11:50:42', NULL, NULL, NULL, 'Live'),
(17, 1, 'Fast and Reliable', 'Fast and Reliable aid is assured here at Agile Consultancy!', 1, 1, '::1', '2020-10-03 11:50:42', NULL, NULL, NULL, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_testimonial`
--

DROP TABLE IF EXISTS `tbl_testimonial`;
CREATE TABLE IF NOT EXISTS `tbl_testimonial` (
  `testimonial_id` int(11) NOT NULL AUTO_INCREMENT,
  `testimonial_name` varchar(250) NOT NULL,
  `testimonial_description` text NOT NULL,
  `testimonial_designation` varchar(250) DEFAULT NULL,
  `testimonial_image` text NOT NULL,
  `testimonial_rating` int(11) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`testimonial_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_testimonial`
--

INSERT INTO `tbl_testimonial` (`testimonial_id`, `testimonial_name`, `testimonial_description`, `testimonial_designation`, `testimonial_image`, `testimonial_rating`, `is_active`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'RAshley Foster', 'I really would like to appreciate Tripzia and the entire team, especially Ms Anandias Alex for helping me get my student visa for the Brunel University. She helped me all way to find the University & right course. She was there for me through the entire process.', '(CEO 7 Founder)', 'assets/images/testimonial/nav-01.jpg', 5, 1, 1, '::1', '2020-09-25 07:18:19', 1, '::1', '2020-09-25 10:30:18', 'Live'),
(2, 'Alan Sears', 'I really would like to appreciate Tripzia and the entire team, especially Ms Anandias Alex for helping me get my student visa for the Brunel University. She helped me all way to find the University & right course. She was there for me through the entire process.', '(CEO 7 Founder)', 'assets/images/testimonial/nav-021.jpg', 4, 1, 1, '::1', '2020-09-25 09:52:23', NULL, NULL, NULL, 'Live'),
(3, 'Raymon Myers', 'I really would like to appreciate Tripzia and the entire team, especially Ms Anandias Alex for helping me get my student visa for the Brunel University. She helped me all way to find the University & right course. She was there for me through the entire process.', '(CEO 7 Founder)', 'assets/images/testimonial/nav-031.jpg', 5, 1, 1, '::1', '2020-09-25 09:52:54', NULL, NULL, NULL, 'Live'),
(4, 'Jhon Martin', 'I really would like to appreciate Tripzia and the entire team, especially Ms Anandias Alex for helping me get my student visa for the Brunel University. She helped me all way to find the University & right course. She was there for me through the entire process.', '(CEO 7 Founder)', 'assets/images/testimonial/nav-041.jpg', 5, 1, 1, '::1', '2020-09-25 09:53:24', NULL, NULL, NULL, 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_testimonial_setting`
--

DROP TABLE IF EXISTS `tbl_testimonial_setting`;
CREATE TABLE IF NOT EXISTS `tbl_testimonial_setting` (
  `testimonial_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `testimonial_setting_title` varchar(250) NOT NULL,
  `testimonial_setting_desc` text NOT NULL,
  `testimonial_setting_sub_desc` text NOT NULL,
  `sub_title_1` varchar(250) NOT NULL,
  `sub_desc_1` text NOT NULL,
  `sub_title_2` varchar(250) NOT NULL,
  `sub_desc_2` text NOT NULL,
  `sub_title_3` varchar(250) NOT NULL,
  `sub_desc_3` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`testimonial_setting_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_testimonial_setting`
--

INSERT INTO `tbl_testimonial_setting` (`testimonial_setting_id`, `testimonial_setting_title`, `testimonial_setting_desc`, `testimonial_setting_sub_desc`, `sub_title_1`, `sub_desc_1`, `sub_title_2`, `sub_desc_2`, `sub_title_3`, `sub_desc_3`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'ABOUT AGENCY', 'Immigration Services From <strong>Experienced Agents.</strong>', 'Lorem Ipsum is simply dummy text of the printing ypesetting industry.Professor at Hampden-Sydney been tndustry dard dummy text ever since the 1500s, when aknown contary tonter.', 'We modify whole system', 'To guard, Avoid Revealing Too Much, Apply for a Provisional Patent, Trademark.', 'Safeguard for Your Bussiness', 'To guard, Avoid Revealing Too Much, Apply for a Provisional Patent, Trademark.', 'Beneficial Strategies', 'To guard, Avoid Revealing Too Much, Apply for a Provisional Patent, Trademark.', NULL, NULL, NULL, 1, '::1', '2020-09-25 09:40:04', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_visa_category_setting`
--

DROP TABLE IF EXISTS `tbl_visa_category_setting`;
CREATE TABLE IF NOT EXISTS `tbl_visa_category_setting` (
  `visa_category_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `visa_category_setting_title` varchar(250) NOT NULL,
  `visa_category_setting_description` text NOT NULL,
  `visa_category_setting_footer` text NOT NULL,
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`visa_category_setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_visa_category_setting`
--

INSERT INTO `tbl_visa_category_setting` (`visa_category_setting_id`, `visa_category_setting_title`, `visa_category_setting_description`, `visa_category_setting_footer`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'what we do', 'We Provide Experts Create Great<br> Value for<strong> Visa Categories</strong>', '<span  class=\"font-weight-normal\">Don’t Hesitate, Contact us for Better Help and Services.</span> <u><a class=\"cmt-textcolor-skincolor\" href=\"{base_url}\">Explore all visa Categories.</a></u>', NULL, NULL, NULL, 1, '::1', '2020-09-30 10:18:20', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_visa_page`
--

DROP TABLE IF EXISTS `tbl_visa_page`;
CREATE TABLE IF NOT EXISTS `tbl_visa_page` (
  `visa_page_id` int(11) NOT NULL AUTO_INCREMENT,
  `visa_page_name` varchar(250) NOT NULL,
  `visa_page_short_name` varchar(250) DEFAULT NULL,
  `visa_page_desc` varchar(300) NOT NULL,
  `visa_page_content` text NOT NULL,
  `visa_page_image` text NOT NULL,
  `service_page_image` text NOT NULL,
  `icon` varchar(250) NOT NULL,
  `background_image` text NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`visa_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_visa_page`
--

INSERT INTO `tbl_visa_page` (`visa_page_id`, `visa_page_name`, `visa_page_short_name`, `visa_page_desc`, `visa_page_content`, `visa_page_image`, `service_page_image`, `icon`, `background_image`, `is_active`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, 'Visitor Visa', NULL, 'To work refers to manage systems used to ensure that work is done nicely.', '<strong>For the people willing to travel the world and want to satisfy the deep desire to travel, we assist through their entire voyage.</strong>&nbsp;The visitor&rsquo;s VISA success ratio is high. Visitors may also be needed to meet certain character and health ire requirements. Contact SWEC for more information in detail.&nbsp;<strong>Contact today and get the whole information in a brief manner.</strong>\r\n<p><strong>Visitor Visa works for the person who wishes to visit any country for a short time or long time period or to meet the loved ones e.g. Relatives, Family and Friends</strong>&nbsp;and if the person wants to see the country, they can visit the country. We are an organization providing solutions to the aspirants according to their individual requirements. In other words, we provide tailor-made solutions; this is possible because of our personalized approach to understand the financial or personal profiles of the aspirants.</p>\r\n\r\n<p>You can trust and know SWEC as the one stop solution for your all international visit. We mainly concentrate on activities in assisting applicants to make the right choice in selection of country.&nbsp;<strong>We provide solutions for the aspirants who are seeking to pursue an abroad visit as per their interests and aptitudes.</strong>&nbsp;Currently, we are offering our services for applicants to visit the United Kingdom (U.K), United States (U.S.A), Australia, Canada, Singapore, Malaysia, Europe and New Zealand.</p>\r\n\r\n<p>In other words, we provide tailor-made solutions; this is possible because of our personalized approach to understand the financial or personal profiles of the aspirants.</p>\r\n\r\n<p>We are proud to say that we have been doing a noble job by helping the applicant in designing their visit.<strong>&nbsp;In this type of visa, the purpose of visiting the country are tourist, business visitor, medical treatment, visitor in transit, sports visitor, entertainment visitor, etc. In this type of visa, the person has granted the visa for 6 months, 1 year, 2 years, 5 years or 10 years. In which he/she has to decide for how long the visit will be. The person cannot stay there more than the visa duration.</strong>&nbsp;After completion of the validity of the visa, the person must have to come back to his/her home country.<strong>&nbsp;For that depending on the purpose, various types of visa forms must be filled by the person and submit that form with the other required documents.</strong></p>\r\n', 'assets/images/services/services-details_041.jpg', 'assets/images/services/services-02-370x2901.jpg', 'flaticon-passport-14', 'assets/images/pagetitle-bg.jpg', 1, 1, '::1', '2020-10-03 10:59:08', 1, '::1', '2020-10-03 11:06:34', 'Live'),
(2, 'Student Visa', NULL, 'For the perception & better career opportunities for the students.', 'For some, Studying overseas is a trend now. But going deeper into the subject, globalization is affecting the way our country may advance. As a part of globalization, Education has its own and rather prime importance.<br />\r\n<br />\r\nToday it is extremely necessary for people to go out of their own world and explore a modern environment that is served by places of the foreign world. Understanding diverse cultures, improving language skills, personal development and various experiences of life is all you get through studies abroad.\r\n<p>Different countries mean different cultures and different education systems. Once you get acquainted with other systems, you are ought to progress in a way the world is progressing.</p>\r\n\r\n<p>Bringing home the advanced knowledge imparted by skilled professors in various ends on the globe will indirectly make you rich with values. It improves one&rsquo;s expertise in the field of liking. A fruitful career comes with the very first step taken in the correct direction!</p>\r\n\r\n<p>Canopus Global Education&nbsp;is one such overseas consulting firm that provides the best guidance to the aspirants. It caters you with proper knowledge of overseas student visa.</p>\r\n\r\n<p>From the outset of deciding where to go, which college to select in respect to the course one wants to pursue till your visas in your hands, &ldquo;Canopus Global Education&rdquo; directs you to the perfect direction.</p>\r\n', 'assets/images/services/services-details_041.jpg', 'assets/images/services/services-02-370x2901.jpg', 'flaticon-graduation-hat', 'assets/images/pagetitle-bg.jpg', 1, 1, '::1', '2020-10-03 10:59:08', 1, '::1', '2020-10-03 11:47:31', 'Live'),
(3, 'Immigration Visa', NULL, 'We help you to have different types of visa according to your needs anywhere in the world', 'Migrating to distant countries and then settling there for a lavish future is now a feasible dream.This is the result of continuous industrialization and thus globalization. It may not come to your notice but such factors do affect each and everyone&rsquo;s lives directly or indirectly.<br />\r\n&nbsp;\r\n<p>Yes, it is definitely an important aspect of the growing economies.</p>\r\n\r\n<p>Better jobs, better income, and better locality is all that drags people to distant countries.Superior educational opportunities for kids well-set homes to buy or on rent, each and every option is a plus there.</p>\r\n\r\n<p>Explore the different provinces, experience newer life and adapt the best.Keep pace with the changing world and live in exceptional conditions.</p>\r\n\r\n<p>One can undoubtedly benefit self and the future generations while living in a foreign country that suits your living and the future generation&rsquo;s growth! Canopus Global Education stays up-to-date with all the latest changes in various governments and democracies.</p>\r\n\r\n<p>Come to us with your big decision of migrating to a distant nation and here we are to help you decide where to go depending on your abilities, qualifications, and interests.</p>\r\n\r\n<p>From application to visa to the settlement, we will be for you at your service.</p>\r\n', 'assets/images/services/services-details_041.jpg', 'assets/images/services/services-02-370x2901.jpg', 'flaticon-visa-1', 'assets/images/pagetitle-bg.jpg', 1, 1, '::1', '2020-10-03 10:59:08', 1, '::1', '2020-10-03 11:57:38', 'Live'),
(4, 'Dependent Visa', NULL, 'Connecting FAMILIES beyond borders', '<p>Canopus Global Education&nbsp;provides VISA services for Dependent VISAS. A person is dependent on another person if the person is wholly or substantially reliant on the other person for financial support to fulfill basic needs like food, clothing and shelter, Incase person willing to travel/transfer abroad comes under the Dependent VISA category.</p>\r\n\r\n<p><strong>Our service for a Spouse visa, Dependent visa, and Family visa and Child visa comes under the category of Dependent visa. The application is simple and success depends on the proper documentation and genuine intent.</strong></p>\r\n\r\n<p><strong>Family visas are split into several sub-categories. With new changes to the Immigration rules on a regular basis, sometimes it becomes tiresome to live together for Immigrants families.</strong></p>\r\n\r\n<p>Canopus Global Education&nbsp;guides you through proper information, guidelines, and assistance. SWEC assists you and ensures that you meet all the Immigration requirements.</p>\r\n\r\n<p>&nbsp;</p>\r\n<p>Spouse Visas for Canada (Student Dependent & Post Study Work Dependent)</p>\r\n\r\n<ul class=\"cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5\">\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Student Dependent:</strong>\r\n            An international student may bring his or her dependents to Canada by one of two routes.\r\n            <ul>\r\n                <li>One is for the student and dependent family members to submit their visa applications concurrently.</li>\r\n                <li>Another option is for the student to wait until he/she has received a study permit, and then submit applications for his or her accompanying dependents.</li>\r\n            </ul>\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\">Eligibility Criteria:</strong> \r\n            <ul>\r\n                <li>He or She must be at least 18 years old.</li>\r\n                <li>Must have a valid passport.</li>\r\n                <li>If Spouse in Canada (Sponsor) is on study permit:\r\n                    <ul>\r\n                        <li>He or She must be enrolled in a full time program at a Designated Learning Institute or Government recognized public post-secondary institution in Canada.</li>\r\n                    </ul>\r\n                </li>\r\n                <li>If Spouse in Canada (Sponsor) is on work permit:\r\n                    <ul>\r\n                        <li>He or She must be working full time with any employer in Canada under NOC Category 0, A and B.</li>\r\n                    </ul>\r\n                </li>\r\n                <li>Documents showing genuine relationships and financial stability.</li>\r\n                <li>A Medical Report from Panel Physicians approved by Immigration, Refugees and Citizenship Canada (IRCC)</li>\r\n            </ul>\r\n        </span>\r\n    </li>\r\n    <li>\r\n        <i class=\"cmt-textcolor-skincolor fa fa-check-circle\"></i>\r\n        <span class=\"cmt-list-li-content\">\r\n            <strong class=\"mr-2 cmt-textcolor-skincolor\"> Government Fees:</strong>\r\n            <ul>\r\n                <li>Visa Processing fees: Due at file submission CAD$ 255 (Per Person)</li>\r\n                <li>Biometric Fees : CAD$ 85 (Per Person)</li>\r\n            </ul>\r\n        </span>\r\n    </li>\r\n</ul>\r\n\r\n', 'assets/images/services/services-details_041.jpg', 'assets/images/services/services-02-370x2901.jpg', 'flaticon-passport-6', 'assets/images/pagetitle-bg.jpg', 1, 1, '::1', '2020-10-03 10:59:08', 1, '::1', '2020-10-03 12:07:08', 'Live');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_visa_page_document`
--

DROP TABLE IF EXISTS `tbl_visa_page_document`;
CREATE TABLE IF NOT EXISTS `tbl_visa_page_document` (
  `visa_page_document_id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_visa_page_id` int(11) NOT NULL,
  `document` text,
  `document_name` varchar(250) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `InsUser` int(11) DEFAULT NULL,
  `InsTerminal` varchar(250) DEFAULT NULL,
  `InsDateTime` datetime DEFAULT NULL,
  `UpdUser` int(11) DEFAULT NULL,
  `UpdTerminal` varchar(250) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(50) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`visa_page_document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_information`
--

DROP TABLE IF EXISTS `user_information`;
CREATE TABLE IF NOT EXISTS `user_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(100) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` text NOT NULL,
  `password` text NOT NULL,
  `address` varchar(255) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `details` varchar(255) NOT NULL,
  `role` varchar(20) DEFAULT NULL,
  `InsUser` varchar(200) DEFAULT NULL,
  `InsTerminal` varchar(100) NOT NULL,
  `InsDateTime` datetime NOT NULL,
  `UpdUser` varchar(200) DEFAULT NULL,
  `UpdTerminal` varchar(100) DEFAULT NULL,
  `UpdDateTime` datetime DEFAULT NULL,
  `del_status` varchar(20) NOT NULL DEFAULT 'Live',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_information`
--

INSERT INTO `user_information` (`id`, `user_id`, `user_name`, `user_email`, `password`, `address`, `mobile`, `details`, `role`, `InsUser`, `InsTerminal`, `InsDateTime`, `UpdUser`, `UpdTerminal`, `UpdDateTime`, `del_status`) VALUES
(1, '1', 'Admin', 'admin@weborative.com', 'e1b4755403710e0deb7aa5d45e43996d', 'Surat', '1234567980', '', 'Admin', '1', '::1', '2020-06-20 00:00:00', '1', '::1', '2020-10-01 07:38:18', 'Live');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
