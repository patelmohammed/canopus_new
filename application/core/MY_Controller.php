<?php

Class My_Controller extends CI_Controller {

    public $page_id;
    public $page_title;
    public $menu_id;
    public $sub_menu_id;
    public $add_message;
    public $user_id;
    public $role;
    public $meta_title;
    public $meta_desc;
    public $meta_key;

    public function __construct() {
        parent::__construct();
        $this->page_id = '';
        $this->page_title = '';
        $this->menu_id = '';
        $this->sub_menu_id = '';
        $this->meta_desc = '';
        $this->meta_title = '';
        $this->meta_key = '';
        $this->user_id = $this->session->userdata('user_id');
        $this->role = $this->session->userdata('role');
        $this->load->model('Common_model');
        $smtp_setting = $this->Common_model->getDataById2('smtp_setting', 'del_status', 'Live', 'Live');
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => $smtp_setting->smtp_host,
            'smtp_port' => $smtp_setting->smtp_port,
            'smtp_user' => $smtp_setting->smtp_user,
            'smtp_pass' => $smtp_setting->smtp_pass,
            'smtp_crypto' => $smtp_setting->smtp_crypto,
            'mailtype' => 'html',
            'smtp_timeout' => '100',
            'charset' => 'UTF-8',
            'wordwrap' => TRUE
        );
        $this->load->library('email', $config);
    }

    public function load_view($view, $data, $flag = true) {
        $data['footer_data'] = $this->Common_model->getDataByIdStatus('footer_setting', 'del_status', 'Live');
        $this->meta_desc = $this->meta_desc == '' ? $data['footer_data']->meta_desc : $this->meta_desc;
        $this->meta_title = $this->meta_title == '' ? $data['footer_data']->meta_title : $this->meta_title;
        $this->meta_key = $this->meta_key == '' ? $data['footer_data']->meta_key : $this->meta_key;
        $data['all_page'] = $this->Common_model->getAllCoachingPage();
        $data['all_country_page'] = $this->Common_model->getAllCountry();
        $data['all_visa_page'] = $this->Common_model->getAllVisaPage();
        $data['all_blog'] = $this->Common_model->getAllBlogPage();
        if ($flag) {
            $this->load->view('home/header', $data);
        }
        $this->load->view($view, $data);
        if ($flag) {
            $this->load->view('home/footer', $data);
        }
    }

    public function load_admin_view($view, $data, $flag = true) {
        $data['add_message'] = $this->session->flashdata('message');
        if ($flag) {
            $this->load->view('admin/home/header', $data);
            $this->load->view('admin/home/side_menu', $data);
        }
        $this->load->view($view, $data);
        if ($flag) {
            $this->load->view('admin/home/footer', $data);
        }
    }

    public function is_login() {
        $dat = $this->user_id;
        if (!empty($dat)) {
            return true;
        }
        return false;
    }

    public function _show_message($message, $type = 'message') {
        $message = 'toastr["' . $type . '"]("' . $message . '");';
        $this->session->set_flashdata('message', $message);
    }

}
