<?php

class Visa_page extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'VISA_PAGE';
        $this->load->model('Common_model');
    }

    public function index() {
        $this->menu_id = 'VISA_PAGE';
        $data = [];
        $data['menu_rights'] = $this->Common_model->get_menu_rights('VISA_PAGE');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('VISA_PAGE', 'VIEW');
        $view = 'admin/visa_page/visa_page';
        $data['visa_page_data'] = $this->Common_model->geAlldata('tbl_visa_page');
        $this->page_title = 'VISA PAGE';
        $this->load_admin_view($view, $data);
    }

    public function addEditVisaPage($encrypted_id = "") {
        $this->menu_id = 'VISA_PAGE';
        $id = $encrypted_id;
        if ($this->input->post()) {

            $document_path = 'assets/document/';
            if (!is_dir($document_path)) {
                if (!mkdir($document_path, 0777, true)) {
                    die('Not Created');
                }
            }

            $upload_document = $_FILES['download_document'];
            $document_name = $this->input->post('document_name');
            $is_active_document = $this->input->post('is_active_document');
            $hidden_download_document = $this->input->post('hidden_download_document');
            if (isset($upload_document) && !empty($upload_document)) {
                foreach ($upload_document['name'] as $key => $value) {
                    if (isset($value) && !empty($value)) {
                        $_FILES['download_document']['name'] = $upload_document['name'][$key];
                        $_FILES['download_document']['type'] = $upload_document['type'][$key];
                        $_FILES['download_document']['tmp_name'] = $upload_document['tmp_name'][$key];
                        $_FILES['download_document']['error'] = $upload_document['error'][$key];
                        $_FILES['download_document']['size'] = $upload_document['size'][$key];

                        $config['upload_path'] = $document_path;
                        $config['allowed_types'] = 'PDF|pdf';
                        $config['max_size'] = "*";
                        $config['max_width'] = "*";
                        $config['max_height'] = "*";
                        $config['encrypt_name'] = FALSE;

                        $this->load->library('upload', $config);
                        if (!$this->upload->do_upload('download_document')) {
                            $error = array('error' => $this->upload->display_errors());
                            $this->_show_message((isset($error['error']) && !empty($error['error']) ? $error['error'] : 'Somethig wrong'), "error");
                            redirect('admin/Visa_page');
                        } else {
                            $image = $this->upload->data();
                            $document = array();
                            $document['file_path'] = $document_path . $image['file_name'];
                            $document['is_active'] = (isset($is_active_document[$key]) && !empty($is_active_document[$key]) ? ($is_active_document[$key] == 'on' ? 1 : 0) : 0);
                            $document['document_name'] = (isset($document_name[$key]) && !empty($document_name[$key]) ? $document_name[$key] : 0);
                            $document_data_info[] = $document;
                        }
                    } else {
                        $document = array();
                        $document['is_active'] = (isset($is_active_document[$key]) && !empty($is_active_document[$key]) ? ($is_active_document[$key] == 'on' ? 1 : 0) : 0);
                        $document['file_path'] = $hidden_download_document[$key];
                        $document['document_name'] = (isset($document_name[$key]) && !empty($document_name[$key]) ? $document_name[$key] : 0);
                        $document_data_info[] = $document;
                    }
                }
            } else {
                if (isset($hidden_download_document) && !empty($hidden_download_document)) {
                    foreach ($hidden_download_document as $key2 => $value2) {
                        $document = array();
                        $document['is_active'] = (isset($is_active_document[$key2]) && !empty($is_active_document[$key2]) ? ($is_active_document[$key2] == 'on' ? 1 : 0) : 0);
                        $document['file_path'] = $value2;
                        $document['document_name'] = (isset($document_name[$key2]) && !empty($document_name[$key2]) ? $document_name[$key2] : 0);
                        $document_data_info[] = $document;
                    }
                }
            }

            unset($this->upload);
            $new_path = 'assets/images/services/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['visa_page_image']['name']) && isset($_FILES['visa_page_image']['name'])) {
                if ($_FILES['visa_page_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('visa_page_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Visa_page');
                    } else {
                        $old_image = $this->input->post('hidden_visa_page_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $new_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_visa_page_image');
                }
            } else {
                $image_url = $this->input->post('hidden_visa_page_image');
            }
            $insert_data['visa_page_image'] = $image_url;

            if (!empty($_FILES['service_page_image']['name']) && isset($_FILES['service_page_image']['name'])) {
                if ($_FILES['service_page_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('service_page_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Visa_page');
                    } else {
                        $old_image = $this->input->post('hidden_service_page_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $new_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_service_page_image');
                }
            } else {
                $image_url = $this->input->post('hidden_service_page_image');
            }
            $insert_data['service_page_image'] = $image_url;

            unset($this->upload);
            $new_path = 'assets/images/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['background_image']['name']) && isset($_FILES['background_image']['name'])) {
                if ($_FILES['background_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('background_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Coaching_page');
                    } else {
                        $old_image = $this->input->post('hidden_background_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
//                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $background_image = $new_path . $image['file_name'];
                    }
                } else {
                    $background_image = $this->input->post('hidden_background_image');
                }
            } else {
                $background_image = $this->input->post('hidden_background_image');
            }
            $insert_data['background_image'] = $background_image;

            $insert_data['visa_page_name'] = $this->input->post('visa_page_name');
            $insert_data['icon'] = $this->input->post('icon');
            $insert_data['visa_page_desc'] = $this->input->post('visa_page_desc');
            $insert_data['visa_page_content'] = $this->input->post('visa_page_content');
            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            $insert_data['meta_title'] = $this->input->post('meta_title');
            $insert_data['meta_desc'] = $this->input->post('meta_desc');
            $insert_data['meta_key'] = $this->input->post('meta_key');

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $page_id = $this->Common_model->InsertInformation($insert_data, 'tbl_visa_page');
                if (isset($page_id) && !empty($page_id)) {
                    $this->Common_model->insertPageDocument($page_id, $document_data_info);

                    $slug_data = array();
                    $slug_data['slug'] = createSlug($this->input->post('slug'));
                    $slug_data['ref_table_name'] = 'tbl_visa_page';
                    $slug_data['ref_primary_id'] = $page_id;
                    $slug_id = $this->Common_model->insertUpdateSlug($slug_data);
                }
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'visa_page_id', $id, 'tbl_visa_page');

                $slug_data = array();
                $slug_data['slug'] = createSlug($this->input->post('slug'));
                $slug_data['ref_table_name'] = 'tbl_visa_page';
                $slug_data['ref_primary_id'] = $id;
                $this->Common_model->insertUpdateSlug($slug_data, $this->input->post('hidden_slug_id'));

                $this->Common_model->deleteRecord($id, 'tbl_visa_page_document', 'ref_visa_page_id');
                $this->Common_model->insertPageDocument($id, $document_data_info);
            }
            redirect('admin/Visa_page');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('VISA_PAGE', 'ADD');
                $data = [];
                $view = 'admin/visa_page/addVisaPage';
                $this->page_title = 'VISA PAGE';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('VISA_PAGE', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['visa_page_data'] = $this->Common_model->getDataById2('tbl_visa_page', 'visa_page_id', $id, 'Live');
                $data['slug_data'] = $this->Common_model->getDataById2('tbl_slug', 'ref_primary_id', $id, '', 'ref_table_name', 'tbl_visa_page');
                $data['download_document'] = $this->Common_model->geAlldataById('tbl_visa_page_document', 'ref_visa_page_id', $id, '');
                $view = 'admin/visa_page/editVisaPage';
                $this->page_title = 'VISA PAGE';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkVisaPage($category_id = '') {
        if ($category_id != '') {
            $catgory_name = $this->Common_model->getVisaPage($category_id);
            $respone = $this->Common_model->checkVisaPage($this->input->get('visa_page_name'), $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Common_model->checkVisaPage($this->input->get('visa_page_name'));
            echo $response;
            die;
        }
    }

    public function checkVisaPageShortName($category_id = '') {
        if ($category_id != '') {
            $catgory_name = $this->Common_model->getVisaPageShortName($category_id);
            $respone = $this->Common_model->checkVisaPageShortName($this->input->get('visa_page_short_name'), $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Common_model->checkVisaPageShortName($this->input->get('visa_page_short_name'));
            echo $response;
            die;
        }
    }

    public function deactiveVisaPage() {
        $update_data = $data = array();
        $visa_page_id = $this->input->post('visa_page_id');
        $update_data['is_active'] = 2;
        $response = $this->Common_model->updateInformation2($update_data, 'visa_page_id', $visa_page_id, 'tbl_visa_page', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function activeVisaPage() {
        $update_data = $data = array();
        $visa_page_id = $this->input->post('visa_page_id');
        $update_data['is_active'] = 1;
        $response = $this->Common_model->updateInformation2($update_data, 'visa_page_id', $visa_page_id, 'tbl_visa_page', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function deleteVisaPage() {
        $this->Common_model->check_menu_access('VISA_PAGE', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('visa_page_id', $id);
            $this->db->update('tbl_visa_page');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function visaItemPageById() {
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $data['visa_item_page_data'] = $this->Common_model->getDataById2('tbl_visa_item_page', 'visa_item_page_id', $id, 'Live');
            if (isset($data['visa_item_page_data']) && !empty($data['visa_item_page_data'])) {
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

}
