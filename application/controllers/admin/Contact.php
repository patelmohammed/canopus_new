<?php

class Contact extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'CONTACT';
        $this->load->model('Common_model');
    }

    public function index() {
        $this->menu_id = 'CONTACT';
        $data['contact_data'] = $this->Common_model->getDataById2('contact', 'del_status', 'Live', 'Live');
        $id = $data['contact_data']->contact_id;
        if ($this->input->post()) {
            $insert_data['contact_email'] = $this->input->post('email');
            $insert_data['contact_email_alt'] = $this->input->post('email_alt');
            $insert_data['contact_phone'] = $this->input->post('contact_number');
            $insert_data['contact_number_title'] = $this->input->post('contact_number_title');
            $insert_data['contact_address'] = $this->input->post('addresss');
            $insert_data['contact_map'] = $this->input->post('map');
            
            $insert_data['contact_title'] = $this->input->post('contact_title');
            $insert_data['contact_description'] = $this->input->post('contact_description');

            $new_path = 'assets/images/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['background_image']['name']) && isset($_FILES['background_image']['name'])) {
                if ($_FILES['background_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('background_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Coaching_page');
                    } else {
                        $old_image = $this->input->post('hidden_background_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
//                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $background_image = $new_path . $image['file_name'];
                    }
                } else {
                    $background_image = $this->input->post('hidden_background_image');
                }
            } else {
                $background_image = $this->input->post('hidden_background_image');
            }
            $insert_data['background_image'] = $background_image;

            $insert_data['meta_title'] = $this->input->post('meta_title');
            $insert_data['meta_desc'] = $this->input->post('meta_desc');
            $insert_data['meta_key'] = $this->input->post('meta_key');

            if (isset($id) && !empty($id)) {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'contact_id', $id, 'contact');
            }
            redirect('admin/Contact');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('CONTACT', 'EDIT');
                $data['slug_data'] = $this->Common_model->getDataById2('tbl_slug', 'ref_primary_id', $id, '', 'ref_table_name', 'contact');
                $view = 'admin/contact/editContact';
                $this->page_title = 'CONTACT';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('CONTACT', 'ADD');
                $this->_show_message("You cant insert new contact detail", "error");
                redirect('admin/Contact');
            }
        }
    }

}
