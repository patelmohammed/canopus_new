<?php

class Footer extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'FOOTER';
        $this->load->model('Footer_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $this->menu_id = 'FOOTER_SETTING';
        $data['footer_data'] = $this->Common_model->getDataByIdStatus('footer_setting', 'del_status', 'Live');
        $id = $data['footer_data']->footer_id;
        if ($this->input->post()) {

            if ($this->user_id == 1) {
                $new_path = 'assets/';
                if (!is_dir($new_path)) {
                    if (!mkdir($new_path, 0777, true)) {
                        die('Not Created');
                    }
                }

                if (!empty($_FILES['logo']['name']) && isset($_FILES['logo']['name'])) {
                    if ($_FILES['logo']['name']) {
                        $config['upload_path'] = $new_path;
                        $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                        $config['max_size'] = "*";
                        $config['max_width'] = "*";
                        $config['max_height'] = "*";
                        $config['encrypt_name'] = FALSE;

                        $this->load->library('upload', $config);
                        if (!$this->upload->do_upload('logo')) {
                            $error = array('error' => $this->upload->display_errors());
                            $this->_show_message($this->upload->display_errors(), "error");
                            redirect('admin/Footer');
                        } else {
                            $old_image = $this->input->post('hidden_logo');
                            if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                                unlink($old_image);
                            }
                            $image = $this->upload->data();
                            $image_url = $new_path . $image['file_name'];
                        }
                    } else {
                        $image_url = $this->input->post('hidden_logo');
                    }
                } else {
                    $image_url = $this->input->post('hidden_logo');
                }
                $insert_data['logo_image'] = $image_url;
            }

            if (!empty($_FILES['register_popup_image']['name']) && isset($_FILES['register_popup_image']['name'])) {
                if ($_FILES['register_popup_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('register_popup_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message($this->upload->display_errors(), "error");
                        redirect('admin/Footer');
                    } else {
                        $old_image = $this->input->post('hidden_register_popup_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $new_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_register_popup_image');
                }
            } else {
                $image_url = $this->input->post('hidden_register_popup_image');
            }
            $insert_data['register_popup_image'] = $image_url;

            $insert_data['description_title'] = $this->input->post('description_title');
            $insert_data['description'] = $this->input->post('description');

            $insert_data['sun_time'] = $this->input->post('sun_time');
            $insert_data['mon_time'] = $this->input->post('mon_time');
            $insert_data['tue_time'] = $this->input->post('tue_time');
            $insert_data['wed_time'] = $this->input->post('wed_time');
            $insert_data['thu_time'] = $this->input->post('thu_time');
            $insert_data['fri_time'] = $this->input->post('fri_time');
            $insert_data['sat_time'] = $this->input->post('sat_time');
            $insert_data['footer_contact'] = $this->input->post('footer_contact');
            $insert_data['footer_address'] = $this->input->post('footer_address');
            $insert_data['header_address'] = $this->input->post('header_address');
            $insert_data['footer_email'] = $this->input->post('footer_email');

            $footer_facebook = $this->input->post('footer_facebook');
            $insert_data['footer_facebook'] = (isset($footer_facebook) && !empty($footer_facebook) ? $footer_facebook : 'javascript:void(0);');

            $footer_twitter = $this->input->post('footer_twitter');
            $insert_data['footer_twitter'] = (isset($footer_twitter) && !empty($footer_twitter) ? $footer_twitter : 'javascript:void(0);');

            $footer_insta = $this->input->post('footer_insta');
            $insert_data['footer_insta'] = (isset($footer_insta) && !empty($footer_insta) ? $footer_insta : 'javascript:void(0);');

            $footer_google = $this->input->post('footer_google');
            $insert_data['footer_google'] = (isset($footer_google) && !empty($footer_google) ? $footer_google : 'javascript:void(0);');

            $footer_linkedin = $this->input->post('footer_linkedin');
            $insert_data['footer_linkedin'] = (isset($footer_linkedin) && !empty($footer_linkedin) ? $footer_linkedin : 'javascript:void(0);');

            $insert_data['meta_title'] = $this->input->post('meta_title');
            $insert_data['meta_desc'] = $this->input->post('meta_desc');
            $insert_data['meta_key'] = $this->input->post('meta_key');

            $register_popup_active = $this->input->post('register_popup_active');
            $insert_data['register_popup_active'] = (isset($register_popup_active) && !empty($register_popup_active) && $register_popup_active == 'on' ? 1 : 0);

            if (isset($id) && !empty($id)) {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'footer_id', $id, 'footer_setting');
            }
            redirect('admin/Footer');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('FOOTER_SETTING', 'EDIT');
                $data = [];
                $data['footer_data'] = $this->Common_model->getDataById2('footer_setting', 'footer_id', $id, 'Live');
                $data['slug_data'] = $this->Common_model->getDataById2('tbl_slug', 'ref_primary_id', $id, '', 'ref_table_name', 'footer_setting');
                $view = 'admin/footer/editFooterSetting';
                $this->page_title = 'FOOTER SETTING';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('FOOTER_SETTING', 'ADD');
                $this->_show_message("You cant insert new contact detail", "error");
                redirect('admin/Footer');
            }
        }
    }

    public function privacyPolicy() {
        $data = [];
        $this->menu_id = 'PRIVACY_POLICY';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('PRIVACY_POLICY');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('PRIVACY_POLICY', 'VIEW');
        $data['privacy_policy_data'] = $this->Common_model->geAlldata('privacy_policy');
        $view = 'admin/footer/privacy_policy';
        $this->page_title = 'PRIVACY POLICY';
        $this->load_admin_view($view, $data);
    }

    public function addEditprivacyPolicy($encrypted_id = "") {
        $this->menu_id = 'PRIVACY_POLICY';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data = array();
            $insert_data['page_title'] = $this->input->post('title');
            $insert_data['page_desc'] = $this->input->post('page_desc');

            $new_path = 'assets/images/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['background_image']['name']) && isset($_FILES['background_image']['name'])) {
                if ($_FILES['background_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('background_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Coaching_page');
                    } else {
                        $old_image = $this->input->post('hidden_background_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
//                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $background_image = $new_path . $image['file_name'];
                    }
                } else {
                    $background_image = $this->input->post('hidden_background_image');
                }
            } else {
                $background_image = $this->input->post('hidden_background_image');
            }
            $insert_data['background_image'] = $background_image;

            $insert_data['meta_title'] = $this->input->post('meta_title');
            $insert_data['meta_desc'] = $this->input->post('meta_desc');
            $insert_data['meta_key'] = $this->input->post('meta_key');

            if (isset($id) && !empty($id)) {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'id', $id, 'privacy_policy');
            }
            redirect('admin/Footer/privacyPolicy');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('PRIVACY_POLICY', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['privacy_policy_data'] = $this->Common_model->getDataById2('privacy_policy', 'id', $id, 'Live');
                $data['slug_data'] = $this->Common_model->getDataById2('tbl_slug', 'ref_primary_id', $id, '', 'ref_table_name', 'privacy_policy');
                $data['background_data'] = $this->Common_model->GetBackgroundImage($data['privacy_policy_data']->page_id);
                $view = 'admin/footer/editPrivacy';
                $this->page_title = 'PRIVACY POLICY';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('PRIVACY_POLICY', 'ADD');
                $this->_show_message("You cant insert new detail", "error");
                redirect('admin/Footer/privacyPolicy');
            }
        }
    }

    public function statistic() {
        $this->menu_id = 'STATISTIC';
        $data['statistic_data'] = $this->Common_model->getDataByIdStatus('tbl_statistic', 'del_status', 'Live');
        $id = $data['statistic_data']->statistic_id;
        if (isset($data['statistic_data']) && !empty($data['statistic_data'])) {
            $data['statistic_data']->statistic_item_data = $this->Common_model->geAlldataById('tbl_statistic_item', 'ref_statistic_id', $id, '');
        }
        if ($this->input->post()) {

            $new_path = 'assets/images/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['main_image']['name']) && isset($_FILES['main_image']['name'])) {
                if ($_FILES['main_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('main_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Footer/statistic');
                    } else {
                        $old_image = $this->input->post('hidden_main_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $new_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_main_image');
                }
            } else {
                $image_url = $this->input->post('hidden_main_image');
            }
            $insert_data['main_image'] = $image_url;

            $insert_data['exp_icon'] = $this->input->post('exp_icon');
            $insert_data['exp_title'] = $this->input->post('exp_title');
            $insert_data['exp_desc'] = $this->input->post('exp_desc');

            $insert_data['cus_icon'] = $this->input->post('cus_icon');
            $insert_data['cus_title'] = $this->input->post('cus_title');
            $insert_data['cus_desc'] = $this->input->post('cus_desc');

            $insert_data['new_cus_icon'] = $this->input->post('new_cus_icon');
            $insert_data['new_cus_title'] = $this->input->post('new_cus_title');
            $insert_data['new_cus_desc'] = $this->input->post('new_cus_desc');

            $insert_data['description'] = $this->input->post('description');
            $insert_data['main_title'] = $this->input->post('main_title');
            $insert_data['main_desc'] = $this->input->post('main_desc');
            $insert_data['sub_desc'] = $this->input->post('sub_desc');
            $insert_data['sub_title'] = $this->input->post('sub_title');
            $insert_data['sub_desc_2'] = $this->input->post('sub_desc_2');

            $insert_data['UpdUser'] = $this->user_id;
            $insert_data['UpdTerminal'] = $this->input->ip_address();
            $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');

            if (isset($id) && !empty($id)) {
                $this->Common_model->deleteRecord($id, 'tbl_statistic_item', 'ref_statistic_id');
                $this->Footer_model->insertStatisticItem($id);

                $this->Common_model->updateInformation2($insert_data, 'statistic_id', $id, 'tbl_statistic');
            }
            redirect('admin/Footer/statistic');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('STATISTIC', 'EDIT');
                $view = 'admin/footer/editStatistic';
                $this->page_title = 'STATISTIC';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('STATISTIC', 'ADD');
                $this->_show_message("You cant insert new contact detail", "error");
                redirect('admin/Footer');
            }
        }
    }

}
