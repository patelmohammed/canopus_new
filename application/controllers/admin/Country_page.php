<?php

class Country_page extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'COUNTRY_PAGE';
        $this->load->model('Common_model');
    }

    public function index() {
        $this->menu_id = 'COUNTRY_MAIN_PAGE';
        $data = [];
        $data['menu_rights'] = $this->Common_model->get_menu_rights('COUNTRY_MAIN_PAGE');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('COUNTRY_MAIN_PAGE', 'VIEW');
        $view = 'admin/country_page/country_page/country_page';
        $data['country_page_data'] = $this->Common_model->geAlldata('tbl_country_page');
        $this->page_title = 'COUNTRY PAGE';
        $this->load_admin_view($view, $data);
    }

    public function addEditCountryPage($encrypted_id = "") {
        $this->menu_id = 'COUNTRY_MAIN_PAGE';
        $id = $encrypted_id;
        if ($this->input->post()) {

            $new_path = 'assets/images/services/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['country_page_image']['name']) && isset($_FILES['country_page_image']['name'])) {
                if ($_FILES['country_page_image']['name']) {
                    $config = array();
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('country_page_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Country_page');
                    } else {
                        $old_image = $this->input->post('hidden_country_page_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $new_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_country_page_image');
                }
            } else {
                $image_url = $this->input->post('hidden_country_page_image');
            }
            $insert_data['country_page_image'] = $image_url;

            $home_page_path = 'assets/images/country/';
            if (!is_dir($home_page_path)) {
                if (!mkdir($home_page_path, 0777, true)) {
                    die('Not Created');
                }
            }

            unset($this->upload);
            if (!empty($_FILES['home_page_image']['name']) && isset($_FILES['home_page_image']['name'])) {
                if ($_FILES['home_page_image']['name']) {
                    $config = array();
                    $config['upload_path'] = $home_page_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('home_page_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Coaching_page');
                    } else {
                        $old_image = $this->input->post('hidden_home_page_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $home_page_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_home_page_image');
                }
            } else {
                $image_url = $this->input->post('hidden_home_page_image');
            }
            $insert_data['home_page_image'] = $image_url;

            if (!empty($_FILES['country_flag_image']['name']) && isset($_FILES['country_flag_image']['name'])) {
                if ($_FILES['country_flag_image']['name']) {
                    $config = array();
                    $config['upload_path'] = $home_page_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('country_flag_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Coaching_page');
                    } else {
                        $old_image = $this->input->post('hidden_country_flag_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $home_page_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_country_flag_image');
                }
            } else {
                $image_url = $this->input->post('hidden_country_flag_image');
            }
            $insert_data['country_flag_image'] = $image_url;

            unset($this->upload);
            $new_path = 'assets/images/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['background_image']['name']) && isset($_FILES['background_image']['name'])) {
                if ($_FILES['background_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('background_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Coaching_page');
                    } else {
                        $old_image = $this->input->post('hidden_background_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
//                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $background_image = $new_path . $image['file_name'];
                    }
                } else {
                    $background_image = $this->input->post('hidden_background_image');
                }
            } else {
                $background_image = $this->input->post('hidden_background_image');
            }
            $insert_data['background_image'] = $background_image;

            $insert_data['country_page_name'] = $this->input->post('country_page_name');
            $insert_data['continent_id'] = $this->input->post('continent_id');
            $insert_data['country_page_desc'] = $this->input->post('country_page_desc');
            $insert_data['home_page_desc'] = $this->input->post('home_page_desc');
            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            $insert_data['meta_title'] = $this->input->post('meta_title');
            $insert_data['meta_desc'] = $this->input->post('meta_desc');
            $insert_data['meta_key'] = $this->input->post('meta_key');

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $plan_id = $this->Common_model->InsertInformation($insert_data, 'tbl_country_page');
                if (isset($plan_id) && !empty($plan_id)) {
                    $slug_data = array();
                    $slug_data['slug'] = createSlug($this->input->post('slug'));
                    $slug_data['ref_table_name'] = 'tbl_country_page';
                    $slug_data['ref_primary_id'] = $plan_id;
                    $slug_id = $this->Common_model->insertUpdateSlug($slug_data);
                }
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'country_page_id', $id, 'tbl_country_page');

                $slug_data = array();
                $slug_data['slug'] = createSlug($this->input->post('slug'));
                $slug_data['ref_table_name'] = 'tbl_country_page';
                $slug_data['ref_primary_id'] = $id;
                $this->Common_model->insertUpdateSlug($slug_data, $this->input->post('hidden_slug_id'));
            }
            redirect('admin/Country_page');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('COUNTRY_MAIN_PAGE', 'ADD');
                $data = [];
                $data['continent'] = $this->Common_model->geAlldataById('tbl_continent', 'del_status', 'Live', '');
                $view = 'admin/country_page/country_page/addCountryPage';
                $this->page_title = 'COUNTRY PAGE';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('COUNTRY_MAIN_PAGE', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['continent'] = $this->Common_model->geAlldataById('tbl_continent', 'del_status', 'Live', '');
                $data['country_page_data'] = $this->Common_model->getDataById2('tbl_country_page', 'country_page_id', $id, 'Live');
                $data['slug_data'] = $this->Common_model->getDataById2('tbl_slug', 'ref_primary_id', $id, '', 'ref_table_name', 'tbl_country_page');
                $view = 'admin/country_page/country_page/editCountryPage';
                $this->page_title = 'COUNTRY PAGE';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkCountryPage($category_id = '') {
        if ($category_id != '') {
            $catgory_name = $this->Common_model->getCountryPage($category_id);
            $respone = $this->Common_model->checkCountryPage($this->input->get('country_page_name'), $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Common_model->checkCountryPage($this->input->get('country_page_name'));
            echo $response;
            die;
        }
    }

    public function checkCountryPageShortName($category_id = '') {
        if ($category_id != '') {
            $catgory_name = $this->Common_model->getCountryPageShortName($category_id);
            $respone = $this->Common_model->checkCountryPageShortName($this->input->get('country_page_short_name'), $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Common_model->checkCountryPageShortName($this->input->get('country_page_short_name'));
            echo $response;
            die;
        }
    }

    public function deactiveCountryPage() {
        $update_data = $data = array();
        $country_page_id = $this->input->post('country_page_id');
        $update_data['is_active'] = 2;
        $response = $this->Common_model->updateInformation2($update_data, 'country_page_id', $country_page_id, 'tbl_country_page', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function activeCountryPage() {
        $update_data = $data = array();
        $country_page_id = $this->input->post('country_page_id');
        $update_data['is_active'] = 1;
        $response = $this->Common_model->updateInformation2($update_data, 'country_page_id', $country_page_id, 'tbl_country_page', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function showCountryHomePage() {
        $update_data = $data = array();
        $id = $this->input->post('id');
        $is_show_home_page = $this->input->post('is_show_home_page');
        $country_page = $this->Common_model->geAlldataById('tbl_country_page', 'is_show_home_page', 1, '');
        if (count($country_page) < 4 || $is_show_home_page == 1) {
            if ($is_show_home_page == 0) {
                $update_data['is_show_home_page'] = 1;
            } else {
                $update_data['is_show_home_page'] = 0;
            }
            $response = $this->Common_model->updateInformation2($update_data, 'country_page_id', $id, 'tbl_country_page', 'Live');
            if ($response == TRUE) {
                $data['result'] = TRUE;
            } else {
                $data['result'] = FALSE;
            }
        } else {
            $data['result'] = FALSE;
            $data['message'] = 'Already 4 show on home page.';
        }
        echo json_encode($data);
        die;
    }

    public function deleteCountryPage() {
        $this->Common_model->check_menu_access('COUNTRY_MAIN_PAGE', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('country_page_id', $id);
            $this->db->update('tbl_country_page');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function countryPageData() {
        $this->menu_id = 'COUNTRY_PAGE_DATA';
        $data = [];
        $data['menu_rights'] = $this->Common_model->get_menu_rights('COUNTRY_PAGE_DATA');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('COUNTRY_PAGE_DATA', 'VIEW');
        $view = 'admin/country_page/country_page_data/country_page_data';
        $data['country_page_data'] = $this->Common_model->geAlldata('tbl_country_item_page');
        $this->page_title = 'COUNTRY PAGE';
        $this->load_admin_view($view, $data);
    }

    public function addEditCountryPageData($encrypted_id = "") {
        $this->menu_id = 'COUNTRY_PAGE_DATA';
        $id = $encrypted_id;
        if ($this->input->post()) {

            $insert_data['ref_country_page_id'] = $this->input->post('ref_country_page_id');
            $insert_data['country_item_page_title'] = $this->input->post('country_item_page_title');
            $insert_data['country_item_page_desc'] = $this->input->post('country_item_page_desc');
            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            if ($id == "" || $id == '' || $id == NULL) {
                $plan_id = $this->Common_model->InsertInformation($insert_data, 'tbl_country_item_page');
            } else {
                $this->Common_model->updateInformation2($insert_data, 'country_item_page_id', $id, 'tbl_country_item_page');
            }
            redirect('admin/Country_page/countryPageData');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('COUNTRY_PAGE_DATA', 'ADD');
                $data = [];
                $data['country_page'] = $this->Common_model->geAlldata('tbl_country_page');
                $view = 'admin/country_page/country_page_data/addCountryPageData';
                $this->page_title = 'COUNTRY PAGE';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('COUNTRY_PAGE_DATA', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['country_page'] = $this->Common_model->geAlldata('tbl_country_page');
                $data['country_page_data'] = $this->Common_model->getDataById2('tbl_country_item_page', 'country_item_page_id', $id, 'Live');
                $view = 'admin/country_page/country_page_data/editCountryPageData';
                $this->page_title = 'COUNTRY PAGE';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function deleteCountryPageData() {
        $this->Common_model->check_menu_access('COUNTRY_PAGE_DATA', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('country_item_page_id', $id);
            $this->db->update('tbl_country_item_page');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function deactiveCountryPageData() {
        $update_data = $data = array();
        $country_item_page_id = $this->input->post('country_item_page_id');
        $update_data['is_active'] = 2;
        $response = $this->Common_model->updateInformation2($update_data, 'country_item_page_id', $country_item_page_id, 'tbl_country_item_page', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function activeCountryPageData() {
        $update_data = $data = array();
        $country_item_page_id = $this->input->post('country_item_page_id');
        $update_data['is_active'] = 1;
        $response = $this->Common_model->updateInformation2($update_data, 'country_item_page_id', $country_item_page_id, 'tbl_country_item_page', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function countryItemPageById() {
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $data['country_item_page_data'] = $this->Common_model->getDataById2('tbl_country_item_page', 'country_item_page_id', $id, 'Live');
            if (isset($data['country_item_page_data']) && !empty($data['country_item_page_data'])) {
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

}
