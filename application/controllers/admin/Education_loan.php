<?php

class Education_loan extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'EDUCATION_LOAN';
        $this->load->model('Common_model');
//        $this->load->model('Faq_model');
    }

    public function index() {
        $this->menu_id = 'EDUCATION_LOAN';
        $education_loan_data = $data['education_loan_data'] = $this->Common_model->getDataById2('tbl_education_loan', 'del_status', 'Live', 'Live');
        $id = (isset($education_loan_data->education_loan_id) && !empty($education_loan_data->education_loan_id) ? $education_loan_data->education_loan_id : NULL);
        if ($this->input->post()) {
            $insert_data['section_1_title'] = $this->input->post('section_1_title');
            $insert_data['section_1_short_desc'] = $this->input->post('section_1_short_desc');
            $insert_data['section_1_desc'] = $this->input->post('section_1_desc');
            $insert_data['section_2_title'] = $this->input->post('section_2_title');
            $insert_data['section_3_title'] = $this->input->post('section_3_title');
            $insert_data['section_4_title'] = $this->input->post('section_4_title');

            $insert_data['section_5_title'] = $this->input->post('section_5_title');
            $insert_data['section_5_desc'] = $this->input->post('section_5_desc');

            unset($this->upload);
            $image_path = 'assets/images/education_loan/';
            if (!is_dir($image_path)) {
                if (!mkdir($image_path, 0777, true)) {
                    die('Not Created');
                }
            }

            $config['upload_path'] = $image_path;
            $config['allowed_types'] = '*';
            $config['max_size'] = "*";
            $config['max_width'] = "*";
            $config['max_height'] = "*";
            $config['encrypt_name'] = FALSE;
            $this->load->library('upload', $config);

            if (!empty($_FILES['section_1_image_1']['name']) && isset($_FILES['section_1_image_1']['name'])) {
                if ($_FILES['section_1_image_1']['name']) {
                    if (!$this->upload->do_upload('section_1_image_1')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message($this->upload->display_errors(), "error");
                        redirect('admin/Education_loan');
                    } else {
                        $old_image = $this->input->post('hidden_section_1_image_1');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $image_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_section_1_image_1');
                }
            } else {
                $image_url = $this->input->post('hidden_section_1_image_1');
            }
            $insert_data['section_1_image_1'] = $image_url;

            if (!empty($_FILES['section_1_image_2']['name']) && isset($_FILES['section_1_image_2']['name'])) {
                if ($_FILES['section_1_image_2']['name']) {
                    if (!$this->upload->do_upload('section_1_image_2')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message($this->upload->display_errors(), "error");
                        redirect('admin/Education_loan');
                    } else {
                        $old_image = $this->input->post('hidden_section_1_image_2');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $image_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_section_1_image_2');
                }
            } else {
                $image_url = $this->input->post('hidden_section_1_image_2');
            }
            $insert_data['section_1_image_2'] = $image_url;

            if (!empty($_FILES['section_1_image_3']['name']) && isset($_FILES['section_1_image_3']['name'])) {
                if ($_FILES['section_1_image_3']['name']) {
                    if (!$this->upload->do_upload('section_1_image_3')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message($this->upload->display_errors(), "error");
                        redirect('admin/Education_loan');
                    } else {
                        $old_image = $this->input->post('hidden_section_1_image_3');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $image_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_section_1_image_3');
                }
            } else {
                $image_url = $this->input->post('hidden_section_1_image_3');
            }
            $insert_data['section_1_image_3'] = $image_url;

            if (!empty($_FILES['section_5_image']['name']) && isset($_FILES['section_5_image']['name'])) {
                if ($_FILES['section_5_image']['name']) {
                    if (!$this->upload->do_upload('section_5_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message($this->upload->display_errors(), "error");
                        redirect('admin/Education_loan');
                    } else {
                        $old_image = $this->input->post('hidden_section_5_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $image_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_section_5_image');
                }
            } else {
                $image_url = $this->input->post('hidden_section_5_image');
            }
            $insert_data['section_5_image'] = $image_url;
            unset($this->upload);

            if ($id == "" || $id == '' || $id == NULL) {
                $id = $this->Common_model->InsertInformation($insert_data, 'tbl_education_loan');
            } else {
                $this->Common_model->updateInformation2($insert_data, 'education_loan_id ', $id, 'tbl_education_loan');
            }

            if (isset($id) && !empty($id)) {
                $this->insertSection2LeftItem($id, $this->input->post('section_2_left_item_data'), $this->input->post('section_2_left_item_is_icon'));
                $this->insertSection2RightItem($id, $this->input->post('section_2_right_item_data'), $this->input->post('section_2_right_item_is_icon'));
                $this->insertSection3Item($id, $this->input->post('section_3_item_title'), $this->input->post('section_3_item_desc'));
                $this->insertSection4Item($id, $_FILES['section_4_image'], $this->input->post('hidden_section_4_image'));
            }

            redirect('admin/Education_loan');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('EDUCATION_LOAN', 'EDIT');
                $view = 'admin/education_loan/editEducationLoan';
                $data['section_2_left_item_data'] = $this->Common_model->geAlldataById('tbl_section_2_left_education_loan', 'ref_education_loan_id', $id);
                $data['section_2_right_item_data'] = $this->Common_model->geAlldataById('tbl_section_2_right_education_loan', 'ref_education_loan_id', $id);
                $data['section_3_item_data'] = $this->Common_model->geAlldataById('tbl_section_3_education_loan', 'ref_education_loan_id', $id);
                $data['section_4_item_data'] = $this->Common_model->geAlldataById('tbl_section_4_education_loan', 'ref_education_loan_id', $id);
                $this->page_title = 'EDUCATION LOAN';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('EDUCATION_LOAN', 'ADD');
                $this->_show_message("You cant insert new data", "error");
                redirect('admin/Education_loan');
            }
        }
    }

    public function insertSection2LeftItem($id, $section_2_left_item_data, $section_2_left_item_is_icon) {
        $this->Common_model->deleteRecord($id, 'tbl_section_2_left_education_loan', 'ref_education_loan_id');
        if (isset($id) && isset($section_2_left_item_data) && isset($section_2_left_item_is_icon) && !empty($id) && !empty($section_2_left_item_data) && !empty($section_2_left_item_is_icon)) {
            foreach ($section_2_left_item_data as $key => $value) {
                $insert_data = array();
                $insert_data['ref_education_loan_id'] = $id;
                $insert_data['section_2_left_item_data'] = $value;
                $insert_data['section_2_left_item_is_icon'] = isset($section_2_left_item_is_icon[$key]) && !empty($section_2_left_item_is_icon[$key]) && $section_2_left_item_is_icon[$key] == 'on' ? 1 : 2;
                $this->Common_model->InsertInformation($insert_data, 'tbl_section_2_left_education_loan');
            }
        }
    }

    public function insertSection2RightItem($id, $section_2_right_item_data, $section_2_right_item_is_icon) {
        $this->Common_model->deleteRecord($id, 'tbl_section_2_right_education_loan', 'ref_education_loan_id');
        if (isset($id) && isset($section_2_right_item_data) && isset($section_2_right_item_is_icon) && !empty($id) && !empty($section_2_right_item_data) && !empty($section_2_right_item_is_icon)) {
            foreach ($section_2_right_item_data as $key => $value) {
                $insert_data = array();
                $insert_data['ref_education_loan_id'] = $id;
                $insert_data['section_2_right_item_data'] = $value;
                $insert_data['section_2_right_item_is_icon'] = isset($section_2_right_item_is_icon[$key]) && !empty($section_2_right_item_is_icon[$key]) && $section_2_right_item_is_icon[$key] == 'on' ? 1 : 2;
                $this->Common_model->InsertInformation($insert_data, 'tbl_section_2_right_education_loan');
            }
        }
    }

    public function insertSection3Item($id, $section_3_item_title, $section_3_item_desc) {
        $this->Common_model->deleteRecord($id, 'tbl_section_3_education_loan', 'ref_education_loan_id');
        if (isset($id) && isset($section_3_item_title) && isset($section_3_item_desc) && !empty($id) && !empty($section_3_item_title) && !empty($section_3_item_desc)) {
            foreach ($section_3_item_title as $key => $value) {
                $insert_data = array();
                $insert_data['ref_education_loan_id'] = $id;
                $insert_data['section_3_item_title'] = $value;
                $insert_data['section_3_item_desc'] = $section_3_item_desc[$key];
                $this->Common_model->InsertInformation($insert_data, 'tbl_section_3_education_loan');
            }
        }
    }

    public function insertSection4Item($id, $section_4_image, $hidden_section_4_image) {
        $this->Common_model->deleteRecord($id, 'tbl_section_4_education_loan', 'ref_education_loan_id');
        $image_path = 'assets/images/bank/';
        if (!is_dir($image_path)) {
            if (!mkdir($image_path, 0777, true)) {
                $this->_show_message('Path folder not created');
                redirect('admin/Education_loan');
            }
        }

        if (isset($id) && isset($section_4_image) && !empty($id) && !empty($section_4_image)) {
            if (isset($section_4_image) && !empty($section_4_image)) {

                $config['upload_path'] = $image_path;
                $config['allowed_types'] = '*';
                $config['max_size'] = "*";
                $config['max_width'] = "*";
                $config['max_height'] = "*";
                $config['encrypt_name'] = FALSE;
                $this->load->library('upload', $config);

                foreach ($section_4_image['name'] as $key => $value) {
                    if (isset($value) && !empty($value)) {
                        $_FILES['section_4_image']['name'] = $section_4_image['name'][$key];
                        $_FILES['section_4_image']['type'] = $section_4_image['type'][$key];
                        $_FILES['section_4_image']['tmp_name'] = $section_4_image['tmp_name'][$key];
                        $_FILES['section_4_image']['error'] = $section_4_image['error'][$key];
                        $_FILES['section_4_image']['size'] = $section_4_image['size'][$key];

                        if (!$this->upload->do_upload('section_4_image')) {
                            $this->_show_message($this->upload->display_errors(), "error");
                            redirect('admin/Education_loan');
                        } else {
                            $image = $this->upload->data();
                            $item_image = array();
                            $item_image['ref_education_loan_id'] = $id;
                            $item_image['section_4_image'] = $image_path . $image['file_name'];
                            $this->Common_model->InsertInformation($item_image, 'tbl_section_4_education_loan');
                        }
                    } else {
                        $item_image = array();
                        $item_image['ref_education_loan_id'] = $id;
                        $item_image['section_4_image'] = $hidden_section_4_image[$key];
                        $this->Common_model->InsertInformation($item_image, 'tbl_section_4_education_loan');
                    }
                }
            } else {
                if (isset($hidden_section_4_image) && !empty($hidden_section_4_image)) {
                    foreach ($hidden_section_4_image as $key2 => $value2) {
                        $item_image = array();
                        $item_image['ref_education_loan_id'] = $id;
                        $item_image['section_4_image'] = $value2;
                        $this->Common_model->InsertInformation($item_image, 'tbl_section_4_education_loan');
                    }
                }
            }
        }
    }

}
