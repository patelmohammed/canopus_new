<?php

class Coaching_page extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'COACHING_PAGE';
        $this->load->model('Common_model');
    }

    public function index() {
        $this->menu_id = 'COACHING_MAIN_PAGE';
        $data = [];
        $data['menu_rights'] = $this->Common_model->get_menu_rights('COACHING_MAIN_PAGE');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('COACHING_MAIN_PAGE', 'VIEW');
        $view = 'admin/coaching_page/coaching_page/coaching_page';
        $data['coaching_page_data'] = $this->Common_model->geAlldata('tbl_coaching_page');
        $this->page_title = 'COACHING PAGE';
        $this->load_admin_view($view, $data);
    }

    public function addEditCoachingPage($encrypted_id = "") {
        $this->menu_id = 'COACHING_MAIN_PAGE';
        $id = $encrypted_id;
        if ($this->input->post()) {

            $new_path = 'assets/images/services/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['coaching_page_image']['name']) && isset($_FILES['coaching_page_image']['name'])) {
                if ($_FILES['coaching_page_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('coaching_page_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Coaching_page');
                    } else {
                        $old_image = $this->input->post('hidden_coaching_page_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $new_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_coaching_page_image');
                }
            } else {
                $image_url = $this->input->post('hidden_coaching_page_image');
            }
            $insert_data['coaching_page_image'] = $image_url;

            unset($this->upload);
            $new_path = 'assets/images/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['background_image']['name']) && isset($_FILES['background_image']['name'])) {
                if ($_FILES['background_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('background_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Coaching_page');
                    } else {
                        $old_image = $this->input->post('hidden_background_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
//                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $background_image = $new_path . $image['file_name'];
                    }
                } else {
                    $background_image = $this->input->post('hidden_background_image');
                }
            } else {
                $background_image = $this->input->post('hidden_background_image');
            }
            $insert_data['background_image'] = $background_image;

            $insert_data['coaching_page_name'] = $this->input->post('coaching_page_name');
            $insert_data['coaching_page_desc'] = $this->input->post('coaching_page_desc');
            $insert_data['icon'] = $this->input->post('icon');
            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            $insert_data['meta_title'] = $this->input->post('meta_title');
            $insert_data['meta_desc'] = $this->input->post('meta_desc');
            $insert_data['meta_key'] = $this->input->post('meta_key');

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $plan_id = $this->Common_model->InsertInformation($insert_data, 'tbl_coaching_page');
                if (isset($plan_id) && !empty($plan_id)) {
                    $slug_data = array();
                    $slug_data['slug'] = createSlug($this->input->post('slug'));
                    $slug_data['ref_table_name'] = 'tbl_coaching_page';
                    $slug_data['ref_primary_id'] = $plan_id;
                    $slug_id = $this->Common_model->insertUpdateSlug($slug_data);
                }
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'coaching_page_id', $id, 'tbl_coaching_page');

                $slug_data = array();
                $slug_data['slug'] = createSlug($this->input->post('slug'));
                $slug_data['ref_table_name'] = 'tbl_coaching_page';
                $slug_data['ref_primary_id'] = $id;
                $this->Common_model->insertUpdateSlug($slug_data, $this->input->post('hidden_slug_id'));
            }
            redirect('admin/Coaching_page');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('COACHING_MAIN_PAGE', 'ADD');
                $data = [];
                $view = 'admin/coaching_page/coaching_page/addCoachingPage';
                $this->page_title = 'COACHING PAGE';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('COACHING_MAIN_PAGE', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['coaching_page_data'] = $this->Common_model->getDataById2('tbl_coaching_page', 'coaching_page_id', $id, 'Live');
                $data['slug_data'] = $this->Common_model->getDataById2('tbl_slug', 'ref_primary_id', $id, '', 'ref_table_name', 'tbl_coaching_page');
                $view = 'admin/coaching_page/coaching_page/editCoachingPage';
                $this->page_title = 'COACHING PAGE';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkCoachingPage($category_id = '') {
        if ($category_id != '') {
            $catgory_name = $this->Common_model->getCoachingPage($category_id);
            $respone = $this->Common_model->checkCoachingPage($this->input->get('coaching_page_name'), $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Common_model->checkCoachingPage($this->input->get('coaching_page_name'));
            echo $response;
            die;
        }
    }

    public function checkCoachingPageShortName($category_id = '') {
        if ($category_id != '') {
            $catgory_name = $this->Common_model->getCoachingPageShortName($category_id);
            $respone = $this->Common_model->checkCoachingPageShortName($this->input->get('coaching_page_short_name'), $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Common_model->checkCoachingPageShortName($this->input->get('coaching_page_short_name'));
            echo $response;
            die;
        }
    }

    public function deactiveCoachingPage() {
        $update_data = $data = array();
        $coaching_page_id = $this->input->post('coaching_page_id');
        $update_data['is_active'] = 2;
        $response = $this->Common_model->updateInformation2($update_data, 'coaching_page_id', $coaching_page_id, 'tbl_coaching_page', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function activeCoachingPage() {
        $update_data = $data = array();
        $coaching_page_id = $this->input->post('coaching_page_id');
        $update_data['is_active'] = 1;
        $response = $this->Common_model->updateInformation2($update_data, 'coaching_page_id', $coaching_page_id, 'tbl_coaching_page', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function showCoachingHomePage() {
        $update_data = $data = array();
        $id = $this->input->post('id');
        $is_show_home_page = $this->input->post('is_show_home_page');
        $country_page = $this->Common_model->geAlldataById('tbl_coaching_page', 'is_show_home_page', 1, '');
        if (count($country_page) < 4 || $is_show_home_page == 1) {
            if ($is_show_home_page == 0) {
                $update_data['is_show_home_page'] = 1;
            } else {
                $update_data['is_show_home_page'] = 0;
            }
            $response = $this->Common_model->updateInformation2($update_data, 'coaching_page_id', $id, 'tbl_coaching_page', 'Live');
            if ($response == TRUE) {
                $data['result'] = TRUE;
            } else {
                $data['result'] = FALSE;
            }
        } else {
            $data['result'] = FALSE;
            $data['message'] = 'Already 4 show on home page.';
        }
        echo json_encode($data);
        die;
    }

    public function deleteCoachingPage() {
        $this->Common_model->check_menu_access('COACHING_MAIN_PAGE', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('coaching_page_id', $id);
            $this->db->update('tbl_coaching_page');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function coachingPageData() {
        $this->menu_id = 'COACHING_PAGE_DATA';
        $data = [];
        $data['menu_rights'] = $this->Common_model->get_menu_rights('COACHING_PAGE_DATA');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('COACHING_PAGE_DATA', 'VIEW');
        $view = 'admin/coaching_page/coaching_page_data/coaching_page_data';
        $data['coaching_page_data'] = $this->Common_model->geAlldata('tbl_coaching_item_page');
        $this->page_title = 'COACHING PAGE';
        $this->load_admin_view($view, $data);
    }

    public function addEditCoachingPageData($encrypted_id = "") {
        $this->menu_id = 'COACHING_PAGE_DATA';
        $id = $encrypted_id;
        if ($this->input->post()) {

            $insert_data['ref_coaching_page_id'] = $this->input->post('ref_coaching_page_id');
            $insert_data['coaching_item_page_title'] = $this->input->post('coaching_item_page_title');
            $insert_data['coaching_item_page_desc'] = $this->input->post('coaching_item_page_desc');
            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            if ($id == "" || $id == '' || $id == NULL) {
                $plan_id = $this->Common_model->InsertInformation($insert_data, 'tbl_coaching_item_page');
            } else {
                $this->Common_model->updateInformation2($insert_data, 'coaching_item_page_id', $id, 'tbl_coaching_item_page');
            }
            redirect('admin/Coaching_page/coachingPageData');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('COACHING_PAGE_DATA', 'ADD');
                $data = [];
                $data['coaching_page'] = $this->Common_model->geAlldata('tbl_coaching_page');
                $view = 'admin/coaching_page/coaching_page_data/addCoachingPageData';
                $this->page_title = 'COACHING PAGE';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('COACHING_PAGE_DATA', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['coaching_page'] = $this->Common_model->geAlldata('tbl_coaching_page');
                $data['coaching_page_data'] = $this->Common_model->getDataById2('tbl_coaching_item_page', 'coaching_item_page_id', $id, 'Live');
                $view = 'admin/coaching_page/coaching_page_data/editCoachingPageData';
                $this->page_title = 'COACHING PAGE';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function deleteCoachingPageData() {
        $this->Common_model->check_menu_access('COACHING_PAGE_DATA', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('coaching_item_page_id', $id);
            $this->db->update('tbl_coaching_item_page');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function deactiveCoachingPageData() {
        $update_data = $data = array();
        $coaching_item_page_id = $this->input->post('coaching_item_page_id');
        $update_data['is_active'] = 2;
        $response = $this->Common_model->updateInformation2($update_data, 'coaching_item_page_id', $coaching_item_page_id, 'tbl_coaching_item_page', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function activeCoachingPageData() {
        $update_data = $data = array();
        $coaching_item_page_id = $this->input->post('coaching_item_page_id');
        $update_data['is_active'] = 1;
        $response = $this->Common_model->updateInformation2($update_data, 'coaching_item_page_id', $coaching_item_page_id, 'tbl_coaching_item_page', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function coachingItemPageById() {
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $data['coaching_item_page_data'] = $this->Common_model->getDataById2('tbl_coaching_item_page', 'coaching_item_page_id', $id, 'Live');
            if (isset($data['coaching_item_page_data']) && !empty($data['coaching_item_page_data'])) {
                $data['result'] = true;
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

}
