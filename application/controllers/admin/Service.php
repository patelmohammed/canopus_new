<?php

class Service extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'SERVICE';
        $this->load->model('Common_model');
        $this->load->model('Service_moodel');
    }

    public function index() {
        $this->menu_id = 'ABOUT';
        $data['about_us_data'] = $this->Common_model->getDataByIdStatus('about_us', 'del_status', 'Live');
        if (isset($data['about_us_data']) && !empty($data['about_us_data'])) {
            $data['about_us_data']->about_us_item_data = $this->About_model->getAboutUsItemData($data['about_us_data']->about_us_id);
        }
        $id = $data['about_us_data']->about_us_id;
        if ($this->input->post()) {
            $insert_data['about_us_title'] = $this->input->post('about_us_title');
            $insert_data['about_us_desc'] = $this->input->post('about_us_desc');

            $insert_data['about_us_alt_title'] = $this->input->post('about_us_alt_title');
            $insert_data['about_us_alt_desc'] = $this->input->post('about_us_alt_desc');

            $insert_data['about_us_alt_sub_icon_1'] = $this->input->post('about_us_alt_sub_icon_1');
            $insert_data['about_us_alt_sub_title_1'] = $this->input->post('about_us_alt_sub_title_1');
            $insert_data['about_us_alt_sub_desc_1'] = $this->input->post('about_us_alt_sub_desc_1');

            $insert_data['about_us_alt_sub_icon_2'] = $this->input->post('about_us_alt_sub_icon_2');
            $insert_data['about_us_alt_sub_title_2'] = $this->input->post('about_us_alt_sub_title_2');
            $insert_data['about_us_alt_sub_desc_2'] = $this->input->post('about_us_alt_sub_desc_2');

            $new_path = 'assets/images/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['about_us_alt_image']['name']) && isset($_FILES['about_us_alt_image']['name'])) {
                if ($_FILES['about_us_alt_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('about_us_alt_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/About');
                    } else {
                        $old_image = $this->input->post('hidden_about_us_alt_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $new_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_about_us_alt_image');
                }
            } else {
                $image_url = $this->input->post('hidden_about_us_alt_image');
            }
            $insert_data['about_us_alt_image'] = $image_url;


            $insert_data['section_3_title'] = $this->input->post('section_3_title');
            $insert_data['section_3_desc'] = $this->input->post('section_3_desc');

            $insert_data['UpdUser'] = $this->user_id;
            $insert_data['UpdTerminal'] = $this->input->ip_address();
            $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
            if (isset($id) && !empty($id)) {
                $this->Common_model->deleteRecord($id, 'about_us_item', 'ref_about_us_id');
                $this->About_model->insertAboutUsItem($id);
                $this->Common_model->updateInformation2($insert_data, 'about_us_id', $id, 'about_us');
            }
            redirect('admin/About');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('ABOUT', 'EDIT');
                $view = 'admin/about/editAboutUs';
                $this->page_title = 'ABOUT US';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('ABOUT', 'ADD');
                $this->_show_message("You cant insert new about us detail", "error");
                redirect('admin/About');
            }
        }
    }

    public function addEditServiceSetting() {
        $this->menu_id = 'SERVICE_SETTING';
        $data['service_setting_data'] = $this->Common_model->getDataByIdStatus('tbl_service_setting', 'del_status', 'Live');
        if (isset($data['service_setting_data']) && !empty($data['service_setting_data'])) {
            $data['service_setting_data']->service_item_data = $this->Service_moodel->getServiceItemData($data['service_setting_data']->service_setting_id);
        }
        $id = $data['service_setting_data']->service_setting_id;
        if ($this->input->post()) {
            $insert_data['visa_section_title'] = $this->input->post('visa_section_title');
            $insert_data['visa_section_desc'] = $this->input->post('visa_section_desc');

            $insert_data['setting_title'] = $this->input->post('setting_title');
            $insert_data['setting_desc'] = $this->input->post('setting_desc');

            $insert_data['sub_setting_icon_1'] = $this->input->post('sub_setting_icon_1');
            $insert_data['sub_setting_title_1'] = $this->input->post('sub_setting_title_1');
            $insert_data['sub_setting_desc_1'] = $this->input->post('sub_setting_desc_1');

            $insert_data['sub_setting_icon_2'] = $this->input->post('sub_setting_icon_2');
            $insert_data['sub_setting_title_2'] = $this->input->post('sub_setting_title_2');
            $insert_data['sub_setting_desc_2'] = $this->input->post('sub_setting_desc_2');

            $insert_data['sub_setting_icon_3'] = $this->input->post('sub_setting_icon_3');
            $insert_data['sub_setting_title_3'] = $this->input->post('sub_setting_title_3');
            $insert_data['sub_setting_desc_3'] = $this->input->post('sub_setting_desc_3');

            $insert_data['sub_setting_icon_4'] = $this->input->post('sub_setting_icon_4');
            $insert_data['sub_setting_title_4'] = $this->input->post('sub_setting_title_4');
            $insert_data['sub_setting_desc_4'] = $this->input->post('sub_setting_desc_4');

            $new_path = 'assets/images/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['background_image']['name']) && isset($_FILES['background_image']['name'])) {
                if ($_FILES['background_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('background_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Coaching_page');
                    } else {
                        $old_image = $this->input->post('hidden_background_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
//                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $background_image = $new_path . $image['file_name'];
                    }
                } else {
                    $background_image = $this->input->post('hidden_background_image');
                }
            } else {
                $background_image = $this->input->post('hidden_background_image');
            }
            $insert_data['background_image'] = $background_image;

            $insert_data['meta_title'] = $this->input->post('meta_title');
            $insert_data['meta_desc'] = $this->input->post('meta_desc');
            $insert_data['meta_key'] = $this->input->post('meta_key');

            $insert_data['UpdUser'] = $this->user_id;
            $insert_data['UpdTerminal'] = $this->input->ip_address();
            $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
            if (isset($id) && !empty($id)) {
                $this->Common_model->deleteRecord($id, 'service_item', 'ref_service_id');
                $this->Service_moodel->insertServiceItem($id);
                $this->Common_model->updateInformation2($insert_data, 'service_setting_id', $id, 'tbl_service_setting');
            }
            redirect('admin/Service/addEditServiceSetting');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('SERVICE_SETTING', 'EDIT');
                $data['slug_data'] = $this->Common_model->getDataById2('tbl_slug', 'ref_primary_id', $id, '', 'ref_table_name', 'tbl_service_setting');
                $view = 'admin/service/editServiceSetting';
                $this->page_title = 'SERVICE_SETTING';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('SERVICE_SETTING', 'ADD');
                $this->_show_message("You cant insert new about us detail", "error");
                redirect('admin/Service');
            }
        }
    }

}
