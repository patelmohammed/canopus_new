<?php

class Faq extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'FAQ';
        $this->load->model('Common_model');
        $this->load->model('Faq_model');
    }

    public function index() {
        $this->menu_id = 'FAQ';
        $data = [];
        $data['menu_rights'] = $this->Common_model->get_menu_rights('FAQ');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('FAQ', 'VIEW');
        $view = 'admin/faq/faq';
        $data['faq_data'] = $this->Faq_model->getFaqCountryDataAdmin();
//        preprint($data['faq_data']);
        $this->page_title = 'FAQ';
        $this->load_admin_view($view, $data);
    }

    public function addEditFaq($encrypted_id = "") {
        $this->menu_id = 'FAQ';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data['ref_country_page_id'] = $this->input->post('ref_country_page_id');
            $insert_data['faq_item_title'] = $this->input->post('faq_item_title');

            if ($id == "" || $id == '' || $id == NULL) {
                $id = $this->Common_model->InsertInformation($insert_data, 'tbl_faq_item');
            } else {
                $this->Common_model->updateInformation2($insert_data, 'faq_item_id', $id, 'tbl_faq_item');
            }

            if (isset($id) && !empty($id)) {
                $faq_item_det_title = $this->input->post('faq_item_det_title');
                $faq_item_det_desc = $this->input->post('faq_item_det_desc');
                $this->insertFaqItemdet($id, $faq_item_det_title, $faq_item_det_desc);
            }

            redirect('admin/Faq');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('FAQ', 'ADD');
                $data = [];
                $view = 'admin/faq/addEditFaq';
                $this->page_title = 'FAQ';
                $data['country_page_data'] = $this->Common_model->geAlldata('tbl_country_page');
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('FAQ', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['country_page_data'] = $this->Common_model->geAlldata('tbl_country_page');
                $data['faq_item_data'] = $this->Common_model->getDataById2('tbl_faq_item', 'faq_item_id', $id, 'Live');
                $data['faq_item_det_data'] = $this->Common_model->geAllDataById('tbl_faq_item_det', 'ref_faq_item_id', $id);
                $view = 'admin/faq/addEditFaq';
                $this->page_title = 'VISA PAGE';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkFaq($category_id = '') {
        if ($category_id != '') {
            $catgory_name = $this->Common_model->getFaq($category_id);
            $respone = $this->Common_model->checkFaq($this->input->get('faq_name'), $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Common_model->checkFaq($this->input->get('faq_name'));
            echo $response;
            die;
        }
    }

    public function deleteFaqItem() {
        $this->Common_model->check_menu_access('FAQ', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('faq_item_id', $id);
            $this->db->update('tbl_faq_item');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function insertFaqItemdet($faq_item_id, $faq_item_det_title, $faq_item_det_desc) {
        $this->Common_model->deleteRecord($faq_item_id, 'tbl_faq_item_det', 'ref_faq_item_id');
        if (isset($faq_item_det_title) && !empty($faq_item_det_title) && isset($faq_item_det_desc) && !empty($faq_item_det_desc)) {
            foreach ($faq_item_det_title as $key => $value) {
                $insert_data = array();
                $insert_data['ref_faq_item_id'] = $faq_item_id;
                $insert_data['faq_item_det_title'] = $value;
                $insert_data['faq_item_det_desc'] = $faq_item_det_desc[$key];
                $this->Common_model->InsertInformation($insert_data, 'tbl_faq_item_det');
            }
        }
    }

}
