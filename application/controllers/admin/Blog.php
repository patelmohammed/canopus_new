<?php

class Blog extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'BLOG_PAGE';
        $this->load->model('Common_model');
    }

    public function index() {
        $this->menu_id = 'BLOG';
        $data = [];
        $data['menu_rights'] = $this->Common_model->get_menu_rights('BLOG');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('BLOG', 'VIEW');
        $view = 'admin/blog/blog';
        $data['blog_data'] = $this->Common_model->geAlldata('tbl_blog');
        $this->page_title = 'BLOG';
        $this->load_admin_view($view, $data);
    }

    public function addEditBlog($encrypted_id = "") {
        $this->menu_id = 'BLOG';
        $id = $encrypted_id;
        if ($this->input->post()) {

            $new_path = 'assets/images/blog/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['blog_poster_image']['name']) && isset($_FILES['blog_poster_image']['name'])) {
                if ($_FILES['blog_poster_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('blog_poster_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Blog');
                    } else {
                        $old_image = $this->input->post('hidden_blog_poster_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $new_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_blog_poster_image');
                }
            } else {
                $image_url = $this->input->post('hidden_blog_poster_image');
            }
            $insert_data['blog_poster_image'] = $image_url;

            if (!empty($_FILES['blog_image']['name']) && isset($_FILES['blog_image']['name'])) {
                if ($_FILES['blog_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('blog_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Blog');
                    } else {
                        $old_image = $this->input->post('hidden_blog_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $new_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_blog_image');
                }
            } else {
                $image_url = $this->input->post('hidden_blog_image');
            }
            $insert_data['blog_image'] = $image_url;

            unset($this->upload);
            $new_path = 'assets/images/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['background_image']['name']) && isset($_FILES['background_image']['name'])) {
                if ($_FILES['background_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('background_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Coaching_page');
                    } else {
                        $old_image = $this->input->post('hidden_background_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
//                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $background_image = $new_path . $image['file_name'];
                    }
                } else {
                    $background_image = $this->input->post('hidden_background_image');
                }
            } else {
                $background_image = $this->input->post('hidden_background_image');
            }
            $insert_data['background_image'] = $background_image;

            $insert_data['blog_title'] = $this->input->post('blog_title');
            $insert_data['blog_short_desc'] = $this->input->post('blog_short_desc');
            $blog_date = $this->input->post('blog_date');
            $insert_data['blog_date'] = isset($blog_date) && !empty($blog_date) ? date('Y-m-d', strtotime($blog_date)) : date('Y-m-d');
            $insert_data['blog_desc'] = $this->input->post('blog_desc');
            $insert_data['blog_video'] = $this->input->post('blog_video');
            $insert_data['blog_poster_type'] = $this->input->post('blog_poster_type');
            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 0;

            $facebook = $this->input->post('facebook');
            $insert_data['facebook'] = (isset($facebook) && !empty($facebook) ? $facebook : 'javascript:void(0);');

            $instagram = $this->input->post('instagram');
            $insert_data['instagram'] = (isset($instagram) && !empty($instagram) ? $instagram : 'javascript:void(0);');

            $linkedin = $this->input->post('linkedin');
            $insert_data['linkedin'] = (isset($linkedin) && !empty($linkedin) ? $linkedin : 'javascript:void(0);');

            $twitter = $this->input->post('twitter');
            $insert_data['twitter'] = (isset($twitter) && !empty($twitter) ? $twitter : 'javascript:void(0);');

            $insert_data['meta_title'] = $this->input->post('meta_title');
            $insert_data['meta_desc'] = $this->input->post('meta_desc');
            $insert_data['meta_key'] = $this->input->post('meta_key');

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $page_id = $this->Common_model->InsertInformation($insert_data, 'tbl_blog');

                if (isset($page_id) && !empty($page_id)) {
                    $slug_data = array();
                    $slug_data['slug'] = createSlug($this->input->post('slug'));
                    $slug_data['ref_table_name'] = 'tbl_blog';
                    $slug_data['ref_primary_id'] = $page_id;
                    $slug_id = $this->Common_model->insertUpdateSlug($slug_data);
                }
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'blog_id', $id, 'tbl_blog');

                $slug_data = array();
                $slug_data['slug'] = createSlug($this->input->post('slug'));
                $slug_data['ref_table_name'] = 'tbl_blog';
                $slug_data['ref_primary_id'] = $id;
                $this->Common_model->insertUpdateSlug($slug_data, $this->input->post('hidden_slug_id'));
            }
            redirect('admin/Blog');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('BLOG', 'ADD');
                $data = [];
                $view = 'admin/blog/addBlog';
                $this->page_title = 'BLOG';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('BLOG', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['blog_data'] = $this->Common_model->getDataById2('tbl_blog', 'blog_id', $id, 'Live');
                $data['slug_data'] = $this->Common_model->getDataById2('tbl_slug', 'ref_primary_id', $id, '', 'ref_table_name', 'tbl_blog');
                $view = 'admin/blog/editBlog';
                $this->page_title = 'BLOG';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkBlogTitle($category_id = '') {
        if ($category_id != '') {
            $catgory_name = $this->Common_model->getBlogTitle($category_id);
            $respone = $this->Common_model->checkBlogTitle($this->input->get('blog_title'), $catgory_name);
            echo $respone;
            die;
        } else {
            $response = $this->Common_model->checkBlogTitle($this->input->get('blog_title'));
            echo $response;
            die;
        }
    }

    public function deactiveBlog() {
        $update_data = $data = array();
        $blog_id = $this->input->post('blog_id');
        $update_data['is_active'] = 2;
        $response = $this->Common_model->updateInformation2($update_data, 'blog_id', $blog_id, 'tbl_blog', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function activeBlog() {
        $update_data = $data = array();
        $blog_id = $this->input->post('blog_id');
        $update_data['is_active'] = 1;
        $response = $this->Common_model->updateInformation2($update_data, 'blog_id', $blog_id, 'tbl_blog', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function deleteBlog() {
        $this->Common_model->check_menu_access('BLOG', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('blog_id', $id);
            $this->db->update('tbl_blog');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function addEditBlogSetting() {
        $this->menu_id = 'BLOG_SETTING';
        $data['blog_setting_data'] = $this->Common_model->getDataByIdStatus('tbl_blog_setting', 'del_status', 'Live');

        $id = $data['blog_setting_data']->blog_setting_id;
        if ($this->input->post()) {
            $insert_data['blog_setting_title'] = $this->input->post('blog_setting_title');
            $insert_data['blog_setting_desc'] = $this->input->post('blog_setting_desc');

            $new_path = 'assets/images/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['background_image']['name']) && isset($_FILES['background_image']['name'])) {
                if ($_FILES['background_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('background_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Coaching_page');
                    } else {
                        $old_image = $this->input->post('hidden_background_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
//                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $background_image = $new_path . $image['file_name'];
                    }
                } else {
                    $background_image = $this->input->post('hidden_background_image');
                }
            } else {
                $background_image = $this->input->post('hidden_background_image');
            }
            $insert_data['background_image'] = $background_image;

            $insert_data['meta_title'] = $this->input->post('meta_title');
            $insert_data['meta_desc'] = $this->input->post('meta_desc');
            $insert_data['meta_key'] = $this->input->post('meta_key');

            $insert_data['UpdUser'] = $this->user_id;
            $insert_data['UpdTerminal'] = $this->input->ip_address();
            $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
            if (isset($id) && !empty($id)) {
                $this->Common_model->updateInformation2($insert_data, 'blog_setting_id', $id, 'tbl_blog_setting');
            }
            redirect('admin/Blog/addEditBlogSetting');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('BLOG_SETTING', 'EDIT');
                $data['slug_data'] = $this->Common_model->getDataById2('tbl_slug', 'ref_primary_id', $id, '', 'ref_table_name', 'tbl_blog_setting');
                $view = 'admin/blog/editBlogSetting';
                $this->page_title = 'BLOG SETTING';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('BLOG_SETTING', 'ADD');
                $this->_show_message("You cant insert new Blog Setting", "error");
                redirect('admin/Blog');
            }
        }
    }

}
