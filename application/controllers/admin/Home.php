<?php

class Home extends My_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->is_login()) {
            redirect('admin');
        }
        $this->page_id = 'HOME';
        $this->load->model('Home_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $this->menu_id = 'PRODUCT_TYPE';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('PRODUCT_TYPE');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('PRODUCT_TYPE', 'VIEW');
        $view = 'admin/product_type/product_type';
        $data['product_type_data'] = $this->Common_model->geAlldata('product_type');
        $this->page_title = 'PRODUCT TYPE';
        $this->load_admin_view($view, $data);
    }

    public function statistic() {
        $data = [];
        $this->menu_id = 'STATISTIC';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('STATISTIC');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('STATISTIC', 'VIEW');
        $view = 'admin/statistic/statistic';
        $data['statistic_data'] = $this->Common_model->geAlldata('statistic');
        $this->page_title = 'STATISTIC';
        $this->load_admin_view($view, $data);
    }

    public function addEditStatistic($encrypted_id = "") {
        $this->menu_id = 'STATISTIC';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $new_path = 'files/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['product_image']['name']) && isset($_FILES['product_image']['name'])) {
                if ($_FILES['product_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('product_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Home/statistic');
                    } else {
                        $image = $this->upload->data();
                        $image_url = $new_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_product_image');
                }
            } else {
                $image_url = $this->input->post('hidden_product_image');
            }
            $insert_data['statistic_image'] = $image_url;
            $insert_data['statistic_name'] = $this->input->post('statistic_name');
            $insert_data['statistic_number'] = $this->input->post('statistic_number');

            $insert_data['UpdUser'] = $this->user_id;
            $insert_data['UpdTerminal'] = $this->input->ip_address();
            $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
            if (isset($id) && !empty($id)) {
                $this->Common_model->updateInformation2($insert_data, 'statistic_id', $id, 'statistic');
            }
            redirect('admin/Home/statistic');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('STATISTIC', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['statistic_data'] = $this->Common_model->getDataById2('statistic', 'statistic_id', $id, 'Live');
                $view = 'admin/statistic/editStatistic';
                $this->page_title = 'STATISTIC';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('STATISTIC', 'ADD');
                $this->_show_message("You cant insert new statistic", "error");
                redirect('admin/Home/statistic');
            }
        }
    }

    public function addEditBackgroundImage($encrypted_id = "") {
        $id = $encrypted_id;
        if ($this->input->post()) {
            $new_path = 'files/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['background_image']['name']) && isset($_FILES['background_image']['name'])) {
                if ($_FILES['background_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('background_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Home/latestCreation');
                    } else {
                        $image = $this->upload->data();
                        $image_url = $new_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_background_image');
                }
            } else {
                $image_url = $this->input->post('hidden_background_image');
            }
            $insert_data['bg_image'] = $image_url;

            $insert_data['UpdUser'] = $this->user_id;
            $insert_data['UpdTerminal'] = $this->input->ip_address();
            $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
            if (isset($id) && !empty($id)) {
                $this->Common_model->updateInformation2($insert_data, 'bg_id', $id, 'background_img');
            }
        }
        $url = $this->input->post('this_page_location');
        if (isset($url) && !empty($url)) {
            redirect($url);
        } else {
            redirect('admin');
        }
    }

    public function addEditProfile() {
        $this->menu_id = 'PROFILE';
        $id = $this->user_id;
        if ($this->input->post()) {
            $user_info = $this->Common_model->getDataById('user_information', $id);
            $old_password = $this->input->post('old_password');
            $new_password = $this->input->post('new_password');
            $confirm_new_password = $this->input->post('confirm_new_password');
            if ((isset($old_password) && !empty($old_password)) && (md5($old_password) == $user_info->password)) {
                if ((isset($new_password) && !empty($new_password)) && (isset($confirm_new_password) && !empty($confirm_new_password))) {
                    if ($new_password == $confirm_new_password) {
                        $insert_data['password'] = md5($new_password);
                    } else {
                        $this->_show_message("New Password And Confirm Password not matched", "error");
                        redirect('admin/Home/addEditProfile');
                    }
                }
            } else {
                $this->_show_message("Old Password not matched", "error");
                redirect('admin/Home/addEditProfile');
            }
            $insert_data['user_name'] = $this->input->post('user_name');

            $insert_data['UpdUser'] = $this->user_id;
            $insert_data['UpdTerminal'] = $this->input->ip_address();
            $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
            if (isset($id) && !empty($id)) {
                $this->_show_message("Your password changed successfully", "success");
                $this->Common_model->updateInformation2($insert_data, 'id', $id, 'user_information');
            }
            redirect('admin/Dashboard');
        } else {
            if (isset($id) && !empty($id)) {
                $data = [];
                $data['user_data'] = $this->Common_model->getDataById2('user_information', 'id', $id, 'Live');
                $view = 'admin/home/editProfile';
                $this->page_title = 'PROFILE';
                $this->load_admin_view($view, $data);
            } else {
                $this->_show_message("You cant insert new profile", "error");
                redirect('admin/Dashboard');
            }
        }
    }

    public function visaCategory() {
        $this->menu_id = 'VISA_CATEGORY';
        $data = [];
        $data['menu_rights'] = $this->Common_model->get_menu_rights('VISA_CATEGORY');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('VISA_CATEGORY', 'VIEW');
        $view = 'admin/visa_category/visa_category';
        $data['visa_category_data'] = $this->Common_model->geAlldata('tbl_visa_category');
        $this->page_title = 'VISA_CATEGORY';
        $this->load_admin_view($view, $data);
    }

    public function addEditVisaCategory($encrypted_id = "") {
        $this->menu_id = 'VISA_CATEGORY';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data['visa_category_title'] = $this->input->post('visa_category_title');
            $insert_data['visa_category_description'] = $this->input->post('visa_category_description');
            $insert_data['icon'] = $this->input->post('icon');

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->insertInformation($insert_data, 'tbl_visa_category');
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'visa_category_id', $id, 'tbl_visa_category');
            }
            redirect('admin/Home/visaCategory');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('VISA_CATEGORY', 'ADD');
                $data = [];
                $view = 'admin/visa_category/addVisaCategory';
                $this->page_title = 'CHOOSE COUNTRY';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('VISA_CATEGORY', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['visa_category_data'] = $this->Common_model->getDataById2('tbl_visa_category', 'visa_category_id', $id, 'Live');
                $view = 'admin/visa_category/editVisaCategory';
                $this->page_title = 'VISA CATEGORY';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function deactiveVisaCategory() {
        $update_data = $data = array();
        $visa_category_id = $this->input->post('visa_category_id');
        $update_data['is_active'] = 2;
        $response = $this->Common_model->updateInformation2($update_data, 'visa_category_id', $visa_category_id, 'tbl_visa_category', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function activeVisaCategory() {
        $update_data = $data = array();
        $visa_category_id = $this->input->post('visa_category_id');
        $update_data['is_active'] = 1;
        $response = $this->Common_model->updateInformation2($update_data, 'visa_category_id', $visa_category_id, 'tbl_visa_category', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function testimonial() {
        $this->menu_id = 'TESTIMONIAL';
        $data = [];
        $data['menu_rights'] = $this->Common_model->get_menu_rights('TESTIMONIAL');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('TESTIMONIAL', 'VIEW');
        $view = 'admin/testimonial/testimonial';
        $data['testimonial_data'] = $this->Common_model->geAlldata('tbl_testimonial');
        $this->page_title = 'TESTIMONIAL';
        $this->load_admin_view($view, $data);
    }

    public function addEditTestimonial($encrypted_id = "") {
        $this->menu_id = 'TESTIMONIAL';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $insert_data['testimonial_name'] = $this->input->post('testimonial_name');
            $insert_data['testimonial_designation'] = $this->input->post('testimonial_designation');
            $insert_data['testimonial_description'] = $this->input->post('testimonial_description');
            $insert_data['testimonial_rating'] = $this->input->post('testimonial_rating');

            $new_path = 'assets/images/testimonial/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['testimonial_image']['name']) && isset($_FILES['testimonial_image']['name'])) {
                if ($_FILES['testimonial_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('testimonial_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Slider');
                    } else {
                        $old_image = $this->input->post('hidden_testimonial_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $new_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_testimonial_image');
                }
            } else {
                $image_url = $this->input->post('hidden_testimonial_image');
            }
            $insert_data['testimonial_image'] = $image_url;

            if ($id == "" || $id == '' || $id == NULL) {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->insertInformation($insert_data, 'tbl_testimonial');
            } else {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'testimonial_id', $id, 'tbl_testimonial');
            }
            redirect('admin/Home/testimonial');
        } else {
            if ($id == "" || $id == '' || $id == NULL) {
                $this->Common_model->check_menu_access('TESTIMONIAL', 'ADD');
                $data = [];
                $view = 'admin/testimonial/addTestimonial';
                $this->page_title = 'TESTIMONIAL';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('TESTIMONIAL', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['testimonial_data'] = $this->Common_model->getDataById2('tbl_testimonial', 'testimonial_id', $id, 'Live');
                $view = 'admin/testimonial/editTestimonial';
                $this->page_title = 'TESTIMONIAL';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function deactiveTestimonial() {
        $update_data = $data = array();
        $testimonial_id = $this->input->post('testimonial_id');
        $update_data['is_active'] = 2;
        $response = $this->Common_model->updateInformation2($update_data, 'testimonial_id', $testimonial_id, 'tbl_testimonial', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function activeTestimonial() {
        $update_data = $data = array();
        $testimonial_id = $this->input->post('testimonial_id');
        $update_data['is_active'] = 1;
        $response = $this->Common_model->updateInformation2($update_data, 'testimonial_id', $testimonial_id, 'tbl_testimonial', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function deleteTestimonial() {
        $this->Common_model->check_menu_access('TESTIMONIAL', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('testimonial_id', $id);
            $this->db->update('tbl_testimonial');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

    public function visaCategorySetting($encrypted_id = "") {
        $this->menu_id = 'VISA_CATEGORY_SETTING';
        $data = [];
        $data['setting_data'] = $this->Home_model->getVisaCategorySetting();
        $id = $data['setting_data']->visa_category_setting_id;
        if ($this->input->post()) {
            $insert_data['visa_category_setting_title'] = $this->input->post('visa_category_setting_title');
            $insert_data['visa_category_setting_description'] = $this->input->post('visa_category_setting_description');
            $insert_data['visa_category_setting_footer'] = $this->input->post('visa_category_setting_footer');

            if (isset($id) && !empty($id)) {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'visa_category_setting_id', $id, 'tbl_visa_category_setting');
            }
            redirect('admin/Home/visaCategorySetting');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('VISA_CATEGORY_SETTING', 'EDIT');
                $view = 'admin/visa_category/editVisaCategorySetting';
                $this->page_title = 'VISA CATEGORY SETTING';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function CountrySetting($encrypted_id = "") {
        $this->menu_id = 'COUNTRY_SETTING';
        $data = [];
        $data['country_setting_data'] = $this->Common_model->getDataById2('tbl_country_setting', 'del_status', 'Live', 'Live');
        $id = $data['country_setting_data']->country_setting_id;
        if ($this->input->post()) {
            $insert_data['country_setting_title'] = $this->input->post('country_setting_title');
            $insert_data['country_setting_description'] = $this->input->post('country_setting_description');
            $insert_data['country_setting_desc_2'] = $this->input->post('country_setting_desc_2');

            if (isset($id) && !empty($id)) {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'country_setting_id', $id, 'tbl_country_setting');
            }
            redirect('admin/Home/CountrySetting');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('COUNTRY_SETTING', 'EDIT');
                $view = 'admin/visa_category/editCountrySetting';
                $this->page_title = 'COUNTRY SETTING';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function testimonialSetting($encrypted_id = "") {
        $this->menu_id = 'TESTIMONIAL_SETTING';
        $data = [];
        $data['setting_data'] = $this->Home_model->getTestimonialSetting();
        $id = $data['setting_data']->testimonial_setting_id;
        if ($this->input->post()) {
            $insert_data['testimonial_setting_title'] = $this->input->post('testimonial_setting_title');
            $insert_data['testimonial_setting_desc'] = $this->input->post('testimonial_setting_desc');
            $insert_data['testimonial_setting_sub_desc'] = $this->input->post('testimonial_setting_sub_desc');

            $insert_data['sub_title_1'] = $this->input->post('sub_title_1');
            $insert_data['sub_desc_1'] = $this->input->post('sub_desc_1');

            $insert_data['sub_title_2'] = $this->input->post('sub_title_2');
            $insert_data['sub_desc_2'] = $this->input->post('sub_desc_2');

            $insert_data['sub_title_3'] = $this->input->post('sub_title_3');
            $insert_data['sub_desc_3'] = $this->input->post('sub_desc_3');

            if (isset($id) && !empty($id)) {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'testimonial_setting_id', $id, 'tbl_testimonial_setting');
            }
            redirect('admin/Home/testimonialSetting');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('TESTIMONIAL_SETTING', 'EDIT');
                $view = 'admin/testimonial/editTestimonialSetting';
                $this->page_title = 'TESTIMONIAL SETTING';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function coachingPreparationSetting($encrypted_id = "") {
        $this->menu_id = 'VISA_PREPARATION_SETTING';
        $data = [];
        $data['setting_data'] = $this->Home_model->getCoachingPreparationSetting();
        $id = $data['setting_data']->coaching_setting_id;
        if ($this->input->post()) {
            $insert_data['coaching_setting_title'] = $this->input->post('coaching_setting_title');
            $insert_data['coaching_setting_desc'] = $this->input->post('coaching_setting_desc');

            $insert_data['setting_image_title'] = $this->input->post('setting_image_title');
            $insert_data['setting_image_desc'] = $this->input->post('setting_image_desc');

            $new_path = 'assets/images/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['coaching_setting_image']['name']) && isset($_FILES['coaching_setting_image']['name'])) {
                if ($_FILES['coaching_setting_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('coaching_setting_image')) {
                        $error = array('error' => $this->upload->display_errors());
                        $this->_show_message("Somethig wrong", "error");
                        redirect('admin/Home/coachingPreparationSetting');
                    } else {
                        $old_image = $this->input->post('hidden_coaching_setting_image');
                        if (isset($old_image) && !empty($old_image) && file_exists($old_image)) {
                            unlink($old_image);
                        }
                        $image = $this->upload->data();
                        $image_url = $new_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_coaching_setting_image');
                }
            } else {
                $image_url = $this->input->post('hidden_coaching_setting_image');
            }
            $insert_data['coaching_setting_image'] = $image_url;

            if (isset($id) && !empty($id)) {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->updateInformation2($insert_data, 'coaching_setting_id', $id, 'tbl_coaching_setting');
            }
            redirect('admin/Home/coachingPreparationSetting');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('VISA_PREPARATION_SETTING', 'EDIT');
                $view = 'admin/visa_preparation/editVisaPreparationSetting';
                $this->page_title = 'VISA PREPARATION SETTING';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkSlug($slug_id = '') {
        if ($slug_id != '') {
            $slug_name = $this->Common_model->getSlugName($slug_id);
            $respone = $this->Common_model->checkSlugName($this->input->get('slug'), $slug_name);
            echo $respone;
            die;
        } else {
            $response = $this->Common_model->checkSlugName($this->input->get('slug'));
            echo $response;
            die;
        }
    }

    public function client() {
        $data = [];
        $this->menu_id = 'UNIVERSITY';
        $data['menu_rights'] = $this->Common_model->get_menu_rights('UNIVERSITY');
        if (empty($data['menu_rights'])) {
            redirect('admin/Auth/Unauthorized');
        }
        $this->Common_model->check_menu_access('UNIVERSITY', 'VIEW');
        $view = 'admin/client/client';
        $data['client_data'] = $this->Common_model->geAlldata('client');
        $this->page_title = 'UNIVERSITY';
        $this->load_admin_view($view, $data);
    }

    public function addEditClient($encrypted_id = "") {
        $this->menu_id = 'UNIVERSITY';
        $id = $encrypted_id;
        if ($this->input->post()) {
            $new_path = 'assets/images/client/';
            if (!is_dir($new_path)) {
                if (!mkdir($new_path, 0777, true)) {
                    die('Not Created');
                }
            }

            if (!empty($_FILES['client_image']['name']) && isset($_FILES['client_image']['name'])) {
                if ($_FILES['client_image']['name']) {
                    $config['upload_path'] = $new_path;
                    $config['allowed_types'] = 'jpg|png|jpeg|JPEG|JPG|PNG';
                    $config['max_size'] = "*";
                    $config['max_width'] = "*";
                    $config['max_height'] = "*";
                    $config['encrypt_name'] = FALSE;

                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('client_image')) {
                        $this->_show_message($this->upload->display_errors(), "error");
                        redirect('admin/Home/addEditClient');
                    } else {
                        $image = $this->upload->data();
                        $image_url = $new_path . $image['file_name'];
                    }
                } else {
                    $image_url = $this->input->post('hidden_client_image');
                }
            } else {
                $image_url = $this->input->post('hidden_client_image');
            }
            $insert_data['client_image'] = $image_url;
            $insert_data['client_name'] = $this->input->post('client_name');
            $insert_data['is_active'] = $this->input->post('is_active') == 'on' ? 1 : 2;

            if (isset($id) && !empty($id)) {
                $insert_data['UpdUser'] = $this->user_id;
                $insert_data['UpdTerminal'] = $this->input->ip_address();
                $insert_data['UpdDateTime'] = date('Y/m/d H:i:s');

                $this->Common_model->updateInformation2($insert_data, 'client_id', $id, 'client');
            } else {
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

                $this->Common_model->insertInformation($insert_data, 'client');
            }
            redirect('admin/Home/client');
        } else {
            if (isset($id) && !empty($id)) {
                $this->Common_model->check_menu_access('UNIVERSITY', 'EDIT');
                $data = [];
                $data['encrypted_id'] = $encrypted_id;
                $data['client_data'] = $this->Common_model->getDataById2('client', 'client_id', $id, 'Live');
                $view = 'admin/client/addEditClient';
                $this->page_title = 'UNIVERSITY';
                $this->load_admin_view($view, $data);
            } else {
                $this->Common_model->check_menu_access('UNIVERSITY', 'EDIT');
                $data = [];
                $view = 'admin/client/addEditClient';
                $this->page_title = 'UNIVERSITY';
                $this->load_admin_view($view, $data);
            }
        }
    }

    public function checkClientName($slug_id = '') {
        if ($slug_id != '') {
            $slug_name = $this->Common_model->getClientName($slug_id);
            $respone = $this->Common_model->checkClientName($this->input->get('slug'), $slug_name);
            echo $respone;
            die;
        } else {
            $response = $this->Common_model->checkClientName($this->input->get('slug'));
            echo $response;
            die;
        }
    }

    public function deactiveClient() {
        $update_data = $data = array();
        $client_id = $this->input->post('client_id');
        $update_data['is_active'] = 2;
        $response = $this->Common_model->updateInformation2($update_data, 'client_id ', $client_id, 'client', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function activeClient() {
        $update_data = $data = array();
        $client_id = $this->input->post('client_id');
        $update_data['is_active'] = 1;
        $response = $this->Common_model->updateInformation2($update_data, 'client_id ', $client_id, 'client', 'Live');
        if ($response == TRUE) {
            $data['result'] = TRUE;
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function deleteClient() {
        $this->Common_model->check_menu_access('UNIVERSITY', 'DELETE');
        $id = $this->input->post('id');
        if (isset($id) && !empty($id)) {
            $this->db->set('del_status', "Deleted");
            $this->db->where('client_id ', $id);
            $this->db->update('client');
            if ($this->db->affected_rows() > 0) {
                $this->_show_message("Information has been deleted successfully!", "success");
                $data['result'] = 'success';
            } else {
                $data['result'] = false;
            }
        } else {
            $data['result'] = false;
        }
        echo json_encode($data);
        die;
    }

}
