<?php

class Country extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'COUNTRY';
        $this->load->model('Home_model');
        $this->load->model('Common_model');
    }

    public function index($slug = NULL) {
        $page_id = $this->Common_model->getPrimaryBySlug($slug);
        $data = [];
        $view = 'country_page';
        if (isset($page_id) && !empty($page_id)) {
            $data['page_data'] = $this->Common_model->getDataById2('tbl_country_page', 'country_page_id', $page_id, 'Live', 'is_active', 1);
        } else {
            $data['page_data'] = $this->Common_model->getDataById2('tbl_country_page', 'is_active', 1, 'Live');
        }
        $data['contact_data'] = $this->Common_model->getDataById2('contact', 'del_status', 'Live');
        if (isset($data['page_data']) && !empty($data['page_data'])) {
            $data['page_data']->item_data = $this->Common_model->getCountryPageitemData($data['page_data']->country_page_id, 1);
            $this->meta_desc = $data['page_data']->meta_desc;
            $this->meta_title = $data['page_data']->meta_title;
            $this->meta_key = $data['page_data']->meta_key;
        } else {
            redirect(base_url());
        }
        $this->menu_id = isset($data['page_data']->country_page_id) && !empty($data['page_data']->country_page_id) ? 'COUNTRY_' . $data['page_data']->country_page_id : '';
        $this->page_title = 'HOME';
        $this->load_view($view, $data);
    }

}
