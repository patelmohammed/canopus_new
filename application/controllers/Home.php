<?php

class Home extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'HOME';
        $this->load->model('Home_model');
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $view = 'home/index';
        $data['slider_data'] = $this->Home_model->geSliderdata();
        $data['country_setting_data'] = $this->Common_model->getDataById2('tbl_country_setting', 'del_status', 'Live', 'Live');
        $data['country_data'] = $this->Common_model->getCountyPage();
        $data['visa_category_data'] = $this->Common_model->getAllVisaPage();
        $data['visa_category_setting_data'] = $this->Home_model->getVisaCategorySetting();
        $data['testimonial_data'] = $this->Common_model->geAlldataById('tbl_testimonial', 'is_active', 1, 'del_status = "Live"');
        $data['testimonial_setting_data'] = $this->Home_model->getTestimonialSetting();
        $data['visa_preparation_data'] = $this->Common_model->getAllCoachingPageHome();
        $data['visa_pre_setting_data'] = $this->Home_model->getCoachingPreparationSetting();
        $data['blog_data'] = $this->Common_model->getLatestBlog();
        $data['client_data'] = $this->Common_model->geAlldataById('client', 'is_active', 1, 'del_status = "Live"');
        $this->page_title = 'HOME';
        $this->load_view($view, $data);
    }

    public function newsletter() {
        $template_data = $this->Common_model->getDataById2('mail_template', 'template_for', 'newsletter', 'Live');
        if ($this->input->post()) {
            $newsletter_email = $this->input->post('newsletter_email');
            $response = $this->Common_model->chkUniqueData('newsletter', 'newsletter_email', $newsletter_email, 'Live');
            if ($response == TRUE) {
                $from_email = $smtp_setting->smtp_user;
                $this->email->from($from_email, COMPANY_NAME);
                $this->email->subject('Newsletter');
                $this->email->message($template_data->template);
                $this->email->to($newsletter_email);

                $insert_data['newsletter_email'] = $newsletter_email;
                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

                if ($this->email->send()) {

                    $this->email->from($from_email, COMPANY_NAME);
                    $this->email->subject('Newsletter');
                    $this->email->message($newsletter_email . ' just subscribed Sweet n Rush');
                    $admin_email = isset($template_data->mail_to) && !empty($template_data->mail_to) ? $template_data->mail_to : COMPANY_EMAIL;
                    $this->email->to($admin_email);
                    $this->email->send();

                    $id = $this->Common_model->insertInformation($insert_data, 'newsletter');
                }
                if (isset($id) && !empty($id)) {
                    $data['result'] = TRUE;
                } else {
                    $data['result'] = FALSE;
                }
            } else {
                $data['result'] = FALSE;
            }
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function privacy() {
        $data = [];
        $this->page_id = 'privacy';
        $view = 'privacy';
        $data['privacy_policy_data'] = $this->Common_model->getDataById2('privacy_policy', 'page_id', $view, 'Live');
        $this->meta_desc = $data['privacy_policy_data']->meta_desc;
        $this->meta_title = $data['privacy_policy_data']->meta_title;
        $this->meta_key = $data['privacy_policy_data']->meta_key;
//        $data['background_data'] = $this->Common_model->GetBackgroundImage($this->page_id);
        $this->page_title = 'PRIVACY';
        $this->load_view($view, $data);
    }

    public function termsCondition() {
        $data = [];
        $this->page_id = 'terms_condition';
        $view = 'privacy';
        $data['privacy_policy_data'] = $this->Common_model->getDataById2('privacy_policy', 'page_id', 'terms_condition', 'Live');
        $data['background_data'] = $this->Common_model->GetBackgroundImage($this->page_id);
        $this->meta_desc = $data['privacy_policy_data']->meta_desc;
        $this->meta_title = $data['privacy_policy_data']->meta_title;
        $this->meta_key = $data['privacy_policy_data']->meta_key;
        $this->page_title = 'TERMS';
        $this->load_view($view, $data);
    }
    
    public function page_not_found() {
        $data = [];
        $view = 'page_not_found';
        $this->page_title = 'Page Not Found';
        $this->load_view($view, $data);
    }

}
