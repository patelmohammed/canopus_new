<?php

class Faq extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'FAQ';
        $this->load->model('Common_model');
        $this->load->model('Faq_model');
        $this->load->model('Home_model');
    }

    public function index($slug = NULL) {
        $data = [];
        $view = 'faq';
        $data['country_data'] = $this->Faq_model->getFaqCountryData();

        $country_id = $this->Common_model->getPrimaryBySlug($slug);
        $data['country_page_data'] = $this->Common_model->getDataById2('tbl_country_page', 'country_page_id', $country_id, 'Live');
        $data['faq_data'] = $this->Faq_model->getFaqDataById($country_id);

        $this->page_title = 'FAQ';
        $this->load_view($view, $data);
    }

    public function student_service() {
        $data = [];
        $view = 'student_service';
        $data['testimonial_data'] = $this->Common_model->geAlldataById('tbl_testimonial', 'is_active', 1, 'del_status = "Live"');
        $data['testimonial_setting_data'] = $this->Home_model->getTestimonialSetting();
        $this->page_title = 'student service';
        $this->load_view($view, $data);
    }

}
