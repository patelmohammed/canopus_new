<?php

class Register extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'CONTACT';
        $this->load->model('Common_model');
    }

    public function index() {
        $data = array();
        if ($this->input->post()) {
            $name = $this->input->post('name');
            $phone = $this->input->post('phone');
            $whatsapp_no = $this->input->post('whatsapp_no');
            $email = $this->input->post('email');
            $dob = $this->input->post('dob');
            $dob = (isset($dob) && !empty($dob) ? date('d-M-Y', strtotime($dob)) : '');
            $current_education = $this->input->post('current_education');
            $experience = $this->input->post('experience');
            $service = $this->input->post('service');
            $registration = $this->input->post('registration');
            $ielts_appeared = $this->input->post('ielts_appeared');
            $appointment_date = $this->input->post('appointment_date');
            $appointment_date = (isset($appointment_date) && !empty($appointment_date) ? date('d-M-Y', strtotime($appointment_date)) : '');
            $appointment_time = $this->input->post('appointment_time');
            $appointment_time = (isset($appointment_time) && !empty($appointment_time) ? date('h:i A', strtotime($appointment_time)) : '');

            $data = array();
            $data['template'] = $this->Common_model->getEmailTemplate('register_customer');
            $data['company_data'] = $this->Common_model->getDataById2('footer_setting', 'del_status', 'Live', 'Live');
            $html_template = $this->load->view('email_template', $data, true);

            $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
            $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
            $this->email->from($from_email, $from_name);
            $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
            if (isset($data['template']) && !empty($data['template'])) {
                $word = array('{person_name}');
                $replace = array($name);
                $body = str_replace($word, $replace, $html_template);
            }
            $this->email->message($body);
            $this->email->to($email);

            if ($this->email->send()) {
                $data['template'] = $this->Common_model->getEmailTemplate('register_admin');
                $html_template = $this->load->view('email_template', $data, true);

                $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                $this->email->from($from_email, $from_name);
                $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
                if (isset($data['template']) && !empty($data['template'])) {
                    $word = array('{name}', '{phone}', '{whatsapp_no}', '{email}', '{dob}', '{current_education}', '{experience}', '{service}', '{registration}', '{ielts_appeared}', '{appointment_date}', '{appointment_time}');
                    $replace = array($name, $phone, $whatsapp_no, $email, $dob, $current_education, $experience, $service, $registration, $ielts_appeared, $appointment_date, $appointment_time);
                    $body = str_replace($word, $replace, $html_template);
                }
                $this->email->message($body);
                $this->email->to($data['company_data']->footer_email);
                $this->email->send();
                $data['result'] = TRUE;
            } else {
                $data['result'] = FALSE;
            }
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

}
