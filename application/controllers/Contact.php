<?php

class Contact extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'CONTACT';
        $this->load->model('Common_model');
    }

    public function index() {
        $data = [];
        $view = 'contact';
        $data['contact_data'] = $this->Common_model->getDataById2('contact', 'del_status', 'Live');
        $this->meta_desc = $data['contact_data']->meta_desc;
        $this->meta_title = $data['contact_data']->meta_title;
        $this->meta_key = $data['contact_data']->meta_key;
//        $data['background_data'] = $this->Common_model->GetBackgroundImage($this->page_id);
        $this->page_title = 'CONTACT';
        $this->load_view($view, $data);
    }

    public function contact_us() {
        $data = array();
        $html_template = $this->load->view('email_template', $data, true);
        if ($this->input->post()) {
            $contact_name = $this->input->post('contact_name');
            $insert_data['contact_name'] = isset($contact_name) && !empty($contact_name) ? strip_tags($contact_name) : '';

            $contact_email = $this->input->post('contact_email');
            $insert_data['contact_email'] = isset($contact_email) && !empty($contact_email) ? $contact_email : '';

            $contact_phone = $this->input->post('contact_phone');
            $insert_data['contact_phone'] = isset($contact_phone) && !empty($contact_phone) ? $contact_phone : '';

            $contact_message = $this->input->post('contact_message');
            $insert_data['contact_message'] = isset($contact_message) && !empty($contact_message) ? strip_tags($contact_message) : '';

            $insert_data['InsUser'] = $this->user_id;
            $insert_data['InsTerminal'] = $this->input->ip_address();
            $insert_data['InsDateTime'] = date('Y/m/d H:i:s');


            $data = array();
            $data['template'] = $this->Common_model->getEmailTemplate('contact_us_customer');
            $data['company_data'] = $this->Common_model->getDataById2('footer_setting', 'del_status', 'Live', 'Live');
            $html_template = $this->load->view('email_template', $data, true);

            $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
            $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
            $this->email->from($from_email, $from_name);
            $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
            if (isset($data['template']) && !empty($data['template'])) {
                $word = array('{person_name}');
                $replace = array($insert_data['contact_name']);
                $body = str_replace($word, $replace, $html_template);
            }
            $this->email->message($body);
            $this->email->to($insert_data['contact_email']);

            if ($this->email->send()) {
                $data['template'] = $this->Common_model->getEmailTemplate('contact_us_admin');
                $html_template = $this->load->view('email_template', $data, true);

                $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                $this->email->from($from_email, $from_name);
                $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
                if (isset($data['template']) && !empty($data['template'])) {
                    $word = array('{contact_name}', '{contact_email}', '{contact_phone}', '{contact_message}');
                    $replace = array($insert_data['contact_name'], $insert_data['contact_email'], $insert_data['contact_phone'], $insert_data['contact_message']);
                    $body = str_replace($word, $replace, $html_template);
                }
                $this->email->message($body);
                $this->email->to($data['company_data']->footer_email);
                $this->email->send();

                $id = $this->Common_model->insertInformation($insert_data, 'contact_us');
            }
            if (isset($id) && !empty($id)) {
                $data['result'] = TRUE;
            } else {
                $data['result'] = FALSE;
            }
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

    public function register() {
        $data = array();
        if ($this->input->post()) {
            $name = $this->input->post('name');
            $phone = $this->input->post('phone');
            $whatsapp_no = $this->input->post('whatsapp_no');
            $email = $this->input->post('email');
            $dob = $this->input->post('dob');
            $current_education = $this->input->post('current_education');
            $experience = $this->input->post('experience');
            $service = $this->input->post('service');
            $registration = $this->input->post('registration');
            $ielts_appeared = $this->input->post('ielts_appeared');
            $appointment_date = $this->input->post('appointment_date');
            $appointment_time = $this->input->post('appointment_time');

            $data = array();
            $data['template'] = $this->Common_model->getEmailTemplate('register_customer');
            $data['company_data'] = $this->Common_model->getDataById2('footer_setting', 'del_status', 'Live', 'Live');
            $html_template = $this->load->view('email_template', $data, true);

            $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
            $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
            $this->email->from($from_email, $from_name);
            $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
            if (isset($data['template']) && !empty($data['template'])) {
                $word = array('{person_name}');
                $replace = array($name);
                $body = str_replace($word, $replace, $html_template);
            }
            $this->email->message($body);
            $this->email->to($email);

            if ($this->email->send()) {
                $data['template'] = $this->Common_model->getEmailTemplate('register_admin');
                $html_template = $this->load->view('email_template', $data, true);

                $from_email = (isset($data['template']->fromemail) && !empty($data['template']->fromemail) ? $data['template']->fromemail : FROM_EMAIL);
                $from_name = (isset($data['template']->fromname) && !empty($data['template']->fromname) ? $data['template']->fromname : COMPANY_NAME);
                $this->email->from($from_email, $from_name);
                $this->email->subject(($data['template']->subject . ADDITIONAL_EMAIL_SUBJECT));
                if (isset($data['template']) && !empty($data['template'])) {
                    $word = array('{name}', '{phone}', '{whatsapp_no}', '{email}', '{dob}', '{current_education}', '{experience}', '{service}', '{registration}', '{ielts_appeared}', '{appointment_date}', '{appointment_time}');
                    $replace = array($name, $phone, $whatsapp_no, $email, $dob, $current_education, $experience, $service, $registration, $ielts_appeared, $appointment_date, $appointment_time);
                    $body = str_replace($word, $replace, $html_template);
                }
                $this->email->message($body);
                $this->email->to($data['company_data']->footer_email);
                $this->email->send();
            }
            if (isset($id) && !empty($id)) {
                $data['result'] = TRUE;
            } else {
                $data['result'] = FALSE;
            }
        } else {
            $data['result'] = FALSE;
        }
        echo json_encode($data);
        die;
    }

}
