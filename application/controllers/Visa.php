<?php

class Visa extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'VISA';
        $this->load->model('Home_model');
        $this->load->model('Common_model');
    }

    public function index($slug = NULL) {
        $page_id = $this->Common_model->getPrimaryBySlug($slug);
        $data = [];
        $data['statistic_data'] = $this->Common_model->getDataById2('tbl_statistic', 'del_status', 'Live', 'Live');
        if (isset($data['statistic_data']) && !empty($data['statistic_data'])) {
            $data['statistic_data']->statistic_item_data = $this->Common_model->geAlldataById('tbl_statistic_item', 'ref_statistic_id', $data['statistic_data']->statistic_id, '', 'is_active', 1);
        }
        if (isset($page_id) && !empty($page_id) && $page_id == 6) {
            $view = 'education_loan';

            $data['page_data'] = $this->Common_model->getDataById2('tbl_education_loan', 'del_status', 'Live');
            $page_data = $this->Common_model->getDataById2('tbl_visa_page', 'visa_page_id', $page_id, 'Live', 'is_active', 1);
            $data['page_data']->background_image = (isset($page_data->background_image) && !empty($page_data->background_image) ? $page_data->background_image : '');
            $data['page_data']->visa_page_name = (isset($page_data->visa_page_name) && !empty($page_data->visa_page_name) ? $page_data->visa_page_name : '');
            $data['page_data']->visa_page_desc = (isset($page_data->visa_page_desc) && !empty($page_data->visa_page_desc) ? $page_data->visa_page_desc : '');
            $data['page_data']->visa_page_id = (isset($page_data->visa_page_id) && !empty($page_data->visa_page_id) ? $page_data->visa_page_id : '');
            $data['page_data']->meta_desc = (isset($page_data->meta_desc) && !empty($page_data->meta_desc) ? $page_data->meta_desc : '');
            $data['page_data']->meta_title = (isset($page_data->meta_title) && !empty($page_data->meta_title) ? $page_data->meta_title : '');
            $data['page_data']->meta_key = (isset($page_data->meta_key) && !empty($page_data->meta_key) ? $page_data->meta_key : '');

            $data['section_2_left_item_data'] = $this->Common_model->geAlldataById('tbl_section_2_left_education_loan', 'ref_education_loan_id', $data['page_data']->education_loan_id);
            $data['section_2_right_item_data'] = $this->Common_model->geAlldataById('tbl_section_2_right_education_loan', 'ref_education_loan_id', $data['page_data']->education_loan_id);
            $data['section_3_item_data'] = $this->Common_model->geAlldataById('tbl_section_3_education_loan', 'ref_education_loan_id', $data['page_data']->education_loan_id);
            $data['section_4_item_data'] = $this->Common_model->geAlldataById('tbl_section_4_education_loan', 'ref_education_loan_id', $data['page_data']->education_loan_id);

            $data['testimonial_data'] = $this->Common_model->geAlldataById('tbl_testimonial', 'is_active', 1, 'del_status = "Live"');
        } else {
            $view = 'visa_page';

            if (isset($page_id) && !empty($page_id)) {
                $data['page_data'] = $this->Common_model->getDataById2('tbl_visa_page', 'visa_page_id', $page_id, 'Live', 'is_active', 1);
            } else {
                $data['page_data'] = $this->Common_model->getDataById2('tbl_visa_page', 'is_active', 1, 'Live');
            }
        }

        if (isset($data['page_data']) && !empty($data['page_data'])) {
            $data['download_document'] = $this->Common_model->geAlldataById('tbl_visa_page_document', 'ref_visa_page_id', $page_id, '', 'is_active', 1);
            $this->meta_desc = $data['page_data']->meta_desc;
            $this->meta_title = $data['page_data']->meta_title;
            $this->meta_key = $data['page_data']->meta_key;
        } else {
            redirect(base_url());
        }
        $this->menu_id = isset($data['page_data']->visa_page_id) && !empty($data['page_data']->visa_page_id) ? 'VISA_' . $data['page_data']->visa_page_id : '';
        $this->page_title = 'HOME';
        $this->load_view($view, $data);
    }

}
