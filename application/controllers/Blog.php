<?php

class Blog extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'BLOG';
        $this->load->model('Home_model');
        $this->load->model('Common_model');
    }

    public function index($slug = NULL) {
        $blog_id = $this->Common_model->getPrimaryBySlug($slug);
        $data = [];
        $view = 'blog';
//        $data['statistic_data'] = $this->Common_model->getDataById2('tbl_statistic', 'del_status', 'Live', 'Live');
//        if (isset($data['statistic_data']) && !empty($data['statistic_data'])) {
//            $data['statistic_data']->statistic_item_data = $this->Common_model->geAlldataById('tbl_statistic_item', 'ref_statistic_id', $data['statistic_data']->statistic_id, '', 'is_active', 1);
//        }
        if (isset($blog_id) && !empty($blog_id)) {
            $data['blog_data'] = $this->Common_model->getDataById2('tbl_blog', 'blog_id', $blog_id, 'Live', 'is_active', 1);
            $this->meta_desc = $data['blog_data']->meta_desc;
            $this->meta_title = $data['blog_data']->meta_title;
            $this->meta_key = $data['blog_data']->meta_key;
            $view = 'blog_single';
        } else {
            $data['blog_setting_data'] = $this->Common_model->getDataById2('tbl_blog_setting', 'blog_setting_id', 1, 'Live');
            $this->meta_desc = $data['blog_setting_data']->meta_desc;
            $this->meta_title = $data['blog_setting_data']->meta_title;
            $this->meta_key = $data['blog_setting_data']->meta_key;
            $data['blog_data'] = $this->Common_model->getLatestBlog();
            $view = 'blog';
        }
        $data['latest_blog_data'] = $this->Common_model->getLatestBlog(3, $blog_id);
        $data['contact_data'] = $this->Common_model->getDataById2('contact', 'del_status', 'Live');
        $this->page_title = 'BLOG';
        $this->load_view($view, $data);
    }

}
