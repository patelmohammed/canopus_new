<?php

class Coaching extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'COACHING';
        $this->load->model('Home_model');
        $this->load->model('Common_model');
    }

    public function index($slug = NULL) {
        $page_id = $this->Common_model->getPrimaryBySlug($slug);
        $data = [];
        $view = 'coaching_page';
        if (isset($page_id) && !empty($page_id)) {
            $data['page_data'] = $this->Common_model->getDataById2('tbl_coaching_page', 'coaching_page_id', $page_id, 'Live', 'is_active', 1);
        } else {
            $data['page_data'] = $this->Common_model->getDataById2('tbl_coaching_page', 'is_active', 1, 'Live');
        }
        if (isset($data['page_data']) && !empty($data['page_data'])) {
            $data['page_data']->item_data = $this->Common_model->getPageitemData($data['page_data']->coaching_page_id, 1);
            $this->meta_desc = $data['page_data']->meta_desc;
            $this->meta_title = $data['page_data']->meta_title;
            $this->meta_key = $data['page_data']->meta_key;
        } else {
            redirect(base_url());
        }
        $this->menu_id = isset($data['page_data']->coaching_page_id) && !empty($data['page_data']->coaching_page_id) ? 'COACHING_' . $data['page_data']->coaching_page_id : '';
        $this->page_title = 'HOME';
        $this->load_view($view, $data);
    }

}
