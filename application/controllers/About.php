<?php

class About extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'ABOUT';
        $this->load->model('Common_model');
        $this->load->model('About_model');
    }

    public function index() {
        $data = [];
        $view = 'about';
        $data['about_us_data'] = $this->Common_model->getDataByIdStatus('about_us', 'del_status', 'Live');
        if (isset($data['about_us_data']) && !empty($data['about_us_data'])) {
            $data['about_us_data']->about_us_item_data = $this->About_model->getAboutUsItemData($data['about_us_data']->about_us_id, 1);
            $this->meta_desc = $data['about_us_data']->meta_desc;
            $this->meta_title = $data['about_us_data']->meta_title;
            $this->meta_key = $data['about_us_data']->meta_key;
        }
        $this->page_title = 'ABOUT';
        $this->load_view($view, $data);
    }

}
