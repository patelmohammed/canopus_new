<?php

class Service extends My_Controller {

    public function __construct() {
        parent::__construct();
        $this->page_id = 'SERVICE';
        $this->load->model('Common_model');
        $this->load->model('Service_moodel');
    }

    public function index() {
        $data = [];
        $view = 'service';
        $data['service_setting_data'] = $this->Common_model->getDataById2('tbl_service_setting', 'del_status', 'Live', 'is_active', 1);
        if (isset($data['service_setting_data']) && !empty($data['service_setting_data'])) {
            $data['service_setting_data']->service_item_data = $this->Service_moodel->getServiceItemData($data['service_setting_data']->service_setting_id);
            $this->meta_desc = $data['service_setting_data']->meta_desc;
            $this->meta_title = $data['service_setting_data']->meta_title;
            $this->meta_key = $data['service_setting_data']->meta_key;
        }
        $data['visa_data'] = $this->Common_model->getAllVisaPage();
        $this->page_title = 'SERVICE';
        $this->load_view($view, $data);
    }

}
