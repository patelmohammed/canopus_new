<?php

class Service_moodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getServiceItemData($service_id, $is_active = NULL) {
        $sql = "SELECT * 
                FROM service_item si 
                WHERE si.ref_service_id = '$service_id' AND si.del_status = 'Live' AND si.is_active = 1";
        if (isset($is_active) && !empty($is_active)) {
            $sql .= " AND si.is_active = '$is_active' ";
        }
        return $this->db->query($sql)->result();
    }

    public function insertServiceItem($id) {
        $service_item_title = $this->input->post('service_item_title');
        $is_active_service_item = $this->input->post('is_active_service_item');
        $service_item_desc = $this->input->post('service_item_desc');
        if (isset($service_item_title) && !empty($service_item_title)) {
            foreach ($service_item_title as $key => $value) {
                $insert_data['ref_service_id'] = $id;
                $insert_data['service_item_title'] = $value;
                $insert_data['service_item_desc'] = isset($service_item_desc[$key]) && !empty($service_item_desc[$key]) ? $service_item_desc[$key] : '';
                $insert_data['is_active'] = isset($is_active_service_item[$key]) && !empty($is_active_service_item[$key]) ? ( $is_active_service_item[$key] == 'on' ? 1 : 0) : 0;

                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');
                $this->Common_model->insertInformation($insert_data, 'service_item');
            }
        }
    }

}
