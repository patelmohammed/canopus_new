<?php

class Home_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function geSliderdata() {
        $sql = "SELECT * 
                FROM slider s 
                WHERE s.is_active = 1 AND del_status = 'Live' 
                ORDER BY slider_order_no ASC";
        return $this->db->query($sql)->result();
    }

    public function getVisaCategorySetting() {
        $sql = "SELECT * 
                FROM tbl_visa_category_setting 
                WHERE del_status = 'Live' LIMIT 1";
        return $this->db->query($sql)->row();
    }

    public function getTestimonialSetting() {
        $sql = "SELECT * 
                FROM tbl_testimonial_setting 
                WHERE del_status = 'Live' LIMIT 1";
        return $this->db->query($sql)->row();
    }

    public function getCoachingPreparationSetting() {
        $sql = "SELECT * 
                FROM tbl_coaching_setting 
                WHERE del_status = 'Live' LIMIT 1";
        return $this->db->query($sql)->row();
    }

}
