<?php

class Dashboard_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function checkUserName($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND user_name != '$currentName'" : '');
        $sql = "SELECT user_name FROM user_information 
                WHERE user_name = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getUserName($id) {
        $sql = "SELECT user_name FROM user_information
                WHERE del_status = 'Live'
                AND id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->user_name;
    }

    public function checkUserEmail($name, $currentName = '', $data = false) {
        $condition = ($currentName != '' ? " AND user_email != '$currentName'" : '');
        $sql = "SELECT user_email FROM user_information 
                WHERE user_email = '$name' AND del_status = 'Live' $condition";
        $check = $this->db->query($sql)->result();

        if ($data == true) {
            return $check;
        } else {
            if (count($check) > 0) {
                return 'false';
            } else {
                return 'true';
            }
        }
    }

    public function getUserEmail($id) {
        $sql = "SELECT user_email FROM user_information
                WHERE del_status = 'Live'
                AND id = $id LIMIT 1";
        $mobile = $this->db->query($sql)->row();
        return $mobile->user_email;
    }

}
