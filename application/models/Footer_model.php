<?php

class Footer_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function insertStatisticItem($id) {
        $statistic_item_title = $this->input->post('statistic_item_title');
        $is_active_service_item = $this->input->post('is_active_service_item');
        $statistic_item_desc = $this->input->post('statistic_item_desc');
        if (isset($statistic_item_title) && !empty($statistic_item_title)) {
            foreach ($statistic_item_title as $key => $value) {
                $insert_data['ref_statistic_id'] = $id;
                $insert_data['statistic_item_title'] = $value;
                $insert_data['statistic_item_desc'] = isset($statistic_item_desc[$key]) && !empty($statistic_item_desc[$key]) ? $statistic_item_desc[$key] : '';
                $insert_data['is_active'] = isset($is_active_service_item[$key]) && !empty($is_active_service_item[$key]) ? ( $is_active_service_item[$key] == 'on' ? 1 : 0) : 0;

                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

                $this->Common_model->insertInformation($insert_data, 'tbl_statistic_item');
            }
        }
    }

}
