<?php

class About_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getAboutUsItemData($about_us_id, $is_active = NULL) {
        $sql = "SELECT * 
                FROM about_us_item aui 
                WHERE aui.ref_about_us_id = '$about_us_id' AND aui.del_status = 'Live'";
        if (isset($is_active) && !empty($is_active)) {
            $sql .= " AND aui.is_active = '$is_active' ";
        }
        return $this->db->query($sql)->result();
    }

    public function insertAboutUsItem($id) {
        $about_us_item_title = $this->input->post('about_us_item_title');
        $is_active_service_item = $this->input->post('is_active_service_item');
        $about_us_item_desc = $this->input->post('about_us_item_desc');
        if (isset($about_us_item_title) && !empty($about_us_item_title)) {
            foreach ($about_us_item_title as $key => $value) {
                $insert_data['ref_about_us_id'] = $id;
                $insert_data['about_us_item_title'] = $value;
                $insert_data['about_us_item_desc'] = isset($about_us_item_desc[$key]) && !empty($about_us_item_desc[$key]) ? $about_us_item_desc[$key] : '';
                $insert_data['is_active'] = isset($is_active_service_item[$key]) && !empty($is_active_service_item[$key]) ? ( $is_active_service_item[$key] == 'on' ? 1 : 0) : 0;

                $insert_data['InsUser'] = $this->user_id;
                $insert_data['InsTerminal'] = $this->input->ip_address();
                $insert_data['InsDateTime'] = date('Y/m/d H:i:s');

                $this->Common_model->insertInformation($insert_data, 'about_us_item');
            }
        }
    }

}
