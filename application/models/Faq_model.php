<?php

class Faq_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getFaqCountryData() {
        $sql = "SELECT cp.country_page_name, cp.country_page_id, cp.country_flag_image, s.slug_id, s.slug 
                FROM tbl_country_page cp 
                    INNER JOIN tbl_slug s ON s.ref_primary_id = cp.country_page_id AND s.ref_table_name = 'tbl_country_page' 
                    INNER JOIN tbl_faq_item fi ON fi.ref_country_page_id = cp.country_page_id AND fi.del_status = 'Live' 
                    INNER JOIN tbl_faq_item_det fid ON fid.ref_faq_item_id = fi.faq_item_id 
                WHERE cp.del_status = 'Live' AND cp.is_active = 1 
                GROUP BY cp.country_page_id";
        return $this->db->query($sql)->result();
    }

    public function getFaqDataById($country_id = NULL) {
        $sql = "SELECT * 
                FROM tbl_faq_item fi 
                WHERE fi.del_status = 'Live'";
        if (isset($country_id) && !empty($country_id)) {
            $sql .= " AND fi.ref_country_page_id = $country_id";
        } else {
            $sql .= " AND fi.ref_country_page_id = 0";
        }
        $result = $this->db->query($sql)->result();
        if (isset($result) && !empty($result)) {
            foreach ($result as $key => $value) {
                $sql = "SELECT * FROM tbl_faq_item_det fid WHERE fid.ref_faq_item_id = $value->faq_item_id ";
                $result[$key]->faq_sub_data = $this->db->query($sql)->result();
            }
        }
        return $result;
    }

    public function getFaqCountryDataAdmin() {
        $sql = "SELECT CONCAT('" . MAIN_FAQ_TITLE . "', ' FAQs') AS country_page_name, fi.faq_item_id, fi.faq_item_title 
                FROM tbl_faq_item fi 
                WHERE fi.del_status = 'Live' AND fi.ref_country_page_id = 0 
                UNION(
                    SELECT cp.country_page_name, fi.faq_item_id, fi.faq_item_title 
                    FROM tbl_faq_item fi 
                        INNER JOIN tbl_country_page cp ON cp.country_page_id = fi.ref_country_page_id AND cp.del_status = 'Live'
                    WHERE fi.del_status = 'Live'
                )";
        return $this->db->query($sql)->result();
    }

}
