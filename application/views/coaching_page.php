<div class="cmt-page-title-row">
    <div class="cmt-page-title-row-inner" style="background: url('<?= base_url() . (isset($page_data->background_image) && !empty($page_data->background_image) ? $page_data->background_image : 'assets/images/pagetitle-bg.jpg') ?>') !important;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="page-title-heading">
                        <h2 class="title"><?= isset($page_data->coaching_page_name) && !empty($page_data->coaching_page_name) ? $page_data->coaching_page_name : '' ?></h2>
                        <p><?= isset($page_data->coaching_page_desc) && !empty($page_data->coaching_page_desc) ? $page_data->coaching_page_desc : '' ?></p>
                    </div>
                    <div class="breadcrumb-wrapper">
                        <span>
                            <a title="Homepage" href="<?= base_url() ?>">Home</a>
                        </span>
                        <span><?= isset($page_data->coaching_page_name) && !empty($page_data->coaching_page_name) ? $page_data->coaching_page_name : '' ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>                    
</div>

<div class="site-main">

    <div class="cmt-row sidebar cmt-sidebar-left cmt-bgcolor-white clearfix">
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-lg-4 ttm-col-bgcolor-yes cmt-bg cmt-left-span cmt-bgcolor-grey mt_80 pt-60 mb_80 pb-60 res-991-mt-0 res-991-pt-0 widget-area sidebar-left">
                    <div class="cmt-col-wrapper-bg-layer cmt-bg-layer"></div>
                    <aside class="widget widget-nav-menu">
                        <ul class="widget-menu">
                            <?php
                            if (isset($all_page) && !empty($all_page)) {
                                foreach ($all_page as $key2 => $value2) {
                                    ?>
                                    <li class="<?= isset($value2->coaching_page_id) && !empty($value2->coaching_page_id) ? (isset($page_data->coaching_page_id) && !empty($page_data->coaching_page_id) ? ($value2->coaching_page_id == $page_data->coaching_page_id ? 'active' : '') : '') : '' ?>"><a href="<?= isset($value2->slug) && !empty($value2->slug) ? base_url(COACHING_SLUG_PREPEND) . $value2->slug : 'javascript:void(0);' ?>"><?= isset($value2->coaching_page_name) && !empty($value2->coaching_page_name) ? strtoupper($value2->coaching_page_name) : '' ?></a></li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </aside>
                    <aside class="widget widget-contact">
                        <!--<img class="img-fluid" src="<?= base_url() ?>assets/images/single-img-12.jpg" alt="single_12" />-->
                        <div class="cmt-col-bgcolor-yes cmt-bgcolor-skincolor cmt-bg pt-20 pl-20 pr-20 pr-20 pb-20">
                            <div class="cmt-col-wrapper-bg-layer cmt-bg-layer">
                                <div class="cmt-col-wrapper-bg-layer-inner"></div>
                            </div>
                            <div class="layer-content">
                                <p class="mb-10">Our Appoinment Service Call Us</p>
                                <h4><i class="flaticon-call mr-3"></i><?= isset($footer_data->footer_contact) && !empty($footer_data->footer_contact) ? $footer_data->footer_contact : '' ?></h4>
                            </div>
                        </div>
                    </aside>
                    <aside class="widget widget-form with-title">
                        <h3 class="widget-title">Free Immigration Assessment</h3>
                        <form id="contact_form" class="immigration_form wrap-form clearfix" method="post" novalidate="novalidate" action="javascript:void(0);">
                            <label>
                                <span class="text-input">
                                    <input name="contact_name" id="contact_name" type="text" value="" placeholder="Your Name" required="required">
                                </span>
                            </label>
                            <label>
                                <span class="text-input">
                                    <input name="contact_email" id="contact_email" type="email" value="" placeholder="Email Id" required="required">
                                </span>
                            </label>
                            <label>
                                <span class="text-input">
                                    <input name="contact_phone" id="contact_phone" type="text" value="" placeholder="Cell Phone" required="required">
                                </span>
                            </label>
                            <label>
                                <span class="text-input">
                                    <input name="contact_subject" id="contact_subject" type="text" value="" placeholder="Subject" required="required">
                                </span>
                            </label>
                            <label>
                                <span class="text-input"><textarea name="contact_message" id="contact_message" rows="4" cols="40" placeholder="Message" required="required"></textarea></span>
                            </label>
                            <span class="success please_wait" style="display: none;color: green;"><em>Please Wait...</em></span>
                            <span class="success contact_success" style="display: none;color: green;"><em>Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.</em></span>
                            <span class="success contact_error" style="display: none;color: red;"><em>Something went wrong. Try again!</em></span>
                            <button id="contact_us_form" class="submit cmt-btn cmt-btn-size-md cmt-btn-shape-rounded cmt-btn-style-fill cmt-btn-color-skincolor" type="submit">Send A Message</button>
                        </form>
                    </aside>
                </div>
                <div class="col-lg-8 content-area">
                    <div class="cmt-service-single-content-area">
                        <div class="cmt-featured-wrapper mb-40 res-991-mb-20">
                            <img class="img-fluid" src="<?= base_url() ?><?= isset($page_data->coaching_page_image) && !empty($page_data->coaching_page_image) && file_exists($page_data->coaching_page_image) ? $page_data->coaching_page_image : '' ?>" alt="coaching-image">
                        </div>
                    </div>
                    <div class="res-991-pr-0 res-991-pb-40 mt-30">
                        <!-- section title -->
                        <!-- section title end -->
                        <div class="accordion pt-10">
                            <?php
                            if (isset($page_data->item_data) && !empty($page_data->item_data)) {
                                foreach ($page_data->item_data as $key => $value) {
                                    ?>
                                    <div class="toggle cmt-style-classic cmt-toggle-title-bgcolor-grey cmt-control-right-true">
                                        <div class="toggle-title"><a href="#"><?= isset($value->coaching_item_page_title) && !empty($value->coaching_item_page_title) ? $value->coaching_item_page_title : '' ?></a></div>
                                        <div class="toggle-content">
                                            <?= isset($value->coaching_item_page_desc) && !empty($value->coaching_item_page_desc) ? $value->coaching_item_page_desc : '' ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div><!-- row end -->
        </div>
    </div>
</div>