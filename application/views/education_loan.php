<div class="cmt-page-title-row">
    <div class="cmt-page-title-row-inner" style="background: url('<?= base_url() . (isset($page_data->background_image) && !empty($page_data->background_image) ? $page_data->background_image : 'assets/images/pagetitle-bg.jpg') ?>') !important;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="page-title-heading">
                        <h2 class="title"><?= isset($page_data->visa_page_name) && !empty($page_data->visa_page_name) ? $page_data->visa_page_name : '' ?></h2>
                        <p><?= isset($page_data->visa_page_desc) && !empty($page_data->visa_page_desc) ? $page_data->visa_page_desc : '' ?></p>
                    </div>
                    <div class="breadcrumb-wrapper">
                        <span>
                            <a title="Homepage" href="<?= base_url() ?>">Home</a>
                        </span>
                        <span><?= isset($page_data->visa_page_name) && !empty($page_data->visa_page_name) ? $page_data->visa_page_name : '' ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>                    
</div>

<div class="site-main">

    <!--features-section-->
    <section class="cmt-row bg-img1 cmt-bg cmt-bgimage-yes clearfix">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-xs-12">
                    <div class="cmt-col-bgcolor-yes cmt-bg cmt-bgcolor-white cmt-left-span spacing-5" style="margin-top: 0px;">
                        <div class="cmt-col-wrapper-bg-layer cmt-bg-layer">
                            <div class="cmt-col-wrapper-bg-layer-inner" style="height: 100%;"></div>
                        </div>
                        <div class="layer-content">
                            <!-- section title -->
                            <div class="section-title">
                                <div class="title-header">
                                    <h2 class="title"><?= isset($page_data->section_1_title) && !empty($page_data->section_1_title) ? $page_data->section_1_title : '' ?></h2>
                                </div>
                                <div class="title-desc"><?= isset($page_data->section_1_short_desc) && !empty($page_data->section_1_short_desc) ? $page_data->section_1_short_desc : '' ?></div>
                            </div><!-- section title end -->
                            <?= isset($page_data->section_1_desc) && !empty($page_data->section_1_desc) ? $page_data->section_1_desc : '' ?>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 mx-md-auto col-sm-7 col-10 mx-auto">
                    <div class="d-flex cmt-boxes-spacing-20px mt-90 res-991-mt-0 mb_20">
                        <div class="cmt-box-col-wrapper">
                            <!-- ttm_single_image-wrapper -->
                            <div class="ttm_single_image-wrapper">
                                <img class="img-fluid" src="<?= isset($page_data->section_1_image_1) && !empty($page_data->section_1_image_1) && file_exists($page_data->section_1_image_1) ? base_url($page_data->section_1_image_1) : '' ?>" alt="single_05">
                            </div>
                        </div>
                        <div class="cmt-box-col-wrapper">
                            <!-- ttm_single_image-wrapper -->
                            <div class="ttm_single_image-wrapper pb-20 mt-35 pt-80 res-991-pt-0">
                                <img class="img-fluid" src="<?= isset($page_data->section_1_image_2) && !empty($page_data->section_1_image_2) && file_exists($page_data->section_1_image_2) ? base_url($page_data->section_1_image_2) : '' ?>" alt="single_06">
                            </div>
                            <!-- ttm_single_image-wrapper -->
                            <div class="ttm_single_image-wrapper">
                                <img class="img-fluid" src="<?= isset($page_data->section_1_image_3) && !empty($page_data->section_1_image_3) && file_exists($page_data->section_1_image_3) ? base_url($page_data->section_1_image_3) : '' ?>" alt="single_07">
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- row end -->
        </div> 
    </section>
    <!--features-section-->

    <?php if ((isset($section_2_left_item_data) && !empty($section_2_left_item_data)) || (isset($section_2_right_item_data) && !empty($section_2_right_item_data))) { ?>
        <section class="cmt-row cat-section cmt-bgcolor-darkgrey bg-img2 cmt-bg cmt-bgimage-yes cmt-bg-pattern clearfix">
            <div class="cmt-row-wrapper-bg-layer cmt-bg-layer"></div>
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <!-- section title -->
                        <div class="section-title title-style-center_text">
                            <div class="title-header">
                                <h2 class="title"><?= isset($page_data->section_2_title) && !empty($page_data->section_2_title) ? $page_data->section_2_title : '' ?></h2>
                            </div>
                        </div><!-- section title end -->
                    </div>
                </div>
                <!-- row end -->
                <!-- row -->
                <div class="row">
                    <div class="col-lg-6">
                        <ul class="cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor mt-20">
                            <?php
                            if (isset($section_2_left_item_data) && !empty($section_2_left_item_data)) {
                                foreach ($section_2_left_item_data as $k4 => $v4) {
                                    ?>
                                    <li>
                                        <?= isset($v4->section_2_left_item_is_icon) && !empty($v4->section_2_left_item_is_icon) && $v4->section_2_left_item_is_icon == 1 ? '<i class="cmt-textcolor-skincolor fa fa-check-circle"></i>' : '' ?>
                                        <span class="cmt-list-li-content" style="<?= $v4->section_2_left_item_is_icon == 2 ? 'padding-left: 0;' : '' ?>"><?= isset($v4->section_2_left_item_data) && !empty($v4->section_2_left_item_data) ? $v4->section_2_left_item_data : '' ?></span>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <ul class="cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor mt-20">
                            <?php
                            if (isset($section_2_right_item_data) && !empty($section_2_right_item_data)) {
                                foreach ($section_2_right_item_data as $k6 => $v6) {
                                    ?>
                                    <li>
                                        <?= isset($v6->section_2_right_item_is_icon) && !empty($v6->section_2_right_item_is_icon) && $v6->section_2_right_item_is_icon == 1 ? '<i class="cmt-textcolor-skincolor fa fa-check-circle"></i>' : '' ?>
                                        <span class="cmt-list-li-content" style="<?= $v6->section_2_right_item_is_icon == 2 ? 'padding-left: 0;' : '' ?>"><?= isset($v6->section_2_right_item_data) && !empty($v6->section_2_right_item_data) ? $v6->section_2_right_item_data : '' ?></span>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                <!-- row end -->
            </div>
        </section>
    <?php } ?>

    <?php if (isset($section_3_item_data) && !empty($section_3_item_data)) { ?>
        <section class="cmt-row sidebar cmt-sidebar-right cmt-bgcolor-white clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="pr-30 res-991-pr-0 res-991-pb-40">
                            <!-- section title -->
                            <div class="row">
                                <div class="col-lg-7 m-auto">
                                    <!-- section title -->
                                    <div class="section-title title-style-center_text">
                                        <div class="title-header">
                                            <h2 class="title"><?= isset($page_data->section_3_title) && !empty($page_data->section_3_title) ? $page_data->section_3_title : '' ?></h2>
    <!--                                        <h2 class="title"><strong>An Insight</strong></h2>-->
                                        </div>
                                    </div><!-- section title end -->
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="accordion pt-0">
                                        <?php foreach ($section_3_item_data as $k2 => $v2) { ?>
                                            <div class="toggle cmt-style-classic cmt-toggle-title-bgcolor-grey cmt-control-right-true">
                                                <div class="toggle-title"><a href="#"><?= isset($v2->section_3_item_title) && !empty($v2->section_3_item_title) ? $v2->section_3_item_title : '' ?></a></div>
                                                <div class="toggle-content">
                                                    <p><?= isset($v2->section_3_item_desc) && !empty($v2->section_3_item_desc) ? $v2->section_3_item_desc : '' ?></p>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div><!-- row end -->
            </div>
        </section>
    <?php } ?>

    <?php if (isset($section_4_item_data) && !empty($section_4_item_data)) { ?>
        <!--services-section-->
        <section class="cmt-row services-section cmt-bgcolor-darkgrey bg-img3 cmt-bg cmt-bgimage-yes clearfix">
            <div class="cmt-row-wrapper-bg-layer cmt-bg-layer"></div>
            <div class="container">
                <style>
                    .services-section .featured-icon-box{
                        background-color: white !important;
                    }
                    .featured-icon-box .featured-content{
                        padding-top: 0 !important;
                        margin-top: 0 !important;
                    }
                </style>
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <!-- section title -->
                        <div class="section-title title-style-center_text">
                            <div class="title-header">
                                <h2 class="title"><?= isset($page_data->section_4_title) && !empty($page_data->section_4_title) ? $page_data->section_4_title : '' ?></h2>
                            </div>
                        </div><!-- section title end -->
                    </div>
                </div><!-- row end -->
                <!-- row -->
                <div class="row mb-40 res-991-mb-15">
                    <?php foreach ($section_4_item_data as $k3 => $v3) { ?>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <!--featured-icon-box-->
                            <div class=" featured-icon-box icon-align-top-content style5 border bor_rad_3">
                                <div class="featured-content">
                                    <div class="featured-title">
                                        <img class="img-fluid" src="<?= isset($v3->section_4_image) && !empty($v3->section_4_image) && file_exists($v3->section_4_image) ? base_url() . $v3->section_4_image : '' ?>" alt="image">
                                    </div>
                                </div>
                            </div><!-- featured-icon-box end-->
                        </div>
                    <?php } ?>
                </div><!-- row end -->
            </div>
        </section>
        <!--services-section-->
    <?php } ?>

    <?php if (isset($testimonial_data) && !empty($testimonial_data)) { ?>
        <!--testimonial-section-->
        <section class="cmt-row testimonial_2-section bg-layer-equal-height cmt-bg bg-img4 cmt-bgimage-yes clearfix" style="padding: 90px 0 200px;">
            <div class="bg-anim-2 text-right">
                <img class="img-fluid" src="<?= base_url() ?>assets/images/anim_img_02.png" alt="">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <!-- col-img-img-three -->
                        <div class="col-bg-img-three cmt-col-bgimage-yes cmt-bg pt-100 pb-90">
                            <div class="cmt-col-wrapper-bg-layer cmt-bg-layer">
                                <div class="cmt-col-wrapper-bg-layer-inner"></div>
                            </div>
                        </div><!-- col-img-bg-img-one end-->
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="cmt-col-bgcolor-yes cmt-bg cmt-bgcolor-white box-shadow spacing-3">
                            <div class="cmt-col-wrapper-bg-layer cmt-bg-layer">
                                <div class="cmt-col-wrapper-bg-layer-inner"></div>
                            </div>
                            <div class="layer-content">
                                <!-- section title -->
                                <div class="section-title title-style-center_text">
                                    <div class="title-header">
                                        <h5><?= isset($page_data->section_5_title) && !empty($page_data->section_5_title) ? $page_data->section_5_title : '' ?></h5>
                                        <h2 class="title"><?= isset($page_data->section_5_desc) && !empty($page_data->section_5_desc) ? $page_data->section_5_desc : '' ?></h2>
                                    </div>
                                </div><!-- section title end -->
                                <div class="row slick_slider slick-dots-style1" data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "dots":true, "arrows":false, "autoplay":true, "infinite":true,  "responsive": [{"breakpoint":991,"settings":{"slidesToShow": 1}}, {"breakpoint":550,"settings":{"slidesToShow": 1}}]}'>
                                    <?php foreach ($testimonial_data as $k5 => $v5) { ?>
                                        <div class="col-lg-12">
                                            <!-- testimonials -->
                                            <div class="testimonials cmt-testimonial-box-view-style1">
                                                <div class="testimonial-content">
                                                    <blockquote class="testimonial-text"><?= isset($v5->testimonial_description) && !empty($v5->testimonial_description) ? $v5->testimonial_description : '' ?></blockquote>
                                                    <div class="testimonial-avatar">
                                                        <div class="testimonial-caption">
                                                            <h5><?= isset($v5->testimonial_name) && !empty($v5->testimonial_name) ? $v5->testimonial_name : '' ?></h5>
                                                            <label><?= isset($v5->testimonial_designation) && !empty($v5->testimonial_designation) ? $v5->testimonial_designation : '' ?></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- testimonials end -->
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- row end -->
            </div>
            <div class="bg-anim-1 mt_120">
                <img class="img-fluid" src="<?= isset($page_data->section_5_image) && !empty($page_data->section_5_image) ? base_url() . $page_data->section_5_image : '' ?>" alt="">
            </div>
        </section>
        <!--testimonial-section-->
    <?php } ?>

</div>