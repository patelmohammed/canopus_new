<div class="cmt-page-title-row">
    <div class="cmt-page-title-row-inner" style="background: url('<?= base_url() . (isset($contact_data->background_image) && !empty($contact_data->background_image) ? $contact_data->background_image : 'assets/images/pagetitle-bg.jpg') ?>') !important;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="page-title-heading">
                        <h2 class="title">Contact Us</h2>
                        <p>Founded In 2010 Surat, India</p>
                    </div>
                    <div class="breadcrumb-wrapper">
                        <span>
                            <a title="Homepage" href="<?= base_url() ?>">Home</a>
                        </span>
                        <span>Contact Us</span>
                    </div>
                </div>
            </div>
        </div>
    </div>                    
</div>

<div class="site-main">

    <!--google_map-->
    <div id="google_map" class="google_map res-991-mt-30">
        <div class="map_container">
            <iframe src="<?= isset($contact_data->contact_map) && !empty($contact_data->contact_map) ? $contact_data->contact_map : '' ?>" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
    </div>

    <!--- conatact-section -->
    <section class="cmt-row conatact-section clearfix">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="cmt-col-bgcolor-yes cmt-bg cmt-bgcolor-white z-index-2 spacing-7 box-shadow">
                        <div class="cmt-col-wrapper-bg-layer cmt-bg-layer"></div>
                        <div class="row ">
                            <div class="col-lg-4 col-md-5">
                                <div class="cmt-bgcolor-darkgrey pt-30 pb-30 pl-30 pr-30">
                                    <div class="mb-20">
                                        <h4>Our Location</h4>
                                        <p><?= isset($contact_data->contact_address) && !empty($contact_data->contact_address) ? $contact_data->contact_address : '' ?></p>
                                    </div>
                                    <h4>Quick Contact</h4>
                                    <div class="cmt-textcolor-white">Email: <a href="mailto:<?= isset($contact_data->contact_email) && !empty($contact_data->contact_email) ? $contact_data->contact_email : '' ?>"><?= isset($contact_data->contact_email) && !empty($contact_data->contact_email) ? $contact_data->contact_email : '' ?></a></div>
                                    <?php
                                    if (isset($contact_data->contact_email_alt) && !empty($contact_data->contact_email_alt)) {
                                        ?>
                                        <div class="cmt-textcolor-white">Support: <a href="mailto:<?= $contact_data->contact_email_alt ?>"><?= $contact_data->contact_email_alt ?></a></div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="cmt-bgcolor-skincolor pt-30 pb-25 pl-30 pr-30">
                                    <h5 class="font-weight-normal"><?= isset($contact_data->contact_number_title) && !empty($contact_data->contact_number_title) ? $contact_data->contact_number_title : '' ?></h5>
                                    <div class="d-flex align-items-center pt-10">
                                        <div class="cmt-icon cmt-icon_element-border cmt-icon_element-color-white cmt-icon_element-size-xs cmt-icon_element-style-rounded mb-10 mr-15">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                        <h4><?= isset($contact_data->contact_phone) && !empty($contact_data->contact_phone) ? $contact_data->contact_phone : '' ?></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-7">
                                <div class="pl-30 res-991-pl-0 res-767-mt-30">
                                    <!-- section title -->
                                    <div class="section-title with-desc clearfix">
                                        <div class="title-header">
                                            <h5>why choose us</h5>
                                            <h2 class="title">Get In <strong>Touch?</strong></h2>
                                        </div>
                                    </div><!-- section title end -->
                                    <form id="contact_form" class="contact_form wrap-form pt-15 clearfix" method="post" novalidate="novalidate" action="javascript:void(0);">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <label>
                                                    <span class="text-input">
                                                        <input name="contact_name" id="contact_name" type="text" value="" placeholder="Your Name" required="required">
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <label>
                                                    <span class="text-input">
                                                        <input name="contact_email" id="contact_email" type="text" value="" placeholder="Your Email" required="required">
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6">
                                                <label>
                                                    <span class="text-input">
                                                        <input name="contact_phone" id="contact_phone" type="text" value="" placeholder="Phone Number" required="required">
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <label>
                                                    <span class="text-input">
                                                        <input name="contact_subject" id="contact_subject" type="text" value="" placeholder="Subject" required="required">
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                        <label>
                                            <span class="text-input">
                                                <textarea name="contact_message" id="contact_message" rows="5" placeholder="Message" required="required"></textarea>
                                            </span>
                                        </label>
                                        <span class="success please_wait" style="display: none;color: green;"><em>Please Wait...</em></span>
                                        <span class="success contact_success" style="display: none;color: green;"><em>Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.</em></span>
                                        <span class="success contact_error" style="display: none;color: red;"><em>Something went wrong. Try again!</em></span>
                                        <button id="contact_us_form" class="submit cmt-btn cmt-btn-size-lg cmt-btn-shape-rounded cmt-btn-style-border cmt-btn-color-dark w-100" type="submit">Submit Request !</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- row -->
        </div>
    </section>
    <!-- conatact-section end -->

</div><!--site-main end-->