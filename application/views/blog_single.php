<!-- page-title -->
<div class="cmt-page-title-row">
    <div class="cmt-page-title-row-inner" style="background: url('<?= base_url() . (isset($blog_data->background_image) && !empty($blog_data->background_image) ? $blog_data->background_image : 'assets/images/pagetitle-bg.jpg') ?>') !important;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="page-title-heading">
                        <h2 class="title"><?= isset($blog_data->blog_title) && !empty($blog_data->blog_title) ? $blog_data->blog_title : '' ?></h2>
                        <p><?= isset($blog_data->blog_short_desc) && !empty($blog_data->blog_short_desc) ? $blog_data->blog_short_desc : '' ?></p>
                    </div>
                    <div class="breadcrumb-wrapper">
                        <span>
                            <a title="Homepage" href="<?= base_url() ?>">Home</a>
                        </span>
                        <span>Blog</span>
                    </div>
                </div>
            </div>
        </div>
    </div>                    
</div>
<!-- page-title end -->


<!--site-main start-->
<div class="site-main">

    <div class="cmt-row sidebar cmt-sidebar-right cmt-bgcolor-white clearfix">
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-lg-8 content-area">
                    <!-- post -->
                    <article class="post cmt-blog-single clearfix">
                        <!-- cmt-post-featured-wrapper -->
                        <div class="cmt-post-featured-wrapper cmt-featured-wrapper mb-20">
                            <div class="cmt-post-featured">
                                <?php
                                if (isset($blog_data->blog_poster_type) && !empty($blog_data->blog_poster_type) && $blog_data->blog_poster_type == 'image') {
                                    ?>
                                    <img class="img-fluid" src="<?= isset($blog_data->blog_image) && !empty($blog_data->blog_image) && file_exists($blog_data->blog_image) ? base_url() . $blog_data->blog_image : '' ?>" alt="blog-image">
                                    <?php
                                } else if (isset($blog_data->blog_poster_type) && !empty($blog_data->blog_poster_type) && $blog_data->blog_poster_type == 'video') {
                                    ?>
                                    <div class="col-bg-img-three cmt-col-bgimage-yes cmt-bg pt-100 pb-90">
                                        <div class="cmt-col-wrapper-bg-layer cmt-bg-layer" style="background-image:url(<?= isset($blog_data->blog_image) && !empty($blog_data->blog_image) && file_exists($blog_data->blog_image) ? base_url() . $blog_data->blog_image : '' ?>);">
                                            <div class="cmt-col-wrapper-bg-layer-inner"></div>
                                        </div>
                                        <div class="layer-content h-100">
                                            <div class="d-flex justify-content-center align-items-center h-100">
                                                <div class="cmt-play-icon-btn">
                                                    <div class="cmt-play-icon-animation">
                                                        <a href="<?= isset($blog_data->blog_video) && !empty($blog_data->blog_video) ? $blog_data->blog_video : '' ?>" target="_self" class="cmt_prettyphoto">
                                                            <div class="cmt-icon cmt-icon_element-fill cmt-icon_element-color-skincolor cmt-icon_element-size-sm cmt-icon_element-style-round">
                                                                <i class="fa fa-play"></i>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="cmt-box-post-date">
                                <span class="cmt-entry-date">
                                    <time class="entry-date"><?= isset($blog_data->blog_date) && !empty($blog_data->blog_date) ? date('d', strtotime($blog_data->blog_date)) : '' ?><span class="entry-month"><?= isset($blog_data->blog_date) && !empty($blog_data->blog_date) ? date('M', strtotime($blog_data->blog_date)) : '' ?></span></time>
                                </span>
                            </div>
                        </div>
                        <!-- cmt-post-featured-wrapper end -->
                        <!-- cmt-blog-single-content -->
                        <div class="cmt-blog-single-content">
                            <div class="cmt-post-entry-header">
                                <div class="post-meta">
                                    <span class="cmt-meta-line entry-date"><i class="fa fa-calendar"></i><time class="entry-date published"><?= isset($blog_data->blog_date) && !empty($blog_data->blog_date) ? date('F d, Y', strtotime($blog_data->blog_date)) : '' ?></time></span>
                                    <div class="sharethis-inline-share-buttons"></div>
                                </div>
                            </div>
                            <div class="entry-content mt-10">
                                <div class="cmt-box-desc-text">
                                    <?= isset($blog_data->blog_desc) && !empty($blog_data->blog_desc) ? $blog_data->blog_desc : '' ?>

                                    <div class="cmt-blogbox-desc-media">
                                        <div class="ttm_tag_lists">
                                            <div><span class="cmt-textcolor-darkgrey">Get More Details On Our Social Profiles</span></div>
                                        </div>
                                        <div class="cmt-social-share-wrapper">
                                            <ul class="social-icons circle">
                                                <?php
                                                if (isset($blog_data->facebook) && !empty($blog_data->facebook) && $blog_data->facebook != 'javascript:void(0);') {
                                                    ?>
                                                    <li><a href="<?= $blog_data->facebook ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                                    <?php
                                                }
                                                if (isset($blog_data->instagram) && !empty($blog_data->instagram) && $blog_data->instagram != 'javascript:void(0);') {
                                                    ?>
                                                    <li><a href="<?= $blog_data->instagram ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                                    <?php
                                                }
                                                if (isset($blog_data->linkedin) && !empty($blog_data->linkedin) && $blog_data->linkedin != 'javascript:void(0);') {
                                                    ?>
                                                    <li><a href="<?= $blog_data->linkedin ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                                    <?php
                                                }
                                                if (isset($blog_data->twitter) && !empty($blog_data->twitter) && $blog_data->twitter != 'javascript:void(0);') {
                                                    ?>
                                                    <li><a href="<?= $blog_data->twitter ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                                    <?php
                                                }
                                                ?>
                                                <li><a href="javascript:void(0);" id="share_on_whatsapp" style="font-size: 17px;"><i class="fa fa-whatsapp"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- cmt-blog-single-content -->
                    </article><!-- post end -->
                </div>
                <div class="col-lg-4 widget-area sidebar-right cmt-col-bgcolor-yes cmt-bg cmt-right-span cmt-bgcolor-grey mt_80 pt-50 mb_80 pb-60 res-991-mt-0 res-991-pt-0">
                    <?php if (isset($all_visa_page) && !empty($all_visa_page)) { ?>
                        <aside class="widget widget-Categories with-title">
                            <h3 class="widget-title">Services</h3>
                            <ul>
                                <?php foreach ($all_visa_page as $key => $value) { ?>
                                    <li><a href="<?= isset($value->slug) && !empty($value->slug) ? base_url(SERVICE_SLUG_PREPEND) . $value->slug : 'javascript:void(0);' ?>"><?= $value->visa_page_name ?></a></li>
                                <?php } ?>
                            </ul>
                        </aside>
                    <?php } ?>

                    <?php if (isset($latest_blog_data) && !empty($latest_blog_data)) { ?>
                        <aside class="widget widget-recent-post with-title">
                            <h3 class="widget-title">Recent Posts</h3>
                            <ul class="widget-post cmt-recent-post-list">
                                <?php foreach ($latest_blog_data as $key => $value) { ?>
                                    <li>
                                        <a href="<?= isset($value->slug) && !empty($value->slug) ? base_url(BLOG_SLUG_PREPEND) . $value->slug : 'javascript:void(0);' ?>"><img src="<?= isset($value->blog_poster_image) && !empty($value->blog_poster_image) && file_exists($value->blog_poster_image) ? base_url() . $value->blog_poster_image : '' ?>" alt="post-img"></a>
                                        <div class="post-detail">
                                            <span class="post-date"><i class="fa fa-calendar"></i><?= isset($value->blog_date) && !empty($value->blog_date) ? date('F d, Y', strtotime($value->blog_date)) : '' ?></span>
                                            <a href="<?= isset($value->slug) && !empty($value->slug) ? base_url(BLOG_SLUG_PREPEND) . $value->slug : 'javascript:void(0);' ?>"><?= isset($value->blog_title) && !empty($value->blog_title) ? $value->blog_title : '' ?></a>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </aside>
                    <?php } ?>

                    <aside class="widget widget-contact">
                        <div class="cmt-col-bgcolor-yes cmt-bgcolor-darkgrey col-bg-img-five cmt-col-bgimage-yes cmt-bg pt-50 pl-25 pr-20 pb-50">
                            <div class="cmt-col-wrapper-bg-layer cmt-bg-layer">
                                <div class="cmt-col-wrapper-bg-layer-inner"></div>
                            </div>
                            <div class="layer-content">
                                <h4><?= isset($contact_data->contact_title) && !empty($contact_data->contact_title) ? $contact_data->contact_title : '' ?></h4>
                                <p><?= isset($contact_data->contact_description) && !empty($contact_data->contact_description) ? $contact_data->contact_description : '' ?></p>
                                <!-- featured-icon-box -->
                                <div class="featured-icon-box icon-align-before-content style5 mt-60 cmt-bgcolor-white">
                                    <div class="featured-icon">
                                        <div class="cmt-icon cmt-icon_element-fill cmt-icon_element-color-skincolor cmt-icon_element-size-xs cmt-icon_element-style-rounded">
                                            <i class="fa fa-phone"></i>
                                        </div>
                                    </div>
                                    <div class="featured-content">
                                        <div class="featured-title">
                                            <h5>Call Now: <span class="cmt-textcolor-skincolor"><?= isset($footer_data->footer_contact) && !empty($footer_data->footer_contact) ? $footer_data->footer_contact : '' ?></span></h5>
                                        </div>
                                    </div>
                                </div><!-- featured-icon-box end -->
                            </div>
                        </div>
                    </aside>
                </div>
            </div><!-- row end -->
        </div>
    </div>


</div><!--site-main end-->