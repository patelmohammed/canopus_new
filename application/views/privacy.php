<div class="cmt-page-title-row">
    <div class="cmt-page-title-row-inner" style="background: url('<?= base_url() . (isset($privacy_policy_data->background_image) && !empty($privacy_policy_data->background_image) ? $privacy_policy_data->background_image : 'assets/images/pagetitle-bg.jpg') ?>') !important;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="page-title-heading">
                        <h2 class="title">Privacy Policy</h2>
                        <p>Founded In 2010 Surat, India</p>
                    </div>
                    <div class="breadcrumb-wrapper">
                        <span>
                            <a title="Homepage" href="<?= base_url() ?>">Home</a>
                        </span>
                        <span>Privacy Policy</span>
                    </div>
                </div>
            </div>
        </div>
    </div>                    
</div>

<div class="site-main">

    <div class="cmt-row cmt-sidebar-left cmt-bgcolor-white clearfix">
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-lg-12 content-area">
                    <div class="pr-30 res-991-pr-0 res-991-pb-40 mt-30">
                        <?= isset($privacy_policy_data->page_desc) && !empty($privacy_policy_data->page_desc) ? $privacy_policy_data->page_desc : '' ?>
                    </div>
                </div>
            </div><!-- row end -->
        </div>
    </div>


</div>