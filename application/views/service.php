<div class="cmt-page-title-row">
    <div class="cmt-page-title-row-inner" style="background: url('<?= base_url() . (isset($service_setting_data->background_image) && !empty($service_setting_data->background_image) ? $service_setting_data->background_image : 'assets/images/pagetitle-bg.jpg') ?>') !important;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="page-title-heading">
                        <h2 class="title">Services</h2>
                        <p>Founded In 210 Surat,India</p>
                    </div>
                    <div class="breadcrumb-wrapper">
                        <span>
                            <a title="Homepage" href="<?= base_url() ?>">Home</a>
                        </span>
                        <span>Services</span>
                    </div>
                </div>
            </div>
        </div>
    </div>                    
</div>
<div class="site-main">

    <?php if (isset($visa_data) && !empty($visa_data)) { ?>
        <section class="cmt-row services_row-section clearfix">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <!-- section title -->
                        <div class="section-title title-style-center_text">
                            <div class="title-header">
                                <h5><?= isset($service_setting_data->visa_section_title) && !empty($service_setting_data->visa_section_title) ? $service_setting_data->visa_section_title : '' ?></h5>
                                <h2 class="title"><?= isset($service_setting_data->visa_section_desc) && !empty($service_setting_data->visa_section_desc) ? $service_setting_data->visa_section_desc : '' ?></h2>
                            </div>
                        </div><!-- section title end -->
                    </div>
                </div><!-- row end -->
                <!-- slick_slider -->
                <div class="row mb_15 slick_slider" data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "arrows":false,  "dots":false, "autoplay":true, "centerMode":false, "centerPadding":0, "infinite":true, "responsive": [{"breakpoint":1100,"settings":{"slidesToShow": 3}} , {"breakpoint":777,"settings":{"slidesToShow": 2}}, {"breakpoint":500,"settings":{"slidesToShow": 1}}]}'>
                    <?php
                    foreach ($visa_data as $key2 => $value2) {
                        ?>
                        <div class="cmt-box-col-wrapper col-lg-12">
                            <!--featured-imagebox-->
                            <div class="featured-imagebox featured-imagebox-services style2">
                                <div class="cmt-box-view-content-inner">
                                    <!-- featured-thumbnail -->
                                    <div class="featured-thumbnail">
                                        <a href="<?= isset($value2->slug) && !empty($value2->slug) ? base_url(SERVICE_SLUG_PREPEND) . $value2->slug : '' ?>" tabindex="-1"> <img class="img-fluid" src="<?= base_url() ?><?= isset($value2->service_page_image) && !empty($value2->service_page_image) ? $value2->service_page_image : '' ?>" alt="service-image"></a>
                                    </div><!-- featured-thumbnail end-->
                                    <div class="featured-content">
                                        <div class="featured-title">
                                            <h5><a href="<?= isset($value2->slug) && !empty($value2->slug) ? base_url(SERVICE_SLUG_PREPEND) . $value2->slug : '' ?>" tabindex="-1"><?= isset($value2->visa_page_name) && !empty($value2->visa_page_name) ? $value2->visa_page_name : '' ?></a></h5>
                                        </div>
                                        <div class="featured-desc">
                                            <p><?= isset($value2->visa_page_desc) && !empty($value2->visa_page_desc) ? $value2->visa_page_desc : '' ?></p>
                                        </div>
                                    </div>
                                    <div class="bottom-footer">
                                        <a class="cmt-btn cmt-btn-size-md cmt-btn-shape-square cmt-btn-style-fill cmt-icon-btn-right cmt-btn-color-grey" href="<?= isset($value2->slug) && !empty($value2->slug) ? base_url(SERVICE_SLUG_PREPEND) . $value2->slug : '' ?>" tabindex="0">Read More<i class="fa fa-long-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div><!-- featured-imagebox end-->
                        </div>
                        <?php
                    }
                    ?>
                </div><!-- row end -->
            </div>
        </section>
    <?php } ?>

    <section class="cmt-row services-section cmt-bgcolor-darkgrey bg-img5 cmt-bg cmt-bgimage-yes clearfix">
        <div class="cmt-row-wrapper-bg-layer cmt-bg-layer"></div>
        <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-lg-12">
                    <!-- section title -->
                    <div class="section-title title-style-center_text">
                        <div class="title-header">
                            <h5><?= isset($service_setting_data->setting_title) && !empty($service_setting_data->setting_title) ? $service_setting_data->setting_title : '' ?></h5>
                            <h2 class="title"><?= isset($service_setting_data->setting_desc) && !empty($service_setting_data->setting_desc) ? $service_setting_data->setting_desc : '' ?></h2>
                        </div>
                    </div><!-- section title end -->
                </div>
            </div><!-- row end -->
            <!-- row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="featuredbox-number">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <!--featured-icon-box-->
                                <div class="featured-icon-box icon-align-top-content style6">
                                    <div class="featured-icon">
                                        <div class="cmt-icon cmt-icon_element-onlytxt cmt-icon_element-color-white cmt-icon_element-size-md">
                                            <i class="<?= isset($service_setting_data->sub_setting_icon_1) && !empty($service_setting_data->sub_setting_icon_1) ? $service_setting_data->sub_setting_icon_1 : '' ?>"></i>
                                            <span class="fea_num cmt-textcolor-darkgrey">
                                                <i class="cmt-num ti-info"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="featured-content">
                                        <div class="featured-title">
                                            <h5><?= isset($service_setting_data->sub_setting_title_1) && !empty($service_setting_data->sub_setting_title_1) ? $service_setting_data->sub_setting_title_1 : '' ?></h5>
                                        </div>
                                        <div class="featured-desc">
                                            <p><?= isset($service_setting_data->sub_setting_desc_1) && !empty($service_setting_data->sub_setting_desc_1) ? $service_setting_data->sub_setting_desc_1 : '' ?></p>
                                        </div>
                                    </div>
                                </div><!-- featured-icon-box end-->
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <!--featured-icon-box-->
                                <div class="featured-icon-box icon-align-top-content style6">
                                    <div class="featured-icon">
                                        <div class="cmt-icon cmt-icon_element-onlytxt cmt-icon_element-color-white cmt-icon_element-size-md">
                                            <i class="<?= isset($service_setting_data->sub_setting_icon_2) && !empty($service_setting_data->sub_setting_icon_2) ? $service_setting_data->sub_setting_icon_2 : '' ?>"></i>
                                            <span class="fea_num cmt-textcolor-darkgrey">
                                                <i class="cmt-num ti-info"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="featured-content">
                                        <div class="featured-title">
                                            <h5><?= isset($service_setting_data->sub_setting_title_2) && !empty($service_setting_data->sub_setting_title_2) ? $service_setting_data->sub_setting_title_2 : '' ?></h5>
                                        </div>
                                        <div class="featured-desc">
                                            <p><?= isset($service_setting_data->sub_setting_desc_2) && !empty($service_setting_data->sub_setting_desc_2) ? $service_setting_data->sub_setting_desc_2 : '' ?></p>
                                        </div>
                                    </div>
                                </div><!-- featured-icon-box end-->
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <!--featured-icon-box-->
                                <div class="featured-icon-box icon-align-top-content style6">
                                    <div class="featured-icon">
                                        <div class="cmt-icon cmt-icon_element-onlytxt cmt-icon_element-color-white cmt-icon_element-size-md">
                                            <i class="<?= isset($service_setting_data->sub_setting_icon_3) && !empty($service_setting_data->sub_setting_icon_3) ? $service_setting_data->sub_setting_icon_3 : '' ?>"></i>
                                            <span class="fea_num cmt-textcolor-darkgrey">
                                                <i class="cmt-num ti-info"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="featured-content">
                                        <div class="featured-title">
                                            <h5><?= isset($service_setting_data->sub_setting_title_3) && !empty($service_setting_data->sub_setting_title_3) ? $service_setting_data->sub_setting_title_3 : '' ?></h5>
                                        </div>
                                        <div class="featured-desc">
                                            <p><?= isset($service_setting_data->sub_setting_desc_3) && !empty($service_setting_data->sub_setting_desc_3) ? $service_setting_data->sub_setting_desc_3 : '' ?></p>
                                        </div>
                                    </div>
                                </div><!-- featured-icon-box end-->
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <!--featured-icon-box-->
                                <div class="featured-icon-box icon-align-top-content style6">
                                    <div class="featured-icon">
                                        <div class="cmt-icon cmt-icon_element-onlytxt cmt-icon_element-color-white cmt-icon_element-size-md">
                                            <i class="<?= isset($service_setting_data->sub_setting_icon_4) && !empty($service_setting_data->sub_setting_icon_4) ? $service_setting_data->sub_setting_icon_4 : '' ?>"></i>
                                            <span class="fea_num cmt-textcolor-darkgrey">
                                                <i class="cmt-num ti-info"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="featured-content">
                                        <div class="featured-title">
                                            <h5><?= isset($service_setting_data->sub_setting_title_4) && !empty($service_setting_data->sub_setting_title_4) ? $service_setting_data->sub_setting_title_4 : '' ?></h5>
                                        </div>
                                        <div class="featured-desc">
                                            <p><?= isset($service_setting_data->sub_setting_desc_4) && !empty($service_setting_data->sub_setting_desc_4) ? $service_setting_data->sub_setting_desc_4 : '' ?></p>
                                        </div>
                                    </div>
                                </div><!-- featured-icon-box end-->
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- row end -->
        </div>
    </section>

    <section class="cmt-row faq-section clearfix">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <?php if (isset($service_setting_data->service_item_data) && !empty($service_setting_data->service_item_data)) { ?>
                        <div class="pr-30 res-991-pr-0 res-991-pb-40">
                            <!-- section title -->
                            <div class="accordion pt-10">
                                <!-- toggle -->
                                <?php
                                foreach ($service_setting_data->service_item_data as $key => $value) {
                                    ?>
                                    <div class="toggle cmt-style-classic cmt-toggle-title-bgcolor-grey cmt-control-right-true">
                                        <div class="toggle-title"><a href="javascript:void(0);"><?= isset($value->service_item_title) && !empty($value->service_item_title) ? $value->service_item_title : '' ?></a></div>
                                        <div class="toggle-content">
                                            <p><?= isset($value->service_item_desc) && !empty($value->service_item_desc) ? $value->service_item_desc : '' ?></p>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-lg-5">
                    <div class="cmt-bg cmt-col-bgcolor-yes cmt-bgcolor-darkgrey cmt-col-bgimage-yes col-bg-img-two spacing-2 pt-40 mt-0 z-index-1">
                        <div class="cmt-col-wrapper-bg-layer cmt-bg-layer">
                            <div class="cmt-col-wrapper-bg-layer-inner"></div>
                        </div>
                        <div class="layer-content">
                            <div class="section-title">
                                <div class="title-header">
                                    <h2 class="title">Register<strong class="cmt-textcolor-skincolor"> Now ?</strong></h2>
                                </div>
                            </div>
                            <form id="contact_form" class="contact_form wrap-form pt-10 pb-10 clearfix" method="post" novalidate="novalidate" action="javascripr:void(0);" >
                                <label>
                                    <span class="text-input">
                                        <input name="contact_name" id="contact_name" type="text" value="" placeholder="Your Name" required="required">
                                    </span>
                                </label>
                                <label>
                                    <span class="text-input">
                                        <input name="contact_email" id="contact_email" type="email" value="" placeholder="Email Id" required="required">
                                    </span>
                                </label>
                                <label>
                                    <span class="text-input">
                                        <input name="contact_phone" id="contact_phone" type="text" value="" placeholder="Cell Phone" required="required">
                                    </span>
                                </label>
                                <label>
                                    <span class="text-input">
                                        <input name="contact_subject" id="contact_subject" type="text" value="" placeholder="Subject" required="required">
                                    </span>
                                </label>
                                <label>
                                    <span class="text-input">
                                        <textarea name="contact_message" id="contact_message" rows="4" cols="40" placeholder="Message" required="required"></textarea>
                                    </span>
                                </label>
                                <span class="success please_wait" style="display: none;color: green;"><em>Please Wait...</em></span>
                                <span class="success contact_success" style="display: none;color: green;"><em>Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.</em></span>
                                <span class="success contact_error" style="display: none;color: red;"><em>Something went wrong. Try again!</em></span>
                                <label>
                                    <button id="contact_us_form" class="submit cmt-btn cmt-btn-size-md cmt-btn-shape-round cmt-btn-style-fill cmt-btn-color-white" type="submit">Submit Request</button>
                                </label>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- row end -->
        </div>
    </section>


</div>