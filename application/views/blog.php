<!-- page-title -->
<div class="cmt-page-title-row">
    <div class="cmt-page-title-row-inner" style="background: url('<?= base_url() . (isset($blog_setting_data->background_image) && !empty($blog_setting_data->background_image) ? $blog_setting_data->background_image : '') ?>') !important;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="page-title-heading">
                        <h2 class="title"><?= isset($blog_setting_data->blog_setting_title) && !empty($blog_setting_data->blog_setting_title) ? $blog_setting_data->blog_setting_title : '' ?></h2>
                        <p><?= isset($blog_setting_data->blog_setting_desc) && !empty($blog_setting_data->blog_setting_desc) ? $blog_setting_data->blog_setting_desc : '' ?></p>
                    </div>
                    <div class="breadcrumb-wrapper">
                        <span>
                            <a title="Homepage" href="<?= base_url() ?>">Home</a>
                        </span>
                        <span>Blog</span>
                    </div>
                </div>
            </div>
        </div>
    </div>                    
</div>
<!-- page-title end -->


<!--site-main start-->
<div class="site-main">

    <section class="cmt-row grid-section clearfix">
        <div class="container">
            <div class="row">
                <?php
                if (isset($blog_data) && !empty($blog_data)) {
                    foreach ($blog_data as $key => $value) {
                        ?>
                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <!-- featured-imagebox-post -->
                            <div class="featured-imagebox featured-imagebox-post style3">
                                <div class="cmt-post-thumbnail featured-thumbnail"> 
                                    <img class="img-fluid" src="<?= isset($value->blog_poster_image) && !empty($value->blog_poster_image) && file_exists($value->blog_poster_image) ? $value->blog_poster_image : '' ?>" alt="image">
                                </div>
                                <div class="featured-content featured-content-post">
                                    <div class="post-header">
                                        <div class="post-title featured-title">
                                            <h5><a href="<?= isset($value->slug) && !empty($value->slug) ? base_url(BLOG_SLUG_PREPEND) . $value->slug : 'javascript:void(0);' ?>"><?= isset($value->blog_title) && !empty($value->blog_title) ? $value->blog_title : '' ?></a></h5>
                                        </div>
                                    </div>
                                    <div class="post-meta">
                                        <span class="cmt-meta-line post-date"><i class="fa fa-calendar"></i><?= isset($value->blog_date) && !empty($value->blog_date) ? date('d F Y', strtotime($value->blog_date)) : '' ?></span>
                                    </div>
                                    <div class="post-desc featured-desc">
                                        <p><?= isset($value->blog_short_desc) && !empty($value->blog_short_desc) ? $value->blog_short_desc : '' ?></p>
                                    </div>
                                    <div class="post-bottom cmt-post-link">
                                        <a class="cmt-btn cmt-btn-size-sm cmt-icon-btn-left cmt-btn-color-skincolor btn-inline" href="<?= isset($value->slug) && !empty($value->slug) ? base_url(BLOG_SLUG_PREPEND) . $value->slug : 'javascript:void(0);' ?>" tabindex="0"><i class="fa fa-minus"></i>Read more</a>
                                    </div>
                                </div>
                            </div><!-- featured-imagebox-post end-->
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
    </section>


</div><!--site-main end-->