<div class="mt-30 mb-30 cmt-bgcolor-white box-shadow pt-20 pb-15 pl-30 res-991-pl-15 pr-15">
    <div class="row cmt-vertical_sep">
        <div class="col-lg-4 col-md-4 col-sm-6 cmt-box-col-wrapper">
            <!-- cmt-fid -->
            <div class="cmt-fid inside cmt-fid-with-icon cmt-fid-view-lefticon">
                <div class="cmt-fid-icon-wrapper cmt-textcolor-skincolor">
                    <i class="<?= isset($statistic_data->exp_icon) && !empty($statistic_data->exp_icon) ? $statistic_data->exp_icon : '' ?>"></i>
                </div>
                <div class="cmt-fid-contents">
                    <h4 class="cmt-fid-inner">
                        <span   data-appear-animation="animateDigits" 
                                data-from="0" 
                                data-to="<?= isset($statistic_data->exp_title) && !empty($statistic_data->exp_title) ? $statistic_data->exp_title : '' ?>" 
                                data-interval="3" 
                                data-before="" 
                                data-before-style="sup" 
                                data-after="" 
                                data-after-style="sub" 
                                class="numinate"><?= isset($statistic_data->exp_title) && !empty($statistic_data->exp_title) ? $statistic_data->exp_title : '' ?>
                        </span>
                        <sub>+</sub>
                    </h4>
                    <h3 class="cmt-fid-title"><?= isset($statistic_data->exp_desc) && !empty($statistic_data->exp_desc) ? $statistic_data->exp_desc : '' ?></h3>
                </div>
            </div><!-- cmt-fid end -->
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 cmt-box-col-wrapper">
            <!-- cmt-fid -->
            <div class="cmt-fid inside cmt-fid-with-icon cmt-fid-view-lefticon">
                <div class="cmt-fid-icon-wrapper cmt-textcolor-skincolor">
                    <i class="<?= isset($statistic_data->cus_icon) && !empty($statistic_data->cus_icon) ? $statistic_data->cus_icon : '' ?>"></i>
                </div>
                <div class="cmt-fid-contents">
                    <h4 class="cmt-fid-inner">
                        <span   data-appear-animation="animateDigits" 
                                data-from="0" 
                                data-to="<?= isset($statistic_data->cus_title) && !empty($statistic_data->cus_title) ? $statistic_data->cus_title : '' ?>" 
                                data-interval="15" 
                                data-before="" 
                                data-before-style="sup" 
                                data-after="" 
                                data-after-style="sub" 
                                class="numinate"><?= isset($statistic_data->cus_title) && !empty($statistic_data->cus_title) ? $statistic_data->cus_title : '' ?>
                        </span>
                        <sub>+</sub>
                    </h4>
                    <h3 class="cmt-fid-title"><?= isset($statistic_data->cus_desc) && !empty($statistic_data->cus_desc) ? $statistic_data->cus_desc : '' ?></h3>
                </div>
            </div><!-- cmt-fid end -->
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 cmt-box-col-wrapper">
            <!-- cmt-fid -->
            <div class="cmt-fid inside cmt-fid-with-icon cmt-fid-view-lefticon">
                <div class="cmt-fid-icon-wrapper cmt-textcolor-skincolor">
                    <i class="<?= isset($statistic_data->new_cus_icon) && !empty($statistic_data->new_cus_icon) ? $statistic_data->new_cus_icon : '' ?>"></i>
                </div>
                <div class="cmt-fid-contents">
                    <h4 class="cmt-fid-inner">
                        <span   data-appear-animation="animateDigits" 
                                data-from="0" 
                                data-to="<?= isset($statistic_data->new_cus_title) && !empty($statistic_data->new_cus_title) ? $statistic_data->new_cus_title : '' ?>" 
                                data-interval="15" 
                                data-before="" 
                                data-before-style="sup" 
                                data-after="" 
                                data-after-style="sub" 
                                class="numinate"><?= isset($statistic_data->new_cus_title) && !empty($statistic_data->new_cus_title) ? $statistic_data->new_cus_title : '' ?>
                        </span>
                        <sub>+</sub>
                    </h4>
                    <h3 class="cmt-fid-title"><?= isset($statistic_data->new_cus_desc) && !empty($statistic_data->new_cus_desc) ? $statistic_data->new_cus_desc : '' ?></h3>
                </div>
            </div><!-- cmt-fid end -->
        </div>
    </div>
</div>
<p><?= isset($statistic_data->description) && !empty($statistic_data->description) ? $statistic_data->description : '' ?></p>
<div class="mt-25 mb-25">
    <h4><?= isset($statistic_data->main_title) && !empty($statistic_data->main_title) ? $statistic_data->main_title : '' ?></h4>
</div>
<div class="cmt-bgcolor-white box-shadow pt-40 pl-30 pb-40 pr-30 mb-30">
    <div class="row">
        <div class="col-sm-6">
            <div class="ttm_single_image-wrapper res-991-pb-30">
                <img class="img-fluid" src="<?= base_url() ?><?= isset($statistic_data->main_image) && !empty($statistic_data->main_image) && file_exists($statistic_data->main_image) ? $statistic_data->main_image : '' ?>" alt="single_12">
            </div>
        </div>
        <div class="col-sm-6">
            <?= isset($statistic_data->main_desc) && !empty($statistic_data->main_desc) ? $statistic_data->main_desc : '' ?>
<!--            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. It has survived not only five centuries.</p>
            <ul class="cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5">
                <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i><span class="cmt-list-li-content">Which material types can you work with?</span>
                </li>
                <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i><span class="cmt-list-li-content">Can I have multiple  in a single feature?</span>
                </li>
                <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i><span class="cmt-list-li-content">Is Smart Lock required for instant apps?</span>
                </li>
                <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i><span class="cmt-list-li-content">Which material types can you work with?</span>
                </li>
                <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i><span class="cmt-list-li-content">Is Smart Lock required for instant apps?</span>
                </li>
            </ul>-->
        </div>
    </div>
</div>
<p><?= isset($statistic_data->sub_desc) && !empty($statistic_data->sub_desc) ? $statistic_data->sub_desc : '' ?></p>
<div class="pt-5 pb-5">
    <h4><?= isset($statistic_data->sub_title) && !empty($statistic_data->sub_title) ? $statistic_data->sub_title : '' ?></h4>
</div>
<p><?= isset($statistic_data->sub_desc_2) && !empty($statistic_data->sub_desc_2) ? $statistic_data->sub_desc_2 : '' ?></p>
<div class="accordion pt-15">
    <?php
    if (isset($statistic_data->statistic_item_data) && !empty($statistic_data->statistic_item_data)) {
        foreach ($statistic_data->statistic_item_data as $key9 => $value9) {
            ?>
            <!-- toggle -->
            <div class="toggle cmt-control-left-true">
                <div class="toggle-title"><a href="javascript:void(0);"><?= isset($value9->statistic_item_title) && !empty($value9->statistic_item_title) ? $value9->statistic_item_title : '' ?></a></div>
                <div class="toggle-content">
                    <p><?= isset($value9->statistic_item_desc) && !empty($value9->statistic_item_desc) ? $value9->statistic_item_desc : '' ?></p>
                </div>
            </div>
            <!-- toggle end -->
            <?php
        }
    }
    ?>
</div>