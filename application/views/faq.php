<style>
    .faq_font{
        font-size: 1.5rem !important;
        color: #0067ed;
    }
</style>
<div class="cmt-page-title-row">
    <div class="cmt-page-title-row-inner" style="background: url('<?= base_url() . 'assets/images/pagetitle-bg.jpg' ?>') !important;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="page-title-heading">
                        <h2 class="title">Frequently Asked Questions</h2>
                        <p><?= isset($country_page_data->country_page_name) && !empty($country_page_data->country_page_name) ? $country_page_data->country_page_name : MAIN_FAQ_TITLE ?> FAQs</p>
                    </div>
                    <div class="breadcrumb-wrapper">
                        <span>
                            <a title="Homepage" href="<?= base_url() ?>">Home</a>
                        </span>
                        <span>FAQ</span>
                    </div>
                </div>
            </div>
        </div>
    </div>                    
</div>

<div class="site-main">
    <section class="cmt-row sidebar cmt-sidebar-right cmt-bgcolor-white clearfix">
        <div class="container">
            <div class="row">
                <div class="<?= isset($country_data) && !empty($country_data) ? 'col-lg-9' : 'col-lg-12' ?>">
                    <div class="pr-30 res-991-pr-0 res-991-pb-40">
                        <!-- section title -->
                        <div class="section-title">
                            <div class="title-header">
                                <h5>Frequently Asked Questions</h5>
                                <h2 class="title"><?= isset($country_page_data->country_page_name) && !empty($country_page_data->country_page_name) ? $country_page_data->country_page_name : MAIN_FAQ_TITLE ?> FAQs</h2>
                            </div>
                        </div><!-- section title end -->

                        <?php
                        if (isset($faq_data) && !empty($faq_data)) {
                            foreach ($faq_data as $k1 => $v1) {
                                if (isset($v1->faq_item_title) && !empty($v1->faq_item_title)) {
                                    ?>
                                    <div class="section-title pt-20">
                                        <div class="title-header">
                                            <h2 class="title faq_font"><?= isset($v1->faq_item_title) && !empty($v1->faq_item_title) ? $v1->faq_item_title : '' ?></h2>
                                        </div>
                                    </div>
                                    <?php
                                }
                                if (isset($v1->faq_sub_data) && !empty($v1->faq_sub_data)) {
                                    ?>
                                    <div class="accordion pt-0">
                                        <?php
                                        foreach ($v1->faq_sub_data as $k2 => $v2) {
                                            ?>
                                            <div class="toggle cmt-style-classic cmt-toggle-title-bgcolor-grey cmt-control-right-true">
                                                <div class="toggle-title"><a href="#"><?= isset($v2->faq_item_det_title) && !empty($v2->faq_item_det_title) ? $v2->faq_item_det_title : '' ?></a></div>
                                                <div class="toggle-content">
                                                    <p><?= isset($v2->faq_item_det_desc) && !empty($v2->faq_item_det_desc) ? $v2->faq_item_det_desc : '' ?></p>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>

                <?php if (isset($country_data) && !empty($country_data)) { ?>
                    <div class="col-lg-3">
                        <aside class="widget widget-recent-post with-title">
                            <h3 class="widget-title">Country FAQs</h3>
                            <ul class="widget-post cmt-recent-post-list">
                                <?php foreach ($country_data as $key => $value) { ?>
                                    <li>
                                        <a href="<?= isset($value->slug) && !empty($value->slug) ? base_url(FAQ_SLUG_PREPEND) . $value->slug : 'javascript:void(0);' ?>"><img class="img-fluid" src="<?= isset($value->country_flag_image) && !empty($value->country_flag_image) && file_exists($value->country_flag_image) ? base_url() . $value->country_flag_image : '' ?>" alt="post-img" style="border-radius: 50%;border: 5px solid #fff;box-shadow: 0 0px 10px 0 rgb(18 29 39 / 70%);height: 55px;width: 55px;"></a>
                                        <div class="post-detail" style="padding-top: 12.5px; font-size: 17px;">
                                            <a href="<?= isset($value->slug) && !empty($value->slug) ? base_url(FAQ_SLUG_PREPEND) . $value->slug : 'javascript:void(0);' ?>"><?= isset($value->country_page_name) && !empty($value->country_page_name) ? $value->country_page_name : '' ?> FAQs</a>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </aside>
                    </div>
                <?php } ?>
            </div><!-- row end -->
        </div>
    </section>
</div>