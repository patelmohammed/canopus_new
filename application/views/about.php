<div class="cmt-page-title-row">
    <div class="cmt-page-title-row-inner" style="background: url('<?= base_url() . (isset($about_us_data->background_image) && !empty($about_us_data->background_image) ? $about_us_data->background_image : 'assets/images/pagetitle-bg.jpg') ?>') !important;">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="page-title-heading">
                        <h2 class="title">About Our Visa Consultancy</h2>
                        <p>Founded In 2010 Surat, India</p>
                    </div>
                    <div class="breadcrumb-wrapper">
                        <span>
                            <a title="Homepage" href="<?= base_url() ?>">Home</a>
                        </span>
                        <span>About Us</span>
                    </div>
                </div>
            </div>
        </div>
    </div>                    
</div>

<div class="site-main">
    <section class="about_2-section clearfix" style="padding-top: 60px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-5 col-5 mr-lg-auto ml-lg-0 offset-3">
                    <div class="cmt-bg cmt-col-bgcolor-yes cmt-bgcolor-grey pt-25 pb-25 pl-25 pr-25 ml-80 res-991-ml-0">
                        <div class="cmt-col-wrapper-bg-layer cmt-bg-layer"></div>
                        <div class="layer-content">
                            <div class="cmt-bgcolor-white pt-60 pb-50 res-991-pt-50 res-991-pb-40">
                                <div class="col-lg-12">
                                    <div class="ttm_single_image-wrapper ml_110 mr_200 res-575-mr_140">
                                        <img class="img-fluid" src="<?= base_url() . (isset($about_us_data->about_us_alt_image) && !empty($about_us_data->about_us_alt_image) ? $about_us_data->about_us_alt_image : '') ?>" alt="about-us">
                                    </div>      
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-xs-12">
                    <div class="pt-15 pr-30 mb_30 res-991-pr-0 res-991-pt-30">
                        <!-- section title -->
                        <div class="section-title">
                            <div class="title-header">
                                <!--<h5>why choose us</h5>-->
                                <h2 class="title"><?= isset($about_us_data->about_us_alt_title) && !empty($about_us_data->about_us_alt_title) ? $about_us_data->about_us_alt_title : '' ?></h2>
                            </div>
                        </div>
                        <p><?= isset($about_us_data->about_us_alt_desc) && !empty($about_us_data->about_us_alt_desc) ? $about_us_data->about_us_alt_desc : '' ?></p>
                        <div class="pt-10 pr-15 res-991-pr-0">
                            <!-- featured-icon-box -->
                            <div class="featured-icon-box icon-align-before-content style2">
                                <div class="featured-icon">
                                    <div class="cmt-icon cmt-icon_element-border cmt-icon_element-color-skincolor cmt-icon_element-size-lg cmt-icon_element-style-square"> 
                                        <i class="<?= isset($about_us_data->about_us_alt_sub_icon_1) && !empty($about_us_data->about_us_alt_sub_icon_1) ? $about_us_data->about_us_alt_sub_icon_1 : '' ?>"></i>
                                    </div>
                                </div>
                                <div class="featured-content">
                                    <div class="featured-title">
                                        <h5><?= isset($about_us_data->about_us_alt_sub_title_1) && !empty($about_us_data->about_us_alt_sub_title_1) ? $about_us_data->about_us_alt_sub_title_1 : '' ?></h5>
                                    </div>
                                    <div class="featured-desc">
                                        <p><?= isset($about_us_data->about_us_alt_sub_desc_1) && !empty($about_us_data->about_us_alt_sub_desc_1) ? $about_us_data->about_us_alt_sub_desc_1 : '' ?></p>
                                    </div>
                                </div>
                            </div><!-- featured-icon-box end -->
                            <!-- featured-icon-box -->
                            <div class="featured-icon-box icon-align-before-content style2">
                                <div class="featured-icon">
                                    <div class="cmt-icon cmt-icon_element-border cmt-icon_element-color-skincolor cmt-icon_element-size-lg cmt-icon_element-style-square"> 
                                        <i class="<?= isset($about_us_data->about_us_alt_sub_icon_2) && !empty($about_us_data->about_us_alt_sub_icon_2) ? $about_us_data->about_us_alt_sub_icon_2 : '' ?>"></i>
                                    </div>
                                </div>
                                <div class="featured-content">
                                    <div class="featured-title">
                                        <h5><?= isset($about_us_data->about_us_alt_sub_title_2) && !empty($about_us_data->about_us_alt_sub_title_2) ? $about_us_data->about_us_alt_sub_title_2 : '' ?></h5>
                                    </div>
                                    <div class="featured-desc">
                                        <p><?= isset($about_us_data->about_us_alt_sub_desc_2) && !empty($about_us_data->about_us_alt_sub_desc_2) ? $about_us_data->about_us_alt_sub_desc_2 : '' ?></p>
                                    </div>
                                </div>
                            </div><!-- featured-icon-box end -->
                        </div>
                    </div>
                </div>
            </div><!-- row end -->
        </div>
    </section>

    <section class="cmt-row cta_2-section cmt-bgcolor-skincolor bg-img8 cmt-bg clearfix"><!--style="background-image: url(/images/bg-image/row-bgimage-8.png);background-repeat: no-repeat;background-position: bottom;"-->
        <div class="cmt-row-wrapper-bg-layer cmt-bg-layer"></div>
        <div class="container">
            <!-- row -->
            <div class="row align-items-center">
                <div class="col-xl-9 col-lg-9 col-md-12">
                    <div class="row-title">
                        <!-- section title -->
                        <div class="section-title">
                            <div class="title-header">
                                <h5><?= isset($about_us_data->section_3_title) && !empty($about_us_data->section_3_title) ? $about_us_data->section_3_title : '' ?></h5>
                                <h2 class="title"><?= isset($about_us_data->section_3_desc) && !empty($about_us_data->section_3_desc) ? $about_us_data->section_3_desc : '' ?></h2>
                            </div>
                        </div><!-- section title end -->
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-12">
                    <a class="cmt-btn cmt-btn-size-md cmt-btn-shape-round cmt-btn-style-border cmt-btn-color-white float-lg-right mb-25 res-991-mb-0 res-991-mt-15" href="contact">book a consultation</a>
                </div>
            </div><!-- row end -->
        </div>
    </section>

    <section class="cmt-row faq-section clearfix">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <?php if (isset($about_us_data->about_us_item_data) && !empty($about_us_data->about_us_item_data)) { ?>
                        <div class="pr-30 res-991-pr-0 res-991-pb-40">
                            <!-- section title -->
                            <div class="section-title">
                                <div class="title-header">
                                    <h5><?= isset($about_us_data->about_us_title) && !empty($about_us_data->about_us_title) ? $about_us_data->about_us_title : '' ?></h5>
                                    <h2 class="title"><?= isset($about_us_data->about_us_desc) && !empty($about_us_data->about_us_desc) ? $about_us_data->about_us_desc : '' ?></h2>
                                </div>
                            </div><!-- section title end -->
                            <div class="accordion pt-10">
                                <?php
                                foreach ($about_us_data->about_us_item_data as $key1 => $value1) {
                                    ?>
                                    <div class="toggle cmt-style-classic cmt-toggle-title-bgcolor-grey cmt-control-right-true">
                                        <div class="toggle-title"><a href="#"><?= isset($value1->about_us_item_title) && !empty($value1->about_us_item_title) ? $value1->about_us_item_title : '' ?></a></div>
                                        <div class="toggle-content">
                                            <p><?= isset($value1->about_us_item_desc) && !empty($value1->about_us_item_desc) ? $value1->about_us_item_desc : '' ?></p>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-lg-5">
                    <div class="cmt-bg cmt-col-bgcolor-yes cmt-bgcolor-darkgrey cmt-col-bgimage-yes col-bg-img-two spacing-2 pt-40 mt-0 z-index-1">
                        <div class="cmt-col-wrapper-bg-layer cmt-bg-layer">
                            <div class="cmt-col-wrapper-bg-layer-inner"></div>
                        </div>
                        <div class="layer-content">
                            <div class="section-title">
                                <div class="title-header">
                                    <h2 class="title">Can't <strong>Find</strong> The <strong class="cmt-textcolor-skincolor"> Answers ?</strong></h2>
                                </div>
                            </div>
                            <form id="contact_form" class="contact_form wrap-form pt-10 pb-10 clearfix" method="post" novalidate="novalidate" action="javascripr:void(0);" >
                                <label>
                                    <span class="text-input">
                                        <input name="contact_name" id="contact_name" type="text" value="" placeholder="Your Name" required="required">
                                    </span>
                                </label>
                                <label>
                                    <span class="text-input">
                                        <input name="contact_email" id="contact_email" type="email" value="" placeholder="Email Id" required="required">
                                    </span>
                                </label>
                                <label>
                                    <span class="text-input">
                                        <input name="contact_phone" id="contact_phone" type="text" value="" placeholder="Cell Phone" required="required">
                                    </span>
                                </label>
                                <label>
                                    <span class="text-input">
                                        <input name="contact_subject" id="contact_subject" type="text" value="" placeholder="Subject" required="required">
                                    </span>
                                </label>
                                <label>
                                    <span class="text-input">
                                        <textarea name="contact_message" id="contact_message" rows="4" cols="40" placeholder="Message" required="required"></textarea>
                                    </span>
                                </label>
                                <span class="success please_wait" style="display: none;color: green;"><em>Please Wait...</em></span>
                                <span class="success contact_success" style="display: none;color: green;"><em>Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.</em></span>
                                <span class="success contact_error" style="display: none;color: red;"><em>Something went wrong. Try again!</em></span>
                                <label>
                                    <button id="contact_us_form" class="submit cmt-btn cmt-btn-size-md cmt-btn-shape-round cmt-btn-style-fill cmt-btn-color-white" type="submit">Submit Request</button>
                                </label>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!-- row end -->
        </div>
    </section>
</div>