<?php if (isset($slider_data) && !empty($slider_data)) { ?>
    <rs-module-wrap id="rev_slider_1_1_wrapper" data-source="gallery"> 
        <rs-module id="rev_slider_1_1" data-version="6.1.0"> 
            <rs-slides>
                <?php
                $slider_nm = 0;
                foreach ($slider_data as $key1 => $value1) {
                    if (isset($value1->slider_image) && !empty($value1->slider_image) && file_exists($value1->slider_image)) {
                        $slider_layer = 0;
                        $slider_nm++;
                        ?>
                        <rs-slide data-key="rs-<?= $slider_nm ?>" data-title="Slide1" data-thumb="<?= isset($value1->slider_image) && !empty($value1->slider_image) ? $value1->slider_image : '' ?>" data-anim="ei:d;eo:d;s:d;r:0;t:3dcurtain-vertical;sl:d;"> 

                            <img src="<?= base_url() . $value1->slider_image ?>" alt="slider-bg-image" title="slider-bg-image" width="1920" height="690" class="rev-slidebg" data-no-retina>

                            <?php $slider_layer++; ?>
                            <rs-layer id="slider-<?= $slider_nm ?>-slide-<?= $slider_nm ?>-layer-<?= $slider_layer ?>" 
                                      data-type="text" 
                                      data-xy="xo:50px,20px,15px,10px;yo:230px,230px,119px,78px;" 
                                      data-text="w:normal;s:20,20,18,18;l:20,20,18,18;a:left;fw:400;"
                                      data-color="#0b0c26" 
                                      data-rsp_ch="on"
                                      data-padding="t:12,12,10,10;r:30,30,20,15;b:12,12,10,10;l:30,30,20,15;" 
                                      data-ford="frame_0;frame_1;frame_2;frame_999;" 
                                      data-frame_0="x:100px,82px,62px,38px;sX:0.8;sY:0.8;rY:-20deg;oY:0%;" 
                                      data-frame_1="x:50px,41px,31px,19px;sX:1.5;sY:1.5;rY:-10deg;oY:0%;e:power4.in;st:500;sp:500;sR:200;" data-frame_999="st:w;sp:2000;sR:7500;" 
                                      data-frame_2="x:0px;sX:1;sY:1;rY:0deg;oX:50%;oY:0%;oZ:0;tp:600px;e:power4.out;st:1000;sp:500;" 
                                      style="z-index:50;background-color:rgba(255,255,255,.88);text-transform:uppercase;" ><?= isset($value1->slider_title) && !empty($value1->slider_title) ? $value1->slider_title : '' ?>
                            </rs-layer>

                            <?php $slider_layer++; ?>
                            <rs-layer id="slider-<?= $slider_nm ?>-slide-<?= $slider_nm ?>-layer-<?= $slider_layer ?>" 
                                      data-type="text" 
                                      data-xy="xo:50px,20px,15px,10px;yo:285px,285px,169px,127px;" 
                                      data-text="w:normal;s:59,59,49,36;l:60,60,50,38;a:left;fw:300;" 
                                      data-color="#0b0c26" 
                                      data-rsp_ch="on"
                                      data-padding="t:15,15,14,12;r:30,30,20,20;b:15,15,14,12;l:30,30,20,20;" 
                                      data-ford="frame_0;frame_1;frame_2;frame_999;" 
                                      data-frame_0="x:100px,82px,62px,38px;sX:0.8;sY:0.8;rY:-10deg;oY:0%;" 
                                      data-frame_1="x:50px,41px,31px,19px;sX:1.5;sY:1.5;rY:-5deg;oY:0%;e:power4.in;st:900;sp:500;sR:600;" data-frame_999="st:w;sp:2000;sR:7100;" 
                                      data-frame_2="x:0px;sX:1;sY:1;rY:0deg;oX:50%;oY:0%;oZ:0;tp:600px;e:power4.out;st:1400;sp:500;" 
                                      style="z-index:48;background-color:rgba(255,255,255,.88);font-family: 'Roboto', sans-serif; text-transform:uppercase;" ><?= isset($value1->slider_description) && !empty($value1->slider_description) ? $value1->slider_description : '' ?>
                            </rs-layer>

                            <?php $slider_layer++; ?>
                            <rs-layer  id="slider-<?= $slider_nm ?>-slide-<?= $slider_nm ?>-layer-<?= $slider_layer ?>" 
                                       class="book-consulation rev-btn cmt-btn cmt-btn-size-md cmt-btn-shape-round cmt-btn-style-fill cmt-btn-color-white" 
                                       data-type="button" 
                                       data-xy="x:l;xo:50px,20px,15px,10px;y:b,b,b,b;yo:230px,230px,117px,78px;"
                                       data-rsp_ch="on"
                                       data-color="#1d2143" 
                                       data-text="s:15;a:c;fw:600;"
                                       data-frame_0="y:50px;" 
                                       data-frame_1="e:power4.out;st:900;sp:500;sR:900;" 
                                       data-frame_999="st:w;sp:2000;sR:7600;"
                                       data-frame_hover="c:#fff;bow:1px,1px,1px,1px;boc:#0067ed;bgc:#0067ed"
                                       style="z-index:7;text-transform: capitalize;">Book a consultation!
                            </rs-layer>

                        </rs-slide> 
                        <?php
                    }
                }
                ?>
            </rs-slides> 
            <rs-progress class="rs-bottom" style="visibility: hidden !important;"></rs-progress> 
        </rs-module>
    </rs-module-wrap>
    <!-- END REVOLUTION SLIDER -->
<?php } ?>


<!--site-main start-->
<div class="site-main">
    <?php if (isset($country_data) && !empty($country_data)) { ?>
        <!--features-section-->
        <section class="cmt-row features-section cmt-bgcolor-grey bg-img1 cmt-bg cmt-bgimage-yes cmt-bg-pattern clearfix">
            <div class="cmt-row-wrapper-bg-layer cmt-bg-layer"></div>
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <!-- section title -->
                        <div class="section-title style2">
                            <div class="title-header">
                                <h5><?= isset($country_setting_data->country_setting_title) && !empty($country_setting_data->country_setting_title) ? $country_setting_data->country_setting_title : '' ?></h5>
                                <h2 class="title"><?= isset($country_setting_data->country_setting_description) && !empty($country_setting_data->country_setting_description) ? $country_setting_data->country_setting_description : '' ?></h2>
                            </div>
                            <div class="title-desc">
                                <p><?= isset($country_setting_data->country_setting_desc_2) && !empty($country_setting_data->country_setting_desc_2) ? $country_setting_data->country_setting_desc_2 : '' ?></p>
                            </div>
                        </div><!-- section title end -->
                    </div>
                </div><!-- row end -->
                <!-- slick_slider -->
                <div class="row slick_slider" data-slick='{"slidesToShow": 3, "slidesToScroll": 3, "arrows":false,  "dots":false, "autoplay":true, "centerMode":false, "centerPadding":0, "infinite":true, "responsive": [{"breakpoint":1100,"settings":{"slidesToShow": 3}} , {"breakpoint":777,"settings":{"slidesToShow": 2}}, {"breakpoint":590,"settings":{"slidesToShow": 1}}]}'>
                    <?php
                    $country_data_cnt = count($country_data);
                    foreach ($country_data as $key6 => $value6) {
                        ?>
                        <div class="cmt-box-col-wrapper col-lg-12">
                            <!--featured-imagebox-->
                            <div class="featured-imagebox featured-imagebox-country style2 bor_rad_5">
                                <div class="cmt-box-view-content-inner">
                                    <!-- featured-thumbnail -->
                                    <div class="featured-thumbnail">
                                        <a href="<?= isset($value6->slug) && !empty($value6->slug) ? base_url(COUNTRY_SLUG_PREPEND) . $value6->slug : 'javascript:void(0);' ?>" tabindex="-1"> <img class="img-fluid" src="<?= base_url() . (isset($value6->home_page_image) && !empty($value6->home_page_image) && file_exists($value6->home_page_image) ? $value6->home_page_image : '') ?>" alt="image"></a>
                                    </div><!-- featured-thumbnail end-->
                                    <div class="featured-content">
                                        <div class="featured-content-icon_img-block">
                                            <img class="img-fluid" src="<?= base_url() . (isset($value6->country_flag_image) && !empty($value6->country_flag_image) && file_exists($value6->country_flag_image) ? $value6->country_flag_image : '') ?>" alt="image">
                                        </div>
                                        <div class="featured-title">
                                            <h5><a href="<?= isset($value6->slug) && !empty($value6->slug) ? base_url(COUNTRY_SLUG_PREPEND) . $value6->slug : 'javascript:void(0);' ?>" tabindex="-1"><?= isset($value6->country_page_name) && !empty($value6->country_page_name) ? $value6->country_page_name : '' ?></a></h5>
                                        </div>
                                        <div class="featured-desc">
                                            <p><?= isset($value6->home_page_desc) && !empty($value6->home_page_desc) ? $value6->home_page_desc : '' ?></p>
                                        </div>
                                        <a class="cmt-btn cmt-btn-size-sm btn-inline cmt-btn-color-skincolor" href="<?= isset($value6->slug) && !empty($value6->slug) ? base_url(COUNTRY_SLUG_PREPEND) . $value6->slug : 'javascript:void(0);' ?>" tabindex="-1">Read More</a>
                                    </div>
                                </div>
                            </div><!-- featured-imagebox end-->
                        </div>
                        <?php
                    }
                    ?>
                </div><!-- row end -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center mt-35 res-991-mt-20">
                            <a class="cmt-btn cmt-btn-size-md cmt-btn-shape-round cmt-btn-style-border cmt-btn-color-dark" href="<?= base_url('country') ?>">View More country!</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--features-section-->
    <?php } ?>

    <?php if (isset($visa_category_data) && !empty($visa_category_data)) { ?>
        <!--cat-section-->
        <section class="cmt-row cat-section cmt-bgcolor-darkgrey bg-img2 cmt-bg cmt-bgimage-yes cmt-bg-pattern clearfix">
            <div class="cmt-row-wrapper-bg-layer cmt-bg-layer"></div>
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <!-- section title -->
                        <div class="section-title title-style-center_text">
                            <div class="title-header">
                                <h5><?= isset($visa_category_setting_data->visa_category_setting_title) && !empty($visa_category_setting_data->visa_category_setting_title) ? $visa_category_setting_data->visa_category_setting_title : '' ?></h5>
                                <h2 class="title"><?= isset($visa_category_setting_data->visa_category_setting_description) && !empty($visa_category_setting_data->visa_category_setting_description) ? $visa_category_setting_data->visa_category_setting_description : '' ?></h2>
                            </div>
                        </div><!-- section title end -->
                    </div>
                </div><!-- row end -->
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="featuredbox-number">
                            <div class="row slick_slider" data-slick='{"slidesToShow": 4, "slidesToScroll": 4, "arrows":false, "autoplay":true, "infinite":false, "responsive": [{"breakpoint":991,"settings":{"slidesToShow": 3}}, {"breakpoint":678,"settings":{"slidesToShow": 2}}, {"breakpoint":460,"settings":{"slidesToShow": 1}}]}'>
                                <?php
                                foreach ($visa_category_data as $key2 => $value2) {
                                    ?>
                                    <div class="col-lg-12 col-md-6 col-sm-6">
                                        <!--featured-icon-box-->
                                        <div class="featured-icon-box style1 icon-align-top-content bor_rad_5">
                                            <div class="featured-icon">
                                                <div class="cmt-icon cmt-icon_element-onlytxt cmt-icon_element-color-skincolor cmt-icon_element-size-lg">
                                                    <i class="<?= isset($value2->icon) && !empty($value2->icon) ? $value2->icon : '' ?>"></i>
                                                </div>
                                            </div>
                                            <div class="featured-content">
                                                <div class="featured-title">
                                                    <h5><?= isset($value2->visa_page_name) && !empty($value2->visa_page_name) ? $value2->visa_page_name : '' ?></h5>
                                                </div>
                                                <div class="featured-desc">
                                                    <p><?= isset($value2->visa_page_desc) && !empty($value2->visa_page_desc) ? $value2->visa_page_desc : '' ?></p>
                                                </div>
                                                <div class="cmt-di_links">
                                                    <a href="<?= isset($value2->slug) && !empty($value2->slug) ? base_url(SERVICE_SLUG_PREPEND) . $value2->slug : '' ?>" class="di_link">
                                                        <i class="ti ti-angle-right"></i>
                                                    </a>
                                                    <span class="di_num">
                                                        <i class="cmt-num ti-info"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div><!-- featured-icon-box end-->
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div><!-- row end -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center mt-35 mb_20 res-991-mt-20">
                            <h6>
                                <?php
                                $visa_category_setting_footer = isset($visa_category_setting_data->visa_category_setting_footer) && !empty($visa_category_setting_data->visa_category_setting_footer) ? $visa_category_setting_data->visa_category_setting_footer : '';
                                $word = array('{base_url}');
                                $replace = array(base_url('visa'));
                                $visa_category_setting_footer = str_replace($word, $replace, $visa_category_setting_footer);
                                echo $visa_category_setting_footer;
                                ?>
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--cat-section-->
    <?php } ?>


    <!--testimonial-section-->
    <section class="cmt-row testimonial-section clearfix">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-xs-12">
                    <div class="pt-15 res-991-pt-0">
                        <!-- section title -->
                        <div class="section-title">
                            <div class="title-header">
                                <h5><?= isset($testimonial_setting_data->testimonial_setting_title) && !empty($testimonial_setting_data->testimonial_setting_title) ? $testimonial_setting_data->testimonial_setting_title : '' ?></h5>
                                <h2 class="title"><?= isset($testimonial_setting_data->testimonial_setting_desc) && !empty($testimonial_setting_data->testimonial_setting_desc) ? $testimonial_setting_data->testimonial_setting_desc : '' ?></h2>
                            </div>
                        </div><!-- section title end -->
                        <p><?= isset($testimonial_setting_data->testimonial_setting_sub_desc) && !empty($testimonial_setting_data->testimonial_setting_sub_desc) ? $testimonial_setting_data->testimonial_setting_sub_desc : '' ?></p>
                        <div class="featuredbox-number">
                            <!--featured-icon-box-->
                            <div class="featured-icon-box icon-align-before-content icon-ver_align-top style1">
                                <div class="featured-icon">
                                    <div class="cmt-icon cmt-icon_element-fill cmt-icon_element-color-skincolor cmt-icon_element-size-xs cmt-icon_element-style-rounded"> 
                                        <i class="cmt-num ti-info"></i>
                                    </div>
                                </div>
                                <div class="featured-content">
                                    <div class="featured-title">
                                        <h5><?= isset($testimonial_setting_data->sub_title_1) && !empty($testimonial_setting_data->sub_title_1) ? $testimonial_setting_data->sub_title_1 : '' ?></h5>
                                    </div>
                                    <div class="featured-desc">
                                        <p><?= isset($testimonial_setting_data->sub_desc_1) && !empty($testimonial_setting_data->sub_desc_1) ? $testimonial_setting_data->sub_desc_1 : '' ?></p>
                                    </div>
                                </div>
                            </div><!-- featured-icon-box end-->
                            <!--featured-icon-box-->
                            <div class="featured-icon-box icon-align-before-content icon-ver_align-top style1">
                                <div class="featured-icon">
                                    <div class="cmt-icon cmt-icon_element-fill cmt-icon_element-color-skincolor cmt-icon_element-size-xs cmt-icon_element-style-rounded"> 
                                        <i class="cmt-num ti-info"></i>
                                    </div>
                                </div>
                                <div class="featured-content">
                                    <div class="featured-title">
                                        <h5><?= isset($testimonial_setting_data->sub_title_2) && !empty($testimonial_setting_data->sub_title_2) ? $testimonial_setting_data->sub_title_2 : '' ?></h5>
                                    </div>
                                    <div class="featured-desc">
                                        <p><?= isset($testimonial_setting_data->sub_desc_2) && !empty($testimonial_setting_data->sub_desc_2) ? $testimonial_setting_data->sub_desc_2 : '' ?></p>
                                    </div>
                                </div>
                            </div><!-- featured-icon-box end-->
                            <!--featured-icon-box-->
                            <div class="featured-icon-box icon-align-before-content icon-ver_align-top style1">
                                <div class="featured-icon">
                                    <div class="cmt-icon cmt-icon_element-fill cmt-icon_element-color-skincolor cmt-icon_element-size-xs cmt-icon_element-style-rounded"> 
                                        <i class="cmt-num ti-info"></i>
                                    </div>
                                </div>
                                <div class="featured-content">
                                    <div class="featured-title">
                                        <h5><?= isset($testimonial_setting_data->sub_title_3) && !empty($testimonial_setting_data->sub_title_3) ? $testimonial_setting_data->sub_title_3 : '' ?></h5>
                                    </div>
                                    <div class="featured-desc">
                                        <p><?= isset($testimonial_setting_data->sub_desc_3) && !empty($testimonial_setting_data->sub_desc_3) ? $testimonial_setting_data->sub_desc_3 : '' ?></p>
                                    </div>
                                </div>
                            </div><!-- featured-icon-box end-->
                        </div>
                    </div>
                </div>
                <div class="col-xl-6">
                    <?php if (isset($testimonial_data) && !empty($testimonial_data)) { ?>
                        <div class="testimonials-nav z-index-2" >
                            <?php
                            foreach ($testimonial_data as $key3 => $value3) {
                                ?>
                                <div class="testimonial-nav_item">
                                    <div class="nav_item">
                                        <img class="img-fluid" src="<?= base_url() ?><?= isset($value3->testimonial_image) && !empty($value3->testimonial_image) ? $value3->testimonial_image : '' ?>" alt="testimonial-nav_item">
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="cmt-col-bgcolor-yes cmt-bgcolor-skincolor cmt-col-bgimage-yes cmt-bg col-bg-img-one cmt-right-span cmt-bg-pattern spacing-1">
                            <div class="cmt-col-wrapper-bg-layer cmt-bg-layer">
                                <div class="cmt-col-wrapper-bg-layer-inner"></div>
                            </div>
                            <div class="layer-content">
                                <div class="testimonials-info">
                                    <!-- testimonials -->
                                    <?php
                                    foreach ($testimonial_data as $key4 => $value4) {
                                        ?>
                                        <div class="testimonials">
                                            <div class="testimonial-content">
                                                <blockquote class="testimonial-text"><?= isset($value4->testimonial_description) && !empty($value4->testimonial_description) ? $value4->testimonial_description : '' ?></blockquote>
                                                <div class="testimonial-bottom">
                                                    <div class="testimonial-caption cmt-textcolor-white">
                                                        <h5><?= isset($value4->testimonial_name) && !empty($value4->testimonial_name) ? $value4->testimonial_name : '' ?></h5>
                                                        <label><?= isset($value4->testimonial_designation) && !empty($value4->testimonial_designation) ? $value4->testimonial_designation : '' ?></label>
                                                    </div>
                                                    <div class="star-ratings">
                                                        <ul class="rating">
                                                            <li><i class="fa <?= isset($value4->testimonial_rating) && !empty($value4->testimonial_rating) ? (1 <= $value4->testimonial_rating ? 'fa-star' : 'fa-star-o') : '' ?>"></i></li>
                                                            <li><i class="fa <?= isset($value4->testimonial_rating) && !empty($value4->testimonial_rating) ? (2 <= $value4->testimonial_rating ? 'fa-star' : 'fa-star-o') : '' ?>"></i></li>
                                                            <li><i class="fa <?= isset($value4->testimonial_rating) && !empty($value4->testimonial_rating) ? (3 <= $value4->testimonial_rating ? 'fa-star' : 'fa-star-o') : '' ?>"></i></li>
                                                            <li><i class="fa <?= isset($value4->testimonial_rating) && !empty($value4->testimonial_rating) ? (4 <= $value4->testimonial_rating ? 'fa-star' : 'fa-star-o') : '' ?>"></i></li>
                                                            <li><i class="fa <?= isset($value4->testimonial_rating) && !empty($value4->testimonial_rating) ? (5 <= $value4->testimonial_rating ? 'fa-star' : 'fa-star-o') : '' ?>"></i></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <!--testimonial-section end-->


    <section class="cmt-row introduction-section cmt-bgcolor-grey cmt-bg cmt-bgimage-yes bg-img7 cmt-bg-pattern mt_60 res-991-mt-0 clearfix">
        <div class="cmt-row-wrapper-bg-layer cmt-bg-layer"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-8 col-sm-11 mx-auto col-xs-12">
                    <!-- ttm_single_image-wrapper -->
                    <div class="ttm_single_image-wrapper mr-60 pt-60 res-991-pt-0 res-991-mr-0 position-relative">
                        <img class="img-fluid w-100" src="<?= base_url() ?><?= isset($visa_pre_setting_data->coaching_setting_image) && !empty($visa_pre_setting_data->coaching_setting_image) ? $visa_pre_setting_data->coaching_setting_image : '' ?>" alt="single_04">
                        <!--featured-icon-box-->
                        <div class="featured-icon-box icon-align-top-content cmt-bgcolor-darkgrey style2">
                            <div class="featured-content">
                                <div class="featured-title">
                                    <h5><?= isset($visa_pre_setting_data->setting_image_title) && !empty($visa_pre_setting_data->setting_image_title) ? $visa_pre_setting_data->setting_image_title : '' ?></h5>
                                </div>
                                <div class="featured-desc">
                                    <p>
                                        <?= isset($visa_pre_setting_data->setting_image_desc) && !empty($visa_pre_setting_data->setting_image_desc) ? $visa_pre_setting_data->setting_image_desc : '' ?>
                                    </p>
                                </div>
                            </div>
                            <a class="cmt-btn cmt-btn-size-sm cmt-icon-btn-left btn-inline cmt-btn-color-skincolor" href="<?= base_url('coaching') ?>" tabindex="0"><i class="fa fa-minus"></i>View More Details</a>
                        </div><!-- featured-icon-box end-->
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <div class="pt-60">
                        <!-- section title -->
                        <div class="section-title">
                            <div class="title-header">
                                <h5><?= isset($visa_pre_setting_data->coaching_setting_title) && !empty($visa_pre_setting_data->coaching_setting_title) ? $visa_pre_setting_data->coaching_setting_title : '' ?></h5>
                                <h2 class="title"><?= isset($visa_pre_setting_data->coaching_setting_desc) && !empty($visa_pre_setting_data->coaching_setting_desc) ? $visa_pre_setting_data->coaching_setting_desc : '' ?></h2>
                            </div>
                        </div><!-- section title end -->
                        <div class="row">
                            <?php
                            if (isset($visa_preparation_data) && !empty($visa_preparation_data)) {
                                foreach ($visa_preparation_data as $key5 => $value5) {
                                    $key5++;
                                    if ($key5 < 5) {
                                        ?>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="featured-icon-box icon-align-top-content style3">
                                                <div class="featured-icon">
                                                    <div class="cmt-icon cmt-icon_element-onlytxt cmt-icon_element-color-skincolor cmt-icon_element-size-md">
                                                        <i class="<?= isset($value5->icon) && !empty($value5->icon) ? $value5->icon : '' ?>"></i>
                                                    </div>
                                                </div>
                                                <div class="featured-content">
                                                    <div class="featured-title">
                                                        <h5><a href="<?= isset($value5->slug) && !empty($value5->slug) ? base_url(COACHING_SLUG_PREPEND) . $value5->slug : 'javascript:void(0);' ?>"><?= isset($value5->coaching_page_name) && !empty($value5->coaching_page_name) ? $value5->coaching_page_name : '' ?></a></h5>
                                                    </div>
                                                    <div class="featured-desc">
                                                        <p><?= isset($value5->coaching_page_desc) && !empty($value5->coaching_page_desc) ? $value5->coaching_page_desc : '' ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div><!-- row end -->
        </div>
    </section>


    <?php if (isset($blog_data) && !empty($blog_data)) { ?>
        <!--blog-section-->
        <section class="cmt-row blog-section clearfix">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-7 m-auto">
                        <!-- section title -->
                        <div class="section-title title-style-center_text">
                            <div class="title-header">
                                <h5>blog corner</h5>
                                <h2 class="title">in your <strong> inbox news</strong></h2>
                            </div>
                        </div><!-- section title end -->
                    </div>
                </div><!-- row end -->
                <!-- slick_slider -->
                <div class="row">
                    <div class="col-lg-6 col-md-5 col-sm-12">
                        <div class="blog-info">
                            <?php foreach ($blog_data as $key => $value) { ?>
                                <div class="blog-info_item">
                                    <!-- featured-imagebox-post -->
                                    <div class="featured-imagebox featured-imagebox-post style1 bor_rad_5">
                                        <div class="cmt-post-thumbnail featured-thumbnail"> 
                                            <img class="img-fluid" src="<?= isset($value->blog_poster_image) && !empty($value->blog_poster_image) && file_exists($value->blog_poster_image) ? $value->blog_poster_image : '' ?>" alt="image">
                                        </div>
                                        <div class="featured-content featured-content-post">
                                            <div class="cmt-post-link">
                                                <div class="post-meta">
                                                    <span class="cmt-meta-line post-date"><i class="fa fa-calendar"></i><?= isset($value->blog_date) && !empty($value->blog_date) ? date('d M Y', strtotime($value->blog_date)) : '' ?></span>
                                                </div>
                                            </div>
                                            <div class="post-title featured-title">
                                                <h5><a href="<?= isset($value->slug) && !empty($value->slug) ? base_url(BLOG_SLUG_PREPEND) . $value->slug : 'javascript:void(0);' ?>" tabindex="0"><?= isset($value->blog_title) && !empty($value->blog_title) ? $value->blog_title : '' ?></a></h5>
                                            </div>
                                            <div class="post-desc featured-desc">
                                                <p><?= isset($value->blog_short_desc) && !empty($value->blog_short_desc) ? $value->blog_short_desc : '' ?></p>
                                            </div>
                                        </div>
                                    </div><!-- featured-imagebox-post end-->
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-7 col-sm-12">
                        <div class="blog-nav">
                            <?php foreach ($blog_data as $key => $value) { ?>
                                <div class="blog-nav_item">
                                    <!-- featured-imagebox-post -->
                                    <div class="d-flex align-items-center featured-imagebox featured-imagebox-post style2 bor_rad_5">
                                        <div class="cmt-post-thumbnail featured-thumbnail"> 
                                            <img class="img-fluid" src="<?= isset($value->blog_poster_image) && !empty($value->blog_poster_image) && file_exists($value->blog_poster_image) ? $value->blog_poster_image : '' ?>" alt="image">
                                        </div>
                                        <div class="featured-content featured-content-post">
                                            <div class="post-top cmt-post-link">
                                                <div class="post-meta">
                                                    <span class="cmt-meta-line post-date"><i class="fa fa-calendar"></i><?= isset($value->blog_date) && !empty($value->blog_date) ? date('d M Y', strtotime($value->blog_date)) : '' ?></span>
                                                </div>
                                            </div>
                                            <div class="post-title featured-title">
                                                <h5><a href="<?= isset($value->slug) && !empty($value->slug) ? base_url(BLOG_SLUG_PREPEND) . $value->slug : 'javascript:void(0);' ?>" tabindex="0"><?= isset($value->blog_title) && !empty($value->blog_title) ? $value->blog_title : '' ?></a></h5>
                                            </div>
                                        </div>
                                    </div><!-- featured-imagebox-post end-->
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--blog-section end-->
    <?php } ?>


    <?php if (isset($client_data) && !empty($client_data)) { ?>
        <!--client-section-->
        <div class="cmt-row client-section cmt-bgcolor-grey cmt-bg cmt-bgimage-yes bg-img7 cmt-bg-pattern border-bottom clearfix">
            <div class="cmt-row-wrapper-bg-layer cmt-bg-layer"></div>
            <div class="container">
                <!-- row -->
                <div class="row text-center">
                    <div class="col-md-12">
                        <!-- slick_slider -->
                        <div class="slick_slider row" data-slick='{"slidesToShow": 6, "slidesToScroll": 1, "arrows":false, "autoplay":true, "infinite":true, "responsive": [{"breakpoint":1200,"settings":{"slidesToShow": 5}}, {"breakpoint":1024,"settings":{"slidesToShow": 4}}, {"breakpoint":777,"settings":{"slidesToShow": 3}}, {"breakpoint":575,"settings":{"slidesToShow": 2}}, {"breakpoint":380,"settings":{"slidesToShow": 1}}]}'>
                            <?php
                            foreach ($client_data as $k7 => $v7) {
                                if (isset($v7->client_image) && !empty($v7->client_image) && file_exists($v7->client_image)) {
                                    ?>
                                    <div class="client-box">
                                        <div class="cmt-client-logo-tooltip" data-tooltip="<?= isset($v7->client_name) && !empty($v7->client_name) ? $v7->client_name : '' ?>">
                                            <div class="client-thumbnail">
                                                <img class="img-fluid" src="<?= base_url() . $v7->client_image ?>" alt="image">
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div><!-- cmt-client end -->      
                    </div>
                </div><!-- row end -->
            </div>
        </div>
        <!--client-section end-->
    <?php } ?>

</div><!--site-main end-->

