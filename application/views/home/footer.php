<style>
    #register-form label{
        color: black;
        /*font-size: 0.7rem;*/
    }
</style>
<!--footer start-->
<?php if (isset($footer_data->register_popup_active) && !empty($footer_data->register_popup_active) && $footer_data->register_popup_active == 1) { ?>
    <div class="modal fade show" style="z-index: 99999; display: block; padding-right: 15px;" id="onLoadModal" tabindex="-1" aria-labelledby="exampleModalLabel" data-backdrop="static" data-keyboard="false" aria-modal="true">
        <div class="modal-dialog  modal-dialog-centered modal-lg">
            <!--width: 60%; max-width: 100%;margin-left: auto;margin-right: auto;-->
            <div class="modal-content " style="box-shadow: 0px 6px 8px 4px rgb(3 123 255 / 32%);">
                <form method="post" action="javascript:void(0);" id="register-form" class="assessment-form" novalidate="novalidate">
                    <div class="modal-header" style="position: absolute;right: -12px;border-bottom: 0;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="background: #1f2437;border-radius: 2rem;opacity: 1;padding: 5px;margin: -2rem -1rem;height: 2rem;width: 2rem;z-index: 99;">
                            <span style="color: #fff; font-weight: 400;" aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="padding: 0 1rem 0 1rem;">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 text-center p-0">
                                <!--d-none d-sm-none d-md-none d-lg-block d-xl-block-->
                                <img src="<?= base_url() . (isset($footer_data->register_popup_image) && !empty($footer_data->register_popup_image) && file_exists($footer_data->register_popup_image) ? $footer_data->register_popup_image : '') ?>" alt="Register" style="height: 550px;width: 400px;max-width:100%;">
                            </div>
                            <div class="col-lg-6 col-md-6"> <h5 style="border-bottom: 1px solid #d3d3d3;text-align: center;padding-bottom: 5px;margin-bottom: 5px;padding-top: 10px;">Book A Free Counselling</h5>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label for="name" class="col-form-label" id="label_my">Name:</label>
                                        <input type="text" class="form-control textonly" id="name" name="name" required="" placeholder="Name">
                                        <span></span>
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="phone" class="col-form-label">Phone:</label>
                                        <input type="text" class="form-control contactnumber" id="phone" name="phone" required="" placeholder="Phone">
                                        <span></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label for="whatsapp_no" class="col-form-label">WhatsApp No:</label>
                                        <input type="text" class="form-control contactnumber" id="whatsapp_no" name="whatsapp_no" required="" placeholder="WhatsApp Number">
                                        <span></span>
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="email" class="col-form-label">Email:</label>
                                        <input type="email" class="form-control" id="email" name="email" required="" placeholder="Email Address">
                                        <span></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label for="dob" class="col-form-label">Date of Birth:</label>
                                        <input type="date" class="form-control" id="dob" name="dob" required="">
                                        <span></span>
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="current_education" class="col-form-label">Current Education:</label>
                                        <select class="form-control" name="current_education" id="current_education" required="">
                                            <option value="">Select Education Qualification</option>
                                            <option value="Below 12">Below 12</option>
                                            <option value="12">12</option>
                                            <option value="Diploma">Diploma</option>
                                            <option value="Bachelors">Bachelors</option>
                                            <option value="Masters">Masters</option>
                                        </select>
                                        <span></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label for="experience" class="col-form-label">Experience:</label>
                                        <select class="form-control" name="experience" id="experience" required="">
                                            <option value="">Select Experience</option>
                                            <option value="0 Year">0 Year</option>
                                            <option value="1 Year">1 Year</option>
                                            <option value="2 Year">2 Year</option>
                                            <option value="3 Year">3 Year</option>
                                            <option value="4 Year">4 Year</option>
                                            <option value="5 Year">5 Year</option>
                                            <option value="6 Year">6 Year</option>
                                            <option value="6+ Year">6+ Year</option>
                                        </select>
                                        <span></span>
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="service" class="col-form-label">Service:</label>
                                        <select class="form-control" name="service" id="service" required="">
                                            <option value="">Select Service</option>
                                            <option value="Student Visa">Student Visa</option>
                                            <option value="Foreign Education">Foreign Education</option>
                                            <option value="Immigration Visa">Immigration Visa (Canada PR)</option>
                                            <option value="Non-Immigration Visa">Non-Immigration Visa (Visitor PR)</option>
                                            <option value="IELTS Coaching">IELTS Coaching</option>
                                        </select>
                                        <span></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label for="registration" class="col-form-label">Registration Status:</label>
                                        <select class="form-control" name="registration" id="registration" required="">
                                            <option value="">Select Registration</option>
                                            <option value="New Inquiry">New Inquiry</option>
                                            <option value="Visited">Visited</option>
                                            <option value="Registered">Registered</option>
                                        </select>
                                        <span></span>
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="ielts_appeared" class="col-form-label">IELTS Appeared:</label>
                                        <select class="form-control" name="ielts_appeared" id="ielts_appeared" required="">
                                            <option value="">--Select Options--</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                        <span></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label for="appointment_date" class="col-form-label">Appointment Date:</label>
                                        <input type="date" class="form-control" id="appointment_date" name="appointment_date" required="">
                                        <span></span>
                                    </div>
                                    <div class="col-lg-6">
                                        <label for="appointment_time" class="col-form-label">Appointment Time:</label>
                                        <input type="text" class="form-control" id="appointment_time" name="appointment_time">
                                        <span></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="text-center">
                                        <span class="success register_success" style="display: none;color: green;"><em>Your registration was submitted and will be responded to as soon as possible.</em></span>
                                        <span class="success register_error" style="display: none;color: red;"><em>Something went wrong. Try again!</em></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 text-right mt-3 mb-2">
                                        <button type="submit" class="theme-btn-two btn-primary">Submit Now</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>

<footer class="footer cmt-bgcolor-darkgrey widget-footer clearfix">
    <div class="first-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 widget-area">
                    <div class="widget widget_text mr-25 clearfix">
                        <h3 class="widget-title"><?= isset($footer_data->description_title) && !empty($footer_data->description_title) ? $footer_data->description_title : '' ?></h3>
                        <div class="textwidget widget-text">
                            <p><?= isset($footer_data->description) && !empty($footer_data->description) ? $footer_data->description : '' ?></p>
                        </div>
                        <div class="cmt-horizontal_sep mt-25 mb-30"></div>
                        <div class="social-icons circle">
                            <ul class="list-inline cmt-textcolor-skincolor">
                                <li class="social-facebook"><a class="tooltip-top" target="<?= isset($footer_data->footer_facebook) && !empty($footer_data->footer_facebook) && $footer_data->footer_facebook != 'javascript:void(0);' ? '_blank' : '' ?>" href="<?= isset($footer_data->footer_facebook) && !empty($footer_data->footer_facebook) && $footer_data->footer_facebook != 'javascript:void(0);' ? $footer_data->footer_facebook : 'javascript:void(0);' ?>" data-tooltip="Facebook"><i class="ti ti-facebook"></i></a></li>
                                <li class="social-twitter"><a class="tooltip-top" target="<?= isset($footer_data->footer_twitter) && !empty($footer_data->footer_twitter) && $footer_data->footer_twitter != 'javascript:void(0);' ? '_blank' : '' ?>" href="<?= isset($footer_data->footer_twitter) && !empty($footer_data->footer_twitter) && $footer_data->footer_twitter != 'javascript:void(0);' ? $footer_data->footer_twitter : 'javascript:void(0);' ?>" data-tooltip="Twitter"><i class="ti ti-twitter-alt"></i></a></li>
                                <li class="social-instagram"><a class="tooltip-top" target="<?= isset($footer_data->footer_insta) && !empty($footer_data->footer_insta) && $footer_data->footer_insta != 'javascript:void(0);' ? '_blank' : '' ?>" href="<?= isset($footer_data->footer_insta) && !empty($footer_data->footer_insta) && $footer_data->footer_insta != 'javascript:void(0);' ? $footer_data->footer_insta : 'javascript:void(0);' ?>" data-tooltip="Instagram"><i class="ti ti-instagram"></i></a></li>
                                <li class="social-twitter"><a class="tooltip-top" target="<?= isset($footer_data->footer_linkedin) && !empty($footer_data->footer_linkedin) && $footer_data->footer_linkedin != 'javascript:void(0);' ? '_blank' : '' ?>" href="<?= isset($footer_data->footer_linkedin) && !empty($footer_data->footer_linkedin) && $footer_data->footer_linkedin != 'javascript:void(0);' ? $footer_data->footer_linkedin : 'javascript:void(0);' ?>" data-tooltip="Linkedin"><i class="ti ti-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 widget-area">
                    <?php if (isset($all_visa_page) && !empty($all_visa_page)) { ?>
                        <div class="widget widget_nav_menu clearfix">
                            <h3 class="widget-title">Visa Services</h3>
                            <ul id="menu-footer-quick-links">
                                <?php
                                foreach ($all_visa_page as $key4 => $value4) {
                                    $key4++;
                                    if ($key4 < 8) {
                                        ?>
                                        <li><a href="<?= isset($value4->slug) && !empty($value4->slug) ? base_url(SERVICE_SLUG_PREPEND) . $value4->slug : 'javascript:void(0);' ?>"><?= isset($value4->visa_page_name) && !empty($value4->visa_page_name) ? $value4->visa_page_name : '' ?></a></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 widget-area">
                    <?php if (isset($all_country_page) && !empty($all_country_page)) { ?>
                        <div class="widget widget-recent-post clearfix">
                            <h3 class="widget-title">Countries </h3>
                            <ul id="menu-footer-quick-links">
                                <?php
                                foreach ($all_country_page as $key2 => $value3) {
                                    if (isset($value3->sub_country) && !empty($value3->sub_country)) {
                                        foreach ($value3->sub_country as $key11 => $value11) {
                                            $key2++;
                                            if ($key2 < 8) {
                                                ?>
                                                <li><a href="<?= isset($value11->slug) && !empty($value11->slug) ? base_url(COUNTRY_SLUG_PREPEND) . $value11->slug : 'javascript:void(0);' ?>"><?= isset($value11->country_page_name) && !empty($value11->country_page_name) ? $value11->country_page_name : '' ?></a></li>
                                                <?php
                                            }
                                        }
                                    } else {
                                        $key2++;
                                        if ($key2 < 8) {
                                            ?>
                                            <li><a href="<?= isset($value3->slug) && !empty($value3->slug) ? base_url(COUNTRY_SLUG_PREPEND) . $value3->slug : 'javascript:void(0);' ?>"><?= isset($value3->country_page_name) && !empty($value3->country_page_name) ? $value3->country_page_name : '' ?></a></li>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 widget-area">
                    <div class="widget contact_map clearfix">
                        <h3 class="widget-title" style="margin-bottom: 36px !important;">Working Hours</h3>
                        <ul class="working-hours" id="menu-footer-quick-links">
                            <li>
                                <span class="day">Monday</span>
                                <span class="hours"><?= isset($footer_data->mon_time) && !empty($footer_data->mon_time) ? $footer_data->mon_time : '' ?></span>
                            </li>
                            <li>
                                <span class="day">Tuesday</span>
                                <span class="hours"><?= isset($footer_data->tue_time) && !empty($footer_data->tue_time) ? $footer_data->tue_time : '' ?></span>
                            </li><li>
                                <span class="day">Wednesday</span>
                                <span class="hours"><?= isset($footer_data->wed_time) && !empty($footer_data->wed_time) ? $footer_data->wed_time : '' ?></span>
                            </li><li>
                                <span class="day">Thursday</span>
                                <span class="hours"><?= isset($footer_data->thu_time) && !empty($footer_data->thu_time) ? $footer_data->thu_time : '' ?></span>
                            </li><li>
                                <span class="day">Friday</span>
                                <span class="hours"><?= isset($footer_data->fri_time) && !empty($footer_data->fri_time) ? $footer_data->fri_time : '' ?></span>
                            </li><li>
                                <span class="day">Saturday</span>
                                <span class="hours"><?= isset($footer_data->sat_time) && !empty($footer_data->sat_time) ? $footer_data->sat_time : '' ?></span>
                            </li><li>
                                <span class="day">Sunday</span>
                                <span class="hours"><?= isset($footer_data->sun_time) && !empty($footer_data->sun_time) ? $footer_data->sun_time : '' ?></span>
                            </li>
                        </ul>    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="second-footer">
        <div class="container">
            <div class="row no-gutters">
                <div class="widget-area col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <aside class="widget widget-text">
                        <!--featured-icon-box-->
                        <div class="featured-icon-box icon-align-before-content">
                            <div class="featured-icon">
                                <div class="cmt-icon cmt-icon_element-fill cmt-icon_element-color-darkgrey cmt-icon_element-size-sm cmt-icon_element-style-square">
                                    <i class="fa fa-envelope-o"></i>
                                </div>
                            </div>
                            <div class="featured-content">
                                <div class="featured-title">
                                    <h5><a href="mailto:<?= isset($footer_data->footer_email) && !empty($footer_data->footer_email) ? $footer_data->footer_email : '' ?>"><?= isset($footer_data->footer_email) && !empty($footer_data->footer_email) ? $footer_data->footer_email : '' ?></a></h5>
                                </div>
                                <div class="featured-desc">
                                    <p>Drop Us a Line</p>
                                </div>
                            </div>
                        </div><!-- featured-icon-box end-->
                    </aside>
                </div>

                <div class="widget-area col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <aside class="widget widget-text cmt-bgcolor-skincolor">
                        <!--featured-icon-box-->
                        <div class="featured-icon-box icon-align-before-content">
                            <div class="featured-icon">
                                <div class="cmt-icon cmt-icon_element-fill cmt-icon_element-color-darkgrey cmt-icon_element-size-sm cmt-icon_element-style-square">
                                    <i class="fa fa-phone"></i>
                                </div>
                            </div>
                            <div class="featured-content">
                                <div class="featured-title">
                                    <h5 class="tel"><a  href="tel:<?= isset($footer_data->footer_contact) && !empty($footer_data->footer_contact) ? str_replace(' ', '', $footer_data->footer_contact) : 'javascript:void(0);' ?>" target="_blank">
                                            <?= isset($footer_data->footer_contact) && !empty($footer_data->footer_contact) ? $footer_data->footer_contact : '' ?></a>
                                    </h5>
                                </div>
                                <div class="featured-desc">
                                    <p>Call Us Now!</p>
                                </div>
                            </div>
                        </div><!-- featured-icon-box end-->
                    </aside>
                </div>
                <div class="widget-area col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <aside class="widget widget-text">
                        <!--featured-icon-box-->
                        <div class="featured-icon-box icon-align-before-content" style="margin: 4px 0;">
                            <div class="featured-icon">
                                <div class="cmt-icon cmt-icon_element-fill cmt-icon_element-color-darkgrey cmt-icon_element-size-sm cmt-icon_element-style-square">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                            </div>
                            <div class="featured-content">
                                <div class="featured-title">
                                    <h5 style="font-size: 15px;"><?= isset($footer_data->footer_address) && !empty($footer_data->footer_address) ? $footer_data->footer_address : '' ?></h5>
                                </div>
                                <div class="featured-desc">
                                    <p>Get Direction</p>
                                </div>
                            </div>
                        </div><!-- featured-icon-box end-->
                    </aside>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-footer-text">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="copyright text-center">
                        <div id="menu-footer-menu">
                            <ul class="footer-nav-menu text-center">
                                <li><a href="<?= base_url() ?>">Home</a></li>
                                <li><a href="<?= base_url('about') ?>">About</a></li>
                                <li><a href="<?= base_url('contact') ?>">Contact Us</a></li>
                            </ul>
                        </div>
                        <span>Copyright © 2020&nbsp;<a href="<?= base_url() ?>">Canopus Global Education</a>. All rights reserved. | <a href="<?= base_url('privacy-policy') ?>">Privacy Policy</a> | Powered By <a href="http://weborative.com/" target="_blank">Weborative</a> </span>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</footer>
<!--footer end-->
<!--back-to-top start-->
<a id="totop" href="#top">
    <i class="fa fa-angle-up"></i>
</a>
<!--back-to-top end-->
</div><!-- page end -->
<!-- Javascript -->
<script src="<?= base_url() ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url() ?>assets/js/tether.min.js"></script>
<script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script> 
<script src="<?= base_url() ?>assets/js/jquery.easing.js"></script>    
<script src="<?= base_url() ?>assets/js/jquery-waypoints.js"></script>    
<script src="<?= base_url() ?>assets/js/jquery-validate.js"></script> 
<script src="<?= base_url() ?>assets/js/jquery.prettyPhoto.js"></script>
<script src="<?= base_url() ?>assets/js/slick.min.js"></script>
<script src="<?= base_url() ?>assets/js/numinate.min.js"></script>
<script src="<?= base_url() ?>assets/js/imagesloaded.min.js"></script>
<script src="<?= base_url() ?>assets/js/jquery-isotope.js"></script>
<script src="<?= base_url() ?>assets/js/main.js"></script>
<!-- Revolution Slider -->
<script src="<?= base_url() ?>assets/revolution/js/slider.js"></script>
<!-- SLIDER REVOLUTION 6.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->    
<script  src='<?= base_url() ?>assets/revolution/js/revolution.tools.min.js'></script>
<script  src='<?= base_url() ?>assets/revolution/js/rs6.min.js'></script>

<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<script>
    $(document).ready(function () {
        (function () {
            'use strict'

            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.querySelectorAll('.needs-validation')

            // Loop over them and prevent submission
            Array.prototype.slice.call(forms)
                    .forEach(function (form) {
                        form.addEventListener('submit', function (event) {
                            if (!form.checkValidity()) {
                                event.preventDefault()
                                event.stopPropagation()
                            }

                            form.classList.add('was-validated')
                        }, false)
                    })
        })();

//        $('#appointment_time').timepicker({
//            uiLibrary: 'bootstrap4',
//        });

        $('#appointment_time').datetimepicker({
            uiLibrary: 'materialdesign',
            footer: true,
            format: "dd/mm/yyyy hh:MM",
        });

        $('#register-form').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            beforeSend: function () {
//                $(placeholder).addClass('loading');
            },
            submitHandler: function (form) {
                $.ajax({
                    type: 'post',
                    url: '<?= base_url('Register') ?>',
                    data: $('#register-form').serialize(),
                    dataType: 'json',
                    success: function (returnData) {
                        $('.please_wait').css({"display": "none"});
                        if (returnData.result == true) {
                            $('#register-form').trigger("reset");
                            $('.register_success').css({"display": "block"});
                            setTimeout(function () {
                                $('.register_success').css({"display": "none"});
                                $('#onLoadModal').modal('hide');
                            }, 5000);
                        } else {
                            $('.register_error').css({"display": "block"});
                            setTimeout(function () {
                                $('.register_error').css({"display": "none"});
                            }, 3000);
                        }
                    }
                });
            },
            errorPlacement: function (error, element) {
                if (element.attr("id") == 'dob') {
                    error.insertAfter(element.next().next());
                    element.next().next().addClass('text-danger');
                } else {
                    error.insertAfter(element.next());
                    element.next().next().addClass('text-danger');
                }
            }
        });

        $("#share_on_whatsapp").on("click", function () {
            var link = window.location.href;
            var message = 'You%20can%20see%20blog%20click%20here%20' + link;
            var person = prompt("Please Enter number to Share:", "");
            if (person == null || person == "") {
                alert("Please Enter Mobile Number.");
            } else if (isNaN(person) || person.length != 10) {
                alert("Please Enter Only 10 digit Mobile Number.");
            } else {
                window.open("https://api.whatsapp.com/send?text=" + message + "&phone=+91" + person, "_BLANK");
            }
        });
    });

    $(document).on('click', '#contact_us_form', function () {
        $('.please_wait').css({"display": "block"});
        var ContactFormName = $('#contact_name').val();
        var ContactFormEmail = $('#contact_email').val();
        var ContactFormPhone = $('#contact_phone').val();
        var ContactFormSubject = $('#contact_subject').val();
        var ContactFormMessage = $('#contact_message').val();
        if (ContactFormName != '' && ContactFormEmail != '' && ContactFormPhone != '' && ContactFormSubject != '' && ContactFormMessage != '') {
            $.ajax({
                type: 'post',
                url: '<?= base_url('Contact/contact_us') ?>',
                data: $('#contact_form').serialize(),
                dataType: 'json',
                success: function (returnData) {
                    $('.please_wait').css({"display": "none"});
                    if (returnData.result == true) {
                        $('#contact_name').val('');
                        $('#contact_email').val('');
                        $('#contact_phone').val('');
                        $('#contact_subject').val('');
                        $('#contact_message').val('');
                        $('.contact_success').css({"display": "block"});
                        setTimeout(function () {
                            $('.contact_success').css({"display": "none"});
                        }, 5000);
                    } else {
                        $('.contact_error').css({"display": "block"});
                        setTimeout(function () {
                            $('.contact_error').css({"display": "none"});
                        }, 3000);
                    }
                }
            });
        }
    });



    $(document).on('click', '.book-consulation', function () {
        window.location.href = "contact";
    });

    window.onload = function () {
//        $(".loader-active").css("overflow", "auto", "important");
//        $(".loader-container").css({"display": "none"});
    }

<?php
if ($this->page_id == 'HOME') {
    ?>
        setTimeout(function () {
            $('#onLoadModal').modal("show");
        }, 1500);
    <?php
}
?>
</script>
<!-- Javascript end-->
</body>
</html>
