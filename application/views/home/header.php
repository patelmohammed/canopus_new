<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="author" content="Canopus Global Education" />

        <meta name="keywords" content="<?= $this->meta_key ?>" />
        <meta name="description" content="<?= $this->meta_desc ?>" />

        <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1" />
        <!--        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
                <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
                <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">-->
        <!--<link rel="manifest" href="/site.webmanifest">-->
        <!--<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">-->
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        <title><?= $this->meta_title ?></title>
        <!-- favicon icon -->
        <!--<link rel="shortcut icon" href="favicon.png" />-->
        <!-- bootstrap -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/bootstrap.min.css"/>
        <!-- animate -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/animate.css"/>
        <!-- fontawesome -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/font-awesome.css"/>
        <!-- themify -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/themify-icons.css"/>
        <!-- flaticon -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/flaticon.css"/>
        <!-- slick -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/slick.css">
        <!-- REVOLUTION LAYERS STYLES -->
        <link rel='stylesheet' id='rs-plugin-settings-css' href="<?= base_url() ?>assets/revolution/css/rs6.css"> 
        <!-- prettyphoto -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/prettyPhoto.css">
        <!-- shortcodes -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/shortcodes.css"/>
        <!-- main -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/main.css"/>
        <!-- main -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/megamenu.css"/>
        <!-- responsive -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/responsive.css"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/loader.css"/>
        <link rel="canonical" href="https://www.canopusedu.com" />
        <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=61275510a3aee000125e7037&product=inline-share-buttons' async='async'></script>
        <style>
            .tel a:hover {
                color: #fff;
            }
            .working-hours li{
                color: #FFF;
                padding: 0 0 37px 0 !important;
            }
            .day{
                width: 35%;
                text-align: left;
                float: left;
            }
            .hours{
                width: 65%;
                text-align: right;
                float: right;
            }
            .modal-backdrop{
                z-index: 0;
            }
        </style>
        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-NP6SNM9');</script>
        <!-- End Google Tag Manager -->

        <!-- Google Tag Manager (noscript) -->
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NP6SNM9"height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <!-- End Google Tag Manager (noscript) -->

        <!-- Facebook Pixel Code -->
        <script>
            !function (f, b, e, v, n, t, s)
            {
                if (f.fbq)
                    return;
                n = f.fbq = function () {
                    n.callMethod ?
                            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)
                    f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
                    'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '544668956733991');
            fbq('track', 'PageView');
        </script>
        <noscript>
    <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=544668956733991&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

    <script>
        var chatbot_id = 7979;
        !function () {
            var t, e, a = document, s = "smatbot-chatbot";
            a.getElementById(s) || (t = a.createElement("script"), t.id = s, t.type = "text/javascript", t.src = "https://smatbot.s3.amazonaws.com/files/smatbot_plugin.js.gz", e = a.getElementsByTagName("script")[0], e.parentNode.insertBefore(t, e))
        }();
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fingerprintjs2/1.5.1/fingerprint2.min.js"></script>
</head>
<body >



    <!--page start-->
    <div class="page">
        <!--header start-->
        <header id="masthead" class="header cmt-header-style-01">
            <!-- top_bar -->
            <div class="top_bar cmt-bgcolor-darkgrey cmt-textcolor-white clearfix">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="d-flex flex-row align-items-center justify-content-center">
                                <div class="top_bar_contact_item">
                                    <div class="top_bar_icon"><i class="ti ti-alarm-clock"></i></div>Mon - Sat 10.00 - 18:00
                                </div>
                                <div class="top_bar_contact_item">
                                    <div class="top_bar_icon"><i class="ti ti-location-pin"></i></div><?= isset($footer_data->header_address) && !empty($footer_data->header_address) ? $footer_data->header_address : '' ?>
                                </div>
                                <div class="top_bar_contact_item ml-auto">
                                    <div class="top_bar_icon"><i class="fa fa-envelope-o"></i></div><a href="mailto:<?= isset($footer_data->footer_email) && !empty($footer_data->footer_email) ? $footer_data->footer_email : '' ?>"><?= isset($footer_data->footer_email) && !empty($footer_data->footer_email) ? $footer_data->footer_email : '' ?></a>
                                </div>
                                <div class="top_bar_social">
                                    <ul class="social-icons">
                                        <li class="linkedin-icon"><a target="<?= isset($footer_data->footer_linkedin) && !empty($footer_data->footer_linkedin) && $footer_data->footer_linkedin != 'javascript:void(0);' ? '_blank' : '' ?>" href="<?= isset($footer_data->footer_linkedin) && !empty($footer_data->footer_linkedin) && $footer_data->footer_linkedin != 'javascript:void(0);' ? $footer_data->footer_linkedin : 'javascript:void(0);' ?>"><i class="ti ti-linkedin"></i></a>
                                        </li>
                                        <li class="instagram-icon"><a target="<?= isset($footer_data->footer_insta) && !empty($footer_data->footer_insta) && $footer_data->footer_insta != 'javascript:void(0);' ? '_blank' : '' ?>" href="<?= isset($footer_data->footer_insta) && !empty($footer_data->footer_insta) && $footer_data->footer_insta != 'javascript:void(0);' ? $footer_data->footer_insta : 'javascript:void(0);' ?>"><i class="ti ti-instagram"></i></a>
                                        </li>
                                        <li class="twitter-icon"><a target="<?= isset($footer_data->footer_twitter) && !empty($footer_data->footer_twitter) && $footer_data->footer_twitter != 'javascript:void(0);' ? '_blank' : '' ?>" href="<?= isset($footer_data->footer_twitter) && !empty($footer_data->footer_twitter) && $footer_data->footer_twitter != 'javascript:void(0);' ? $footer_data->footer_twitter : 'javascript:void(0);' ?>"><i class="ti ti-twitter-alt"></i></a>
                                        </li>
                                        <li class="facebook-icon"><a target="<?= isset($footer_data->footer_facebook) && !empty($footer_data->footer_facebook) && $footer_data->footer_facebook != 'javascript:void(0);' ? '_blank' : '' ?>" href="<?= isset($footer_data->footer_facebook) && !empty($footer_data->footer_facebook) && $footer_data->footer_facebook != 'javascript:void(0);' ? $footer_data->footer_facebook : 'javascript:void(0);' ?>"><i class="ti ti-facebook"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="cmt-bg cmt-col-bgcolor-yes cmt-right-span cmt-bgcolor-skincolor pl-20">
                                    <div class="cmt-col-wrapper-bg-layer cmt-bg-layer"></div>
                                    <div class="layer-content">
                                        <div class="top_bar_contact_item tel">
                                            <div class="top_bar_icon"><i class="fa fa-phone"> </i>
                                            </div>
                                            <a  href="tel:<?= isset($footer_data->footer_contact) && !empty($footer_data->footer_contact) ? str_replace(' ', '', $footer_data->footer_contact) : 'javascript:void(0);' ?>" target="_blank">
                                                <?= isset($footer_data->footer_contact) && !empty($footer_data->footer_contact) ? $footer_data->footer_contact : '' ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- top_bar end-->
            <!-- site-header-menu -->
            <div id="site-header-menu" class="site-header-menu cmt-bgcolor-white">
                <div class="site-header-menu-inner cmt-stickable-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 pr-0">
                                <!--site-navigation -->
                                <div class="site-navigation d-flex flex-row align-items-center justify-content-between">
                                    <!-- site-branding -->
                                    <div class="site-branding ">
                                        <a class="home-link" href="<?= base_url() ?>" title="Canopus" rel="home">
                                            <img id="logo-img" class="img-center" src="<?= base_url() . (isset($footer_data->logo_image) && !empty($footer_data->logo_image) ? $footer_data->logo_image : '') ?>" alt="logo-img">
                                        </a>
                                    </div><!-- site-branding end -->
                                    <!-- widget-info -->
                                    <div class="widget_info mr-auto">
                                        <div>Visa &amp; Immigration <br/>Consultants</div>
                                    </div>
                                    <div class="d-flex flex-row">
                                        <div class="btn-show-menu-mobile menubar menubar--squeeze">
                                            <span class="menubar-box">
                                                <span class="menubar-inner"></span>
                                            </span>
                                        </div>
                                        <!-- menu -->
                                        <nav class="main-menu menu-mobile" id="menu">
                                            <ul class="menu">
                                                <li class="<?= $this->page_id == 'HOME' ? 'active' : '' ?>">
                                                    <a href="<?= base_url() ?>" class="">Home</a>
                                                </li>
                                                <li class="<?= $this->page_id == 'ABOUT' ? 'active' : '' ?>">
                                                    <a href="<?= base_url('about') ?>" class="">About</a>
                                                </li>
                                                <li class="<?= $this->page_id == 'E-LEARN' ? 'active' : '' ?>">
                                                    <a href="http://elearn.canopusedu.com/" target="_blank" class="">E-Learn</a>
                                                </li>
                                                <li class="mega-menu-item <?= $this->page_id == 'COACHING' ? 'active' : '' ?>">
                                                    <a href="javascript:void(0);" class="mega-menu-link">Coaching</a>
                                                    <ul class="mega-submenu">
                                                        <?php
                                                        if (isset($all_page) && !empty($all_page)) {
                                                            foreach ($all_page as $key2 => $value2) {
                                                                ?>
                                                                <li class="<?= $this->menu_id == 'COACHING_' . $value2->coaching_page_id ? 'active' : '' ?>"><a href="<?= isset($value2->slug) && !empty($value2->slug) ? base_url(COACHING_SLUG_PREPEND) . $value2->slug : 'javascript:void(0);' ?>"><?= isset($value2->coaching_page_name) && !empty($value2->coaching_page_name) ? strtoupper($value2->coaching_page_name) : '' ?></a></li>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </ul>
                                                </li>
                                                <li class="mega-menu-item <?= $this->page_id == 'VISA' || $this->page_id == 'SERVICE' ? 'active' : '' ?>">
                                                    <a href="<?= base_url('services') ?>" class="mega-menu-link">Services</a>
                                                    <ul class="mega-submenu">
                                                        <?php
                                                        if (isset($all_visa_page) && !empty($all_visa_page)) {
                                                            foreach ($all_visa_page as $key4 => $value4) {
                                                                ?>
                                                                <li class="<?= $this->menu_id == 'VISA_' . $value4->visa_page_id ? 'active' : '' ?>"><a href="<?= isset($value4->slug) && !empty($value4->slug) ? base_url(SERVICE_SLUG_PREPEND) . $value4->slug : 'javascript:void(0);' ?>"><?= isset($value4->visa_page_name) && !empty($value4->visa_page_name) ? strtoupper($value4->visa_page_name) : '' ?></a></li>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </ul>
                                                </li>
                                                <li class="mega-menu-item <?= $this->page_id == 'COUNTRY' ? 'active' : '' ?>">
                                                    <a href="javascript:void(0);" class="mega-menu-link">Countries</a>
                                                    <ul class="mega-submenu">
                                                        <?php
                                                        if (isset($all_country_page) && !empty($all_country_page)) {
                                                            foreach ($all_country_page as $key3 => $value3) {
                                                                if (isset($value3->sub_country) && !empty($value3->sub_country) && $value3->sub_country != NULL) {
                                                                    ?>
                                                                    <li class="mega-menu-item <?= $this->menu_id == 'COUNTRY_' . $value3->country_page_id ? 'active' : '' ?>">
                                                                        <a href="javascript:void(0);" class="mega-menu-link"><?= $value3->continent_name ?></a>
                                                                        <ul class="mega-submenu">
                                                                            <?php
                                                                            if (isset($value3->sub_country) && !empty($value3->sub_country)) {
                                                                                foreach ($value3->sub_country as $key9 => $value9) {
                                                                                    ?>
                                                                                    <li class="<?= $this->menu_id == 'COUNTRY_' . $value9->country_page_id ? 'active' : '' ?>"><a href="<?= isset($value9->slug) && !empty($value9->slug) ? base_url(COUNTRY_SLUG_PREPEND) . $value9->slug : 'javascript:void(0);' ?>"><?= isset($value9->country_page_name) && !empty($value9->country_page_name) ? strtoupper($value9->country_page_name) : '' ?></a></li>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </ul>
                                                                    </li>
                                                                    <?php
                                                                } else {
                                                                    ?>
                                                                    <li class="<?= $this->menu_id == 'COUNTRY_' . $value3->country_page_id ? 'active' : '' ?>"><a href="<?= isset($value3->slug) && !empty($value3->slug) ? base_url(COUNTRY_SLUG_PREPEND) . $value3->slug : 'javascript:void(0);' ?>"><?= isset($value3->country_page_name) && !empty($value3->country_page_name) ? strtoupper($value3->country_page_name) : '' ?></a></li>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </ul>
                                                </li>
                                                <li class="<?= $this->page_id == 'BLOG' ? 'active' : '' ?>">
                                                    <a href="<?= base_url('blog') ?>" class="">Blog</a>
                                                </li>
                                                <li class="<?= $this->page_id == 'FAQ' ? 'active' : '' ?>">
                                                    <a href="<?= base_url('faq') ?>" class="">FAQ</a>
                                                </li>
<!--                                                    <li class="<?php // echo $this->page_id == 'SERVICE' ? 'active' : ''                                                 ?>">
                                                    <a href="<?php // echo base_url('Service')                                                 ?>" class="">Services</a>
                                                </li>-->
                                                <li class="<?= $this->page_id == 'CONTACT' ? 'active' : '' ?>">
                                                    <a href="<?= base_url('contact') ?>" class="">Contact</a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div><!-- site-navigation end-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- site-header-menu end-->
        </header><!--header end-->

