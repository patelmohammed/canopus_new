<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="Canopus Global Education" />
        <meta name="description" content="Canopus Global Educatoion - Visa & Immigration Consultants" />
        <meta name="author" content="Canopus Global Education" />
        <meta name="viewport" content=" width=device-width, initial-scale=1, maximum-scale=1" />
        <!--        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
                <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
                <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">-->
        <!--<link rel="manifest" href="/site.webmanifest">-->
        <!--<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">-->
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        <title>Canopus Global Education</title>
        <!-- favicon icon -->
        <!--<link rel="shortcut icon" href="favicon.png" />-->
        <!-- bootstrap -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/bootstrap.min.css"/>
        <!-- animate -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/animate.css"/>
        <!-- fontawesome -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/font-awesome.css"/>
        <!-- themify -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/themify-icons.css"/>
        <!-- flaticon -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/flaticon.css"/>
        <!-- slick -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/slick.css">
        <!-- REVOLUTION LAYERS STYLES -->
        <link rel='stylesheet' id='rs-plugin-settings-css' href="<?= base_url() ?>assets/revolution/css/rs6.css"> 
        <!-- prettyphoto -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/prettyPhoto.css">
        <!-- shortcodes -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/shortcodes.css"/>
        <!-- main -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/main.css"/>
        <!-- main -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/megamenu.css"/>
        <!-- responsive -->
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/css/responsive.css"/>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

        <style>
            .tel a:hover {
                color: #fff;
            }

            .loader{
                position: relative;
                width: 120px;
                height: 120px;
            }

            .loader span{
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                transform: rotate(calc(18deg * var(--i)));
            }
            .loader span::before{
                content: '';
                position: absolute;
                top: 0;
                left: 0;
                width: 15px;
                height: 15px;
                background: red;
                border-radius: 50%;
                transform: scale(0);
                animation :animate 2s linear infinite;
                animation-delay: calc(0.1s * var(--i));
            }

            @keyframes animate
            {
                0%
                {
                    transform: scale(0);
                }
                10%
                {
                    transform: scale(1.2);
                }
                80%,100%
                {
                    transform: scale(0);

                }
            }
            .rocket{
                position: absolute;
                top: 0;
                left: 0;
                width: 120%;
                height: 120%;
                animation: rotating 2s linear infinite;
                animation-delay: -1s;
            }

            @keyframes rotating{
                0%
                {
                    transform: scale(10deg);
                }
                100%
                {
                    transform: scale(370deg);

                }
            }

            .rocket::before{
                content: '\f072';
                font-family: fontAwesome;
                position: absolute;
                top: 80px;
                left: 85px;
                color: #fff;
                font-size: 60px;
                transform: rotate(180deg);
            }
        </style>
    </head>
    <body>
        <div class="loader">
            <span style="--i:1;"></span>
            <span style="--i:2;"></span>
            <span style="--i:3;"></span>
            <span style="--i:4;"></span>
            <span style="--i:5;"></span>
            <span style="--i:6;"></span>
            <span style="--i:7;"></span>
            <span style="--i:8;"></span>
            <span style="--i:9;"></span>
            <span style="--i:10;"></span>
            <span style="--i:11;"></span>
            <span style="--i:12;"></span>
            <span style="--i:13;"></span>
            <span style="--i:14;"></span>
            <span style="--i:15;"></span>
            <span style="--i:16;"></span>
            <span style="--i:17;"></span>
            <span style="--i:18;"></span>
            <span style="--i:19;"></span>
            <span style="--i:20;"></span>
            <!--<span class="rocket"></span>-->
        </div>

        <!--page start-->
        <div class="page">
            <!--header start-->
            <header id="masthead" class="header cmt-header-style-01">
                <!-- top_bar -->
                <div class="top_bar cmt-bgcolor-darkgrey cmt-textcolor-white clearfix">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="d-flex flex-row align-items-center justify-content-center">
                                    <div class="top_bar_contact_item">
                                        <div class="top_bar_icon"><i class="ti ti-alarm-clock"></i></div>Mon - Sat 10.00 - 19.30
                                    </div>
                                    <div class="top_bar_contact_item">
                                        <div class="top_bar_icon"><i class="ti ti-location-pin"></i></div><?= isset($footer_data->header_address) && !empty($footer_data->header_address) ? $footer_data->header_address : '' ?>
                                    </div>
                                    <div class="top_bar_contact_item ml-auto">
                                        <div class="top_bar_icon"><i class="fa fa-envelope-o"></i></div><a href="mailto:<?= isset($footer_data->footer_email) && !empty($footer_data->footer_email) ? $footer_data->footer_email : '' ?>"><?= isset($footer_data->footer_email) && !empty($footer_data->footer_email) ? $footer_data->footer_email : '' ?></a>
                                    </div>
                                    <div class="top_bar_social">
                                        <ul class="social-icons">
                                            <li class="linkedin-icon"><a target="<?= isset($footer_data->footer_linkedin) && !empty($footer_data->footer_linkedin) && $footer_data->footer_linkedin != 'javascript:void(0);' ? '_blank' : '' ?>" href="<?= isset($footer_data->footer_linkedin) && !empty($footer_data->footer_linkedin) && $footer_data->footer_linkedin != 'javascript:void(0);' ? $footer_data->footer_linkedin : 'javascript:void(0);' ?>"><i class="ti ti-linkedin"></i></a>
                                            </li>
                                            <li class="instagram-icon"><a target="<?= isset($footer_data->footer_insta) && !empty($footer_data->footer_insta) && $footer_data->footer_insta != 'javascript:void(0);' ? '_blank' : '' ?>" href="<?= isset($footer_data->footer_insta) && !empty($footer_data->footer_insta) && $footer_data->footer_insta != 'javascript:void(0);' ? $footer_data->footer_insta : 'javascript:void(0);' ?>"><i class="ti ti-instagram"></i></a>
                                            </li>
                                            <li class="twitter-icon"><a target="<?= isset($footer_data->footer_twitter) && !empty($footer_data->footer_twitter) && $footer_data->footer_twitter != 'javascript:void(0);' ? '_blank' : '' ?>" href="<?= isset($footer_data->footer_twitter) && !empty($footer_data->footer_twitter) && $footer_data->footer_twitter != 'javascript:void(0);' ? $footer_data->footer_twitter : 'javascript:void(0);' ?>"><i class="ti ti-twitter-alt"></i></a>
                                            </li>
                                            <li class="facebook-icon"><a target="<?= isset($footer_data->footer_facebook) && !empty($footer_data->footer_facebook) && $footer_data->footer_facebook != 'javascript:void(0);' ? '_blank' : '' ?>" href="<?= isset($footer_data->footer_facebook) && !empty($footer_data->footer_facebook) && $footer_data->footer_facebook != 'javascript:void(0);' ? $footer_data->footer_facebook : 'javascript:void(0);' ?>"><i class="ti ti-facebook"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="cmt-bg cmt-col-bgcolor-yes cmt-right-span cmt-bgcolor-skincolor pl-20">
                                        <div class="cmt-col-wrapper-bg-layer cmt-bg-layer"></div>
                                        <div class="layer-content">
                                            <div class="top_bar_contact_item tel">
                                                <div class="top_bar_icon"><i class="fa fa-phone"> </i>
                                                </div>
                                                <a  href="tel:<?= isset($footer_data->footer_contact) && !empty($footer_data->footer_contact) ? str_replace(' ', '', $footer_data->footer_contact) : 'javascript:void(0);' ?>" target="_blank">
                                                    <?= isset($footer_data->footer_contact) && !empty($footer_data->footer_contact) ? $footer_data->footer_contact : '' ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- top_bar end-->
                <!-- site-header-menu -->
                <div id="site-header-menu" class="site-header-menu cmt-bgcolor-white">
                    <div class="site-header-menu-inner cmt-stickable-header">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12">
                                    <!--site-navigation -->
                                    <div class="site-navigation d-flex flex-row align-items-center justify-content-between">
                                        <!-- site-branding -->
                                        <div class="site-branding ">
                                            <a class="home-link" href="<?= base_url() ?>" title="Canopus" rel="home">
                                                <img id="logo-img" class="img-center" src="<?= base_url() ?>assets/images/logo-img.png" alt="logo-img">
                                            </a>
                                        </div><!-- site-branding end -->
                                        <!-- widget-info -->
                                        <div class="widget_info mr-auto">
                                            <div>Visa &amp; Immigration Consultants</div>
                                        </div>
                                        <div class="d-flex flex-row">
                                            <div class="btn-show-menu-mobile menubar menubar--squeeze">
                                                <span class="menubar-box">
                                                    <span class="menubar-inner"></span>
                                                </span>
                                            </div>
                                            <!-- menu -->
                                            <nav class="main-menu menu-mobile" id="menu">
                                                <ul class="menu">
                                                    <li class="mega-menu-item <?= $this->page_id == 'HOME' ? 'active' : '' ?>">
                                                        <a href="<?= base_url() ?>" class="mega-menu-link">Home</a>
                                                    </li>
                                                    <li class="mega-menu-item <?= $this->page_id == 'ABOUT' ? 'active' : '' ?>">
                                                        <a href="<?= base_url('About') ?>" class="mega-menu-link">About</a>
                                                    </li>
                                                    <li class="mega-menu-item <?= $this->page_id == 'E-LEARN' ? 'active' : '' ?>">
                                                        <a href="http://elearn.canopusedu.com/" class="mega-menu-link">E-Learn</a>
                                                    </li>
                                                    <li class="mega-menu-item <?= $this->page_id == 'COACHING' ? 'active' : '' ?>">
                                                        <a href="javascript:void(0);" class="mega-menu-link">Coaching</a>
                                                        <ul class="mega-submenu">
                                                            <?php
                                                            if (isset($all_page) && !empty($all_page)) {
                                                                foreach ($all_page as $key2 => $value2) {
                                                                    ?>
                                                                    <li class="<?= $this->menu_id == 'COACHING_' . $value2->coaching_page_id ? 'active' : '' ?>"><a href="<?= isset($value2->coaching_page_id) && !empty($value2->coaching_page_id) ? base_url('Coaching/') . $value2->coaching_page_id : 'javascript:void(0);' ?>"><?= isset($value2->coaching_page_name) && !empty($value2->coaching_page_name) ? strtoupper($value2->coaching_page_name) : '' ?></a></li>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </ul>
                                                    </li>
                                                    <li class="mega-menu-item <?= $this->page_id == 'VISA' ? 'active' : '' ?>">
                                                        <a href="javascript:void(0);" class="mega-menu-link">Visa</a>
                                                        <ul class="mega-submenu">
                                                            <?php
                                                            if (isset($all_visa_page) && !empty($all_visa_page)) {
                                                                foreach ($all_visa_page as $key4 => $value4) {
                                                                    ?>
                                                                    <li class="<?= $this->menu_id == 'VISA_' . $value4->visa_page_id ? 'active' : '' ?>"><a href="<?= isset($value4->visa_page_id) && !empty($value4->visa_page_id) ? base_url('Visa/') . $value4->visa_page_id : 'javascript:void(0);' ?>"><?= isset($value4->visa_page_name) && !empty($value4->visa_page_name) ? strtoupper($value4->visa_page_name) : '' ?></a></li>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </ul>
                                                    </li>
                                                    <li class="mega-menu-item <?= $this->page_id == 'COUNTRY' ? 'active' : '' ?>">
                                                        <a href="javascript:void(0);" class="mega-menu-link">Countries</a>
                                                        <ul class="mega-submenu">
                                                            <?php
                                                            if (isset($all_country_page) && !empty($all_country_page)) {
                                                                foreach ($all_country_page as $key3 => $value3) {
                                                                    if (isset($value3->continent_id) && !empty($value3->continent_id) && $value3->continent_id != NULL) {
                                                                        ?>
                                                                        <li class="mega-menu-item <?= $this->menu_id == 'COUNTRY_' . $value3->country_page_id ? 'active' : '' ?>">
                                                                            <a href="javascript:void(0);" class="mega-menu-link"><?= getContinentNameById($value3->continent_id) ?></a>
                                                                            <ul class="mega-submenu">
                                                                                <li class="<?= $this->menu_id == 'COUNTRY_' . $value3->country_page_id ? 'active' : '' ?>"><a href="<?= isset($value3->country_page_id) && !empty($value3->country_page_id) ? base_url('Country/') . $value3->country_page_id : 'javascript:void(0);' ?>"><?= isset($value3->country_page_name) && !empty($value3->country_page_name) ? strtoupper($value3->country_page_name) : '' ?></a></li>
                                                                            </ul>
                                                                        </li>
                                                                        <?php
                                                                    } else {
                                                                        ?>
                                                                        <li class="<?= $this->menu_id == 'COUNTRY_' . $value3->country_page_id ? 'active' : '' ?>"><a href="<?= isset($value3->country_page_id) && !empty($value3->country_page_id) ? base_url('Country/') . $value3->country_page_id : 'javascript:void(0);' ?>"><?= isset($value3->country_page_name) && !empty($value3->country_page_name) ? strtoupper($value3->country_page_name) : '' ?></a></li>
                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </ul>
                                                    </li>
                                                    <li class="mega-menu-item <?= $this->page_id == 'SERVICE' ? 'active' : '' ?>">
                                                        <a href="<?= base_url('Service') ?>" class="mega-menu-link">Services</a>
                                                    </li>
                                                    <li class="mega-menu-item <?= $this->page_id == 'CONTACT' ? 'active' : '' ?>">
                                                        <a href="<?= base_url('Contact') ?>" class="mega-menu-link">Contact</a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div><!-- site-navigation end-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- site-header-menu end-->
            </header><!--header end-->
