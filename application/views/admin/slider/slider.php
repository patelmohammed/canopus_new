<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Slider
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Slider Title</th>
                                    <th>Slider Description</th>
                                    <th>Slider Image</th>
                                    <th>Order Number</th>
                                    <th>Active / Deactive</th>
                                    <th>Added By</th>
                                    <th class="notexport no-sort">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($slider_data) && !empty($slider_data)) {
                                    $sn = 0;
                                    foreach ($slider_data as $key => $value) {
                                        $sn++;
                                        ?>                       
                                        <tr> 
                                            <td><?= $sn ?></td>
                                            <td><?= isset($value->slider_title) && !empty($value->slider_title) ? $value->slider_title : '' ?></td>
                                            <td><?= isset($value->slider_description) && !empty($value->slider_description) ? $value->slider_description : '' ?></td>
                                            <td><img src="<?= base_url() . $value->slider_image ?>" height="100px" width="250px"></td> 
                                            <td><?= $value->slider_order_no ?></td> 
                                            <td style="text-align: center;">
                                                <?php if ($value->is_active == 1) { ?>
                                                    <button type="button" class="btn btn-xs btn-success waves-effect waves-themed deactivate_slider" data-id="<?= $value->slider_id ?>">Active</button>
                                                <?php } else { ?>
                                                    <button type="button" class="btn btn-xs btn-danger waves-effect waves-themed activate_slider" data-id="<?= $value->slider_id ?>">Deactivated</button>
                                                <?php } ?>
                                            </td>
                                            <td><?= isset($value->InsUser) && !empty($value->InsUser) ? getUserNameById($value->InsUser) : '' ?></td>   
                                            <td>
                                                <div class='d-flex'>
                                                    <?php if ($menu_rights['edit_right']) { ?>
                                                        <a href='<?php echo base_url() ?>admin/Slider/addEditSlider/<?= $value->slider_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                            <i class="fal fa-edit"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    $(document).on('click', '.deactivate_slider', function () {
        var id = $(this).data('id');
        $.ajax({
            type: "POST",
            url: '<?= base_url('admin/Slider/deactiveSlider') ?>',
            data: {slider_id: id},
            success: function (returnData) {
                var data = JSON.parse(returnData);
                if (data.result == true) {
                    window.location.reload();
                } else {
                    swalWithBootstrapButtons.fire("Something Wrong", data.message, "error");
                    return false;
                }
            }
        });
    });

    $(document).on('click', '.activate_slider', function () {
        var id = $(this).data('id');
        $.ajax({
            type: "POST",
            url: '<?= base_url('admin/Slider/activeSlider') ?>',
            data: {slider_id: id},
            success: function (returnData) {
                var data = JSON.parse(returnData);
                if (data.result == true) {
                    window.location.reload();
                }
            }
        });
    });
</script>