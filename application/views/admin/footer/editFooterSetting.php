<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Footer Setting
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Footer', $arrayName = array('id' => 'addEditFooterSetting', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="description_title">Description Title <span class="text-danger">*</span></label>
                                <textarea rows="2" name="description_title" id="description_title" class="form-control" required=""><?= isset($footer_data->description_title) && !empty($footer_data->description_title) ? $footer_data->description_title : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Description Title Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="description">Description<span class="text-danger">*</span></label>
                                <textarea rows="2" name="description" id="description" class="form-control" required=""><?= isset($footer_data->description) && !empty($footer_data->description) ? $footer_data->description : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Description Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="sun_time">Sunday Time <span class="text-danger">*</span></label>
                                <textarea rows="1" name="sun_time" id="sun_time" class="form-control" required=""><?= isset($footer_data->sun_time) && !empty($footer_data->sun_time) ? $footer_data->sun_time : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Sunday Time Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="mon_time">Monday Time<span class="text-danger">*</span></label>
                                <textarea rows="1" name="mon_time" id="mon_time" class="form-control" required=""><?= isset($footer_data->mon_time) && !empty($footer_data->mon_time) ? $footer_data->mon_time : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Mon - Thu Time Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="tue_time">Tuesday Time <span class="text-danger">*</span></label>
                                <textarea rows="1" name="tue_time" id="tue_time" class="form-control" required=""><?= isset($footer_data->tue_time) && !empty($footer_data->tue_time) ? $footer_data->tue_time : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Sunday Time Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="wed_time">Wednesday Time <span class="text-danger">*</span></label>
                                <textarea rows="1" name="wed_time" id="wed_time" class="form-control" required=""><?= isset($footer_data->wed_time) && !empty($footer_data->wed_time) ? $footer_data->wed_time : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Mon - Thu Time Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="thu_time">Thursday Time <span class="text-danger">*</span></label>
                                <textarea rows="1" name="thu_time" id="thu_time" class="form-control" required=""><?= isset($footer_data->thu_time) && !empty($footer_data->thu_time) ? $footer_data->thu_time : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Fri - Sat Time Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="fri_time">Friday Time <span class="text-danger">*</span></label>
                                <textarea rows="1" name="fri_time" id="fri_time" class="form-control" required=""><?= isset($footer_data->fri_time) && !empty($footer_data->fri_time) ? $footer_data->fri_time : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Fri - Sat Time Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="sat_time">Saturday Time <span class="text-danger">*</span></label>
                                <textarea rows="1" name="sat_time" id="sat_time" class="form-control" required=""><?= isset($footer_data->sat_time) && !empty($footer_data->sat_time) ? $footer_data->sat_time : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Fri - Sat Time Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="footer_contact">Contact Number <span class="text-danger">*</span></label>
                                <textarea rows="1" name="footer_contact" id="footer_contact" class="form-control contactnumber" required=""><?= isset($footer_data->footer_contact) && !empty($footer_data->footer_contact) ? $footer_data->footer_contact : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Contact Number Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="footer_address">Footer Address <span class="text-danger">*</span></label>
                                <textarea name="footer_address" id="footer_address" rows="1" maxlength="200" class="form-control address" required=""><?= isset($footer_data->footer_address) && !empty($footer_data->footer_address) ? $footer_data->footer_address : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Footer Address Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="header_address">Header Address <span class="text-danger">*</span></label>
                                <textarea name="header_address" id="header_address" rows="1" maxlength="200" class="form-control address" required=""><?= isset($footer_data->header_address) && !empty($footer_data->header_address) ? $footer_data->header_address : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Header Address Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="footer_email">Email <span class="text-danger">*</span></label>
                                <input type="email" name="footer_email" id="footer_email" class="form-control" value="<?= isset($footer_data->footer_email) && !empty($footer_data->footer_email) ? $footer_data->footer_email : '' ?>" required="">
                                <div class="invalid-feedback">
                                    Email Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="footer_facebook">Facebook</label>
                                <input type="text" name="footer_facebook" id="footer_facebook" class="form-control" value="<?= isset($footer_data->footer_facebook) && !empty($footer_data->footer_facebook) && $footer_data->footer_facebook != 'javascript:void(0);' ? $footer_data->footer_facebook : '' ?>">
                                <div class="invalid-feedback">
                                    Facebook Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="footer_twitter">Twitter</label>
                                <input type="text" name="footer_twitter" id="footer_twitter" class="form-control" value="<?= isset($footer_data->footer_twitter) && !empty($footer_data->footer_twitter) && $footer_data->footer_twitter != 'javascript:void(0);' ? $footer_data->footer_twitter : '' ?>">
                                <div class="invalid-feedback">
                                    Twitter Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="footer_insta">Instagram</label>
                                <input type="text" name="footer_insta" id="footer_insta" class="form-control" value="<?= isset($footer_data->footer_insta) && !empty($footer_data->footer_insta) && $footer_data->footer_insta != 'javascript:void(0);' ? $footer_data->footer_insta : '' ?>">
                                <div class="invalid-feedback">
                                    Instagram Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="footer_google">Google</label>
                                <input type="text" name="footer_google" id="footer_google" class="form-control" value="<?= isset($footer_data->footer_google) && !empty($footer_data->footer_google) && $footer_data->footer_google != 'javascript:void(0);' ? $footer_data->footer_google : '' ?>">
                                <div class="invalid-feedback">
                                    Google Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="footer_linkedin">Linkedin</label>
                                <input type="text" name="footer_linkedin" id="footer_linkedin" class="form-control" value="<?= isset($footer_data->footer_linkedin) && !empty($footer_data->footer_linkedin) && $footer_data->footer_linkedin != 'javascript:void(0);' ? $footer_data->footer_linkedin : '' ?>">
                                <div class="invalid-feedback">
                                    Linkedin Required
                                </div>
                            </div>
                            <?php if ($this->user_id == 1) { ?>
                                <div class="col-md-6 mb-3">
                                    <label class="form-label">Logo <i class="text-danger">(File in PNG) File Size 1024x683px</i></label>
                                    <div class="custom-file">
                                        <input type="file" name="logo" class="custom-file-input" id="logo" <?= isset($footer_data->logo_image) && !empty($footer_data->logo_image) ? '' : 'required=""' ?>>
                                        <label class="custom-file-label" for="logo">Choose file</label>
                                    </div>
                                    <img src="<?= base_url() . (isset($footer_data->logo_image) && !empty($footer_data->logo_image) ? $footer_data->logo_image : '') ?>" class="m-2" style="width: 20%;">
                                    <input type="hidden" name="hidden_logo" id="hidden_logo" value="<?= isset($footer_data->logo_image) && !empty($footer_data->logo_image) ? $footer_data->logo_image : '' ?>">
                                </div>
                            <?php } ?>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Register Popup Image <i class="text-danger">(File in PNG) File Size 400x550px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="register_popup_image" class="custom-file-input" id="register_popup_image" <?= isset($footer_data->register_popup_image) && !empty($footer_data->register_popup_image) ? '' : 'required=""' ?>>
                                    <label class="custom-file-label" for="register_popup_image">Choose file</label>
                                </div>
                                <img src="<?= base_url() . (isset($footer_data->register_popup_image) && !empty($footer_data->register_popup_image) ? $footer_data->register_popup_image : '') ?>" class="m-2" style="width: 10%;">
                                <input type="hidden" name="hidden_register_popup_image" id="hidden_register_popup_image" value="<?= isset($footer_data->register_popup_image) && !empty($footer_data->register_popup_image) ? $footer_data->register_popup_image : '' ?>">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="register_popup_active">&nbsp;</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" name="register_popup_active" class="custom-control-input" id="register_popup_active" <?= isset($footer_data->register_popup_active) && !empty($footer_data->register_popup_active) && $footer_data->register_popup_active == 1 ? 'checked' : '' ?>>
                                    <label class="custom-control-label" for="register_popup_active">Register Popup Active</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_title">Meta Title <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="meta_title" id="meta_title" placeholder="Meta Title" required value="<?= isset($footer_data->meta_title) && !empty($footer_data->meta_title) ? $footer_data->meta_title : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_desc">Meta Description <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="meta_desc" id="meta_desc" placeholder="Meta Description" required value="<?= isset($footer_data->meta_desc) && !empty($footer_data->meta_desc) ? $footer_data->meta_desc : '' ?>">
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_key">Meta Key <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="meta_key" id="meta_key" placeholder="Meta Key" required value="<?= isset($footer_data->meta_key) && !empty($footer_data->meta_key) ? $footer_data->meta_key : '' ?>">
                                <span></span>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#addEditFooterSetting').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                slug: {
                    remote: {
                        url: "<?= base_url('admin/Home/checkSlug/') ?>" + $('#hidden_slug_id').val(),
                        type: "get"
                    }
                }
            },
            messages: {
                slug: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>