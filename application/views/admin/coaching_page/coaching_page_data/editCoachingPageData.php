<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-file-code'></i> Edit Coaching Page
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Coaching_page">Coaching Page</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Coaching_page/addEditCoachingPageData/' . $encrypted_id, $arrayName = array('id' => 'addEditCoachingPageData', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="coaching_page_name">Coaching Page Name <span class="text-danger">*</span></label>
                                <select class="select2 form-control" name="ref_coaching_page_id" id="ref_coaching_page_id" required="">
                                    <option></option>
                                    <?php
                                    if (isset($coaching_page) && !empty($coaching_page)) {
                                        foreach ($coaching_page as $k1 => $v1) {
                                            ?>
                                            <option value="<?= isset($v1->coaching_page_id) && !empty($v1->coaching_page_id) ? $v1->coaching_page_id : '' ?>" <?= isset($coaching_page_data->ref_coaching_page_id) && !empty($coaching_page_data->ref_coaching_page_id) ? set_selected($coaching_page_data->ref_coaching_page_id, $v1->coaching_page_id) : '' ?>><?= isset($v1->coaching_page_name) && !empty($v1->coaching_page_name) ? $v1->coaching_page_name : '' ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <div class="invalid-feedback">
                                    Coaching Page Name Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="coaching_item_page_title">Title <span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input tabindex="2" type="text" class="form-control textonly" name="coaching_item_page_title" id="coaching_item_page_title" placeholder="Title" required value="<?= isset($coaching_page_data->coaching_item_page_title) && !empty($coaching_page_data->coaching_item_page_title) ? $coaching_page_data->coaching_item_page_title : '' ?>">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <div class="custom-control d-flex custom-switch">
                                                <input id="is_active" name="is_active" type="checkbox" class="custom-control-input" checked>
                                                <label class="custom-control-label fw-500" for="is_active"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="invalid-feedback">
                                    Title Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="coaching_item_page_desc">Description <span class="text-danger">*</span></label>
                                <textarea class="form-control" id="coaching_item_page_desc" name="coaching_item_page_desc"><?= isset($coaching_page_data->coaching_item_page_desc) && !empty($coaching_page_data->coaching_item_page_desc) ? $coaching_page_data->coaching_item_page_desc : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Description Required
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        CKEDITOR.replace('coaching_item_page_desc', {
            enterMode: CKEDITOR.ENTER_BR,
            toolbar: [
                {name: 'items', items: ["Source", "-", "Save", "NewPage", "DocProps", "Preview", "Print", "Templates", "-", "document"]},
                {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                {name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']},
                '/',
                {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', "Subscript", "Superscript", '-', 'RemoveFormat']},
                {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']},

                {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
                {name: 'insert', items: ['Table', 'HorizontalRule', 'PageBreak']},
                '/',
                {name: 'styles', items: ['Styles', 'Format', "FontSize"]},
                {name: 'color', items: ["TextColor", "BGColor"]},
                {name: 'tools', items: ['Maximize', '-', 'ShowBlocks']},
                {name: 'info', items: ['About']},
                {name: 'tokens', items: ['tokens']}
            ],
            removeDialogTabs: 'link:target;image:Link;link:upload;image:Upload;image:advanced;link:advanced',
            removePlugins: 'flashupload'
        });
        $("#ref_coaching_page_id").select2({
            placeholder: "Select page",
            allowClear: true,
            width: '100%'
        });
        $('#addEditCoachingPageData').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>