<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-file-code'></i> Edit Coaching Page
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Coaching_page">Coaching Page</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Coaching_page/addEditCoachingPage/' . $encrypted_id, $arrayName = array('id' => 'addEditCoachingPage', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="coaching_page_name">Coaching Page Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="coaching_page_name" id="coaching_page_name" placeholder="Coaching Page Name" required value="<?= isset($coaching_page_data->coaching_page_name) && !empty($coaching_page_data->coaching_page_name) ? $coaching_page_data->coaching_page_name : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="is_active">&nbsp;</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" name="is_active" class="custom-control-input" id="is_active" <?= isset($coaching_page_data->is_active) && !empty($coaching_page_data->is_active) ? set_checked($coaching_page_data->is_active, 1) : '' ?>>
                                    <label class="custom-control-label" for="is_active">Page Active</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="coaching_page_desc">Coaching Page Description <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="coaching_page_desc" id="coaching_page_desc" placeholder="Coaching Page Description" required value="<?= isset($coaching_page_data->coaching_page_desc) && !empty($coaching_page_data->coaching_page_desc) ? $coaching_page_data->coaching_page_desc : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="icon">Home Page Icon <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="icon" id="icon" placeholder="Home Page Icon" required value="<?= isset($coaching_page_data->icon) && !empty($coaching_page_data->icon) ? $coaching_page_data->icon : '' ?>">
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Image <i class="text-danger">(File in JPG,PNG) File Size 1020x520px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="coaching_page_image" class="custom-file-input" id="coaching_page_image">
                                    <label class="custom-file-label" for="coaching_page_image">Choose file</label>
                                </div>
                                <img class="mt-3" src="<?= isset($coaching_page_data->coaching_page_image) && !empty($coaching_page_data->coaching_page_image) && file_exists($coaching_page_data->coaching_page_image) ? base_url() . $coaching_page_data->coaching_page_image : base_url('assets/admin/img/no_image.jpg') ?>" height="100px" alt="no preview available">
                                <input type="hidden" class="form-control" value="<?= isset($coaching_page_data->coaching_page_image) && !empty($coaching_page_data->coaching_page_image) ? $coaching_page_data->coaching_page_image : '' ?>" name="hidden_coaching_page_image"/>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Page Background Image <i class="text-danger">(File in JPG,PNG) File Size 1920x390px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="background_image" class="custom-file-input" id="background_image">
                                    <label class="custom-file-label" for="background_image">Choose file</label>
                                </div>
                                <img class="mt-3" src="<?= base_url() . (isset($coaching_page_data->background_image) && !empty($coaching_page_data->background_image) ? $coaching_page_data->background_image : 'assets/images/pagetitle-bg.jpg') ?>" height="100px" alt="no preview available">
                                <input type="hidden" name="hidden_background_image" id="hidden_background_image" value="<?= isset($coaching_page_data->background_image) && !empty($coaching_page_data->background_image) ? $coaching_page_data->background_image : 'assets/images/pagetitle-bg.jpg' ?>">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_title">Meta Title <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="meta_title" id="meta_title" placeholder="Meta Title" required value="<?= isset($coaching_page_data->meta_title) && !empty($coaching_page_data->meta_title) ? $coaching_page_data->meta_title : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_desc">Meta Description <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="meta_desc" id="meta_desc" placeholder="Meta Description" required value="<?= isset($coaching_page_data->meta_desc) && !empty($coaching_page_data->meta_desc) ? $coaching_page_data->meta_desc : '' ?>">
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_key">Meta Key <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="meta_key" id="meta_key" placeholder="Meta Key" required value="<?= isset($coaching_page_data->meta_key) && !empty($coaching_page_data->meta_key) ? $coaching_page_data->meta_key : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="slug">Slug <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="slug" id="slug" placeholder="Slug" required value="<?= isset($slug_data->slug) && !empty($slug_data->slug) ? $slug_data->slug : '' ?>">
                                <span></span>
                            </div>
                            <input type="hidden" name="hidden_slug_id" id="hidden_slug_id" value="<?= isset($slug_data->slug_id) && !empty($slug_data->slug_id) ? $slug_data->slug_id : '' ?>">
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var id = '<?= isset($coaching_page_data->coaching_page_id) && !empty($coaching_page_data->coaching_page_id) ? $coaching_page_data->coaching_page_id : '' ?>';
    $(document).ready(function () {
        $('#addEditCoachingPage').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                coaching_page_name: {
                    remote: {
                        url: "<?= base_url('admin/Coaching_page/checkCoachingPage/') ?>" + id,
                        type: "get"
                    }
                },
                slug: {
                    remote: {
                        url: "<?= base_url('admin/Home/checkSlug/') ?>" + $('#hidden_slug_id').val(),
                        type: "get"
                    }
                }
            },
            messages: {
                coaching_page_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                },
                slug: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.next());
                element.next().next().addClass('text-danger');
            }
        });
    });
</script>