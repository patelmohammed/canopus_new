<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-file-code'></i> Coaching Page
        </h1>
        <div class="d-flex mr-0">
            <?php if ($menu_rights['add_right']) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Coaching_page/addEditCoachingPage">Add Coaching Page</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Page Title</th>
                                    <th>Page Description</th>
                                    <th>Page Image</th>
                                    <th>Active / Deactive</th>
                                    <th>Show On Home Page</th>
                                    <th>Added By</th>
                                    <th class="notexport no-sort">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($coaching_page_data) && !empty($coaching_page_data)) {
                                    $sn = 0;
                                    foreach ($coaching_page_data as $key => $value) {
                                        $sn++;
                                        ?>                       
                                        <tr> 
                                            <td><?= $sn ?></td>
                                            <td><?= isset($value->coaching_page_name) && !empty($value->coaching_page_name) ? $value->coaching_page_name : '' ?></td>
                                            <td><?= $value->coaching_page_desc ?></td> 
                                            <td><img src="<?= base_url() . $value->coaching_page_image ?>" height="100px" width="150px"></td> 
                                            <td style="text-align: center;">
                                                <?php if ($value->is_active == 1) { ?>
                                                    <button type="button" class="btn btn-xs btn-success waves-effect waves-themed deactivate_slider" data-id="<?= $value->coaching_page_id ?>">Active</button>
                                                <?php } else { ?>
                                                    <button type="button" class="btn btn-xs btn-danger waves-effect waves-themed activate_slider" data-id="<?= $value->coaching_page_id ?>">Deactivated</button>
                                                <?php } ?>
                                            </td>
                                            <td style="text-align: center;">
                                                <?php if ($value->is_show_home_page == 1) { ?>
                                                    <button type="button" class="btn btn-xs btn-success waves-effect waves-themed show_home_page" data-id="<?= $value->coaching_page_id ?>" data-is_show_home_page="<?= $value->is_show_home_page ?>">Yes</button>
                                                <?php } else { ?>
                                                    <button type="button" class="btn btn-xs btn-danger waves-effect waves-themed show_home_page" data-id="<?= $value->coaching_page_id ?>" data-is_show_home_page="<?= $value->is_show_home_page ?>">No</button>
                                                <?php } ?>
                                            </td>
                                            <td><?= isset($value->InsUser) && !empty($value->InsUser) ? getUserNameById($value->InsUser) : '' ?></td>   
                                            <td>
                                                <div class='d-flex'>
                                                    <?php if ($menu_rights['edit_right']) { ?>
                                                        <a href='<?php echo base_url() ?>admin/Coaching_page/addEditCoachingPage/<?= $value->coaching_page_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                            <i class="fal fa-edit"></i>
                                                        </a>
                                                    <?php } ?>
                                                    <?php if ($menu_rights['delete_right']) { ?>
                                                        <a href='javascript:void(0);' data-url="<?= base_url('admin/Coaching_page/deleteCoachingPage') ?>" data-id="<?= $value->coaching_page_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2 delete_record' title='Delete Record' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'>
                                                            <i class="fal fa-times"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    $(document).on('click', '.deactivate_slider', function () {
        var id = $(this).data('id');
        $.ajax({
            type: "POST",
            url: '<?= base_url('admin/Coaching_page/deactiveCoachingPage') ?>',
            data: {coaching_page_id: id},
            success: function (returnData) {
                var data = JSON.parse(returnData);
                if (data.result == true) {
                    window.location.reload();
                }
            }
        });
    });

    $(document).on('click', '.activate_slider', function () {
        var id = $(this).data('id');
        $.ajax({
            type: "POST",
            url: '<?= base_url('admin/Coaching_page/activeCoachingPage') ?>',
            data: {coaching_page_id: id},
            success: function (returnData) {
                var data = JSON.parse(returnData);
                if (data.result == true) {
                    window.location.reload();
                }
            }
        });
    });

    $(document).on('click', '.show_home_page', function () {
        var id = $(this).data('id');
        var is_show_home_page = $(this).data('is_show_home_page');
        $.ajax({
            type: "POST",
            url: '<?= base_url('admin/Coaching_page/showCoachingHomePage') ?>',
            data: {id: id, is_show_home_page: is_show_home_page},
            success: function (returnData) {
                var data = JSON.parse(returnData);
                if (data.result == true) {
                    window.location.reload();
                } else {
                    swalWithBootstrapButtons.fire("Something Wrong", data.message, "error");
                    return false;
                }
            },
            error: function () {
                swalWithBootstrapButtons.fire("Something Wrong", "Your record not updated :(", "error");
                return false;
            }
        });
    });
</script>