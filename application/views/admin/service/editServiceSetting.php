<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Service Setting
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Service/addEditServiceSetting', $arrayName = array('id' => 'addEditServiceSetting', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="visa_section_title">Visa Section Title <span class="text-danger">*</span></label>
                                <input type="text" name="visa_section_title" id="visa_section_title" class="form-control" required="" placeholder="Visa Section Title" value="<?= isset($service_setting_data->visa_section_title) && !empty($service_setting_data->visa_section_title) ? $service_setting_data->visa_section_title : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="visa_section_desc">Visa Section Description<span class="text-danger">*</span></label>
                                <textarea rows="1" name="visa_section_desc" id="visa_section_desc" class="form-control" required="" placeholder="Visa Section Description"><?= isset($service_setting_data->visa_section_desc) && !empty($service_setting_data->visa_section_desc) ? $service_setting_data->visa_section_desc : '' ?></textarea>
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="setting_title">Service Setting Title <span class="text-danger">*</span></label>
                                <input type="text" name="setting_title" id="setting_title" class="form-control" required="" placeholder="Service Setting Title" value="<?= isset($service_setting_data->setting_title) && !empty($service_setting_data->setting_title) ? $service_setting_data->setting_title : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="setting_desc">Service Setting Description<span class="text-danger">*</span></label>
                                <textarea rows="1" name="setting_desc" id="setting_desc" class="form-control" required="" placeholder="Service Setting Description"><?= isset($service_setting_data->setting_desc) && !empty($service_setting_data->setting_desc) ? $service_setting_data->setting_desc : '' ?></textarea>
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-2 mb-3">
                                <label class="form-label" for="sub_setting_icon_1">Sub Setting Icon <span class="text-danger">*</span></label>
                                <input type="text" name="sub_setting_icon_1" id="sub_setting_icon_1" class="form-control" required="" placeholder="Sub Setting Icon" value="<?= isset($service_setting_data->sub_setting_icon_1) && !empty($service_setting_data->sub_setting_icon_1) ? $service_setting_data->sub_setting_icon_1 : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label class="form-label" for="sub_setting_title_1">Sub Setting Title <span class="text-danger">*</span></label>
                                <input type="text" name="sub_setting_title_1" id="sub_setting_title_1" class="form-control" required="" placeholder="Sub Setting Title" value="<?= isset($service_setting_data->sub_setting_title_1) && !empty($service_setting_data->sub_setting_title_1) ? $service_setting_data->sub_setting_title_1 : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="sub_setting_desc_1">Sub Setting Description<span class="text-danger">*</span></label>
                                <textarea rows="1" name="sub_setting_desc_1" id="sub_setting_desc_1" class="form-control" required="" placeholder="Sub Setting Description"><?= isset($service_setting_data->sub_setting_desc_1) && !empty($service_setting_data->sub_setting_desc_1) ? $service_setting_data->sub_setting_desc_1 : '' ?></textarea>
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-2 mb-3">
                                <label class="form-label" for="sub_setting_icon_2">Sub Setting Icon <span class="text-danger">*</span></label>
                                <input type="text" name="sub_setting_icon_2" id="sub_setting_icon_2" class="form-control" required="" placeholder="Sub Setting Icon" value="<?= isset($service_setting_data->sub_setting_icon_2) && !empty($service_setting_data->sub_setting_icon_2) ? $service_setting_data->sub_setting_icon_2 : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label class="form-label" for="sub_setting_title_2">Sub Setting Title <span class="text-danger">*</span></label>
                                <input type="text" name="sub_setting_title_2" id="sub_setting_title_2" class="form-control" required="" placeholder="Sub Setting Title" value="<?= isset($service_setting_data->sub_setting_title_2) && !empty($service_setting_data->sub_setting_title_2) ? $service_setting_data->sub_setting_title_2 : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="sub_setting_desc_2">Sub Setting Description<span class="text-danger">*</span></label>
                                <textarea rows="1" name="sub_setting_desc_2" id="sub_setting_desc_2" class="form-control" required="" placeholder="Sub Setting Description"><?= isset($service_setting_data->sub_setting_desc_2) && !empty($service_setting_data->sub_setting_desc_2) ? $service_setting_data->sub_setting_desc_2 : '' ?></textarea>
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-2 mb-3">
                                <label class="form-label" for="sub_setting_icon_3">Sub Setting Icon <span class="text-danger">*</span></label>
                                <input type="text" name="sub_setting_icon_3" id="sub_setting_icon_3" class="form-control" required="" placeholder="Sub Setting Icon" value="<?= isset($service_setting_data->sub_setting_icon_3) && !empty($service_setting_data->sub_setting_icon_3) ? $service_setting_data->sub_setting_icon_3 : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label class="form-label" for="sub_setting_title_3">Sub Setting Title <span class="text-danger">*</span></label>
                                <input type="text" name="sub_setting_title_3" id="sub_setting_title_3" class="form-control" required="" placeholder="Sub Setting Title" value="<?= isset($service_setting_data->sub_setting_title_3) && !empty($service_setting_data->sub_setting_title_3) ? $service_setting_data->sub_setting_title_3 : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="sub_setting_desc_3">Sub Setting Description<span class="text-danger">*</span></label>
                                <textarea rows="1" name="sub_setting_desc_3" id="sub_setting_desc_3" class="form-control" required="" placeholder="Sub Setting Description"><?= isset($service_setting_data->sub_setting_desc_3) && !empty($service_setting_data->sub_setting_desc_3) ? $service_setting_data->sub_setting_desc_3 : '' ?></textarea>
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-2 mb-3">
                                <label class="form-label" for="sub_setting_icon_4">Sub Setting Icon <span class="text-danger">*</span></label>
                                <input type="text" name="sub_setting_icon_4" id="sub_setting_icon_4" class="form-control" required="" placeholder="Sub Setting Icon" value="<?= isset($service_setting_data->sub_setting_icon_4) && !empty($service_setting_data->sub_setting_icon_4) ? $service_setting_data->sub_setting_icon_4 : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label class="form-label" for="sub_setting_title_4">Sub Setting Title <span class="text-danger">*</span></label>
                                <input type="text" name="sub_setting_title_4" id="sub_setting_title_4" class="form-control" required="" placeholder="Sub Setting Title" value="<?= isset($service_setting_data->sub_setting_title_4) && !empty($service_setting_data->sub_setting_title_4) ? $service_setting_data->sub_setting_title_4 : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="sub_setting_desc_4">Sub Setting Description<span class="text-danger">*</span></label>
                                <textarea rows="1" name="sub_setting_desc_4" id="sub_setting_desc_4" class="form-control" required="" placeholder="Sub Setting Description"><?= isset($service_setting_data->sub_setting_desc_4) && !empty($service_setting_data->sub_setting_desc_4) ? $service_setting_data->sub_setting_desc_4 : '' ?></textarea>
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Page Background Image <i class="text-danger">(File in JPG,PNG) File Size 1920x390px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="background_image" class="custom-file-input" id="background_image">
                                    <label class="custom-file-label" for="background_image">Choose file</label>
                                </div>
                                <img class="mt-3" src="<?= base_url() . (isset($service_setting_data->background_image) && !empty($service_setting_data->background_image) ? $service_setting_data->background_image : 'assets/images/pagetitle-bg.jpg') ?>" height="100px" alt="no preview available">
                                <input type="hidden" name="hidden_background_image" id="hidden_background_image" value="<?= isset($service_setting_data->background_image) && !empty($service_setting_data->background_image) ? $service_setting_data->background_image : 'assets/images/pagetitle-bg.jpg' ?>">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_title">Meta Title <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="meta_title" id="meta_title" placeholder="Meta Title" required value="<?= isset($service_setting_data->meta_title) && !empty($service_setting_data->meta_title) ? $service_setting_data->meta_title : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_desc">Meta Description <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="meta_desc" id="meta_desc" placeholder="Meta Description" required value="<?= isset($service_setting_data->meta_desc) && !empty($service_setting_data->meta_desc) ? $service_setting_data->meta_desc : '' ?>">
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_key">Meta Key <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="meta_key" id="meta_key" placeholder="Meta Key" required value="<?= isset($service_setting_data->meta_key) && !empty($service_setting_data->meta_key) ? $service_setting_data->meta_key : '' ?>">
                                <span></span>
                            </div>
                        </div>
                        <hr style="border-bottom: 1px dashed #886ab5;">
                        <div id="service_item_div">
                            <?php
                            if (isset($service_setting_data->service_item_data) && !empty($service_setting_data->service_item_data)) {
                                foreach ($service_setting_data->service_item_data as $key => $value) {
                                    $key++;
                                    ?>
                                    <div class="form-row about_item_row" id="row_<?= $key ?>">
                                        <div class="col-md-6 mb-3">
                                            <label class="form-label" for="service_item_title_<?= $key ?>">About Us Item Title <span class="text-danger">*</span></label>
                                            <div class="input-group">
                                                <input tabindex="2" type="text" class="form-control textonly" name="service_item_title[<?= $key ?>]" id="service_item_title_<?= $key ?>" placeholder="About Us Title" required value="<?= isset($value->service_item_title) && !empty($value->service_item_title) ? $value->service_item_title : '' ?>">
                                                <div class="input-group-text">
                                                    <div class="custom-control d-flex custom-switch">
                                                        <input id="is_active_service_item_<?= $key ?>" name="is_active_service_item[<?= $key ?>]" type="checkbox" class="custom-control-input" <?= isset($value->is_active) && !empty($value->is_active) ? (set_checked($value->is_active, 1)) : '' ?>>
                                                        <label class="custom-control-label fw-500" for="is_active_service_item_<?= $key ?>"></label>
                                                    </div>
                                                </div>
                                                <div class="input-group-append">
                                                    <?php if ($key == 1) { ?>
                                                        <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="service_item()" title="Add" data-toggle="tooltip">
                                                            <i class="fal fa-plus"></i>
                                                        </a>
                                                    <?php } else { ?>
                                                        <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_service_item" title="Delete" data-toggle="tooltip" data-id="<?= $key ?>">
                                                            <i class="fal fa-minus"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <span></span>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label class="form-label" for="service_item_desc_<?= $key ?>">About Us Item Description <span class="text-danger">*</span></label>
                                            <textarea class="form-control textonly" rows="3" name="service_item_desc[<?= $key ?>]" id="service_item_desc_<?= $key ?>" placeholder="About Us Description" required><?= isset($value->service_item_desc) && !empty($value->service_item_desc) ? $value->service_item_desc : '' ?></textarea>
                                            <span></span>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var suffix = '<?= isset($service_setting_data->service_item_data) && !empty($service_setting_data->service_item_data) ? count($service_setting_data->service_item_data) : 0 ?>';
    $(document).ready(function () {
        if (suffix == 0) {
            service_item();
        }
        $('#addEditServiceSetting').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.next());
                element.next().next().addClass('text-danger');
            }
        });
    });
    function service_item() {
        suffix++;
        var row = '';
        row += '<div class="form-row about_item_row" id="row_' + suffix + '">';
        row += '<div class="col-md-6 mb-3">';
        row += '<label class="form-label" for="service_item_title_' + suffix + '">About Us Item Title <span class="text-danger">*</span></label>';
        row += '<div class="input-group">';
        row += '<input tabindex="2" type="text" class="form-control textonly" name="service_item_title[' + suffix + ']" id="service_item_title_' + suffix + '" placeholder="About Us Title" required value="">';
        row += '<div class="input-group-text">';
        row += '<div class="custom-control d-flex custom-switch">';
        row += '<input id="is_active_service_item_' + suffix + '" name="is_active_service_item[' + suffix + ']" type="checkbox" class="custom-control-input" checked>';
        row += '<label class="custom-control-label fw-500" for="is_active_service_item_' + suffix + '"></label>';
        row += '</div>';
        row += '</div>';
        row += '<div class="input-group-append">';
        if (suffix == 1) {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="service_item()" title="Add" data-toggle="tooltip">';
            row += '<i class="fal fa-plus"></i>';
            row += '</a>';
        } else {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_service_item" title="Delete" data-toggle="tooltip" data-id="' + suffix + '">';
            row += '<i class="fal fa-minus"></i>';
            row += '</a>';
        }
        row += '</div>';
        row += '</div>';
        row += '<span></span>';
        row += '</div>';
        row += '<div class="col-md-6 mb-3">';
        row += '<label class="form-label" for="service_item_desc_' + suffix + '">About Us Item Description <span class="text-danger">*</span></label>';
        row += '<textarea class="form-control textonly" rows="3" name="service_item_desc[' + suffix + ']" id="service_item_desc_' + suffix + '" placeholder="About Us Description" required></textarea>';
        row += '<span></span>';
        row += '</div>';
        row += '</div>';
        $('#service_item_div').append(row);
    }

    $(document).on('click', '.remove_service_item', function () {
        var id = $(this).data('id');
        if ($('.about_item_row').length > 1) {
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $("#row_" + id).remove();
                }
            });
        } else {
            swalWithBootstrapButtons.fire(
                    "Alert!",
                    "Minimum 1 required.",
                    "error"
                    );
        }
    });
</script>