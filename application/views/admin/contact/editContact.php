<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Contact
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Contact', $arrayName = array('id' => 'addEditContact', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="email">Email <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="email" id="email" placeholder="Email" required value="<?= isset($contact_data->contact_email) && !empty($contact_data->contact_email) ? $contact_data->contact_email : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="email_alt">Support Email</label>
                                <input tabindex="2" type="text" class="form-control textonly" name="email_alt" id="email_alt" placeholder="Support Email" value="<?= isset($contact_data->contact_email_alt) && !empty($contact_data->contact_email_alt) ? $contact_data->contact_email_alt : '' ?>">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="contact_number_title">Contact Number Title <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="contact_number_title" id="contact_number_title" placeholder="Contact Number Title" required value="<?= isset($contact_data->contact_number_title) && !empty($contact_data->contact_number_title) ? $contact_data->contact_number_title : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="contact_number">Contact Number <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control contactnumber" name="contact_number" id="contact_number" placeholder="Contact Number" required value="<?= isset($contact_data->contact_phone) && !empty($contact_data->contact_phone) ? $contact_data->contact_phone : '' ?>">
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="addresss">Address <span class="text-danger">*</span></label>
                                <textarea class="form-control address" rows="5" name="addresss" id="addresss" required=""><?= isset($contact_data->contact_address) && !empty($contact_data->contact_address) ? $contact_data->contact_address : '' ?></textarea>
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="map">Map <span class="text-danger">*</span></label>
                                <textarea class="form-control address" rows="5" name="map" id="map" required=""><?= isset($contact_data->contact_map) && !empty($contact_data->contact_map) ? $contact_data->contact_map : '' ?></textarea>
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Page Background Image <i class="text-danger">(File in JPG,PNG) File Size 1920x390px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="background_image" class="custom-file-input" id="background_image">
                                    <label class="custom-file-label" for="background_image">Choose file</label>
                                </div>
                                <img class="mt-3" src="<?= base_url() . (isset($contact_data->background_image) && !empty($contact_data->background_image) ? $contact_data->background_image : 'assets/images/pagetitle-bg.jpg') ?>" height="100px" alt="no preview available">
                                <input type="hidden" name="hidden_background_image" id="hidden_background_image" value="<?= isset($contact_data->background_image) && !empty($contact_data->background_image) ? $contact_data->background_image : 'assets/images/pagetitle-bg.jpg' ?>">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="contact_title">Contact Title <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="contact_title" id="contact_title" placeholder="Contact Title" required value="<?= isset($contact_data->contact_title) && !empty($contact_data->contact_title) ? $contact_data->contact_title : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="contact_description">Contact Description <span class="text-danger">*</span></label>
                                <textarea class="form-control address" rows="1" name="contact_description" id="contact_description" required=""><?= isset($contact_data->contact_description) && !empty($contact_data->contact_description) ? $contact_data->contact_description : '' ?></textarea>
                                <span></span>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_title">Meta Title <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="meta_title" id="meta_title" placeholder="Meta Title" required value="<?= isset($contact_data->meta_title) && !empty($contact_data->meta_title) ? $contact_data->meta_title : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_desc">Meta Description <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="meta_desc" id="meta_desc" placeholder="Meta Description" required value="<?= isset($contact_data->meta_desc) && !empty($contact_data->meta_desc) ? $contact_data->meta_desc : '' ?>">
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_key">Meta Key <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="meta_key" id="meta_key" placeholder="Meta Key" required value="<?= isset($contact_data->meta_key) && !empty($contact_data->meta_key) ? $contact_data->meta_key : '' ?>">
                                <span></span>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#addEditContact').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.next());
                element.next().next().addClass('text-danger');
            }
        });
    });
</script>