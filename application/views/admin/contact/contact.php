<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Contact
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <?= isset($background_data) && !empty($background_data) ? $this->load->view('admin/home/background_image', $background_data, true) : '' ?>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Address</th>
                                    <th>Email</th>
                                    <th>Contact Number</th>
                                    <th>Map</th>
                                    <th>Added By</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($contact_data) && !empty($contact_data)) {
                                    $sn = 0;
                                    foreach ($contact_data as $key => $value) {
                                        $sn++;
                                        ?>                       
                                        <tr> 
                                            <td><?= $sn ?></td>
                                            <td><?= $value->contact_address ?></td> 
                                            <td><?= $value->contact_email ?><?= isset($value->contact_email_alt) && !empty($value->contact_email_alt) ? '</br>' . $value->contact_email_alt : '' ?></td>
                                            <td><?= $value->contact_phone ?></td>
                                            <td><iframe width="200" height="200" style="border:0;overflow:hidden;" src="<?= $value->contact_map ?>"></iframe></td>
                                            <td><?= isset($value->InsUser) && !empty($value->InsUser) ? getUserNameById($value->InsUser) : '' ?></td>   
                                            <td>
                                                <div class='d-flex'>
                                                    <?php if ($menu_rights['edit_right']) { ?>
                                                        <a href='<?php echo base_url() ?>admin/Contact/addEditContact/<?= $value->contact_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                            <i class="fal fa-edit"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#this_page_location').val(window.location);
        $('#addEditBackgroundImage').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>