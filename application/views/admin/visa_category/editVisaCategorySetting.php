<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Visa Category Setting
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Home/visaCategorySetting', $arrayName = array('id' => 'visaCategorySetting')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="visa_category_setting_title">Title <span class="text-danger">*</span></label>
                                <input type="text" class="form-control textonly" name="visa_category_setting_title" id="visa_category_setting_title" placeholder="Title" required value="<?= isset($setting_data->visa_category_setting_title) && !empty($setting_data->visa_category_setting_title) ? $setting_data->visa_category_setting_title : '' ?>">
                                <div class="invalid-feedback">
                                    Title Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="visa_category_setting_footer">Footer Setting <span class="text-danger">*</span></label>
                                <textarea class="form-control" rows="5" name="visa_category_setting_footer" id="visa_category_setting_footer" placeholder="Description" required><?= isset($setting_data->visa_category_setting_footer) && !empty($setting_data->visa_category_setting_footer) ? $setting_data->visa_category_setting_footer : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Setting Footer Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="visa_category_setting_description">Description <span class="text-danger">*</span></label>
                                <textarea class="form-control" rows="5" name="visa_category_setting_description" id="visa_category_setting_description" placeholder="Description" required><?= isset($setting_data->visa_category_setting_description) && !empty($setting_data->visa_category_setting_description) ? $setting_data->visa_category_setting_description : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Description Required
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#visaCategorySetting').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>