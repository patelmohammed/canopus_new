<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit About
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url($this->uri->uri_string()), $arrayName = array('id' => 'addEditAboutUs', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <ul class="nav nav-pills" role="tablist">
                            <li class="nav-item"><a class="nav-link active" data-toggle="pill" href="#section_1">Section 1</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="pill" href="#section_2">Section 2</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="pill" href="#section_3">Section 3</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="pill" href="#section_4">Section 4</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="pill" href="#section_5">Section 5</a></li>
                        </ul>
                        <div class="tab-content py-3">
                            <div class="tab-pane fade show active" id="section_1" role="tabpanel">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="section_1_title">Title <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control textonly" name="section_1_title" id="section_1_title" placeholder="Title" required value="<?= isset($education_loan_data->section_1_title) && !empty($education_loan_data->section_1_title) ? $education_loan_data->section_1_title : '' ?>">
                                        <span></span>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="section_1_short_desc">Short Description <span class="text-danger">*</span></label>
                                        <textarea class="form-control textonly" rows="1" name="section_1_short_desc" id="about_us_alt_title" placeholder="Short Description" required><?= isset($education_loan_data->section_1_short_desc) && !empty($education_loan_data->section_1_short_desc) ? $education_loan_data->section_1_short_desc : '' ?></textarea>
                                        <span></span>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-12 mb-3">
                                        <label class="form-label" for="section_1_desc">Description <span class="text-danger">*</span></label>
                                        <textarea class="form-control textonly" rows="1" name="section_1_desc" id="section_1_desc" placeholder="Description" required><?= isset($education_loan_data->section_1_desc) && !empty($education_loan_data->section_1_desc) ? $education_loan_data->section_1_desc : '' ?></textarea>
                                        <span></span>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label">Image 1 <i class="text-danger">(File in PNG) File Size 320x531px</i></label>
                                        <div class="custom-file">
                                            <input type="file" name="section_1_image_1" class="custom-file-input" id="section_1_image_1" <?= isset($education_loan_data->section_1_image_1) && !empty($education_loan_data->section_1_image_1) ? '' : 'required=""' ?>>
                                            <label class="custom-file-label" for="section_1_image_1"><?= isset($education_loan_data->section_1_image_1) && !empty($education_loan_data->section_1_image_1) ? basename($education_loan_data->section_1_image_1) : 'Choose file' ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <img src="<?= base_url() . (isset($education_loan_data->section_1_image_1) && !empty($education_loan_data->section_1_image_1) ? $education_loan_data->section_1_image_1 : '') ?>" class="m-2" style="width: 15%;">
                                        <input type="hidden" name="hidden_section_1_image_1" id="hidden_section_1_image_1" value="<?= isset($education_loan_data->section_1_image_1) && !empty($education_loan_data->section_1_image_1) ? $education_loan_data->section_1_image_1 : '' ?>">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label">Image 2 <i class="text-danger">(File in PNG) File Size 240x262px</i></label>
                                        <div class="custom-file">
                                            <input type="file" name="section_1_image_2" class="custom-file-input" id="section_1_image_2" <?= isset($education_loan_data->section_1_image_2) && !empty($education_loan_data->section_1_image_2) ? '' : 'required=""' ?>>
                                            <label class="custom-file-label" for="section_1_image_2"><?= isset($education_loan_data->section_1_image_2) && !empty($education_loan_data->section_1_image_2) ? basename($education_loan_data->section_1_image_2) : 'Choose file' ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <img src="<?= base_url() . (isset($education_loan_data->section_1_image_2) && !empty($education_loan_data->section_1_image_2) ? $education_loan_data->section_1_image_2 : '') ?>" class="m-2" style="width: 15%;">
                                        <input type="hidden" name="hidden_section_1_image_2" id="hidden_section_1_image_2" value="<?= isset($education_loan_data->section_1_image_2) && !empty($education_loan_data->section_1_image_2) ? $education_loan_data->section_1_image_2 : '' ?>">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label">Image 3 <i class="text-danger">(File in PNG) File Size 240x230px</i></label>
                                        <div class="custom-file">
                                            <input type="file" name="section_1_image_3" class="custom-file-input" id="section_1_image_3" <?= isset($education_loan_data->section_1_image_3) && !empty($education_loan_data->section_1_image_3) ? '' : 'required=""' ?>>
                                            <label class="custom-file-label" for="section_1_image_3"><?= isset($education_loan_data->section_1_image_3) && !empty($education_loan_data->section_1_image_3) ? basename($education_loan_data->section_1_image_3) : 'Choose file' ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <img src="<?= base_url() . (isset($education_loan_data->section_1_image_3) && !empty($education_loan_data->section_1_image_3) ? $education_loan_data->section_1_image_3 : '') ?>" class="m-2" style="width: 18%;">
                                        <input type="hidden" name="hidden_section_1_image_3" id="hidden_section_1_image_3" value="<?= isset($education_loan_data->section_1_image_3) && !empty($education_loan_data->section_1_image_3) ? $education_loan_data->section_1_image_3 : '' ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="section_2" role="tabpanel">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="section_2_title">Title <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control address" name="section_2_title" id="section_2_title" required="" placeholder="Title" value="<?= isset($education_loan_data->section_2_title) && !empty($education_loan_data->section_2_title) ? $education_loan_data->section_2_title : '' ?>">
                                        <span></span>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <div id="section_2_left_item_div">
                                            <?php
                                            if (isset($section_2_left_item_data) && !empty($section_2_left_item_data)) {
                                                foreach ($section_2_left_item_data as $k1 => $v1) {
                                                    $k1++;
                                                    ?>
                                                    <div class="form-row section_2_row" id="section_2_left_row_<?= $k1 ?>">
                                                        <div class="col-md-12 mb-3">
                                                            <label class="form-label" for="section_2_left_item_data_<?= $k1 ?>">Section 2 Sub Data <span class="text-danger">*</span></label>
                                                            <div class="input-group">
                                                                <input tabindex="2" type="text" class="form-control textonly" name="section_2_left_item_data[<?= $k1 ?>]" id="section_2_left_item_data_<?= $k1 ?>" placeholder="Section 2 Sub Data" required value="<?= isset($v1->section_2_left_item_data) && !empty($v1->section_2_left_item_data) ? $v1->section_2_left_item_data : '' ?>">
                                                                <div class="input-group-text">
                                                                    <div class="custom-control d-flex custom-switch">
                                                                        <input id="section_2_left_item_is_icon_<?= $k1 ?>" name="section_2_left_item_is_icon[<?= $k1 ?>]" type="checkbox" class="custom-control-input" <?= isset($v1->section_2_left_item_is_icon) && !empty($v1->section_2_left_item_is_icon) ? (set_checked($v1->section_2_left_item_is_icon, 1)) : '' ?>>
                                                                        <label class="custom-control-label fw-500" for="section_2_left_item_is_icon_<?= $k1 ?>">Tick icon</label>
                                                                    </div>
                                                                </div>
                                                                <div class="input-group-append">
                                                                    <?php if ($k1 > 1) { ?>
                                                                        <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_section_2_left_item" title="Delete" data-toggle="tooltip" data-id="<?= $k1 ?>">
                                                                            <i class="fal fa-minus"></i>
                                                                        </a>
                                                                    <?php } ?>
                                                                    <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="section_2_left_item()" title="Add" data-toggle="tooltip">
                                                                        <i class="fal fa-plus"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <div id="section_2_right_item_div">
                                            <?php
                                            if (isset($section_2_right_item_data) && !empty($section_2_right_item_data)) {
                                                foreach ($section_2_right_item_data as $k5 => $v5) {
                                                    $k5++;
                                                    ?>
                                                    <div class="form-row section_2_row" id="section_2_right_row_<?= $k5 ?>">
                                                        <div class="col-md-12 mb-3">
                                                            <label class="form-label" for="section_2_right_item_data_<?= $k5 ?>">Section 2 Sub Data <span class="text-danger">*</span></label>
                                                            <div class="input-group">
                                                                <input tabindex="2" type="text" class="form-control textonly" name="section_2_right_item_data[<?= $k5 ?>]" id="section_2_right_item_data_<?= $k5 ?>" placeholder="Section 2 Sub Data" required value="<?= isset($v5->section_2_right_item_data) && !empty($v5->section_2_right_item_data) ? $v5->section_2_right_item_data : '' ?>">
                                                                <div class="input-group-text">
                                                                    <div class="custom-control d-flex custom-switch">
                                                                        <input id="section_2_right_item_is_icon_<?= $k5 ?>" name="section_2_right_item_is_icon[<?= $k5 ?>]" type="checkbox" class="custom-control-input" <?= isset($v5->section_2_right_item_is_icon) && !empty($v5->section_2_right_item_is_icon) ? (set_checked($v5->section_2_right_item_is_icon, 1)) : '' ?>>
                                                                        <label class="custom-control-label fw-500" for="section_2_right_item_is_icon_<?= $k5 ?>">Tick icon</label>
                                                                    </div>
                                                                </div>
                                                                <div class="input-group-append">
                                                                    <?php if ($k5 > 1) { ?>
                                                                        <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_section_2_right_item" title="Delete" data-toggle="tooltip" data-id="<?= $k5 ?>">
                                                                            <i class="fal fa-minus"></i>
                                                                        </a>
                                                                    <?php } ?>
                                                                    <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="section_2_right_item()" title="Add" data-toggle="tooltip">
                                                                        <i class="fal fa-plus"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="section_3" role="tabpanel">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="section_3_title">About Us Title <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control textonly" name="section_3_title" id="section_3_title" placeholder="Title" required value="<?= isset($education_loan_data->section_3_title) && !empty($education_loan_data->section_3_title) ? $education_loan_data->section_3_title : '' ?>">
                                        <span></span>
                                    </div>
                                </div>
                                <hr style="border-bottom: 1px dashed #886ab5;">
                                <div id="section_3_item_div">
                                    <?php
                                    if (isset($section_3_item_data) && !empty($section_3_item_data)) {
                                        foreach ($section_3_item_data as $k2 => $v2) {
                                            $k2++;
                                            ?>
                                            <div class="form-row section_3_row" id="section_3_row_<?= $k2 ?>">
                                                <div class="col-md-12 mb-3">
                                                    <label class="form-label" for="section_3_item_title">Section 3 Sub Title <span class="text-danger">*</span></label>
                                                    <div class="input-group">
                                                        <input tabindex="2" type="text" class="form-control textonly" name="section_3_item_title[<?= $k2 ?>]" id="section_3_item_title_<?= $k2 ?>" placeholder="Title" required value="<?= isset($v2->section_3_item_title) && !empty($v2->section_3_item_title) ? $v2->section_3_item_title : '' ?>">

                                                        <div class="input-group-append">
                                                            <?php if ($k2 > 1) { ?>
                                                                <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_section_3_item" title="Delete" data-toggle="tooltip" data-id="<?= $k2 ?>">
                                                                    <i class="fal fa-minus"></i>
                                                                </a>
                                                            <?php } ?>
                                                            <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="section_3_item()" title="Add" data-toggle="tooltip">
                                                                <i class="fal fa-plus"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <span></span>

                                                </div>
                                                <div class="col-md-12 mb-3">
                                                    <label class="form-label" for="section_3_item_desc">Section 3 Sub Description <span class="text-danger">*</span></label>
                                                    <textarea tabindex="2" rows="3" class="form-control textonly" name="section_3_item_desc[<?= $k2 ?>]" id="section_3_item_desc_<?= $k2 ?>" placeholder="Description" required><?= isset($v2->section_3_item_desc) && !empty($v2->section_3_item_desc) ? $v2->section_3_item_desc : '' ?></textarea>
                                                    <span></span>
                                                </div>
                                            </div>
                                            <script>
                                                CkEditor('section_3_item_desc[<?= $k2 ?>]');
                                            </script>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="section_4" role="tabpanel">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="section_4_title">About Us Title <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control textonly" name="section_4_title" id="section_4_title" placeholder="Title" required value="<?= isset($education_loan_data->section_4_title) && !empty($education_loan_data->section_4_title) ? $education_loan_data->section_4_title : '' ?>">
                                        <span></span>
                                    </div>
                                </div>
                                <hr style="border-bottom: 1px dashed #886ab5;">
                                <div id="section_4_item_div">
                                    <?php
                                    if (isset($section_4_item_data) && !empty($section_4_item_data)) {
                                        foreach ($section_4_item_data as $k3 => $v3) {
                                            $k3++;
                                            ?>
                                            <div class="form-row section_4_row" id="section_4_row_<?= $k3 ?>">
                                                <div class="col-md-6 mb-3">
                                                    <label class="form-label">image <i class="text-danger">(File in PNG, JPG) File Size 259x108px</i></label>
                                                    <div class="input-group">
                                                        <div class="custom-file">
                                                            <input type="file" name="section_4_image[<?= $k3 ?>]" class="custom-file-input" id="section_4_image_<?= $k3 ?>" <?= isset($v3->section_4_image) && !empty($v3->section_4_image) && file_exists($v3->section_4_image) ? '' : 'required' ?>>
                                                            <label class="custom-file-label" for="section_4_image_<?= $k3 ?>"><?= isset($v3->section_4_image) && !empty($v3->section_4_image) && file_exists($v3->section_4_image) ? basename($v3->section_4_image) : 'Choose file' ?></label>
                                                        </div>

                                                        <div class="input-group-append">
                                                            <?php if ($k3 > 1) { ?>
                                                                <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_section_4_item" title="Delete" data-toggle="tooltip" data-id="<?= $k3 ?>">
                                                                    <i class="fal fa-minus"></i>
                                                                </a>
                                                            <?php } ?>
                                                            <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="section_4_item()" title="Add" data-toggle="tooltip">
                                                                <i class="fal fa-plus"></i>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-md-6 mb-3">
                                                    <img src="<?= base_url() . (isset($v3->section_4_image) && !empty($v3->section_4_image) ? $v3->section_4_image : '') ?>" class="m-2" style="width: 25%;">
                                                    <input type="hidden" name="hidden_section_4_image[<?= $k3 ?>]" id="hidden_section_4_image" value="<?= isset($v3->section_4_image) && !empty($v3->section_4_image) ? $v3->section_4_image : '' ?>">
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="section_5" role="tabpanel">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="section_5_title">Title <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control textonly" name="section_5_title" id="section_5_title" placeholder="Title" required value="<?= isset($education_loan_data->section_5_title) && !empty($education_loan_data->section_5_title) ? $education_loan_data->section_5_title : '' ?>">
                                        <span></span>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="section_5_desc">Short Description <span class="text-danger">*</span></label>
                                        <textarea tabindex="2" rows="1" class="form-control textonly" name="section_5_desc" id="section_5_desc" placeholder="Description" required><?= isset($education_loan_data->section_5_desc) && !empty($education_loan_data->section_5_desc) ? $education_loan_data->section_5_desc : '' ?></textarea>
                                        <span></span>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label">Image <i class="text-danger">(File in PNG) File Size 601x636px</i></label>
                                        <div class="custom-file">
                                            <input type="file" name="section_5_image" class="custom-file-input" id="section_5_image" <?= isset($education_loan_data->section_5_image) && !empty($education_loan_data->section_5_image) ? '' : 'required=""' ?>>
                                            <label class="custom-file-label" for="section_5_image"><?= isset($education_loan_data->section_5_image) && !empty($education_loan_data->section_5_image) ? basename($education_loan_data->section_5_image) : 'Choose file' ?></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <img src="<?= base_url() . (isset($education_loan_data->section_5_image) && !empty($education_loan_data->section_5_image) ? $education_loan_data->section_5_image : '') ?>" class="m-2" style="width: 15%;">
                                        <input type="hidden" name="hidden_section_5_image" id="hidden_section_5_image" value="<?= isset($education_loan_data->section_5_image) && !empty($education_loan_data->section_5_image) ? $education_loan_data->section_5_image : '' ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var section_2_left_suffix = '<?= isset($section_2_left_item_data) && !empty($section_2_left_item_data) ? count($section_2_left_item_data) : 0 ?>';
    var section_2_right_suffix = '<?= isset($section_2_right_item_data) && !empty($section_2_right_item_data) ? count($section_2_right_item_data) : 0 ?>';
    var section_3_suffix = '<?= isset($section_3_item_data) && !empty($section_3_item_data) ? count($section_3_item_data) : 0 ?>';
    var section_4_suffix = '<?= isset($section_4_item_data) && !empty($section_4_item_data) ? count($section_4_item_data) : 0 ?>';
    $(document).ready(function () {
        if (section_2_left_suffix == 0) {
            section_2_left_item();
        }
        if (section_2_right_suffix == 0) {
            section_2_right_item();
        }
        if (section_3_suffix == 0) {
            section_3_item();
        }
        if (section_4_suffix == 0) {
            section_4_item();
        }
        $('#addEditAboutUs').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.next());
                element.next().next().addClass('text-danger');
            }
        });

        CkEditor('section_1_desc');
    });

    function section_2_left_item() {
        section_2_left_suffix++;
        var row = '';

        row += '<div class="form-row section_2_row" id="section_2_left_row_' + section_2_left_suffix + '">';
        row += '<div class="col-md-12 mb-3">';
        row += '<label class="form-label" for="section_2_left_item_data_' + section_2_left_suffix + '">Section 2 Sub Data <span class="text-danger">*</span></label>';
        row += '<div class="input-group">';
        row += '<input tabindex="2" type="text" class="form-control textonly" name="section_2_left_item_data[' + section_2_left_suffix + ']" id="section_2_left_item_data_' + section_2_left_suffix + '" placeholder="Section 2 Sub Data" required value="">';
        row += '<div class="input-group-text">';
        row += '<div class="custom-control d-flex custom-switch">';
        row += '<input id="section_2_left_item_is_icon_' + section_2_left_suffix + '" name="section_2_left_item_is_icon[' + section_2_left_suffix + ']" type="checkbox" class="custom-control-input" checked>';
        row += '<label class="custom-control-label fw-500" for="section_2_left_item_is_icon_' + section_2_left_suffix + '">Tick icon</label>';
        row += '</div>';
        row += '</div>';
        row += '<div class="input-group-append">';
        if (section_2_left_suffix > 1) {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_section_2_left_item" title="Delete" data-toggle="tooltip" data-id="' + section_2_left_suffix + '">';
            row += '<i class="fal fa-minus"></i>';
            row += '</a>';
        }
        row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="section_2_left_item()" title="Add" data-toggle="tooltip">';
        row += '<i class="fal fa-plus"></i>';
        row += '</a>';
        row += '</div>';
        row += '</div>';
        row += '<span></span>';
        row += '</div>';
        row += '</div>';

        $('#section_2_left_item_div').append(row);
    }

    $(document).on('click', '.remove_section_2_left_item', function () {
        var id = $(this).data('id');
        if ($('.section_2_row').length > 1) {
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $("#section_2_left_row_" + id).remove();
                }
            });
        } else {
            swalWithBootstrapButtons.fire(
                    "Alert!",
                    "Minimum 1 required.",
                    "error"
                    );
        }
    });
    function section_2_right_item() {
        section_2_right_suffix++;
        var row = '';

        row += '<div class="form-row section_2_row" id="section_2_right_row_' + section_2_right_suffix + '">';
        row += '<div class="col-md-12 mb-3">';
        row += '<label class="form-label" for="section_2_right_item_data_' + section_2_right_suffix + '">Section 2 Sub Data <span class="text-danger">*</span></label>';
        row += '<div class="input-group">';
        row += '<input tabindex="2" type="text" class="form-control textonly" name="section_2_right_item_data[' + section_2_right_suffix + ']" id="section_2_right_item_data_' + section_2_right_suffix + '" placeholder="Section 2 Sub Data" required value="">';
        row += '<div class="input-group-text">';
        row += '<div class="custom-control d-flex custom-switch">';
        row += '<input id="section_2_right_item_is_icon_' + section_2_right_suffix + '" name="section_2_right_item_is_icon[' + section_2_right_suffix + ']" type="checkbox" class="custom-control-input" checked>';
        row += '<label class="custom-control-label fw-500" for="section_2_right_item_is_icon_' + section_2_right_suffix + '">Tick icon</label>';
        row += '</div>';
        row += '</div>';
        row += '<div class="input-group-append">';
        if (section_2_right_suffix > 1) {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_section_2_right_item" title="Delete" data-toggle="tooltip" data-id="' + section_2_right_suffix + '">';
            row += '<i class="fal fa-minus"></i>';
            row += '</a>';
        }
        row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="section_2_right_item()" title="Add" data-toggle="tooltip">';
        row += '<i class="fal fa-plus"></i>';
        row += '</a>';
        row += '</div>';
        row += '</div>';
        row += '<span></span>';
        row += '</div>';
        row += '</div>';

        $('#section_2_right_item_div').append(row);
    }

    $(document).on('click', '.remove_section_2_right_item', function () {
        var id = $(this).data('id');
        if ($('.section_2_row').length > 1) {
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $("#section_2_right_row_" + id).remove();
                }
            });
        } else {
            swalWithBootstrapButtons.fire(
                    "Alert!",
                    "Minimum 1 required.",
                    "error"
                    );
        }
    });

    function section_3_item() {
        section_3_suffix++;
        var row = '';

        row += '<div class="form-row section_3_row" id="section_3_row_' + section_3_suffix + '">';
        row += '<div class="col-md-12 mb-3">';
        row += '<label class="form-label" for="section_3_item_title">Section 3 Sub Title <span class="text-danger">*</span></label>';
        row += '<div class="input-group">';
        row += '<input tabindex="2" type="text" class="form-control textonly" name="section_3_item_title[' + section_3_suffix + ']" id="section_3_item_title_' + section_3_suffix + '" placeholder="Title" required value="">';

        row += '<div class="input-group-append">';
        if (section_3_suffix > 1) {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_section_3_item" title="Delete" data-toggle="tooltip" data-id="' + section_3_suffix + '">';
            row += '<i class="fal fa-minus"></i>';
            row += '</a>';
        }
        row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="section_3_item()" title="Add" data-toggle="tooltip">';
        row += '<i class="fal fa-plus"></i>';
        row += '</a>';
        row += '</div>';
        row += '</div>';
        row += '<span></span>';

        row += '</div>';
        row += '<div class="col-md-12 mb-3">';
        row += '<label class="form-label" for="section_3_item_desc">Section 3 Sub Description <span class="text-danger">*</span></label>';
        row += '<textarea tabindex="2" rows="3" class="form-control textonly" name="section_3_item_desc[' + section_3_suffix + ']" id="section_3_item_desc_' + section_3_suffix + '" placeholder="Description" required></textarea>';
        row += '<span></span>';
        row += '</div>';
        row += '</div>';

        $('#section_3_item_div').append(row);
        CkEditor('section_3_item_desc[' + section_3_suffix + ']');
    }

    $(document).on('click', '.remove_section_3_item', function () {
        var id = $(this).data('id');
        if ($('.section_3_row').length > 1) {
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $("#section_3_row_" + id).remove();
                }
            });
        } else {
            swalWithBootstrapButtons.fire(
                    "Alert!",
                    "Minimum 1 required.",
                    "error"
                    );
        }
    });

    function section_4_item() {
        section_4_suffix++;
        var row = '';

        row += '<div class="form-row section_4_row" id="section_4_row_' + section_4_suffix + '">';
        row += '<div class="col-md-6 mb-3">';
        row += '<label class="form-label">image <i class="text-danger">(File in PNG, JPG) File Size 259x108px</i></label>';
        row += '<div class="input-group">';
        row += '<div class="custom-file">';
        row += '<input type="file" name="section_4_image[' + section_4_suffix + ']" class="custom-file-input" id="section_4_image_' + section_4_suffix + '" required="">';
        row += '<label class="custom-file-label" for="section_4_image_' + section_4_suffix + '">Choose file</label>';
        row += '</div>';

        row += '<div class="input-group-append">';
        if (section_4_suffix > 1) {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_section_4_item" title="Delete" data-toggle="tooltip" data-id="' + section_4_suffix + '">';
            row += '<i class="fal fa-minus"></i>';
            row += '</a>';
        }
        row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="section_4_item()" title="Add" data-toggle="tooltip">';
        row += '<i class="fal fa-plus"></i>';
        row += '</a>';
        row += '</div>';

        row += '</div>';
        row += '</div>';
        row += '</div>';

        $('#section_4_item_div').append(row);
    }

    $(document).on('click', '.remove_section_4_item', function () {
        var id = $(this).data('id');
        if ($('.section_4_row').length > 1) {
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $("#section_4_row_" + id).remove();
                }
            });
        } else {
            swalWithBootstrapButtons.fire(
                    "Alert!",
                    "Minimum 1 required.",
                    "error"
                    );
        }
    });
</script>