<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Visa Preparation Setting
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Home/coachingPreparationSetting', $arrayName = array('id' => 'coachingPreparationSetting', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="coaching_setting_title">Main Title <span class="text-danger">*</span></label>
                                <input type="text" class="form-control textonly" name="coaching_setting_title" id="coaching_setting_title" placeholder="Title" required value="<?= isset($setting_data->coaching_setting_title) && !empty($setting_data->coaching_setting_title) ? $setting_data->coaching_setting_title : '' ?>">
                                <div class="invalid-feedback">
                                    Title Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="coaching_setting_desc">Main Description <span class="text-danger">*</span></label>
                                <textarea class="form-control textonly" name="coaching_setting_desc" rows="1" id="coaching_setting_desc" placeholder="Description" required><?= isset($setting_data->coaching_setting_desc) && !empty($setting_data->coaching_setting_desc) ? $setting_data->coaching_setting_desc : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Description Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="setting_image_title">Image Title <span class="text-danger">*</span></label>
                                <textarea class="form-control textonly" name="setting_image_title" rows="1" id="setting_image_title" placeholder="Image Title" required><?= isset($setting_data->setting_image_title) && !empty($setting_data->setting_image_title) ? $setting_data->setting_image_title : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Image Title Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="setting_image_desc">Image Description <span class="text-danger">*</span></label>
                                <textarea class="form-control textonly" name="setting_image_desc" rows="1" id="setting_image_desc" placeholder="Image Description" required><?= isset($setting_data->setting_image_desc) && !empty($setting_data->setting_image_desc) ? $setting_data->setting_image_desc : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Image Description Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Image <i class="text-danger">(File in JPG,PNG) File Size 495x568px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="coaching_setting_image" class="custom-file-input" id="coaching_setting_image" <?= isset($setting_data->coaching_setting_image) && !empty($setting_data->coaching_setting_image) ? '' : 'required=""' ?>>
                                    <label class="custom-file-label" for="coaching_setting_image">Choose file</label>
                                </div>
                                <img class="mt-3" src="<?= base_url() . (isset($setting_data->coaching_setting_image) && !empty($setting_data->coaching_setting_image) ? $setting_data->coaching_setting_image : '') ?>" height="100px" width="100px">
                                <input type="hidden" name="hidden_coaching_setting_image" id="hidden_coaching_setting_image" value="<?= (isset($setting_data->coaching_setting_image) && !empty($setting_data->coaching_setting_image) ? $setting_data->coaching_setting_image : '') ?>">
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#coachingPreparationSetting').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>