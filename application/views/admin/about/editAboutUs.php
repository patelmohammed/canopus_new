<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit About
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/About', $arrayName = array('id' => 'addEditAboutUs', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <ul class="nav nav-pills" role="tablist">
                            <li class="nav-item"><a class="nav-link active" data-toggle="pill" href="#section_1">Section 1</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="pill" href="#section_2">Section 2</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="pill" href="#section_3">Section 3</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="pill" href="#section_4">Page Image</a></li>
                            <li class="nav-item"><a class="nav-link" data-toggle="pill" href="#slug_section">Slug</a></li>
                        </ul>
                        <div class="tab-content py-3">
                            <div class="tab-pane fade show active" id="section_1" role="tabpanel">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="about_us_alt_title">Title <span class="text-danger">*</span></label>
                                        <textarea class="form-control textonly" rows="1" name="about_us_alt_title" id="about_us_alt_title" placeholder="Icon" required><?= isset($about_us_data->about_us_alt_title) && !empty($about_us_data->about_us_alt_title) ? $about_us_data->about_us_alt_title : '' ?></textarea>
                                        <span></span>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="about_us_alt_desc">Description <span class="text-danger">*</span></label>
                                        <textarea class="form-control address" rows="2" name="about_us_alt_desc" id="about_us_alt_desc" required=""><?= isset($about_us_data->about_us_alt_desc) && !empty($about_us_data->about_us_alt_desc) ? $about_us_data->about_us_alt_desc : '' ?></textarea>
                                        <span></span>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-2 mb-3">
                                        <label class="form-label" for="about_us_alt_sub_icon_1">Icon <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control textonly" name="about_us_alt_sub_icon_1" id="about_us_alt_sub_icon_1" placeholder="About Us Title" required value="<?= isset($about_us_data->about_us_alt_sub_icon_1) && !empty($about_us_data->about_us_alt_sub_icon_1) ? $about_us_data->about_us_alt_sub_icon_1 : '' ?>">
                                        <span></span>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label class="form-label" for="about_us_alt_sub_title_1">Title <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control address" name="about_us_alt_sub_title_1" id="about_us_alt_sub_title_1" required="" placeholder="Title" value="<?= isset($about_us_data->about_us_alt_sub_title_1) && !empty($about_us_data->about_us_alt_sub_title_1) ? $about_us_data->about_us_alt_sub_title_1 : '' ?>">
                                        <span></span>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="about_us_alt_sub_desc_1">Description <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control address" name="about_us_alt_sub_desc_1" id="about_us_alt_sub_desc_1" required="" placeholder="Description" value="<?= isset($about_us_data->about_us_alt_sub_desc_1) && !empty($about_us_data->about_us_alt_sub_desc_1) ? $about_us_data->about_us_alt_sub_desc_1 : '' ?>">
                                        <span></span>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-2 mb-3">
                                        <label class="form-label" for="about_us_alt_sub_icon_2">Icon <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control textonly" name="about_us_alt_sub_icon_2" id="about_us_alt_sub_icon_2" placeholder="Icon" required value="<?= isset($about_us_data->about_us_alt_sub_icon_2) && !empty($about_us_data->about_us_alt_sub_icon_2) ? $about_us_data->about_us_alt_sub_icon_2 : '' ?>">
                                        <span></span>
                                    </div>
                                    <div class="col-md-4 mb-3">
                                        <label class="form-label" for="about_us_alt_sub_title_2">Title <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control address" name="about_us_alt_sub_title_2" id="about_us_alt_sub_title_2" required="" placeholder="Title" value="<?= isset($about_us_data->about_us_alt_sub_title_2) && !empty($about_us_data->about_us_alt_sub_title_2) ? $about_us_data->about_us_alt_sub_title_2 : '' ?>">
                                        <span></span>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="about_us_alt_sub_desc_2">Description <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control address" name="about_us_alt_sub_desc_2" id="about_us_alt_sub_desc_2" required="" placeholder="Description" value="<?= isset($about_us_data->about_us_alt_sub_desc_2) && !empty($about_us_data->about_us_alt_sub_desc_2) ? $about_us_data->about_us_alt_sub_desc_2 : '' ?>">
                                        <span></span>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label">Logo <i class="text-danger">(File in PNG) File Size 520x392px</i></label>
                                        <div class="custom-file">
                                            <input type="file" name="about_us_alt_image" class="custom-file-input" id="about_us_alt_image" <?= isset($about_us_data->about_us_alt_image) && !empty($about_us_data->about_us_alt_image) ? '' : 'required=""' ?>>
                                            <label class="custom-file-label" for="about_us_alt_image">Choose file</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <img src="<?= base_url() . (isset($about_us_data->about_us_alt_image) && !empty($about_us_data->about_us_alt_image) ? $about_us_data->about_us_alt_image : '') ?>" height="100px" width="120px" class="m-2">
                                        <input type="hidden" name="hidden_about_us_alt_image" id="hidden_about_us_alt_image" value="<?= isset($about_us_data->about_us_alt_image) && !empty($about_us_data->about_us_alt_image) ? $about_us_data->about_us_alt_image : '' ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="section_2" role="tabpanel">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="section_3_title">Title <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control address" name="section_3_title" id="section_3_title" required="" placeholder="Title" value="<?= isset($about_us_data->section_3_title) && !empty($about_us_data->section_3_title) ? $about_us_data->section_3_title : '' ?>">
                                        <span></span>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="section_3_desc">Description <span class="text-danger">*</span></label>
                                        <textarea class="form-control address" name="section_3_desc" id="section_3_desc" required="" placeholder="Description"><?= isset($about_us_data->section_3_desc) && !empty($about_us_data->section_3_desc) ? $about_us_data->section_3_desc : '' ?></textarea>
                                        <span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="section_3" role="tabpanel">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="about_us_title">About Us Title <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control textonly" name="about_us_title" id="about_us_title" placeholder="About Us Title" required value="<?= isset($about_us_data->about_us_title) && !empty($about_us_data->about_us_title) ? $about_us_data->about_us_title : '' ?>">
                                        <span></span>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="about_us_desc">Description <span class="text-danger">*</span></label>
                                        <textarea class="form-control address" rows="2" name="about_us_desc" id="about_us_desc" required=""><?= isset($about_us_data->about_us_desc) && !empty($about_us_data->about_us_desc) ? $about_us_data->about_us_desc : '' ?></textarea>
                                        <span></span>
                                    </div>
                                </div>
                                <hr style="border-bottom: 1px dashed #886ab5;">
                                <div id="about_us_item_div">
                                    <?php
                                    if (isset($about_us_data->about_us_item_data) && !empty($about_us_data->about_us_item_data)) {
                                        foreach ($about_us_data->about_us_item_data as $key => $value) {
                                            $key++;
                                            ?>
                                            <div class="form-row about_item_row" id="row_<?= $key ?>">
                                                <div class="col-md-6 mb-3">
                                                    <label class="form-label" for="about_us_item_title_<?= $key ?>">About Us Item Title <span class="text-danger">*</span></label>
                                                    <div class="input-group">
                                                        <input tabindex="2" type="text" class="form-control textonly" name="about_us_item_title[<?= $key ?>]" id="about_us_item_title_<?= $key ?>" placeholder="About Us Title" required value="<?= isset($value->about_us_item_title) && !empty($value->about_us_item_title) ? $value->about_us_item_title : '' ?>">
                                                        <div class="input-group-text">
                                                            <div class="custom-control d-flex custom-switch">
                                                                <input id="is_active_service_item_<?= $key ?>" name="is_active_service_item[<?= $key ?>]" type="checkbox" class="custom-control-input" <?= isset($value->is_active) && !empty($value->is_active) ? (set_checked($value->is_active, 1)) : '' ?>>
                                                                <label class="custom-control-label fw-500" for="is_active_service_item_<?= $key ?>"></label>
                                                            </div>
                                                        </div>
                                                        <div class="input-group-append">
                                                            <?php if ($key == 1) { ?>
                                                                <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="about_us_item()" title="Add" data-toggle="tooltip">
                                                                    <i class="fal fa-plus"></i>
                                                                </a>
                                                            <?php } else { ?>
                                                                <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_about_us_item" title="Delete" data-toggle="tooltip" data-id="<?= $key ?>">
                                                                    <i class="fal fa-minus"></i>
                                                                </a>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <span></span>
                                                </div>
                                                <div class="col-md-6 mb-3">
                                                    <label class="form-label" for="about_us_item_desc_<?= $key ?>">About Us Item Description <span class="text-danger">*</span></label>
                                                    <textarea class="form-control textonly" rows="3" name="about_us_item_desc[<?= $key ?>]" id="about_us_item_desc_<?= $key ?>" placeholder="About Us Description" required><?= isset($value->about_us_item_desc) && !empty($value->about_us_item_desc) ? $value->about_us_item_desc : '' ?></textarea>
                                                    <span></span>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="section_4" role="tabpanel">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label">Page Image <i class="text-danger">(File in PNG) File Size 1920x390px</i></label>
                                        <div class="custom-file">
                                            <input type="file" name="background_image" class="custom-file-input" id="background_image" <?= isset($about_us_data->background_image) && !empty($about_us_data->background_image) ? '' : 'required=""' ?>>
                                            <label class="custom-file-label" for="background_image">Choose file</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <img src="<?= base_url() . (isset($about_us_data->background_image) && !empty($about_us_data->background_image) ? $about_us_data->background_image : '') ?>" height="100px" width="120px" class="m-2">
                                        <input type="hidden" name="hidden_background_image" id="hidden_background_image" value="<?= isset($about_us_data->background_image) && !empty($about_us_data->background_image) ? $about_us_data->background_image : '' ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="slug_section" role="tabpanel">
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="meta_title">Meta Title <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control" name="meta_title" id="meta_title" placeholder="Meta Title" required value="<?= isset($about_us_data->meta_title) && !empty($about_us_data->meta_title) ? $about_us_data->meta_title : '' ?>">
                                        <span></span>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="meta_desc">Meta Description <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control textonly" name="meta_desc" id="meta_desc" placeholder="Meta Description" required value="<?= isset($about_us_data->meta_desc) && !empty($about_us_data->meta_desc) ? $about_us_data->meta_desc : '' ?>">
                                        <span></span>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label class="form-label" for="meta_key">Meta Key <span class="text-danger">*</span></label>
                                        <input tabindex="2" type="text" class="form-control" name="meta_key" id="meta_key" placeholder="Meta Key" required value="<?= isset($about_us_data->meta_key) && !empty($about_us_data->meta_key) ? $about_us_data->meta_key : '' ?>">
                                        <span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var suffix = '<?= isset($about_us_data->about_us_item_data) && !empty($about_us_data->about_us_item_data) ? count($about_us_data->about_us_item_data) : 0 ?>';
    $(document).ready(function () {
        if (suffix == 0) {
            about_us_item();
        }
        $('#addEditAboutUs').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.next());
                element.next().next().addClass('text-danger');
            }
        });
    });

    function about_us_item() {
        suffix++;
        var row = '';
        row += '<div class="form-row about_item_row" id="row_' + suffix + '">';
        row += '<div class="col-md-6 mb-3">';
        row += '<label class="form-label" for="about_us_item_title_' + suffix + '">About Us Item Title <span class="text-danger">*</span></label>';
        row += '<div class="input-group">';
        row += '<input tabindex="2" type="text" class="form-control textonly" name="about_us_item_title[' + suffix + ']" id="about_us_item_title_' + suffix + '" placeholder="About Us Title" required value="">';
        row += '<div class="input-group-text">';
        row += '<div class="custom-control d-flex custom-switch">';
        row += '<input id="is_active_service_item_' + suffix + '" name="is_active_service_item[' + suffix + ']" type="checkbox" class="custom-control-input" checked>';
        row += '<label class="custom-control-label fw-500" for="is_active_service_item_' + suffix + '"></label>';
        row += '</div>';
        row += '</div>';
        row += '<div class="input-group-append">';
        if (suffix == 1) {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="about_us_item()" title="Add" data-toggle="tooltip">';
            row += '<i class="fal fa-plus"></i>';
            row += '</a>';
        } else {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_about_us_item" title="Delete" data-toggle="tooltip" data-id="' + suffix + '">';
            row += '<i class="fal fa-minus"></i>';
            row += '</a>';
        }
        row += '</div>';
        row += '</div>';
        row += '<span></span>';
        row += '</div>';
        row += '<div class="col-md-6 mb-3">';
        row += '<label class="form-label" for="about_us_item_desc_' + suffix + '">About Us Item Description <span class="text-danger">*</span></label>';
        row += '<textarea class="form-control textonly" rows="3" name="about_us_item_desc[' + suffix + ']" id="about_us_item_desc_' + suffix + '" placeholder="About Us Description" required></textarea>';
        row += '<span></span>';
        row += '</div>';
        row += '</div>';
        $('#about_us_item_div').append(row);
    }

    $(document).on('click', '.remove_about_us_item', function () {
        var id = $(this).data('id');
        if ($('.about_item_row').length > 1) {
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $("#row_" + id).remove();
                }
            });
        } else {
            swalWithBootstrapButtons.fire(
                    "Alert!",
                    "Minimum 1 required.",
                    "error"
                    );
        }
    });
</script>