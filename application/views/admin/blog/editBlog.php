<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-file-code'></i> Edit Blog
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Blog">Blog</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Blog/addEditBlog/' . $encrypted_id, $arrayName = array('id' => 'addEditBlog', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="blog_title">Blog Title <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="blog_title" id="blog_title" placeholder="Blog Title" required value="<?= isset($blog_data->blog_title) && !empty($blog_data->blog_title) ? $blog_data->blog_title : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="blog_date">Blog Date <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="blog_date" id="blog_date" placeholder="Blog Title" required value="<?= isset($blog_data->blog_date) && !empty($blog_data->blog_date) ? date('d-m-Y', strtotime($blog_data->blog_date)) : '' ?>" readonly="">
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="blog_short_desc">Blog Short Description <span class="text-danger">*</span></label>
                                <textarea rows="2" class="form-control address" name="blog_short_desc" id="blog_short_desc" placeholder="Blog Short Description" required><?= isset($blog_data->blog_short_desc) && !empty($blog_data->blog_short_desc) ? $blog_data->blog_short_desc : '' ?></textarea>
                                <span></span>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label class="form-label" for="blog_short_desc">Blog Type <span class="text-danger">*</span></label>
                                <div class="frame-wrap">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" name="blog_poster_type" id="blog_poster_type_image" value="image" <?= isset($blog_data->blog_poster_type) && !empty($blog_data->blog_poster_type) ? set_checked($blog_data->blog_poster_type, 'image') : '' ?>>
                                        <label class="custom-control-label" for="blog_poster_type_image">Image</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" name="blog_poster_type" id="blog_poster_type_video" value="video" <?= isset($blog_data->blog_poster_type) && !empty($blog_data->blog_poster_type) ? set_checked($blog_data->blog_poster_type, 'video') : '' ?>>
                                        <label class="custom-control-label" for="blog_poster_type_video">Video</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <label class="form-label" for="is_active">&nbsp;</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" name="is_active" class="custom-control-input" id="is_active" <?= isset($blog_data->is_active) && !empty($blog_data->is_active) ? set_checked($blog_data->is_active, 1) : '' ?>>
                                    <label class="custom-control-label" for="is_active">Page Active</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Poster Image <i class="text-danger">(File in JPG,PNG) File Size 720x620 px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="blog_poster_image" class="custom-file-input" id="blog_poster_image" <?= isset($blog_data->blog_poster_image) && !empty($blog_data->blog_poster_image) && file_exists($blog_data->blog_poster_image) ? '' : 'required' ?>>
                                    <label class="custom-file-label" for="blog_poster_image"><?= isset($blog_data->blog_poster_image) && !empty($blog_data->blog_poster_image) ? basename($blog_data->blog_poster_image) : '' ?></label>
                                </div>
                                <img class="mt-3" src="<?= isset($blog_data->blog_poster_image) && !empty($blog_data->blog_poster_image) && file_exists($blog_data->blog_poster_image) ? base_url() . $blog_data->blog_poster_image : base_url('assets/admin/img/no_image.jpg') ?>" height="100px" alt="no preview available">
                                <input type="hidden" class="form-control" value="<?= isset($blog_data->blog_poster_image) && !empty($blog_data->blog_poster_image) ? $blog_data->blog_poster_image : '' ?>" name="hidden_blog_poster_image"/>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Blog Image <i class="text-danger">(File in JPG,PNG) File Size 1020x520 px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="blog_image" class="custom-file-input" id="blog_image" <?= isset($blog_data->blog_image) && !empty($blog_data->blog_image) && file_exists($blog_data->blog_image) ? '' : 'required' ?>>
                                    <label class="custom-file-label" for="blog_image"><?= isset($blog_data->blog_image) && !empty($blog_data->blog_image) ? basename($blog_data->blog_image) : '' ?></label>
                                </div>
                                <img class="mt-3" src="<?= isset($blog_data->blog_image) && !empty($blog_data->blog_image) && file_exists($blog_data->blog_image) ? base_url() . $blog_data->blog_image : base_url('assets/admin/img/no_image.jpg') ?>" height="100px" alt="no preview available">
                                <input type="hidden" class="form-control" value="<?= isset($blog_data->blog_image) && !empty($blog_data->blog_image) ? $blog_data->blog_image : '' ?>" name="hidden_blog_image"/>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Page Background Image <i class="text-danger">(File in JPG,PNG) File Size 1920x390 px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="background_image" class="custom-file-input" id="background_image" <?= isset($blog_data->background_image) && !empty($blog_data->background_image) && file_exists($blog_data->background_image) ? '' : 'required' ?>>
                                    <label class="custom-file-label" for="background_image"><?= isset($blog_data->background_image) && !empty($blog_data->background_image) ? basename($blog_data->background_image) : '' ?></label>
                                </div>
                                <img class="mt-3" src="<?= isset($blog_data->background_image) && !empty($blog_data->background_image) && file_exists($blog_data->background_image) ? base_url() . $blog_data->background_image : base_url('assets/admin/img/no_image.jpg') ?>" height="100px" alt="no preview available">
                                <input type="hidden" class="form-control" value="<?= isset($blog_data->background_image) && !empty($blog_data->background_image) ? $blog_data->background_image : '' ?>" name="hidden_background_image"/>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="blog_video">Blog Video Link <span class="text-danger">*</span></label>
                                <textarea rows="2" class="form-control" name="blog_video" id="blog_video" placeholder="Blog Video Link"><?= isset($blog_data->blog_video) && !empty($blog_data->blog_video) ? $blog_data->blog_video : '' ?></textarea>
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="facebook">Facebook</label>
                                <input tabindex="2" type="text" class="form-control" name="facebook" id="facebook" placeholder="Facebook" value="<?= isset($blog_data->facebook) && !empty($blog_data->facebook) && $blog_data->facebook != 'javascript:void(0);' ? $blog_data->facebook : '' ?>">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="instagram">Instagram</label>
                                <input tabindex="2" type="text" class="form-control" name="instagram" id="instagram" placeholder="Instagram" value="<?= isset($blog_data->instagram) && !empty($blog_data->instagram) && $blog_data->instagram != 'javascript:void(0);' ? $blog_data->instagram : '' ?>">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="linkedin">LinkedIn</label>
                                <input tabindex="2" type="text" class="form-control" name="linkedin" id="linkedin" placeholder="LinkedIn" value="<?= isset($blog_data->linkedin) && !empty($blog_data->linkedin) && $blog_data->linkedin != 'javascript:void(0);' ? $blog_data->linkedin : '' ?>">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="twitter">Twitter</label>
                                <input tabindex="2" type="text" class="form-control" name="twitter" id="twitter" placeholder="Twitter" value="<?= isset($blog_data->twitter) && !empty($blog_data->twitter) && $blog_data->twitter != 'javascript:void(0);' ? $blog_data->twitter : '' ?>">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="blog_desc">Page Content <span class="text-danger">*</span></label>
                                <textarea class="form-control" id="blog_desc" name="blog_desc" required=""><?= isset($blog_data->blog_desc) && !empty($blog_data->blog_desc) ? $blog_data->blog_desc : '' ?></textarea>
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_title">Meta Title <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="meta_title" id="meta_title" placeholder="Meta Title" required value="<?= isset($blog_data->meta_title) && !empty($blog_data->meta_title) ? $blog_data->meta_title : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_desc">Meta Description <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="meta_desc" id="meta_desc" placeholder="Meta Description" required value="<?= isset($blog_data->meta_desc) && !empty($blog_data->meta_desc) ? $blog_data->meta_desc : '' ?>">
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_key">Meta Key <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="meta_key" id="meta_key" placeholder="Meta Key" required value="<?= isset($blog_data->meta_key) && !empty($blog_data->meta_key) ? $blog_data->meta_key : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="slug">Slug <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="slug" id="slug" placeholder="Slug" required value="<?= isset($slug_data->slug) && !empty($slug_data->slug) ? $slug_data->slug : '' ?>">
                                <span></span>
                            </div>
                            <input type="hidden" name="hidden_slug_id" id="hidden_slug_id" value="<?= isset($slug_data->slug_id) && !empty($slug_data->slug_id) ? $slug_data->slug_id : '' ?>">
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var blog_id = '<?= isset($blog_data->blog_id) && !empty($blog_data->blog_id) ? $blog_data->blog_id : '' ?>';
    var blog_poster_type = '<?= isset($blog_data->blog_poster_type) && !empty($blog_data->blog_poster_type) ? $blog_data->blog_poster_type : '' ?>';
    $(document).ready(function () {
        $('#blog_date').datepicker({
            format: 'dd-mm-yyyy',
            todayHighlight: true,
            orientation: "bottom left",
            templates: controls,
            autoclose: true,
            onClose: function () {
                $(this).valid();
            }
        });

        CKEDITOR.replace('blog_desc', {
            enterMode: CKEDITOR.ENTER_BR,
            toolbar: [
                {name: 'items', items: ["Source", "-", "Save", "NewPage", "DocProps", "Preview", "Print", "Templates", "-", "document"]},
                {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                {name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']},
                '/',
                {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', "Subscript", "Superscript", '-', 'RemoveFormat']},
                {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']},

                {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
                {name: 'insert', items: ['Table', 'HorizontalRule', 'PageBreak']},
                '/',
                {name: 'styles', items: ['Styles', 'Format', "FontSize"]},
                {name: 'color', items: ["TextColor", "BGColor"]},
                {name: 'tools', items: ['Maximize', '-', 'ShowBlocks']},
                {name: 'info', items: ['About']},
                {name: 'tokens', items: ['tokens']}
            ],
            removeDialogTabs: 'link:target;image:Link;link:upload;image:Upload;image:advanced;link:advanced',
            removePlugins: 'flashupload'
        });

        $('#addEditBlog').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                blog_title: {
                    remote: {
                        url: "<?= base_url('admin/Blog/checkBlogTitle/') ?>" + blog_id,
                        type: "get"
                    }
                },
                slug: {
                    remote: {
                        url: "<?= base_url('admin/Home/checkSlug/') ?>" + $('#hidden_slug_id').val(),
                        type: "get"
                    }
                }
            },
            messages: {
                blog_title: {
                    remote: jQuery.validator.format("{0} is already in use")
                },
                slug: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.next());
                element.next().next().addClass('text-danger');
            }
        });

        $(document).on('change', '[name="blog_poster_type"]', function () {
            blog_poster_type_required($(this).val());
        });

        blog_poster_type_required(blog_poster_type);
    });

    function blog_poster_type_required(blog_poster_type) {
        if (blog_poster_type == 'video') {
            $('#blog_video').prop('required', true);
        } else {
            $('#blog_video').prop('required', false);
        }
    }
</script>