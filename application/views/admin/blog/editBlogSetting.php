<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Blog Setting
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Blog/addEditBlogSetting', $arrayName = array('id' => 'addEditBlogSetting', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="blog_setting_title">Visa Section Title <span class="text-danger">*</span></label>
                                <input type="text" name="blog_setting_title" id="blog_setting_title" class="form-control" required="" placeholder="Visa Section Title" value="<?= isset($blog_setting_data->blog_setting_title) && !empty($blog_setting_data->blog_setting_title) ? $blog_setting_data->blog_setting_title : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="blog_setting_desc">Visa Section Description<span class="text-danger">*</span></label>
                                <textarea rows="1" name="blog_setting_desc" id="blog_setting_desc" class="form-control" required="" placeholder="Visa Section Description"><?= isset($blog_setting_data->blog_setting_desc) && !empty($blog_setting_data->blog_setting_desc) ? $blog_setting_data->blog_setting_desc : '' ?></textarea>
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Page Background Image <i class="text-danger">(File in JPG,PNG) File Size 1920x390px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="background_image" class="custom-file-input" id="background_image">
                                    <label class="custom-file-label" for="background_image">Choose file</label>
                                </div>
                                <img class="mt-3" src="<?= base_url() . (isset($blog_setting_data->background_image) && !empty($blog_setting_data->background_image) ? $blog_setting_data->background_image : 'assets/images/pagetitle-bg.jpg') ?>" height="100px" alt="no preview available">
                                <input type="hidden" name="hidden_background_image" id="hidden_background_image" value="<?= isset($blog_setting_data->background_image) && !empty($blog_setting_data->background_image) ? $blog_setting_data->background_image : 'assets/images/pagetitle-bg.jpg' ?>">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_title">Meta Title <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="meta_title" id="meta_title" placeholder="Meta Title" required value="<?= isset($blog_setting_data->meta_title) && !empty($blog_setting_data->meta_title) ? $blog_setting_data->meta_title : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_desc">Meta Description <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="meta_desc" id="meta_desc" placeholder="Meta Description" required value="<?= isset($blog_setting_data->meta_desc) && !empty($blog_setting_data->meta_desc) ? $blog_setting_data->meta_desc : '' ?>">
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_key">Meta Key <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="meta_key" id="meta_key" placeholder="Meta Key" required value="<?= isset($blog_setting_data->meta_key) && !empty($blog_setting_data->meta_key) ? $blog_setting_data->meta_key : '' ?>">
                                <span></span>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#addEditBlogSetting').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.next());
                element.next().next().addClass('text-danger');
            }
        });
    });
</script>