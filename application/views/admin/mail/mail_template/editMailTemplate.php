<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-envelope'></i> Edit Mail Template
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Mail/mailTemplate">Mail Template</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Mail/addEditMailTemplate/' . $encrypted_id, $arrayName = array('id' => 'addEditMailTemplate', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="type">Type <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="type" id="type" placeholder="Type" required value="<?= isset($template_data->type) && !empty($template_data->type) ? $template_data->type : '' ?>" readonly="">
                                <div class="invalid-feedback">
                                    Type / Already Exist
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="subject">Subject <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="subject" id="subject" placeholder="Subject" required value="<?= isset($template_data->subject) && !empty($template_data->subject) ? $template_data->subject : '' ?>" maxlength="35">
                                <div class="invalid-feedback">
                                    Subject / Already Exist
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="fromname">From Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="fromname" id="fromname" placeholder="From Name" required value="<?= isset($template_data->fromname) && !empty($template_data->fromname) ? $template_data->fromname : '' ?>" maxlength="30">
                                <div class="invalid-feedback">
                                    From Name / Already Exist
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="fromemail">From Email</label>
                                <input tabindex="2" type="email" class="form-control textonly" name="fromemail" id="fromemail" placeholder="From Email" value="<?= isset($template_data->fromemail) && !empty($template_data->fromemail) ? $template_data->fromemail : '' ?>">
                                <div class="invalid-feedback">
                                    From Email / Already Exist
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="message">Message&nbsp;<span class="text-danger">*</span>&nbsp;<i class="text-danger">(Don't change the value which is written in "{&nbsp;}")</i></label>
                                <textarea class="form-control" rows="5" id="message" name="message"><?= isset($template_data->message) && !empty($template_data->message) ? $template_data->message : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Message Required
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        CKEDITOR.replace('message', {
            enterMode: CKEDITOR.ENTER_BR,
            toolbar: [
                {name: 'items', items: ["Source", "-", "Save", "NewPage", "DocProps", "Preview", "Print", "Templates", "-", "document"]},
                {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                {name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']},
                '/',
                {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', "Subscript", "Superscript", '-', 'RemoveFormat']},
                {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']},

                {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
                {name: 'insert', items: ['Table', 'HorizontalRule', 'PageBreak']},
                '/',
                {name: 'styles', items: ['Styles', 'Format', "FontSize"]},
                {name: 'color', items: ["TextColor", "BGColor"]},
                {name: 'tools', items: ['Maximize', '-', 'ShowBlocks']},
                {name: 'info', items: ['About']},
                {name: 'tokens', items: ['tokens']}
            ],
            removeDialogTabs: 'link:target;image:Link;link:upload;image:Upload;image:advanced;link:advanced',
            removePlugins: 'flashupload'
        });
        $('#addEditMailTemplate').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>