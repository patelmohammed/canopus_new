<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Mail Template
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Mail/newsletter">Newsletter</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Mail/addEditNewsletter/' . $encrypted_id, $arrayName = array('id' => 'addEditNewsletter', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="newsletter_title">Title <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="newsletter_title" id="newsletter_title" placeholder="Title" required value="<?= isset($newsletter_data->newsletter_title) && !empty($newsletter_data->newsletter_title) ? $newsletter_data->newsletter_title : '' ?>">
                                <div class="invalid-feedback">
                                    Admin Mail Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="newsletter_desc">Description <span class="text-danger">*</span></label>
                                <textarea  class="form-control address" name="newsletter_desc" id="newsletter_desc" rows="5"><?= isset($newsletter_data->newsletter_desc) && !empty($newsletter_data->newsletter_desc) ? $newsletter_data->newsletter_desc : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Description Required
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#addEditNewsletter').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>