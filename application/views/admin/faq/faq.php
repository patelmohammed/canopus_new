<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-file-code'></i> Faq
        </h1>
        <div class="d-flex mr-0">
            <?php if ($menu_rights['add_right']) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Faq/addEditFaq">Add Faq</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>Country Name</th>
                                    <th>Faq Title</th>
                                    <th class="notexport no-sort">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($faq_data) && !empty($faq_data)) {
                                    $sn = 0;
                                    foreach ($faq_data as $key => $value) {
                                        $sn++;
                                        ?>                       
                                        <tr> 
                                            <td><?= $sn ?></td>
                                            <td><?= isset($value->country_page_name) && !empty($value->country_page_name) ? $value->country_page_name : '' ?></td>
                                            <td><?= isset($value->faq_item_title) && !empty($value->faq_item_title) ? $value->faq_item_title : '' ?></td>
                                            <td>
                                                <div class='d-flex'>
                                                    <?php if ($menu_rights['edit_right']) { ?>
                                                        <a href='<?php echo base_url() ?>admin/Faq/addEditFaq/<?= $value->faq_item_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                            <i class="fal fa-edit"></i>
                                                        </a>
                                                    <?php } ?>
                                                    <?php if ($menu_rights['delete_right']) { ?>
                                                        <a href='javascript:void(0);' data-url="<?= base_url('admin/Faq/deleteFaqItem') ?>" data-id="<?= $value->faq_item_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2 delete_record' title='Delete Record' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'>
                                                            <i class="fal fa-times"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>