<style>
    .panel.panel-locked:not(.panel-fullscreen) .panel-hdr h2:before{
        content: unset;
    }
</style>
<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-file-code'></i> <?= isset($faq_item_data->faq_item_id) && !empty($faq_item_data->faq_item_id) ? 'Edit' : 'Add' ?> Faq
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Faq">Faq</a>
        </div>
    </div>
    <?= form_open(base_url($this->uri->uri_string()), $arrayName = array('id' => 'addEditFaq', 'enctype' => 'multipart/form-data')) ?>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel panel-locked" data-panel-lock="false" data-panel-close="false" data-panel-fullscreen="false" data-panel-collapsed="false" data-panel-color="false" data-panel-locked="false" data-panel-refresh="false" data-panel-reset="false">
                <div class="panel-hdr">
                    <h2>
                        FAQs <span class="fw-300">Title</span>
                    </h2>
                </div>
                <div class="panel-container show">
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="ref_country_page_id">Country <span class="text-danger">*</span></label>
                                <select class="select2 form-control" name="ref_country_page_id" id="ref_country_page_id" required="">
                                    <option></option>
                                    <option value="0" <?= isset($faq_item_data->faq_item_id) && !empty($faq_item_data->faq_item_id) ? set_selected($faq_item_data->ref_country_page_id, 0) : '' ?>><?= MAIN_FAQ_TITLE ?> FAQs</option>
                                    <?php
                                    if (isset($country_page_data) && !empty($country_page_data)) {
                                        foreach ($country_page_data as $k1 => $v1) {
                                            ?>
                                            <option value="<?= isset($v1->country_page_id) && !empty($v1->country_page_id) ? $v1->country_page_id : '' ?>" <?= isset($faq_item_data->ref_country_page_id) && !empty($faq_item_data->ref_country_page_id) ? set_selected($faq_item_data->ref_country_page_id, $v1->country_page_id) : '' ?>><?= isset($v1->country_page_name) && !empty($v1->country_page_name) ? $v1->country_page_name : '' ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="faq_item_title">Faq Title <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="faq_item_title" id="faq_item_title" placeholder="Faq Title" required value="<?= isset($faq_item_data->faq_item_title) && !empty($faq_item_data->faq_item_title) ? $faq_item_data->faq_item_title : '' ?>">
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="faq_item_det_div">
        <?php
        if (isset($faq_item_det_data) && !empty($faq_item_det_data)) {
            foreach ($faq_item_det_data as $key => $value) {
                $key++;
                ?>
                <div class="row about_item_row" id="row_<?= $key ?>">
                    <div class="col-lg-12">
                        <div id="panel-1" class="panel panel-locked" data-panel-lock="false" data-panel-close="false" data-panel-fullscreen="false" data-panel-collapsed="false" data-panel-color="false" data-panel-locked="false" data-panel-refresh="false" data-panel-reset="false">
                            <div class="panel-hdr">
                                <h2>
                                    FAQs <span class="fw-300">Sub Data</span>
                                </h2>
                                <div class="panel-toolbar" style="padding-right: 0;margin-right: 1rem;">
                                    <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_faq_item_det" title="Delete" data-toggle="tooltip" data-id="<?= $key ?>">
                                        <i class="fal fa-minus"></i>
                                    </a>
                                    <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one ml-2" onclick="faq_item_det()" title="Add" data-toggle="tooltip">
                                        <i class="fal fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="panel-container show">
                                <div class="panel-content">
                                    <div class="form-row">
                                        <div class="col-md-12 mb-3">
                                            <label class="form-label" for="faq_item_det_title_<?= $key ?>">Faq Sub Title <span class="text-danger">*</span></label>
                                            <input tabindex="2" type="text" class="form-control textonly" name="faq_item_det_title[<?= $key ?>]" id="faq_item_det_title_<?= $key ?>" placeholder="Faq Sub Title" required value="<?= isset($value->faq_item_det_title) && !empty($value->faq_item_det_title) ? $value->faq_item_det_title : '' ?>">
                                            <span></span>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-md-12 mb-3">
                                            <label class="form-label" for="faq_item_det_desc_<?= $key ?>">Page Content <span class="text-danger">*</span></label>
                                            <textarea class="form-control" id="faq_item_det_desc_<?= $key ?>" name="faq_item_det_desc[<?= $key ?>]"><?= isset($value->faq_item_det_desc) && !empty($value->faq_item_det_desc) ? $value->faq_item_det_desc : '' ?></textarea>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                                <!-- panel footer with utility classes -->
                                <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
            <!--                                <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_faq_item_det ml-auto" title="Delete" data-toggle="tooltip" data-id="<?= $key ?>">
                                        <i class="fal fa-minus"></i>
                                    </a>
                                    <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one ml-2" onclick="faq_item_det()" title="Add" data-toggle="tooltip">
                                        <i class="fal fa-plus"></i>
                                    </a>-->
                                    <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script>
                    $(document).ready(function () {
                        CKEDITOR.replace('faq_item_det_desc[<?= $key ?>]', {
                            enterMode: CKEDITOR.ENTER_BR,
                            toolbar: [
                                {name: 'items', items: ["Source", "-", "Save", "NewPage", "DocProps", "Preview", "Print", "Templates", "-", "document"]},
                                {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                                {name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']},
                                '/',
                                {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', "Subscript", "Superscript", '-', 'RemoveFormat']},
                                {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']},
                                {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
                                {name: 'insert', items: ['Table', 'HorizontalRule', 'PageBreak']},
                                '/',
                                {name: 'styles', items: ['Styles', 'Format', "FontSize"]},
                                {name: 'color', items: ["TextColor", "BGColor"]},
                                {name: 'tools', items: ['Maximize', '-', 'ShowBlocks']},
                                {name: 'info', items: ['About']},
                                {name: 'tokens', items: ['tokens']}
                            ],
                            removeDialogTabs: 'link:target;image:Link;link:upload;image:Upload;image:advanced;link:advanced',
                            removePlugins: 'flashupload'
                        });
                    });
                </script>

                <?php
            }
        }
        ?>
    </div>

    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= form_close() ?>
</main>

<script>
    var suffix = '<?= isset($faq_item_det_data) && !empty($faq_item_det_data) ? count($faq_item_det_data) : 0 ?>';
    $(document).ready(function () {
        $("#ref_country_page_id").select2({
            placeholder: "Select country",
            allowClear: true,
            width: '100%'
        });
        if (suffix == 0) {
            faq_item_det();
        }
        $('#addEditFaq').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                visa_page_name: {
                    remote: {
                        url: "<?= base_url('admin/Visa_page/checkVisaPage') ?>",
                        type: "get"
                    }
                }
            },
            messages: {
                visa_page_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.next());
                element.next().next().addClass('text-danger');
            }
        });
    });


    function faq_item_det() {
        suffix++;
        var row = '';
        row += '<div class="row about_item_row"id="row_' + suffix + '">';
        row += '<div class="col-lg-12">';
        row += '<div id="panel-11" class="panel panel-locked" data-panel-lock="false" data-panel-close="false" data-panel-fullscreen="false" data-panel-collapsed="false" data-panel-color="false" data-panel-locked="false" data-panel-refresh="false" data-panel-reset="false">';
        row += '<div class="panel-hdr">';
        row += '<h2>';
        row += 'FAQs <span class="fw-300">Sub Data</span>';
        row += '</h2>';
        row += '<div class="panel-toolbar" style="padding-right: 0;margin-right: 1rem;">';
        row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_faq_item_det" title="Delete" data-toggle="tooltip" data-id="' + suffix + '">';
        row += '<i class="fal fa-minus"></i>';
        row += '</a>';
        row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one ml-2" onclick="faq_item_det()" title="Add" data-toggle="tooltip">';
        row += '<i class="fal fa-plus"></i>';
        row += '</a>';
        row += '</div>';
        row += '</div>';
        row += '<div class="panel-container show">';
        row += '<div class="panel-content">';
        row += '<div class="form-row">';
        row += '<div class="col-md-12 mb-3">';
        row += '<label class="form-label" for="faq_item_det_title_' + suffix + '">Faq Sub Title <span class="text-danger">*</span></label>';
        row += '<input tabindex="2" type="text" class="form-control textonly" name="faq_item_det_title[' + suffix + ']" id="faq_item_det_title_' + suffix + '" placeholder="Faq Sub Title" required value="">';
        row += '<span></span>';
        row += '</div>';
        row += '</div>';

        row += '<div class="form-row">';
        row += '<div class="col-md-12 mb-3">';
        row += '<label class="form-label" for="faq_item_det_desc_' + suffix + '">Page Content <span class="text-danger">*</span></label>';
        row += '<textarea class="form-control" id="faq_item_det_desc_' + suffix + '" name="faq_item_det_desc[' + suffix + ']"></textarea>';
        row += '<span></span>';
        row += '</div>';
        row += '</div>';
        row += '</div>';

        row += '<div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">';
        row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_faq_item_det ml-auto" title="Delete" data-toggle="tooltip" data-id="' + suffix + '">';
        row += '<i class="fal fa-minus"></i>';
        row += '</a>';
        row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one ml-2" onclick="faq_item_det()" title="Add" data-toggle="tooltip">';
        row += '<i class="fal fa-plus"></i>';
        row += '</a>';
        row += '</div>';
        row += '</div>';
        row += '</div>';
        row += '</div>';
        row += '</div>';
        $('#faq_item_det_div').append(row);

        CKEDITOR.replace('faq_item_det_desc[' + suffix + ']', {
            enterMode: CKEDITOR.ENTER_BR,
            toolbar: [
                {name: 'items', items: ["Source", "-", "Save", "NewPage", "DocProps", "Preview", "Print", "Templates", "-", "document"]},
                {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                {name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']},
                '/',
                {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', "Subscript", "Superscript", '-', 'RemoveFormat']},
                {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']},
                {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
                {name: 'insert', items: ['Table', 'HorizontalRule', 'PageBreak']},
                '/',
                {name: 'styles', items: ['Styles', 'Format', "FontSize"]},
                {name: 'color', items: ["TextColor", "BGColor"]},
                {name: 'tools', items: ['Maximize', '-', 'ShowBlocks']},
                {name: 'info', items: ['About']},
                {name: 'tokens', items: ['tokens']}
            ],
            removeDialogTabs: 'link:target;image:Link;link:upload;image:Upload;image:advanced;link:advanced',
            removePlugins: 'flashupload'
        });
    }

    $(document).on('click', '.remove_faq_item_det', function () {
        var id = $(this).data('id');
        if ($('.about_item_row').length > 1) {
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $("#row_" + id).remove();
                }
            });
        } else {
            swalWithBootstrapButtons.fire(
                    "Alert!",
                    "Minimum 1 required.",
                    "error"
                    );
        }
    });
</script>