<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-sliders-h'></i> Edit Testimonial Setting
        </h1>
        <div class="d-flex mr-0">
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Home/testimonialSetting', $arrayName = array('id' => 'testimonialSetting')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="testimonial_setting_title">Title <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="testimonial_setting_title" id="testimonial_setting_title" placeholder="Title" required value="<?= isset($setting_data->testimonial_setting_title) && !empty($setting_data->testimonial_setting_title) ? $setting_data->testimonial_setting_title : '' ?>">
                                <div class="invalid-feedback">
                                    Title Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="testimonial_setting_desc">Description <span class="text-danger">*</span></label>
                                <textarea class="form-control" rows="3" name="testimonial_setting_desc" id="visa_category_setting_footer" placeholder="Description" required><?= isset($setting_data->testimonial_setting_desc) && !empty($setting_data->testimonial_setting_desc) ? $setting_data->testimonial_setting_desc : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Description Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="testimonial_setting_sub_desc">Sub Description <span class="text-danger">*</span></label>
                                <textarea class="form-control" rows="3" name="testimonial_setting_sub_desc" id="testimonial_setting_sub_desc" placeholder="Description" required><?= isset($setting_data->testimonial_setting_sub_desc) && !empty($setting_data->testimonial_setting_sub_desc) ? $setting_data->testimonial_setting_sub_desc : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Sub Description Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="sub_title_1">Title 1 <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="sub_title_1" id="sub_title_1" placeholder="Title" required value="<?= isset($setting_data->sub_title_1) && !empty($setting_data->sub_title_1) ? $setting_data->sub_title_1 : '' ?>">
                                <div class="invalid-feedback">
                                    Title 1 Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="sub_desc_1">Description 1 <span class="text-danger">*</span></label>
                                <textarea class="form-control" rows="3" name="sub_desc_1" id="sub_desc_1" placeholder="Description" required><?= isset($setting_data->sub_desc_1) && !empty($setting_data->sub_desc_1) ? $setting_data->sub_desc_1 : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Description 1 Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="sub_title_2">Title 2 <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="sub_title_2" id="sub_title_2" placeholder="Title" required value="<?= isset($setting_data->sub_title_2) && !empty($setting_data->sub_title_2) ? $setting_data->sub_title_2 : '' ?>">
                                <div class="invalid-feedback">
                                    Title 2 Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="sub_desc_2">Description 2 <span class="text-danger">*</span></label>
                                <textarea class="form-control" rows="3" name="sub_desc_2" id="sub_desc_2" placeholder="Description" required><?= isset($setting_data->sub_desc_2) && !empty($setting_data->sub_desc_2) ? $setting_data->sub_desc_2 : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Description 2 Required
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="sub_title_3">Title 3 <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="sub_title_3" id="sub_title_3" placeholder="Title" required value="<?= isset($setting_data->sub_title_3) && !empty($setting_data->sub_title_3) ? $setting_data->sub_title_3 : '' ?>">
                                <div class="invalid-feedback">
                                    Title 3 Required
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="sub_desc_3">Description 3 <span class="text-danger">*</span></label>
                                <textarea class="form-control" rows="3" name="sub_desc_3" id="sub_desc_3" placeholder="Description" required><?= isset($setting_data->sub_desc_3) && !empty($setting_data->sub_desc_3) ? $setting_data->sub_desc_3 : '' ?></textarea>
                                <div class="invalid-feedback">
                                    Description 3 Required
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $('#testimonialSetting').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                return true;
            }
        });
    });
</script>