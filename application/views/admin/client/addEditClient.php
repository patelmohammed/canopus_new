<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-file-code'></i> <?= isset($client_data->client_id) && !empty($client_data->client_id) ? 'Edit' : 'Add' ?> University
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Home/client">University</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?= form_open(base_url($this->uri->uri_string()), $arrayName = array('id' => 'addEditClient', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="client_name">University Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="client_name" id="client_name" placeholder="Client Name" required value="<?= isset($client_data->client_name) && !empty($client_data->client_name) ? $client_data->client_name : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Image <i class="text-danger">(File in JPG,PNG) File Size 145x70 px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="client_image" class="custom-file-input" id="client_image" <?= isset($client_data->client_image) && !empty($client_data->client_image) && file_exists($client_data->client_image) ? '' : 'required' ?>>
                                    <label class="custom-file-label" for="client_image"><?= isset($client_data->client_image) && !empty($client_data->client_image) && file_exists($client_data->client_image) ? basename($client_data->client_image) : 'Choose file' ?></label>
                                </div>
                            </div>
                            <input type="hidden" name="hidden_client_image" id="hidden_client_image" value="<?= isset($client_data->client_image) && !empty($client_data->client_image) && file_exists($client_data->client_image) ? $client_data->client_image : '' ?>">
                        </div>
                        <?php if (isset($client_data->client_image) && !empty($client_data->client_image) && file_exists($client_data->client_image)) { ?>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <img src="<?= base_url() . $client_data->client_image ?>" style="width: 18%;">
                                </div>
                                <input type="hidden" name="hidden_client_image" id="hidden_client_image" value="<?= isset($client_data->client_image) && !empty($client_data->client_image) && file_exists($client_data->client_image) ? $client_data->client_image : '' ?>">
                            </div>
                        <?php } ?>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="is_active">&nbsp;</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" name="is_active" class="custom-control-input" id="is_active" <?= isset($client_data->client_id) && !empty($client_data->client_id) ? (isset($client_data->is_active) && !empty($client_data->is_active) && $client_data->is_active == 1 ? 'checked' : '') : 'checked' ?>>
                                    <label class="custom-control-label" for="is_active">University Active</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    $(document).ready(function () {
        $("#continent_id").select2({
            placeholder: "Select continent",
            allowClear: true,
            width: '100%'
        });

        $('#addEditClient').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                client_name: {
                    remote: {
                        url: "<?= base_url('admin/Home/checkClientName') ?>",
                        type: "get"
                    }
                },
                client_image: {
                    extension: "jpg|jpeg|png|JPG|JPEG|PNG"
                }
            },
            messages: {
                client_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                },
                client_image: {
                    extension: "Please select jpg, jpeg, or png file.",
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.next());
                element.next().next().addClass('text-danger');
            }
        });
    });
</script>