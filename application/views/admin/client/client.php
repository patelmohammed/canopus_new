<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-file-code'></i> University
        </h1>
        <div class="d-flex mr-0">
            <?php if ($menu_rights['add_right']) { ?>
                <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Home/addEditClient">Add University</a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <div class="panel-content">
                        <table id="datatable" class="table table-hover table-striped w-100" data-title="Menus" data-msgtop="">
                            <thead class="thead-dark">
                                <tr>
                                    <th>SN</th>
                                    <th>University Name</th>
                                    <th>University Image</th>
                                    <th>Active / Deactive</th>
                                    <th>Added By</th>
                                    <th class="notexport no-sort">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($client_data) && !empty($client_data)) {
                                    $sn = 0;
                                    foreach ($client_data as $key => $value) {
                                        $sn++;
                                        ?>                       
                                        <tr> 
                                            <td><?= $sn ?></td>
                                            <td><?= isset($value->client_name) && !empty($value->client_name) ? $value->client_name : '' ?></td>
                                            <td><img src="<?= isset($value->client_image) && !empty($value->client_image) && file_exists($value->client_image) ? base_url() . $value->client_image : '' ?>" style="width: 50%;"></td> 
                                            <td style="text-align: center;">
                                                <?php if ($value->is_active == 1) { ?>
                                                    <button type="button" class="btn btn-xs btn-success waves-effect waves-themed deactivate_client" data-id="<?= $value->client_id ?>">Active</button>
                                                <?php } else { ?>
                                                    <button type="button" class="btn btn-xs btn-danger waves-effect waves-themed activate_client" data-id="<?= $value->client_id ?>">Deactivated</button>
                                                <?php } ?>
                                            </td>
                                            <td><?= isset($value->InsUser) && !empty($value->InsUser) ? getUserNameById($value->InsUser) : '' ?></td>   
                                            <td>
                                                <div class='d-flex'>
                                                    <?php if ($menu_rights['edit_right']) { ?>
                                                        <a href='<?php echo base_url() ?>admin/Home/addEditClient/<?= $value->client_id ?>' class='btn btn-icon btn-sm hover-effect-dot btn-outline-primary mr-2' title='Edit' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-primary-500"></div></div>'>
                                                            <i class="fal fa-edit"></i>
                                                        </a>
                                                    <?php } ?>
                                                    <?php if ($menu_rights['delete_right']) { ?>
                                                        <a href='javascript:void(0);' data-url="<?= base_url('admin/Home/deleteClient') ?>" data-id="<?= $value->client_id ?>" class='btn btn-icon btn-sm hover-effect-dot btn-outline-danger mr-2 delete_record' title='Delete Record' data-toggle='tooltip' data-template='<div class="tooltip" role="tooltip"><div class="tooltip-inner bg-danger-500"></div></div>'>
                                                            <i class="fal fa-times"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    $(document).on('click', '.deactivate_client', function () {
        var id = $(this).data('id');
        $.ajax({
            type: "POST",
            url: '<?= base_url('admin/Home/deactiveClient') ?>',
            data: {client_id: id},
            success: function (returnData) {
                var data = JSON.parse(returnData);
                if (data.result == true) {
                    window.location.reload();
                }
            }
        });
    });

    $(document).on('click', '.activate_client', function () {
        var id = $(this).data('id');
        $.ajax({
            type: "POST",
            url: '<?= base_url('admin/Home/activeClient') ?>',
            data: {client_id: id},
            success: function (returnData) {
                var data = JSON.parse(returnData);
                if (data.result == true) {
                    window.location.reload();
                }
            }
        });
    });

    $(document).on('click', '.show_home_page', function () {
        var id = $(this).data('id');
        var is_show_home_page = $(this).data('is_show_home_page');
        $.ajax({
            type: "POST",
            url: '<?= base_url('admin/Country_page/showCountryHomePage') ?>',
            data: {id: id, is_show_home_page: is_show_home_page},
            success: function (returnData) {
                var data = JSON.parse(returnData);
                if (data.result == true) {
                    window.location.reload();
                } else {
                    swalWithBootstrapButtons.fire("Something Wrong", data.message, "error");
                    return false;
                }
            },
            error: function () {
                swalWithBootstrapButtons.fire("Something Wrong", "Your record not updated :(", "error");
                return false;
            }
        });
    });
</script>