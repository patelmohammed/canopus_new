<div class="row">
    <div class="col-xl-12">
        <div id="panel-1" class="panel">
            <div class="panel-container show">
                <?php echo form_open(base_url() . 'admin/Home/addEditBackgroundImage/' . (isset($background_data[0]->bg_id) && !empty($background_data[0]->bg_id) ? $background_data[0]->bg_id : ''), $arrayName = array('id' => 'addEditBackgroundImage', 'enctype' => 'multipart/form-data')) ?>
                <input type="hidden" name="this_page_location" id="this_page_location" value="">
                <div class="panel-content">
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label class="form-label">Background Image <i class="text-danger">(File in PNG) File Size 1728x272px</i></label>
                            <div class="custom-file">
                                <input type="file" name="background_image" class="custom-file-input" id="background_image" <?= isset($background_data[0]->bg_image) && !empty($background_data[0]->bg_image) ? '' : 'required=""' ?>>
                                <label class="custom-file-label" for="background_image">Choose file</label>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3 mt-3">
                            <img src="<?= base_url() . (isset($background_data[0]->bg_image) && !empty($background_data[0]->bg_image) ? $background_data[0]->bg_image : '') ?>" height="100px" width="300px" class="m-2" alt="<?= $this->page_id ?>">
                            <input type="hidden" name="hidden_background_image" id="hidden_background_image" value="<?= isset($background_data[0]->bg_image) && !empty($background_data[0]->bg_image) ? $background_data[0]->bg_image : '' ?>">
                        </div>
                    </div>
                </div>
                <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                    <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                </div>
                <?= form_close() ?>
            </div>
        </div>
    </div>
</div>