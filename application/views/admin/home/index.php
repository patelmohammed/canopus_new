<style>
    .bg-info-gradient{
        background-image:linear-gradient(100deg, rgba(9, 96, 165, 0.7), transparent);
    }
</style>
<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-chart-area'></i> Dashboard
        </h1>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="p-3 bg-info-gradient bg-success-500 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <span class="fw-300">Welcome To <?= COMPANY_NAME ?></span><br>
                        <h4><?= $this->session->userdata('user_name') ?></h4>
                    </h3>
                </div>
            </div>
        </div>
    </div>
</main>