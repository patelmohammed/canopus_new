<main id="js-page-content" role="main" class="page-content">
    <div class="subheader">
        <h1 class="subheader-title">
            <i class='subheader-icon fal fa-file-code'></i> Edit Visa Page
        </h1>
        <div class="d-flex mr-0">
            <a class="btn btn-primary bg-trans-gradient ml-auto waves-effect waves-themed" href="<?php echo base_url() ?>admin/Visa_page">Visa Page</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-container show">
                    <?php echo form_open(base_url() . 'admin/Visa_page/addEditVisaPage/' . $encrypted_id, $arrayName = array('id' => 'addEditVisaPage', 'enctype' => 'multipart/form-data')) ?>
                    <div class="panel-content">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="visa_page_name">Visa Page Name <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="visa_page_name" id="visa_page_name" placeholder="Visa Page Name" required value="<?= isset($visa_page_data->visa_page_name) && !empty($visa_page_data->visa_page_name) ? $visa_page_data->visa_page_name : '' ?>">
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="visa_page_desc">Visa Page Description <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="visa_page_desc" id="visa_page_desc" placeholder="Visa Page Description" required value="<?= isset($visa_page_data->visa_page_desc) && !empty($visa_page_data->visa_page_desc) ? $visa_page_data->visa_page_desc : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="icon">Home Page Icon <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="icon" id="icon" placeholder="Home Page Icon" required value="<?= isset($visa_page_data->icon) && !empty($visa_page_data->icon) ? $visa_page_data->icon : '' ?>">
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Image <i class="text-danger">(File in JPG,PNG) File Size 1020x520px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="visa_page_image" class="custom-file-input" id="visa_page_image">
                                    <label class="custom-file-label" for="visa_page_image">Choose file</label>
                                </div>
                                <img class="mt-3" src="<?= isset($visa_page_data->visa_page_image) && !empty($visa_page_data->visa_page_image) && file_exists($visa_page_data->visa_page_image) ? base_url() . $visa_page_data->visa_page_image : base_url('assets/admin/img/no_image.jpg') ?>" height="100px" alt="no preview available">
                                <input type="hidden" class="form-control" value="<?= isset($visa_page_data->visa_page_image) && !empty($visa_page_data->visa_page_image) ? $visa_page_data->visa_page_image : '' ?>" name="hidden_visa_page_image"/>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Service Page Image <i class="text-danger">(File in JPG,PNG) File Size 370x290px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="service_page_image" class="custom-file-input" id="service_page_image">
                                    <label class="custom-file-label" for="service_page_image">Choose file</label>
                                </div>
                                <img class="mt-3" src="<?= isset($visa_page_data->service_page_image) && !empty($visa_page_data->service_page_image) && file_exists($visa_page_data->service_page_image) ? base_url() . $visa_page_data->service_page_image : base_url('assets/admin/img/no_image.jpg') ?>" height="100px" alt="no preview available">
                                <input type="hidden" class="form-control" value="<?= isset($visa_page_data->service_page_image) && !empty($visa_page_data->service_page_image) ? $visa_page_data->service_page_image : '' ?>" name="hidden_service_page_image"/>
                            </div>
                        </div>
                        <div id="download_document_div">
                            <?php
                            if (isset($download_document) && !empty($download_document)) {
                                foreach ($download_document as $key => $value) {
                                    $key++;
                                    ?>
                                    <div class="form-row about_item_row" id="row_<?= $key ?>">
                                        <div class="col-md-6 mb-3 about_item_row" id="row_<?= $key ?>">
                                            <label class="form-label">Document <i class="text-danger">(File in PDF)</i></label>
                                            <div class="custom-file">
                                                <input type="file" name="download_document[<?= $key ?>]" class="custom-file-input" id="download_document_<?= $key ?>">
                                                <input type="hidden" class="form-control" value="<?= isset($value->document) && !empty($value->document) ? $value->document : '' ?>" name="hidden_download_document[<?= $key ?>]"/>
                                                <label class="custom-file-label" for="download_document_<?= $key ?>"><?= isset($value->document) && !empty($value->document) ? str_replace('assets/document/', '', $value->document) : 'Choose file' ?></label>
                                            </div>
                                        </div>

                                        <div class="col-md-6 mb-3">
                                            <label class="form-label" for="document_name_<?= $key ?>">Document Name</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="document_name[<?= $key ?>]" id="document_name_<?= $key ?>" placeholder="Document Name" value="<?= isset($value->document_name) && !empty($value->document_name) ? $value->document_name : '' ?>">
                                                <div class="input-group-append">
                                                    <div class="input-group-text">
                                                        <div class="custom-control d-flex custom-switch">
                                                            <input id="is_active_document_<?= $key ?>" name="is_active_document[<?= $key ?>]" type="checkbox" class="custom-control-input" <?= isset($value->is_active) && !empty($value->is_active) ? set_checked($value->is_active, 1) : '' ?>>
                                                            <label class="custom-control-label fw-500" for="is_active_document_<?= $key ?>"></label>
                                                        </div>
                                                    </div>
                                                    <?php if ($key == 1) { ?>
                                                        <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="download_document()" title="Add" data-toggle="tooltip">
                                                            <i class="fal fa-plus"></i>
                                                        </a>
                                                    <?php } else { ?>
                                                        <a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_download_document" title="Delete" data-toggle="tooltip" data-id="<?= $key ?>">
                                                            <i class="fal fa-minus"></i>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label">Page Background Image <i class="text-danger">(File in JPG,PNG) File Size 1920x390px</i></label>
                                <div class="custom-file">
                                    <input type="file" name="background_image" class="custom-file-input" id="background_image">
                                    <label class="custom-file-label" for="background_image">Choose file</label>
                                </div>
                                <img class="mt-3" src="<?= base_url() . (isset($visa_page_data->background_image) && !empty($visa_page_data->background_image) ? $visa_page_data->background_image : 'assets/images/pagetitle-bg.jpg') ?>" height="100px" alt="no preview available">
                                <input type="hidden" name="hidden_background_image" id="hidden_background_image" value="<?= isset($visa_page_data->background_image) && !empty($visa_page_data->background_image) ? $visa_page_data->background_image : 'assets/images/pagetitle-bg.jpg' ?>">
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="is_active">&nbsp;</label>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" name="is_active" class="custom-control-input" id="is_active" <?= isset($visa_page_data->is_active) && !empty($visa_page_data->is_active) ? set_checked($visa_page_data->is_active, 1) : '' ?>>
                                    <label class="custom-control-label" for="is_active">Page Active</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label class="form-label" for="visa_page_content">Page Content <span class="text-danger">*</span></label>
                                <textarea class="form-control" id="visa_page_content" name="visa_page_content"><?= isset($visa_page_data->visa_page_content) && !empty($visa_page_data->visa_page_content) ? $visa_page_data->visa_page_content : '' ?></textarea>
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_title">Meta Title <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="meta_title" id="meta_title" placeholder="Meta Title" required value="<?= isset($visa_page_data->meta_title) && !empty($visa_page_data->meta_title) ? $visa_page_data->meta_title : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_desc">Meta Description <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="meta_desc" id="meta_desc" placeholder="Meta Description" required value="<?= isset($visa_page_data->meta_desc) && !empty($visa_page_data->meta_desc) ? $visa_page_data->meta_desc : '' ?>">
                                <span></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="meta_key">Meta Key <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control" name="meta_key" id="meta_key" placeholder="Meta Key" required value="<?= isset($visa_page_data->meta_key) && !empty($visa_page_data->meta_key) ? $visa_page_data->meta_key : '' ?>">
                                <span></span>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label class="form-label" for="slug">Slug <span class="text-danger">*</span></label>
                                <input tabindex="2" type="text" class="form-control textonly" name="slug" id="slug" placeholder="Slug" required value="<?= isset($slug_data->slug) && !empty($slug_data->slug) ? $slug_data->slug : '' ?>">
                                <span></span>
                            </div>
                            <input type="hidden" name="hidden_slug_id" id="hidden_slug_id" value="<?= isset($slug_data->slug_id) && !empty($slug_data->slug_id) ? $slug_data->slug_id : '' ?>">
                        </div>
                    </div>
                    <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row">
                        <button type="submit" tabindex="11" class="btn btn-danger ml-auto waves-effect waves-themed"><span class="fal fa-check mr-1"></span>Submit Form</button>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    var id = '<?= isset($visa_page_data->visa_page_id) && !empty($visa_page_data->visa_page_id) ? $visa_page_data->visa_page_id : '' ?>';
    var suffix = '<?= isset($download_document) && !empty($download_document) ? count($download_document) : 0 ?>';
    $(document).ready(function () {
        if (suffix == 0) {
            download_document();
        }

        CKEDITOR.replace('visa_page_content', {
            enterMode: CKEDITOR.ENTER_BR,
            toolbar: [
                {name: 'items', items: ["Source", "-", "Save", "NewPage", "DocProps", "Preview", "Print", "Templates", "-", "document"]},
                {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                {name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']},
                '/',
                {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', "Subscript", "Superscript", '-', 'RemoveFormat']},
                {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language']},

                {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
                {name: 'insert', items: ['Table', 'HorizontalRule', 'PageBreak']},
                '/',
                {name: 'styles', items: ['Styles', 'Format', "FontSize"]},
                {name: 'color', items: ["TextColor", "BGColor"]},
                {name: 'tools', items: ['Maximize', '-', 'ShowBlocks']},
                {name: 'info', items: ['About']},
                {name: 'tokens', items: ['tokens']}
            ],
            removeDialogTabs: 'link:target;image:Link;link:upload;image:Upload;image:advanced;link:advanced',
            removePlugins: 'flashupload'
        });
        $('#addEditVisaPage').validate({
            validClass: "is-valid",
            errorClass: "is-invalid",
            rules: {
                visa_page_name: {
                    remote: {
                        url: "<?= base_url('admin/Visa_page/checkVisaPage/') ?>" + id,
                        type: "get"
                    }
                },
                slug: {
                    remote: {
                        url: "<?= base_url('admin/Home/checkSlug/') ?>" + $('#hidden_slug_id').val(),
                        type: "get"
                    }
                }
            },
            messages: {
                visa_page_name: {
                    remote: jQuery.validator.format("{0} is already in use")
                },
                slug: {
                    remote: jQuery.validator.format("{0} is already in use")
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.next());
                element.next().next().addClass('text-danger');
            }
        });
    });

    function download_document() {
        suffix++;
        var row = '';
        row += '<div class="form-row about_item_row" id="row_' + suffix + '">';
        row += '<div class="col-md-6 mb-3">';
        row += '<label class="form-label">Document <i class="text-danger">(File in PDF)</i></label>';
        row += '<div class="input-group">';
        row += '<div class="custom-file">';
        row += '<input type="file" name="download_document[' + suffix + ']" class="custom-file-input" id="download_document_' + suffix + '">';
        row += '<label class="custom-file-label" for="download_document_' + suffix + '">Choose file</label>';
        row += '</div>';
        row += '</div>';
        row += '</div>';

        row += '<div class="col-md-6 mb-3">';
        row += '<label class="form-label" for="document_name_' + suffix + '">Document Name</label>';
        row += '<div class="input-group">';
        row += '<input type="text" class="form-control" name="document_name[' + suffix + ']" id="document_name_' + suffix + '" placeholder="Document Name" value="">';

        row += '<div class="input-group-append">';
        row += '<div class="input-group-text">';
        row += '<div class="custom-control d-flex custom-switch">';
        row += '<input id="is_active_document_' + suffix + '" name="is_active_document[' + suffix + ']" type="checkbox" class="custom-control-input" checked>';
        row += '<label class="custom-control-label fw-500" for="is_active_document_' + suffix + '"></label>';
        row += '</div>';
        row += '</div>';
        if (suffix == 1) {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-primary show_only_one" onclick="download_document()" title="Add" data-toggle="tooltip">';
            row += '<i class="fal fa-plus"></i>';
            row += '</a>';
        } else {
            row += '<a href="javascript:void(0);" class="btn btn-icon hover-effect-dot btn-outline-danger remove_download_document" title="Delete" data-toggle="tooltip" data-id="' + suffix + '">';
            row += '<i class="fal fa-minus"></i>';
            row += '</a>';
        }
        row += '</div>';
        row += '</div>';
        row += '</div>';
        row += '</div>';
        $('#download_document_div').append(row);
    }

    $(document).on('click', '.remove_download_document', function () {
        var id = $(this).data('id');
        if ($('.about_item_row').length > 1) {
            swalWithBootstrapButtons.fire({
                title: "Alert!",
                text: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $("#row_" + id).remove();
                }
            });
        } else {
            swalWithBootstrapButtons.fire(
                    "Alert!",
                    "Minimum 1 required.",
                    "error"
                    );
        }
    });
</script>