<?php

defined('BASEPATH') OR exit('No direct script access allowed');

function dateprint($dt = '') {
    if ($dt == '') {
        return date('d-m-Y');
    } else {
        return date('d-m-Y', strtotime($dt));
    }
}

function dbdate($dt = '') {
    if ($dt == '') {
        return date('Y-m-d');
    } else {
        $old = explode('-', $dt);
        return date('Y-m-d', strtotime($old[2] . '-' . $old[1] . '-' . $old[0]));
    }
}

function preprint($val, $flag = TRUE) {
    echo '<pre>';
    print_r($val);
    echo '</pre>';

    if ($flag) {
        die;
    }
}

function cleanString($string) {
    $string = str_replace(' ', '-', $string);
    $string = preg_replace('/[^A-Za-z0-9\-]/', '-', $string);
    return preg_replace('/-+/', '-', $string);
}

function set_selected($desired_value, $new_value) {
    if ($desired_value == $new_value) {
        echo ' selected="selected"';
    }
}

function set_checked($desired_value, $new_value) {
    if ($desired_value == $new_value) {
        echo ' checked="checked"';
    }
}

function getFuzzy($str_time) {
    $_time_formats = array(
        array(60, 'just now'),
        array(90, '1 minute'),
        array(3600, 'minutes', 60),
        array(5400, '1 hour'),
        array(86400, 'hours', 3600),
        array(129600, '1 day'),
        array(604800, 'days', 86400),
        array(907200, '1 week'),
        array(2628000, 'weeks', 604800),
        array(3942000, '1 month'),
        array(31536000, 'months', 2628000),
        array(47304000, '1 year'),
        array(3153600000, 'years', 31536000)
    );
    $diff = time() - $str_time;
    $val = '';
    if ($str_time <= 0) {
        $val = 'a long time ago';
    } else if ($diff < 0) {
        $val = 'in the future';
    } else {
        foreach ($_time_formats as $format) {
            if ($diff < $format[0]) {
                if (count($format) == 2) {
                    $val = $format[1] . ($format[0] === 60 ? '' : ' ago');
                    break;
                } else {
                    $val = ceil($diff / $format[2]) . ' ' . $format[1] . ' ago';
                    break;
                }
            }
        }
    }
    return $val;
}

function getmobileNoById($id) {
    $CI = & get_instance();
    $sql = "SELECT si.mobile AS mobile_no
            FROM supplier_information si 
            WHERE si.supplier_id = '$id'
            UNION (
                SELECT ci.customer_mobile AS mobile_no
                FROM customer_information ci
                WHERE ci.customer_id = '$id'
            )
            UNION (
                SELECT oi.mobile AS mobile_no
                FROM outlet_information oi
                WHERE oi.outlet_id = '$id'
            )";
    $mobile_no = $CI->db->query($sql)->row()->mobile_no;
    return isset($mobile_no) && !empty($mobile_no) ? $mobile_no : '';
}

function getUserNameById($id) {
    $CI = & get_instance();
    $sql = "SELECT user_name 
            FROM user_information si 
            WHERE user_id = '$id' ";
    $user_name = $CI->db->query($sql)->row();
    return isset($user_name->user_name) && !empty($user_name->user_name) ? $user_name->user_name : '';
}

function getPageNameById($id) {
    $CI = & get_instance();
    $sql = "SELECT coaching_page_name 
            FROM tbl_coaching_page si 
            WHERE coaching_page_id = '$id' ";
    $user_name = $CI->db->query($sql)->row();
    return isset($user_name->coaching_page_name) && !empty($user_name->coaching_page_name) ? $user_name->coaching_page_name : '';
}

function getCountryPageNameById($id) {
    $CI = & get_instance();
    $sql = "SELECT country_page_name 
            FROM tbl_country_page si 
            WHERE country_page_id = '$id' ";
    $user_name = $CI->db->query($sql)->row();
    return isset($user_name->country_page_name) && !empty($user_name->country_page_name) ? $user_name->country_page_name : '';
}

function getContinentNameById($id) {
    $CI = & get_instance();
    $sql = "SELECT continent_name 
            FROM tbl_continent si 
            WHERE continent_id = '$id' ";
    $user_name = $CI->db->query($sql)->row();
    return isset($user_name->continent_name) && !empty($user_name->continent_name) ? $user_name->continent_name : '';
}

function createSlug($str, $delimiter = '-'){
    $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
    return $slug;
}