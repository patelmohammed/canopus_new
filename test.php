<ul class="cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5">
    <li>
        <i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
            <strong class="mr-2 cmt-textcolor-skincolor">How can I register for the IELTS test? </strong><br/>
            IELTS score is valid 2 years.
        </span>
    </li>
    <li>
        <i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
            <strong class="mr-2 cmt-textcolor-skincolor">How can I register for the IELTS test? </strong><br/>
           A candidate may resit the IELTS test at any time.
        </span>
    </li>
    <li>
        <i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
            <strong class="mr-2 cmt-textcolor-skincolor">How can I register for the IELTS test? </strong><br/>
           No. Payment of the test fee by cheque or cash is NOT acceptable.
        </span>
    </li>
    <li>
        <i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
            <strong class="mr-2 cmt-textcolor-skincolor">How can I register for the IELTS test? </strong><br/>
            Only if the candidate withdraws the application before 5 weeks of the examination date. A particular amount is deducted and rest is transferred. If withdrawn later than 5 weeks nothing will be refunded. 
        </span>
    </li>
    <li>
        <i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
            <strong class="mr-2 cmt-textcolor-skincolor">How can I register for the IELTS test? </strong><br/>
           In the event a registered candidate fails to appear for their IELTS test, the candidate is deemed an absentee and a refund will not be applicable.
        </span>
    </li>
    <li>
        <i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
            <strong class="mr-2 cmt-textcolor-skincolor">How can I register for the IELTS test? </strong><br/>
           Under normal circumstances, you will be treated as an absentee. However, there are some special circumstances under which your application may be considered with some conditions.
        </span>
    </li>
    <li>
        <i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
            <strong class="mr-2 cmt-textcolor-skincolor">How can I register for the IELTS test? </strong><br/>
          This form MUST be completed by the Candidate and the FEE MUST be enclosed to enable processing. Centre may send up to five additional TRF’s to reorganizations. The centre may charge a postal fee for results sent internationally or by courier.
        </span>
    </li>
    
</ul>

