<p>We will be happy to help you in getting your student visa. Thanks to our experience and good contacts with Polish embassies and consulates around the world, we can assist you with the process of acquiring a visa.</p>
<p>It takes from 2 weeks to a month to obtain a Polish visa. In other words, you need to start collecting all the necessary documents for the visa application at least two months prior to your planned departure.</p>
<p>In order to apply for a student visa to Poland you need the following:</p>
<!--<p><strong class="mr-2 cmt-textcolor-skincolor">Make sure that :</strong></p>-->
<!--<p>Pre-departure checklist will help you to keep everything organized and save you from last minute hurdle.</p>-->
<ul class="cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5">
    <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
More than 15 hours of comprehensive program to master every question-type involved in the DET.
        </span>
    </li>
    <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
           Experienced Faculty
        </span>
    </li>
    <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
        Up to date study material to excel in the test.
        </span>
    </li>
    <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
        Test taking tips and strategies to get your dream score on the DET.
        </span>
    </li>
    <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
          Targeted practice according to the weak areas of students.
        </span>
    </li>
    <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
           Applications and visa assistance for admissions to Undergraduate Schools in the US.
        </span>
    </li>
    <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
         A confirmation of funds for living in Poland
        </span>
    </li>
    <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
          A short CV
        </span>
    </li>
    <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
          Your school certificates and diplomas
        </span>
    </li>
    <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
          Evidence of proficiency in the language in which you wish to study
        </span>
    </li>
    <li><i class="cmt-textcolor-skincolor"></i>
        <span class="cmt-list-li-content">
            <strong class="mr-2 cmt-textcolor-skincolor">After Arriving:</strong>
        </span>
    </li>
    <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
            Complete your Student's Pass formalities at the Immigration Checkpoints Authority (ICA).
        </span>
    </li>
     <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
            At almost all good educational institutions at Singapore, there is a department providing specialized services to international students. Contact the department and ask for help, whenever you need it.
        </span>
    </li>
     <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
            If you do not have Singapore currency already, you will find currency exchange counters at the airport in Singapore.
        </span>
    </li>
</ul>
