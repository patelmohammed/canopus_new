
<ul class="cmt-list cmt-list-style-icon cmt-list-icon-color-skincolor pt-5">
    <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
            <strong class="mr-2 cmt-textcolor-skincolor">On Campus: </strong>
          living in university campus will be a great option. Generally first year student choose it. It will minimize student’s travel expenses and on other hand, give you option to live in campus with classmates and friends also.
        </span>
    </li>
    <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
            <strong class="mr-2 cmt-textcolor-skincolor">Off Campus: </strong>
            Many Students choose off campus option (Private, rented accommodation) as they want to explore their life beyond the campus. You can live in private hostels which are run by educational institutions or independent agencies.
        </span>
    </li>
    <li><i class="cmt-textcolor-skincolor fa fa-check-circle"></i>
        <span class="cmt-list-li-content">
            <strong class="mr-2 cmt-textcolor-skincolor">Home stay: </strong>
           Some local families in Singapore open up their homes and offer full boarding to international students. A home-stay programme lets you sample the local flavors of life in Singapore, as well as enjoy a homely environment.
        </span>
    </li>
</ul>
<h5 class="mt-10 mb-2 cmt-textcolor-skincolor">Estimated living costs</h5>
<p>
    The living cost you incur shall depend on your lifestyle
</p>
<table class="table table-striped">
    <thead>
        <tr>
            <th colspan="2" class="text-center">
                Accommodation (You will choose one of them) (Approximate)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Off Campus (Rental)</td>
            <td>S$500 to S$1200 per month</td>
        </tr>
        <tr>
            <td>ON Campus</td>
            <td>S$500 to S$800 per month</td>
        </tr>
        <tr>
            <td>Home-stay</td>
            <td>S$600 to S$1200 per month</td>
        </tr>
        <tr>
            <th colspan="2" class="text-center">Other living expenses  (Approximate)</th>
        </tr>
        <tr>
            <td>Groceries and eating out</td>
            <td>S$300 to S$400 per month</td>
        </tr>
        <tr>
            <td>Public transport</td>
            <td>S$100 to S$150 per month</td>
        </tr>
        <tr>
            <td>Personal expense</td>
            <td>S$200 to S$400 per month</td>
        </tr>
        <tr>
            <td>Miscellaneous Expenses</td>
            <td>S$100 to S$500 per month</td>
        </tr>
      
        <tr>
            <th colspan="2" class="cmt-textcolor-skincolor">Estimated Annual Cost Of living - S$11000 to S$20000</th>
        </tr>
    </tbody>
</table>
<p>*These amounts will vary accordingly depending on your personal needs and lifestyle pattern.</p>